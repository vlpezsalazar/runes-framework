package es.calipso.framework.storage.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface NamedQuery{
    public String name();
    public String query();
    /**
     * Type of query:
     * <ul>
     * <li> 0: create.
     * <li> 1: load.
     * <li> 2: delete.
     * <ul>
     * @return
     */
    public int[] type() default {1};
    /**
     * If the query fails and this property is true,
     * the method will throw a EntityNotFoundException,
     * otherwise it will be silently ignored
     */
    public boolean fail() default true;
}