package es.calipso.framework.storage.daotype;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public interface DAOEntity extends KeyCode{
    public void prepare(int query, PreparedStatement ps) throws SQLException;
    public void populate(int query, ResultSet rs) throws SQLException;
    public void setPK(Object[] pks);
    public Object[] getPrimaryKey();
    public Class<?> getSuperClass();
    Registrable<? extends DAOEntity> getRegistrable();
}
