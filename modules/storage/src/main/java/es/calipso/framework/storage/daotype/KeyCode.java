package es.calipso.framework.storage.daotype;

public interface KeyCode {

    public String get();
}
