package es.calipso.framework.storage.exceptions;

import java.util.Arrays;

import es.calipso.framework.text.TextBuilder;

@SuppressWarnings("serial")
public class EntityNotFoundException extends Exception {
    public EntityNotFoundException(String cl, Object[] primaryKey, String preparedQuery) {
        super(m.get("ENTITYNOTFOUND", cl, Arrays.toString(primaryKey), preparedQuery));
    }
    public EntityNotFoundException(String keycode) {
        super(m.get("ENTITYIDNOTFOUND", keycode));
    }
    private static final TextBuilder m = TextBuilder.getSingletonInstance();
}
