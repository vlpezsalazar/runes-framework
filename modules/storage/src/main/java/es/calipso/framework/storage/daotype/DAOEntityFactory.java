package es.calipso.framework.storage.daotype;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public abstract class DAOEntityFactory {
    public abstract DAOEntity newInstance(Object parent);//para create
    
    public abstract DAOEntity loadInstance(Object[] pks);//para load

    public Class<?> getDAOClass(){
        return DAOClass;
    }
    public void setDAOClass(Class<? extends Object> dclass){
        DAOClass = dclass;
    }
    public void getFields(Class<? extends Object> dclass){
        Field[] fields = DAOEntityFactory.getAllDeclaredFields(dclass);
//        Field.setAccessible(fields, true);
        for (Field field : fields){
            rfields.put(field.getName(), field);            
        }
    }
    public static final Field[] getAllDeclaredFields(Class<? extends Object> classt){
        ArrayList<Field> fields = new ArrayList<Field>();
        Class<? extends Object> cl = classt;
        while (!cl.equals(Object.class)){
            Field[] declaredFields = cl.getDeclaredFields();
            Field.setAccessible(declaredFields, true);
            Collections.addAll(fields, declaredFields);
            cl = cl.getSuperclass();
        }
        return fields.toArray(new Field[0]);
    }
    private Class<?> DAOClass;
    protected HashMap<String, Field> rfields = new HashMap<String, Field>();
}
