package es.calipso.framework.storage.daotype;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.calipso.framework.storage.exceptions.EntityNotFoundException;

public class DAOSQLHandler {

    public enum QueryType {
        OTHERQUERIES("update or delete"), 
        SELECTQUERY("select");

        private final String description;
        QueryType(String description){
            this.description = description;
        }
        @Override
        public String toString() {
            return description;
        }
    };
    private boolean error = false;

    
    public DAOSQLHandler(DataSource ds, HashMap<Integer, ArrayList<Integer>> opQueryIndexes, String[] preparedQueries, QueryType[] queryType,
            boolean [] onfailedquery){
        _queryType = queryType;
        _opQueryIndexes = opQueryIndexes;
        _preparedQueries = preparedQueries;
        _onfailedquery = onfailedquery; 
        _ds = ds;
    }

    public <T extends DAOEntity> DAOSQLHandler create(T o) throws EntityNotFoundException, SQLException{
        doop(0, o);
        return this;
    }

    public <T extends DAOEntity> DAOSQLHandler load(T entity) throws EntityNotFoundException, SQLException {
        doop(1, entity);
        return this;
    }

    public <T extends DAOEntity> void remove(T o) throws EntityNotFoundException, SQLException{
        doop(2, o);
    }

    public <T extends DAOEntity> void doop(int type, T o) throws EntityNotFoundException, SQLException{
        ArrayList<Integer> opQName = _opQueryIndexes.get(type);
        if (opQName != null){
            for (Integer query : opQName){
                op(query, o);
            }            
        }
    }
    public boolean isError() {
        return error;
    }
    private <T extends DAOEntity> void op(int CONSTQUERYINDEX, T o) throws EntityNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            error = false;
            QueryType queryType = _queryType[CONSTQUERYINDEX]; 
            con = _ds.getConnection();
            
            log.debug("preparando consulta: {}", _preparedQueries[CONSTQUERYINDEX]);
            
            ps = con.prepareStatement(_preparedQueries[CONSTQUERYINDEX]);
            
            o.prepare(CONSTQUERYINDEX, ps);
            
            log.debug("lanzando consulta: {}, tipo: {}", ps, queryType);
            switch (queryType){
            case SELECTQUERY:
                rs = ps.executeQuery();
                if (!rs.next()){
                    if (_onfailedquery[CONSTQUERYINDEX]){
                        error = true;
                        throw new EntityNotFoundException(o.getSuperClass().getCanonicalName(), o.getPrimaryKey(), _preparedQueries[CONSTQUERYINDEX]);
                    }
                } else do {
                    o.populate(CONSTQUERYINDEX, rs);
                } while (rs.next());
            break;
            case OTHERQUERIES:
                ps.executeUpdate();
            break;
            default:
                log.error("Tipo de consulta incorrecta {}: {}", queryType, _preparedQueries[CONSTQUERYINDEX]);
            }
        } catch (IllegalArgumentException e) {
            log.error("", e);
            error = true;
        } catch (SecurityException e) {
            error = true;
            log.error("", e);
        } catch (NullPointerException e){
            error = true;
            log.error("", e);
        } finally {
            clean(con, ps, rs);
        }
    }
    private static void clean(Connection con, PreparedStatement ps, ResultSet rs){
        if (rs!= null){
            try {
                rs.close();
            } catch (SQLException e){}
        }

        if (ps != null){
            try {
                ps.close();
            } catch (SQLException e){}
        }
        if (con != null){
            try {
                con.close();
            } catch (SQLException e){}
        }
    }
    
    private static Logger log = LoggerFactory.getLogger(DAOSQLHandler.class);
    private HashMap<Integer, ArrayList<Integer>> _opQueryIndexes; //[tipo_operacion][indice_consulta]

    private String[] _preparedQueries;
    private QueryType[] _queryType; // PREPAREQUERY | POPULATEQUERY

    private boolean[] _onfailedquery;

    private DataSource _ds = null;

}
