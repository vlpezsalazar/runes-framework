package es.calipso.framework.storage.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Esta anotación indica una clase persistente con soporte en una base de datos.
 * Permite especificar que esquema y tabla se encuentran asociadas con esta 
 * entidad mediante las propiedades <i>schema</i> y <i>table</i>. Con la propiedad
 * <i>queries</i> permite especificar un conjunto de @NamedQuery que son las
 * consultas SQL empleadas para rellenar los objetos de esta clase.
 * 
 * Esta anotación provee de un nivel de abstracción en las consultas sobre los
 * espacios de nombres del catalogo y tabla asociados a la entidad que se
 * especifican en @NamedQuery.query. En estas consultas se puede emplear el
 * nombre de la clase asociada a la anotación @DBEntity para referirse
 * tanto al esquema como a su tabla correspondiente. Ejemplo: "Select p.prop
 * from $AnnotadedClass t" en donde "AnnotatedClass" es el nombre de la clase
 * que fue anotada con @DBEntity. De esta manera se pueden especificar consultas entre distintas
 * entidades usando el nombre de la clase asociada.
 *  
 * @author Víctor López Salazar
 *
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface DBEntity {
    /*
    enum DAOType{
        GENERATED, //Emplear JavaAssist para modificar la clase 
        REFLECTION //Utilizar reflexion
    }

    DAOType type() default DAOType.GENERATED;
     */
    /**
     * Identificador que hace referencia a la tabla de la bd en la que se
     * almacena esta entidad. 
     * 
     */
    public String[] table() default {};
    /**
     * Catálogo de la bd en el que se almacena la tabla asociada a esta entidad.
     *   
     */
    public String schema() default "";
     /**
      * Consultas asociadas a esta entidad.
      * 
      * @see Queries
      */
    public NamedQuery[] queries() default {};
}
