package es.calipso.framework.storage.exceptions;

@SuppressWarnings("serial")
public class CannotRegisterException extends Exception {
    public CannotRegisterException(Throwable t){
        super(t);
    }
}
