package es.calipso.framework.storage.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Permite establecer la configuración de un conjunto de clases DAO. Indica el
 * host, la base de datos, usuario, password y el identificador de DBManager
 * que emplearan las clases para acceder a la misma. Esta anotación se establece
 * dentro del archivo package-info.java y se utiliza para marcar el paquete 
 * que contiene las clases DAO anotadas.  
 * @author victor
 *
 */
@Target(ElementType.PACKAGE)
@Retention(RetentionPolicy.RUNTIME)
public @interface DBConfig {
    String schema();
}
