package es.calipso.framework.storage.annotations;

public @interface Field {
    public String value();
}
