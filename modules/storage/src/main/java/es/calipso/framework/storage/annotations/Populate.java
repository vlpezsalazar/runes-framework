package es.calipso.framework.storage.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Populate{
    /**
     * Un conjunto de @Resultset con el que objeto se ha de inicializar/
     * rellenar. 
     * 
     * @return conjunto de consultas a utilizar para la inicialización del
     * objeto.
     */
    public String query();

    /**
     * Indica si el campo actual se debe obtener como un objeto codificado 
     * en una cadena de caracteres como un campo de tipo json de la bd. En
     * caso que este campo sea true con la cadena json se decodificará un
     * objeto del tipo de la propiedad marcada.
     *  
     * @return false si el objeto es un tipo normal, true en otro caso.
     */
    public boolean json() default false;
    /**
     * Indica un código de inicialización para el atributo. En este campo
     * se siguen una reglas de nomenclatura que permiten hacer referencia
     * a lo siguiente:
     * <ul>
     * <li> $this, referencia a la clase de la propiedad.
     * <li> $1..$n otras propiedades del objeto DAO anotadas mediante @Field.
     * <li> $<i>name-property</i> nombre de una propiedad del objeto.
     * <li> $$ la propiedad que se está inicializando.
     * <li> $rs el valor de cada @Resultset de las consultas asociadas a la
     *          propiedad
     * <li> $this es el objeto en el que se encuentra la propiedad. Util para
     *            emplear métodos del objeto. 
     * </ul>
     * 
     * @return codigo de inicialización.
     */
    public String init() default "";
    /**
     * Indica si la propiedad se debe marcar como un DAO que tiene anotaciones
     * y que seguirá los pasos de una clase DAO al ejecutar una consulta sobre
     * la clase en la que se incluye.
     * @return true en caso que la propiedad sea otro objecto DAO,
     * false en otro caso (se obtenga directamente desde alguna de las consultas
     * de la clase).
     */
    public boolean self() default false; 
}
