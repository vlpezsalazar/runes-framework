package es.calipso.framework.storage;

import java.io.IOException;
import java.security.ProtectionDomain;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.calipso.framework.storage.annotations.DBConfig;
import es.calipso.framework.storage.annotations.DBEntity;
import es.calipso.framework.storage.annotations.Execute;
import es.calipso.framework.storage.annotations.NamedQuery;
import es.calipso.framework.storage.annotations.PK;
import es.calipso.framework.storage.annotations.Populate;
import es.calipso.framework.storage.annotations.Query;
import es.calipso.framework.storage.daotype.DAOEntityFactory;
import es.calipso.framework.storage.daotype.DAOSQLHandler;
import es.calipso.framework.storage.daotype.DAOSQLHandler.QueryType;
import es.calipso.framework.text.TextBuilder;
import javassist.CannotCompileException;
import javassist.ClassClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtConstructor;
import javassist.CtField;
import javassist.CtField.Initializer;
import javassist.CtMethod;
import javassist.CtNewConstructor;
import javassist.CtNewMethod;
import javassist.CtPrimitiveType;
import javassist.Modifier;
import javassist.NotFoundException;

class DBEntityFactory {
    private static final String FACTORYIMPL = "FactoryImpl";

    public static DBEntityFactory getInstance() {
        return theInstance;
    }

    public void generate(Class<?> objectClass, DataSource ds, Map<String, String> packageSchemas, Map<Class<?>, DAOEntityFactory> daosfactories, Map<Class<?>, DAOSQLHandler> daohandlers)
            throws InstantiationException,
            IllegalAccessException, NotFoundException, ClassFormatError, CannotCompileException, IOException, ClassNotFoundException {
        DAOEntityFactory newEntity = null;
        CtClass dbentity = null;
        try {
            dbentity = cp.get(objectClass.getCanonicalName());
        } catch (NotFoundException e){
            cp.appendClassPath(new ClassClassPath(objectClass));
            dbentity = cp.get(objectClass.getCanonicalName());
        }

        Map<NamedQuery, CtClass> queries = getQueriesAndDeclaringClassesFrom(dbentity, objectClass, packageSchemas);
        //preparamos cada una de las consultas asociadas a la clase
        if (!queries.isEmpty()) {
            boolean [] failqueries = null;
            /**
             * mapa que asocia a cada consulta con nombre un índice con el que se identificará posteriormente en todas
             * las demás estructuras de datos.
             */
            HashMap<String, Integer> tempQueryNames = new HashMap<String, Integer>();
            /**
             * mapa que asocia un indice con el código sql que le corresponde (despues de eliminar cosas especiales
             * como variables $ y demás)
             */
            LinkedHashMap<Integer, String> tempPreparedQueries = new LinkedHashMap<Integer, String>();
            /**
             * mapa que para cada tipo de consulta (load, create, save, delete, method) conserva vectores de índices
             * que representan a cada consulta.
             */
            HashMap<Integer, ArrayList<Integer>> opQueryIndexes = new HashMap<Integer, ArrayList<Integer>>();
            /**
             * mapa que para cada consulta identifica el conjunto de propiedades de la clase que se deben emplear para
             * preparar la consulta posteriormente. 
             */
            LinkedHashMap<Integer, ArrayList<String>> prepareMethodData = new LinkedHashMap<Integer, ArrayList<String>>();
            /**
             * Este mapa asocia para cada consulta con nombre identificada por un índice, un conjunto ordenado de propiedades de
             * la clase a las que se les tiene que dar el valor correspondiente a la columna con el nombre de la clave en el 
             * resultset resultado de la ejecución de la consulta. De esta manera tenemos para cada consulta un vector de propiedades
             * que se deben rellenar usando cada una de las posiciones del resultset.
             */
            Map<Integer, Map<String, CtField>> populateMethodData = new LinkedHashMap<Integer, Map<String, CtField>>();
            
            HashMap<String, HashMap<String, String>> queryfieldpos = new HashMap<String, HashMap<String,String>>();
            
            TreeMap<String, CtField>  primaryKeys = new TreeMap<String, CtField>();
            /**
             * vector que identifica el tipo de consulta por su valor. Cada indice representa a una consulta y los valores
             * un tipo: 1 prepare, 2 populate, 3 ambas.
             */
            QueryType[] _queryType = null;
            String queryName, query;
            
            Set<Entry<NamedQuery, CtClass>> querySet = queries.entrySet();
            int i = 0;
            _queryType = new QueryType[querySet.size()];
            failqueries = new boolean[querySet.size()];
            
            ArrayList<Integer> opqueries;
            
            boolean insert = false;
            
            for (Entry<NamedQuery, CtClass> queryandclass : querySet) {
                NamedQuery nq = queryandclass.getKey();
                CtClass currentEntity = queryandclass.getValue();
                int[] types = nq.type();
                for (int type : types) {
                    opqueries = opQueryIndexes.get(type);
                    if (opqueries == null) {
                        opqueries = new ArrayList<Integer>();
                        opQueryIndexes.put(type, opqueries);
                    }
                    opqueries.add(i);
                }
                tempcode.delete(0, tempcode.length());
                queryName = nq.name();
                HashMap<String, String> qfieldpos = new HashMap<String, String>();
                queryfieldpos.put(queryName, qfieldpos);
                tempQueryNames.put(queryName, i);
                query = nq.query();
                if (query.matches("\\s*select.+")){
                    _queryType[i] = DAOSQLHandler.QueryType.SELECTQUERY;
                } else {
                    _queryType[i] = DAOSQLHandler.QueryType.OTHERQUERIES;
                }
                failqueries[i] = nq.fail();
                ArrayList<String> fieldNames = new ArrayList<String>();
                insert = false;
                if (query.matches("\\s*insert.+")){
                    insert = true;
                }
                Matcher m = pat.matcher(query);
                String replacement = "";
                //GET THE VARIABLE NAMES
                String columnField;

                while (m.find()) {
                    String fieldName = m.group(2);
//                        log.debug(fieldName + " " + m.groupCount() + " " +  m.group(2));
                    if (insert && !isRegisteredTable(fieldName, currentEntity.getPackageName())){
                        replacement = "?";
                        fieldNames.add(fieldName);
                    } else if (m.group(1) != null && m.group(1).contains("=")) {
                        replacement = " = ?";
                        fieldNames.add(fieldName);
                    } else  if (isRegisteredTable(fieldName, currentEntity.getPackageName())){ //hemos cogido el nombre de una tabla en la consulta
                        String[] schemaTable = getSchemaAndTable(fieldName, currentEntity, packageSchemas);
                        replacement = MessageFormat.format(schemaTable[0], schemaTable[1], schemaTable[2]);
                    } else { // es una variable que se emplea para hacer un populate sobre una propiedad de la clase
                        try {
                            CtField field = dbentity.getField(fieldName);
                            columnField = m.group(4);
                            if (columnField != null && columnField.contains(".")){
                               columnField = m.group(6);
                            }
                            qfieldpos.put(fieldName, columnField);
                            addPopulateProperty(field, i, columnField, populateMethodData);
                        } catch (NotFoundException e){
                            log.warn(msn.get("SQLCLASSPROPERTYNOTFOUND", query, currentEntity.getName(), fieldName));
                        } finally {
                            if (m.group(3) != null && m.group(3).contains("=")){
                                replacement = m.group(4);
                            } else {
                                replacement = fieldName;
                            }
                        }
                    }

//                        log.debug("reemplazo " + replacement);
                    m.appendReplacement(tempcode, replacement);
                }
                m.appendTail(tempcode);

                prepareMethodData.put(i, fieldNames);
                tempPreparedQueries.put(i++, tempcode.toString());
            }
            log.debug("Consultas para la clase {} = \n{}", objectClass.getName(), tempPreparedQueries.toString().replaceAll(", (\\d+=)", "\n$1"));
            /**
             * Mapa de nombre de metodo, datos del metodo. La primera posición de los datos del metodo está reservada a la consulta preparada.
             */
            HashMap<CtMethod, ArrayList<String>> methodData = new HashMap<CtMethod, ArrayList<String>>();
            CtMethod[] methods = dbentity.getMethods();
            for (CtMethod method : methods){
                if (!method.hasAnnotation(Query.class)) continue;
                Query methodQuery = (Query) method.getAnnotation(Query.class);
                query = methodQuery.value();
                Matcher m = pat.matcher(query);
                ArrayList<String> varData = new ArrayList<String>();
                methodData.put(method, varData);
                insert = false;
                if (query.matches("\\s*insert.+")){
                    insert = true;
                }

                tempcode.delete(0, tempcode.length());
                String fieldName,
                replacement;
                while(m.find()){
                    fieldName = m.group(2);
//                        log.debug(fieldName);
                    if (fieldName.matches("\\$\\d*") || isRegisteredTable(fieldName, dbentity.getPackageName())){ //hemos cogido el nombre de una tabla en la consulta
                        String[] schemaTable = getSchemaAndTable(fieldName, dbentity, packageSchemas);
                        replacement = MessageFormat.format(schemaTable[0], schemaTable[1], schemaTable[2]);
                    } else {
                        varData.add(fieldName);
                        if (insert){
                            replacement = "?";
                        } else {
                            replacement = " = ?";                            
                        }

                    }
                    m.appendReplacement(tempcode, replacement);
                }
                m.appendTail(tempcode);
                varData.add(0, tempcode.toString());
            }
            HashMap<String, Integer> classInfo = new HashMap<String, Integer>();
            CtField[] fields = getAllFields(dbentity, classInfo);

    //            TreeMap<Integer, CtField> vars = new TreeMap<Integer, CtField>();
            HashMap<CtField, String> initializer = new HashMap<CtField, String>();
            for (CtField field : fields) {
//                    log.debug(MessageFormat.format("Generando datos para la propiedad {0}", field));
                if (field.hasAnnotation(PK.class)){
                    PK pk = (PK) field.getAnnotation(PK.class);
                    String cf = field.getDeclaringClass().getName();
                    String fieldkey = classInfo.get(cf) + cf + pk.value(); 
                    primaryKeys.put(fieldkey, field);
                }
                if (!field.hasAnnotation(Populate.class)) continue;
                try {
                    Populate p = (Populate) field.getAnnotation(Populate.class);
                    initializer.put(field, p.init());
                    queryName = p.query();
                    int qindex = tempQueryNames.get(queryName);
                    String columnName = null;
                    HashMap<String, String> qfieldpos = queryfieldpos.get(queryName);
                    if (qfieldpos != null){
                        columnName = qfieldpos.get(field.getName());
                    } 
                    addPopulateProperty(field, qindex, columnName, populateMethodData);
//                    if (_queryType[qindex] < 2) //no hay otras consultas distintas a other o select por lo que este if sobra 
                        _queryType[qindex] = DAOSQLHandler.QueryType.SELECTQUERY;
                } catch (ClassNotFoundException e){
                    log.error("", e);
                }
            }
            newEntity = generateIEntity(objectClass.getClassLoader(), objectClass.getProtectionDomain(), 
                    dbentity, primaryKeys.values(),
                    prepareMethodData, populateMethodData, methodData, initializer,
                    tempQueryNames);
            DAOSQLHandler daoHandler = new DAOSQLHandler(ds, opQueryIndexes, tempPreparedQueries.values().toArray(new String[0]), _queryType, failqueries);
            newEntity.getFields(objectClass);
            daosfactories.put(objectClass, newEntity);
            daohandlers.put(newEntity.getDAOClass(), daoHandler);
        }
    }
    private static void getPackageSchema(Class<?> objectClass, Map<String, String> packageSchemas){
        Package clpackage = objectClass.getPackage();
        DBConfig dbconfig = clpackage.getAnnotation(DBConfig.class);
        String packageName = clpackage.getName();
        if (!packageSchemas.containsKey(packageName) && dbconfig != null){
            packageSchemas.put(packageName, dbconfig.schema());
        }
    }
    //SE EMPLEAN DOS CLASES POR UN ERROR AL OBTENER EL HASH DE UN ARRAY DE int EN JAVASSIST 
    private Map<NamedQuery, CtClass> getQueriesAndDeclaringClassesFrom(CtClass dbentity, Class<?> objectClass, Map<String, String> packageSchemas) {
        LinkedHashMap<NamedQuery, CtClass> queriesAndClasses = new LinkedHashMap<NamedQuery, CtClass>();
        CtClass presentClass = dbentity;
        do {
            DBEntityFactory.getPackageSchema(objectClass, packageSchemas);
//            log.debug(MessageFormat.format("Procesando clase: {0} ", presentClass.getName()));
            DBEntity queries = (DBEntity) objectClass.getAnnotation(DBEntity.class);
            if (queries != null){
                String[] tables = queries.table();
                if (tables.length == 0){
                    classTables.put(presentClass.getName(), new String[]{presentClass.getSimpleName()});
                } else {
                    classTables.put(presentClass.getName(), tables);
                }

                // y el esquema de la misma
                if (!queries.schema().isEmpty()){
                    classSchemas.put(presentClass.getName(), queries.schema());
                }
                //asociamos cada una de las consultas a la clase donde se declara
                NamedQuery[] qs = queries.queries();
                for (NamedQuery query : qs){
                    queriesAndClasses.put(query, presentClass);
                }
            }
            try {
                presentClass = presentClass.getSuperclass();
                objectClass = objectClass.getSuperclass();
            } catch (NotFoundException e) {
                log.error("", e);
            }
        } while (!presentClass.equals(ctObject));

        return queriesAndClasses;
    }

    private String[] getSchemaAndTable(String fieldName, CtClass dbentity, Map<String, String> packageSchemas){
        String schemaTable[] = new String[3];
        String packagename, classname;
        Matcher m = pat1.matcher(fieldName);
        int tableIndex = 0; 
        if (m.matches()){ //es el nombre de la tabla de la clase actual
            packagename = dbentity.getPackageName();
            classname = dbentity.getName();
            String number = m.group(1);
            if (number != null && !number.isEmpty()){
                tableIndex = Integer.parseInt(number);
            }
 
        } else {
            if (fieldName.contains(".")){ //clase que no pertenece al mismo paquete de la clase actual
                packagename = fieldName.substring(0, fieldName.lastIndexOf("."));
                classname = fieldName;
            } else { // clase que pertenece al mismo paquete que la clase actual
                packagename = dbentity.getPackageName(); 
                classname = MessageFormat.format("{0}.{1}", packagename, fieldName).toString();
            }
        }
        schemaTable[1] = classSchemas.get(classname);
        if (schemaTable[1] == null){
            schemaTable[1] = packageSchemas.get(packagename);
        }

        if (schemaTable[1] == null){
            schemaTable[0] = "{1}";
        } else {
            schemaTable[0] = "{0}.{1}";
        }
        schemaTable[2] = classTables.get(classname)[tableIndex];

        return schemaTable;
    }
    private void addPopulateProperty(CtField field, int query, String columnField, Map<Integer, Map<String, CtField>> populateMethodData){
        TreeMap<String, CtField> orderedFieldPosView = (TreeMap<String, CtField>) populateMethodData.get(query);
        if (orderedFieldPosView == null){
            orderedFieldPosView = new TreeMap<String, CtField>();
            populateMethodData.put(query, orderedFieldPosView);
        }
        if (columnField == null){
            columnField = field.getName();
        }
        orderedFieldPosView.put(columnField, field);
    }
    private boolean isRegisteredTable(String fieldName, String packageName) {
        boolean isRegistered = false;
        if (fieldName.matches("\\$\\d*")){
            return true;
        }
        if (fieldName.contains(".")){ //clase totalmente cualificada
            isRegistered = classTables.containsKey(fieldName);
        } else {
            isRegistered = classTables.containsKey(MessageFormat.format("{0}.{1}", packageName,fieldName));
        }
        return isRegistered;
    }
/*
    @SuppressWarnings("rawtypes")
    public String createPreparedStatement(
            String rawstatement, Object o) throws IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchFieldException {
        Class<? extends Object> cl = o.getClass();
        Matcher m = pat.matcher(rawstatement);
        tempcode.delete(0, tempcode.length());
        while (m.find()) {
            java.lang.reflect.Field field = cl.getField(m.group(1));
            if (!field.isAccessible())
                field.setAccessible(true);
            Class<?> clType = field.getType();
            if (AbstractCollection.class.isAssignableFrom(clType) || clType.isArray() ){
                List a = new ArrayList();
                field.get(a);
                code.writeCode("?");
                for (int i = 1, n = a.size(); i<n; i++){
                    code.appendCode(", ?");
                }
                m.appendReplacement(tempcode, code.flush());
            } else if (AbstractMap.class.isAssignableFrom(clType)){
                Map a = new LinkedHashMap();
                field.get(a);
                for (Object key : a.keySet()){
                    code.appendCode(key.toString()).appendCode(" = ").appendCode("?").appendCode(", ");
                }
                m.appendReplacement(tempcode, code.flush().replaceFirst(",\\z", ""));
            } else {
                m.appendReplacement(tempcode, "?");
            }

        }
        m.appendTail(tempcode);
        return tempcode.toString();
    }
*/
    private synchronized DAOEntityFactory generateIEntity(
            ClassLoader classLoader, ProtectionDomain protectionDomain, CtClass superClass,
            Collection<CtField> primaryKeys, LinkedHashMap<Integer, ArrayList<String>> prepareMethodData,
            Map<Integer, Map<String, CtField>> populateMethodData,
            HashMap<CtMethod, ArrayList<String>> methodData, HashMap<CtField, String> initializer,
            HashMap<String, Integer> querynames) throws NotFoundException,
            CannotCompileException, IOException, ClassFormatError,
            InstantiationException, IllegalAccessException, ClassNotFoundException {

        String clSimpleName = superClass.getSimpleName();
        String packageName = superClass.getPackageName();
        String iEntityImplName = code.writeCode(packageName)
                .appendCode(INTERFACE).appendCode(clSimpleName)
                .appendCode(IMPL).flush();
//        boolean defaultConsPresent = false, copyConsPresent = false;
        Class<?> defineClass;
        DAOEntityFactory f;
        log.debug("Definiendo clase DAO: {}", iEntityImplName);
        CtClass iEntityImplClass = cp.getOrNull(iEntityImplName);
        if (iEntityImplClass == null){
            iEntityImplClass = cp.makeClass(iEntityImplName);
            iEntityImplClass.addInterface(ctIEntity);
            iEntityImplClass.setSuperclass(superClass);

            String superdotclass = JavaSrcCodeGenerator.getDotClassOfType(superClass.getName());

            // añadir una instancia de DBManager
            CtField ctfdbmanager = new CtField(ctDBManager, JavaSrcCodeGenerator.LOCALDBMANAGERFIELDNAME,
                    iEntityImplClass);

            ctfdbmanager.setModifiers(ctfdbmanager.getModifiers() | Modifier.STATIC);

            iEntityImplClass.addField(ctfdbmanager,
                    Initializer.byExpr(MessageFormat.format("{0}.getManager({1})", ctDBManager.getName(), superdotclass)));

//            CtField ctflog = new CtField(ctLogger, "log", iEntityImplClass);
//            ctflog.setModifiers(ctflog.getModifiers() | Modifier.STATIC | Modifier.FINAL);
//            iEntityImplClass.addField(ctflog, Initializer.byExpr(MessageFormat.format("{0}.getLogger({1}.class)", ctLogger.getName(), iEntityImplName)));

            CtField ctrfields = new CtField(ctHashMap, "rfields", iEntityImplClass);
            iEntityImplClass.addField(ctrfields);
            
            CtField ctregistrable = new CtField(ctRegistrable, "_registrable_", iEntityImplClass);
            iEntityImplClass.addField(ctregistrable);
            
            //constructor 1
            writeCopyCons(superClass);
            CtConstructor defaultcons = CtNewConstructor.make(new CtClass[]{ctHashMap, ctObject}, null, iEntityImplClass);
            defaultcons.setBody(code.flush());
            iEntityImplClass.addConstructor(defaultcons);
 
            //Añadimos el array de primaryKey
            CtField fpk = new CtField(ctObjectArray, "primaryKey", iEntityImplClass);
            iEntityImplClass.addField(fpk, Initializer.byNewArray(ctObjectArray, primaryKeys.size()));

            //creamos los métodos del interfaz Registrable
            log.debug("generando método getSuperClass...");
            code.writeCode(JavaSrcCodeGenerator.codeBlock()).replaceCodePoint(JavaSrcCodeGenerator.returnStatement(superdotclass));
            CtMethod getSuperClass = CtNewMethod.make(ctClass, "getSuperClass", null, null, code.flush(), iEntityImplClass);
            iEntityImplClass.addMethod(getSuperClass);

            log.debug("generando método getPrimaryKey...");
            writeGetPrimaryKeyMethod(primaryKeys);
            CtMethod getPrimaryKey = CtNewMethod.make(ctObjectArray, "getPrimaryKey", null, null, code.flush(), iEntityImplClass);
            iEntityImplClass.addMethod(getPrimaryKey);
            
            log.debug("generando método getRegistrable...");
            CtMethod getRegistrable = CtNewMethod.make(ctRegistrable, "getRegistrable", null, null, "return _registrable_;", iEntityImplClass);
            iEntityImplClass.addMethod(getRegistrable);

            //creamos el método del interfaz KeyCode
            CtMethod get = CtNewMethod.make(ctString, "get", null, null, "return _registrable_.getKey().toString();", iEntityImplClass);
            iEntityImplClass.addMethod(get);
            
            //añadir una propiedad que almacena el tipo de la clase padre
/*            CtField ctfparent = new CtField(ctObject, "parent", iEntityImplClass);
            iEntityImplClass.addField(ctfparent, Initializer.byExpr(superdotclass));

            //añadir un getter para la propiedad anterior
            CtMethod supergetter = CtNewMethod.getter("getSuperClass", ctfparent);
            iEntityImplClass.addMethod(supergetter);*/

            //añadimos los métodos del interfaz DBEntity
            
            log.debug("generando método populate de la clase DAO...");
            writePopulateSwitchCode(populateMethodData, clSimpleName,
                    initializer, querynames);
            // añadir el metodo populate
            CtMethod populateMethod = CtNewMethod.make(CtClass.voidType, "populate",
                    new CtClass[]{CtClass.intType, ctResultSet},
                    new CtClass[]{ctSQLException},
                    code.toString(),
                    iEntityImplClass);
            log.debug("{}{} ", populateMethod.getLongName(), code.toString());
            iEntityImplClass.addMethod(populateMethod);

            // añadir el metodo prepare del interfaz IEntity
            log.debug("generando método prepare de la clase DAO...");
            writePrepareSwitchCode(prepareMethodData, superClass);
            CtMethod prepareMethod = CtNewMethod.make(CtClass.voidType, "prepare",
                    new CtClass[] {CtClass.intType, ctPreparedStatement},
                    new CtClass[]{ctSQLException},
                    code.toString(),
                    iEntityImplClass);
            log.debug("{}{} ", prepareMethod.getLongName(), code.toString());
            iEntityImplClass.addMethod(prepareMethod);

            log.debug("generando método setPK...");
            writeSetPKMethod(primaryKeys);
            log.debug(code.toString());
            CtMethod setPK = CtNewMethod.make(code.toString(), 
                    iEntityImplClass);
            log.debug("{}{} ", setPK.getLongName(), code.toString());
            iEntityImplClass.addMethod(setPK);

            //Añadir los métodos sql declarados en la clase padre
            for (Entry<CtMethod, ArrayList<String>> methodEntry : methodData.entrySet()){
                CtMethod parentMethod = methodEntry.getKey();
                CtMethod sqlMethod = CtNewMethod.copy(parentMethod, iEntityImplClass, null);
                writeSQLMethod(parentMethod, methodEntry.getValue());
                log.debug("método generado: {}{} ", sqlMethod.getLongName(), code.toString());
                sqlMethod.setExceptionTypes(new CtClass[]{ctSQLException});
                sqlMethod.setBody(code.flush());
                iEntityImplClass.addMethod(sqlMethod);
            }
            iEntityImplClass.setModifiers(iEntityImplClass.getModifiers() & ~Modifier.ABSTRACT);
            log.debug("escribiendo clase {} ", iEntityImplClass.getName());
            //          byte[] iEntityImplByteCode = iEntityImplClass.toBytecode();
          defineClass = iEntityImplClass.toClass(classLoader, protectionDomain); 
              /*defineClass(iEntityImplName, iEntityImplByteCode, 0,
                  iEntityImplByteCode.length);*/
        
        } else {
            log.debug("la clase ya estaba definida, cargando... ");
            defineClass = classLoader.loadClass(iEntityImplName);
        }

        //implementar la factoria
        String iEntityFactoryImplName = code.writeCode(packageName)
        .appendCode(INTERFACE).appendCode(clSimpleName)
        .appendCode(FACTORYIMPL).flush();
        log.debug("Implementando la factoría {}... ", iEntityFactoryImplName);
        CtClass iEntityFactoryImplClass = cp.getOrNull(iEntityFactoryImplName);
        if (iEntityFactoryImplClass == null){
            iEntityFactoryImplClass = cp.makeClass(iEntityFactoryImplName);
            iEntityFactoryImplClass.setSuperclass(ctIEntityFactory);
            CtConstructor factorycons = CtNewConstructor.defaultConstructor(iEntityFactoryImplClass);
            iEntityFactoryImplClass.addConstructor(factorycons);

//            CtField ctflog = new CtField(ctLogger, "log", iEntityFactoryImplClass);
//            ctflog.setModifiers(ctflog.getModifiers() | Modifier.STATIC | Modifier.FINAL);
//            iEntityFactoryImplClass.addField(ctflog, Initializer.byExpr(MessageFormat.format("{0}.getLogger({1}.class)", ctLogger.getName(), iEntityFactoryImplName)));
            String daoentity = ctIEntity.getName();
            code.writeCode("public ").appendCode(daoentity).appendCode(" loadInstance(Object[] primarykey){ /*this.log.debug(MessageFormat.format(\"primarykeys = {0}\", new Object[]{Arrays.toString(primarykey)}));*/");
            code.appendCode(daoentity).appendCode(" o = new ").appendCode(iEntityImplClass.getName()).appendCode("(rfields, null); o.setPK(primarykey); return o;}");
            log.debug(code.toString());
            CtMethod loadInstanceMethod = CtNewMethod.make(code.flush(), iEntityFactoryImplClass);//new CtMethod(ctIEntity, "newInstance", new CtClass[]{ctObjectArray}, iEntityFactoryImplClass);
            iEntityFactoryImplClass.addMethod(loadInstanceMethod);

            code.writeCode("public ").appendCode(daoentity).appendCode(" newInstance(Object parent){").appendCode(daoentity).appendCode(" o = new ").appendCode(iEntityImplClass.getName()).appendCode("(rfields, parent); return o;}");
            log.debug(code.toString());
            CtMethod newInstanceMethod = CtMethod.make(code.flush(), iEntityFactoryImplClass);
            log.debug("DAOEntityFactory.newInstance() = {}", newInstanceMethod.toString());
            iEntityFactoryImplClass.addMethod(newInstanceMethod);
            f = (DAOEntityFactory) iEntityFactoryImplClass.toClass(classLoader, protectionDomain).newInstance();
        } else {
            log.debug("la clase ya estaba definida, cargando... ");
            f = (DAOEntityFactory) classLoader.loadClass(iEntityFactoryImplName).newInstance();
        }
        log.debug("hecho...");
        f.setDAOClass(defineClass);
        return f;
    }
    private void writeSetPKMethod(Collection<CtField> primaryKeys) {
        int i = 0;
        code.writeCode("public void setPK(Object[] pks){");
//        code.appendCode("this.log.debug(MessageFormat.format(\"primarykeys = {0}\", new Object[]{Arrays.toString(pks)}));");
        code.appendCode("if (pks == null || pks.length < ").appendCode(String.valueOf(primaryKeys.size())).appendCode("){");
        code.appendCode("return;");
        code.appendCode("}");
        for (CtField f : primaryKeys){
            try {
                if ((f.getModifiers() & Modifier.PRIVATE) == Modifier.PRIVATE){
                    String superfieldName = "superR" + f.getName();
                    code.appendCode("Field ").appendCode(superfieldName).appendCode(" = (java.lang.reflect.Field)").appendCode("rfields.get(\"").appendCode(f.getName()).appendCode("\");");
                    code.appendCode(superfieldName).appendCode(".set($0, ").appendCode("pks[").appendCode(String.valueOf(i++)).appendCode("]);");

                } else {
                    CtClass type = f.getType();
                    if (type.isPrimitive()){
                        if (type.equals(CtClass.booleanType)){
                            code.appendCode("this.").appendCode(f.getName()).appendCode(" = ((Boolean) pks[").appendCode(String.valueOf(i++)).appendCode("]).booleanValue();");
                        } else if (type.equals(CtClass.charType)) {
                            code.appendCode("this.").appendCode(f.getName()).appendCode(" = ((Character) pks[").appendCode(String.valueOf(i++)).appendCode("]).charValue();");
                        } else if (type.equals(CtClass.doubleType)) {
                            code.appendCode("this.").appendCode(f.getName()).appendCode(" = ((Double) pks[").appendCode(String.valueOf(i++)).appendCode("]).doubleValue();");
                        } else if (type.equals(CtClass.floatType)) {
                            code.appendCode("this.").appendCode(f.getName()).appendCode(" = ((Float) pks[").appendCode(String.valueOf(i++)).appendCode("]).floatValue();");
                        } else if (type.equals(CtClass.intType)) {
                            code.appendCode("this.").appendCode(f.getName()).appendCode(" = ((Integer) pks[").appendCode(String.valueOf(i++)).appendCode("]).intValue();");                        
                        } else if (type.equals(CtClass.longType)) {
                            code.appendCode("this.").appendCode(f.getName()).appendCode(" = ((Long) pks[").appendCode(String.valueOf(i++)).appendCode("]).longValue();");                        
                        } else if (type.equals(CtClass.shortType)) {
                            code.appendCode("this.").appendCode(f.getName()).appendCode(" = ((Short) pks[").appendCode(String.valueOf(i++)).appendCode("]).shortValue();");                        
                        } else if (type.equals(CtClass.byteType)) {
                            code.appendCode("this.").appendCode(f.getName()).appendCode(" = ((Byte) pks[").appendCode(String.valueOf(i++)).appendCode("]).byteValue();");
                        }
                    } else {
                        code.appendCode("this.").appendCode(f.getName()).appendCode(" = ((").appendCode(type.getName()).appendCode(") pks[").appendCode(String.valueOf(i++)).appendCode("]);");                    
                    }                    
                }
            } catch (NotFoundException e) {
                log.warn("", e);
            }
        }
        code.appendCode("}");
    }

    private void writeGetPrimaryKeyMethod(Collection<CtField> primaryKeys) {
        int i = 0;
        code.writeCode("{");
//        code.appendCode("this.log.debug(MessageFormat.format(\"inicialmente primarykeys = {0}\", new Object[]{Arrays.toString($0.primaryKey)}));");
        for (CtField f : primaryKeys){
            if ((f.getModifiers() & Modifier.PRIVATE) == Modifier.PRIVATE){
                code.appendCode("$0.primaryKey[").appendCode(String.valueOf(i++)).appendCode("] = ").appendCode("((java.lang.reflect.Field)rfields.get(\"").appendCode(f.getName()).appendCode("\")).get($0);");
            } else {
                code.appendCode("$0.primaryKey[").appendCode(String.valueOf(i++)).appendCode("] = ($w)").appendCode("$0.").appendCode(f.getName()).appendCode(";");                
            }
        }
//        code.appendCode("this.log.debug(MessageFormat.format(\"finalmente primarykeys = {0}\", new Object[]{Arrays.toString($0.primaryKey)}));");
        code.appendCode("return ($r)$0.primaryKey;");
        code.appendCode("}");
    }
    private CtField[] getAllFields(CtClass cl, HashMap<String, Integer> classInfo){
        ArrayList<CtField> fields = new ArrayList<CtField>();
        int i = 0;
        while (!cl.equals(ctObject)){
            if (classInfo != null){
                i++;
                classInfo.put(cl.getName(), i);
            }
            Collections.addAll(fields, cl.getDeclaredFields());
            try {
                cl = cl.getSuperclass();
            } catch (NotFoundException e) {}
        }
        return fields.toArray(new CtField[0]);
    }
    private void writeCopyCons(CtClass superClass) {
        code.writeCode("{;");
        code.appendCode("$0.rfields = $1;");
        code.appendCode("$0._registrable_ = new ").appendCode(ctRegistrable.getName()).appendCode("($0);");
        code.appendCode("if ($2 != null){");
        CtField[] fields = getAllFields(superClass, null);
        int STATICFINAL = Modifier.FINAL | Modifier.STATIC;
//        code.appendCode("try{");
        for (CtField field : fields){
            if ((field.getModifiers() & STATICFINAL) != STATICFINAL){
                String superfieldName = "superR" + field.getName();
                code.appendCode("Field ").appendCode(superfieldName).appendCode(" = (java.lang.reflect.Field)").appendCode("rfields.get(\"").appendCode(field.getName()).appendCode("\");");
                code.appendCode(superfieldName).appendCode(".set($0, ").appendCode(superfieldName).appendCode(".get($2));");
            }
        }
//        code.appendCode("}catch(java.lang.IllegalAccessException e){");
//        code.appendCode("log.error(\"\", e);");
//        code.appendCode("}catch(java.lang.IllegalArgumentException e){");
//        code.appendCode("log.error(\"\", e);");
//        code.appendCode("}");
        code.appendCode("}}");
    }

    private void writeSQLMethod(CtMethod parentMethod, ArrayList<String> vars) throws ClassNotFoundException, NotFoundException {
        boolean abstractMethod = (parentMethod.getModifiers() & Modifier.ABSTRACT) == Modifier.ABSTRACT;
        code.writeCode(JavaSrcCodeGenerator.codeBlock());
        CtClass dbentity = parentMethod.getDeclaringClass();
        ArrayList<Object> params = new ArrayList<Object>();
        String varname;
        CtField[] fields = getAllFields(dbentity, null);
        String sql = MessageFormat.format("\"{0}\"", vars.remove(0));
        int i, n = fields.length;
        for (int j = 0, n1 = vars.size(); j < n1; j++){
            varname = vars.get(j);
            for (i = 0; i < n  && !varname.equals(fields[i].getName()); i++);
            if (i < n){
                if ((fields[i].getModifiers() & Modifier.PRIVATE) == Modifier.PRIVATE){
                    varname = MessageFormat.format("((Field)#0.rfields.get(\"{0}\")).get(#0)", varname); 
                } else {
                    varname = MessageFormat.format("#0.{0}", varname);
                }

            } else {
                varname = MessageFormat.format("#{0}", varname);
            }
            params.add(varname);
        }
        log.debug(params.toString());
        String methodCall = "executeQuery";
        if (sql.matches("\\s*select.+")){
            methodCall = "retrieveSetFromQuery";
        }
        Query q = (Query) parentMethod.getAnnotation(Query.class);
        String callsql = JavaSrcCodeGenerator.callMethod(JavaSrcCodeGenerator.LOCALDBMANAGERFIELDNAME, methodCall, sql, JavaSrcCodeGenerator.newArray("Object", params));
        String condition = q.condition();
        if (condition != null && !condition.isEmpty()){
            callsql = JavaSrcCodeGenerator.ifStatement(condition, JavaSrcCodeGenerator.endStatement(callsql));
        }

        if (!abstractMethod){
            CtClass returnType = parentMethod.getReturnType();
            Execute when = q.when();
            String callsuper = JavaSrcCodeGenerator.endStatement(JavaSrcCodeGenerator.callMethod("super", parentMethod.getName(), "##"));
            switch (when){
            case AFTER:{
                if (!returnType.equals(CtClass.voidType)){
                    code.insertBeforeCodePoint(JavaSrcCodeGenerator.declareVar(returnType.getName(), "ret", callsuper));
                    code.appendAfterCodePoint(JavaSrcCodeGenerator.returnStatement("ret"));
                } else {
                    code.insertBeforeCodePoint(callsuper);
                }
            }
            break;
            case BEFORE: {
                if (!returnType.equals(CtClass.voidType)){
                    code.appendAfterCodePoint(JavaSrcCodeGenerator.returnStatement(callsuper));
                } else {
                    code.appendAfterCodePoint(callsuper);
                }
            }
            break;
            }
            code.replaceCodePoint(JavaSrcCodeGenerator.endStatement(callsql));
        } else code.replaceCodePoint(JavaSrcCodeGenerator.returnStatement(callsql));
        code.replaceCodeVarsToMethodVars();
    }

    private static void writePopulateSwitchCode(
            Map<Integer, Map<String, CtField>> populateMethodData,
            String type,
            HashMap<CtField, String> initializer,
            HashMap<String, Integer> querynames) throws NotFoundException {
        /*
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(1);
        code.writeCode(JavaSrcCodeGenerator.codeBlock()).replaceCodePoint(JavaSrcCodeGenerator.switchStatement("$1",
                list)).replaceCodePoint("this.login = $2.getString(1);");
         */
        code.writeCode(JavaSrcCodeGenerator.codeBlock()).replaceCodePoint(JavaSrcCodeGenerator.switchStatement("#1",
                populateMethodData.keySet()));

        for (Entry<Integer, Map<String, CtField>> data : populateMethodData
                .entrySet()) {
            for (Entry<String, CtField> columnField : data.getValue().entrySet()) {
                CtField field = columnField.getValue();
                String initcode = initializer.get(field);
                if (initcode == null || initcode.isEmpty()) {
                    if((field.getModifiers() & Modifier.PRIVATE) == Modifier.PRIVATE){ //en caso que el campo sea privado tenemos que emplear reflexion
                        code.insertBeforeCodePoint("Field r").insertBeforeCodePoint(field.getName()).insertBeforeCodePoint(" = (java.lang.reflect.Field)rfields.get(\"").insertBeforeCodePoint(field.getName()).insertBeforeCodePoint("\");");
                        code.insertBeforeCodePoint("r").insertBeforeCodePoint(field.getName()).insertBeforeCodePoint(".set(#0, ($w) ").insertBeforeCodePoint(JavaSrcCodeGenerator.getPopulateCallFromType(
                                field.getType().getSimpleName(), columnField.getKey())).insertBeforeCodePoint(");");
                    } else {
                        String fieldcode = JavaSrcCodeGenerator.accessField(
                                "#0", field.getName());
                        initcode = JavaSrcCodeGenerator.getPopulateCallFromType(
                                field.getType().getSimpleName(), columnField.getKey());
                        String setfieldcode = JavaSrcCodeGenerator
                                .endStatement(JavaSrcCodeGenerator.initField(
                                        fieldcode, initcode));
                        code.insertBeforeCodePoint(setfieldcode);
                    }
                } else {
                    String c = JavaSrcCodeGenerator.replaceSpecialVars(
                            initcode, field, columnField.getKey());
                    log.debug(c);
                    code.insertBeforeCodePoint(c);
                }
            }
            code.replaceCodePoint("");
        }
        code.replaceCodeVarsToMethodVars();
    }

    private static void writePrepareSwitchCode(
            LinkedHashMap<Integer, ArrayList<String>> methodData, CtClass superClass)
            throws NotFoundException {
        String incvarcode = JavaSrcCodeGenerator.incVar("i");
        String pssetobjectcall = JavaSrcCodeGenerator.endStatement(JavaSrcCodeGenerator.getPrepareCallFromType("Object"));
        String varcode = JavaSrcCodeGenerator.endStatement(JavaSrcCodeGenerator.declareVar("int", "i", "1"));
        code.writeCode(JavaSrcCodeGenerator.codeBlock()).insertBeforeCodePoint(varcode).replaceCodePoint(JavaSrcCodeGenerator.switchStatement("#1",
                methodData.keySet()));
        for (Entry<Integer, ArrayList<String>> data : methodData.entrySet()) {
            ArrayList<String> prepareVars = data.getValue();
            for (int i = 0, n = prepareVars.size(); i < n; i++) {
                String fieldCall = prepareVars.get(i);
                if (fieldCall.matches("\\w+\\s*.\\s*\\w+\\(.*\\)")){
                    String fieldcode = JavaSrcCodeGenerator.replaceSpecialVars(fieldCall, null, null);
                    String pssetcall = JavaSrcCodeGenerator.endStatement(JavaSrcCodeGenerator.getPrepareCallFromType("Object"));
                    code.insertBeforeCodePoint(pssetcall).replaceCodePoint(incvarcode).replaceCodePoint(JavaSrcCodeGenerator.castObject("Object", fieldcode));
                } else {

                    CtField field = superClass.getField(fieldCall);
                    CtClass fieldType = field.getType();

                    if((field.getModifiers() & Modifier.PRIVATE) == Modifier.PRIVATE){ //en caso que el campo sea privado tenemos que emplear reflexion
                        String rfname = "r"+field.getName();
                        code.insertBeforeCodePoint("Field ").insertBeforeCodePoint(rfname).insertBeforeCodePoint(" = (java.lang.reflect.Field)rfields.get(\"").insertBeforeCodePoint(field.getName()).insertBeforeCodePoint("\");");
                        String fieldcode = JavaSrcCodeGenerator.callMethod(rfname, "get", "#0");
                        String pssetcall = JavaSrcCodeGenerator.endStatement(JavaSrcCodeGenerator.getPrepareCallFromType("Object"));
                        code.insertBeforeCodePoint(pssetcall).replaceCodePoint(incvarcode).replaceCodePoint(fieldcode);
                    } else {
                        String fieldcode = JavaSrcCodeGenerator.accessField("#0", field.getName());
                        if (fieldType.subclassOf(ctAbstractMap)){//FIXME
                            String valuescode = JavaSrcCodeGenerator.callMethod(fieldcode, "values");
                            String forcode = JavaSrcCodeGenerator.forEachStatement("Object", "o", valuescode);
                            code.insertBeforeCodePoint(forcode).replaceCodePoint(pssetobjectcall).replaceCodePoint(incvarcode).replaceCodePoint("o");
                        } else if ((fieldType.isArray() && !fieldType.getComponentType().subtypeOf(CtPrimitiveType.byteType)) || fieldType.subclassOf(ctAbstractCollection)){
                            String forcode = JavaSrcCodeGenerator.forEachStatement("Object", "o", fieldcode);
                            code.insertBeforeCodePoint(forcode).replaceCodePoint(pssetobjectcall).replaceCodePoint(incvarcode).replaceCodePoint("o");
                        } else {
                            String pssetcall = JavaSrcCodeGenerator.endStatement(JavaSrcCodeGenerator.getPrepareCallFromType(
                                    field.getType().getSimpleName()));
                            code.insertBeforeCodePoint(pssetcall).replaceCodePoint(incvarcode).replaceCodePoint(fieldcode);
                        }
                    }

                }
            }
            code.replaceCodePoint("");
        }
        code.replaceCodeVarsToMethodVars();
    }
    //mapa de <clase, tabla>
    private HashMap<String, String[]> classTables = new HashMap<String, String[]>(); 
    
    //mapa de <esquema, clase>
    private HashMap<String, String> classSchemas = new HashMap<String, String>();


    private static Pattern pat = Pattern.compile("(\\s*=\\s*)?\\$(\\w+(?:\\s*\\.\\s*\\w+\\s*\\(.*?\\))?|\\$\\d*)(\\s*=\\s*((\\w+\\.)?(\\w+)))?");
    private static Pattern pat1 = Pattern.compile("\\$(\\d*)");
    private static final String INTERFACE = ".IEntity_";
    private static final String IMPL = "_Impl";
//    private static final CtClass[] NO_ARGS = {};
    private static ClassPool cp = ClassPool.getDefault();
    private static Logger log = LoggerFactory.getLogger(DBEntityFactory.class);
    private static JavaSrcCodeGenerator code = JavaSrcCodeGenerator.get();
    private static StringBuffer tempcode = new StringBuffer();
    private static CtClass ctDBManager;
    private static CtClass ctIEntity;
    private static CtClass ctPreparedStatement;
    private static CtClass ctIEntityFactory;
    private static CtClass ctAbstractMap;
    private static CtClass ctAbstractCollection;
    private static CtClass ctResultSet;
    private static CtClass ctObject;
    private static CtClass ctString;
    private static CtClass ctSQLException;
    private static CtClass ctObjectArray;
    private static CtClass ctClass;
//    private static CtClass ctLogger;
    private static CtClass ctRegistrable;
    private static CtClass ctHashMap;
    private static final TextBuilder msn = TextBuilder.getSingletonInstance();
    static {
        try {
//            cp.importPackage("org.apache.log4j.Logger");
//            cp.importPackage("java.text.MessageFormat");
            cp.importPackage("java.util.Arrays");
            cp.importPackage("java.lang.reflect.Field");
            cp.importPackage("es.calipso.framework.storage.daotype.DAOEntityFactory");
//            cp.insertClassPath(new ClassClassPath(org.apache.log4j.Logger.class));
            cp.insertClassPath(new ClassClassPath(es.calipso.framework.storage.daotype.DAOEntity.class));
            cp.insertClassPath(new ClassClassPath(es.calipso.framework.storage.DBManager.class));
            ctAbstractMap = cp.get("java.util.AbstractMap");
            ctAbstractCollection = cp.get("java.util.AbstractCollection");
            ctHashMap = cp.get("java.util.HashMap");
            ctResultSet = cp.get("java.sql.ResultSet");
            ctPreparedStatement = cp.get("java.sql.PreparedStatement");
            ctObject = cp.get("java.lang.Object");
            ctString = cp.get("java.lang.String");
            ctObjectArray = cp.get("java.lang.Object[]");
            ctSQLException = cp.get("java.sql.SQLException");
            ctClass = cp.get("java.lang.Class");
//            ctLogger = cp.get("org.apache.log4j.Logger");
            ctIEntity = cp.get("es.calipso.framework.storage.daotype.DAOEntity");
            ctDBManager = cp.get("es.calipso.framework.storage.DBManager");
            ctIEntityFactory = cp.get("es.calipso.framework.storage.daotype.DAOEntityFactory");
            ctRegistrable = cp.get("es.calipso.framework.storage.daotype.Registrable");
        } catch (NotFoundException e) {
            log.error("", e);
        }
    }

    private static final DBEntityFactory theInstance = new DBEntityFactory();

}
