package es.calipso.framework.storage;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.UUID;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.hsqldb.cmdline.SqlFile;
import org.hsqldb.cmdline.SqlToolError;
import org.hsqldb.jdbc.Util;

import es.calipso.framework.storage.annotations.DBEntity;
import es.calipso.framework.storage.annotations.PK;
import es.calipso.framework.storage.daotype.DAOEntity;
import es.calipso.framework.storage.daotype.DAOEntityFactory;
import es.calipso.framework.storage.daotype.DAOSQLHandler;
import es.calipso.framework.storage.daotype.Registrable;
import es.calipso.framework.storage.exceptions.CannotRegisterException;
import es.calipso.framework.storage.exceptions.ClassNotRegisteredException;
import es.calipso.framework.storage.exceptions.EntityManagerNotSetException;
import es.calipso.framework.storage.exceptions.EntityNotCreatedException;
import es.calipso.framework.storage.exceptions.EntityNotFoundException;
import es.calipso.framework.text.TextBuilder;

public class DBManager {

    public static final String DEFAULTMANAGER = "DEFAULTMANAGER";

    public static DBManager newManager(String tag, DataSource d) {
        DBManager dbmanager = managers.get(tag);
        if (dbmanager == null){
            dbmanager = new DBManager(tag, d);
            managers.put(tag, dbmanager);
        }
        return dbmanager;
    }
    public void newEntityStore(InputStream sqlDatabaseDefinitionFile, InputStream...sqlDataFiles) throws IOException, SQLException {
        newEntityStore(ds, sqlDatabaseDefinitionFile, sqlDataFiles);
    }
    public static void newEntityStore(DataSource ds, InputStream sqlDatabaseDefinitionFile, InputStream...sqlDataFiles) throws IOException, SQLException {
        Connection con = null; 
        try {
            con = ds.getConnection();
            SqlFile fdef = new SqlFile(new InputStreamReader(sqlDatabaseDefinitionFile), "1", null, "UTF-8", false, null);
            
            fdef.setConnection(con);
            fdef.execute(); //setAutoclose y closeReader son por defecto true
            fdef.setAutoClose(true);
            fdef.closeReader();
            for (InputStream sqldatafile : sqlDataFiles){
                fdef = new SqlFile(new InputStreamReader(sqldatafile), "1", null, "UTF-8", false, null);
                fdef.setConnection(con);
                fdef.execute();
                fdef.setAutoClose(true);
                fdef.closeReader();
            }
        } catch (SqlToolError e) {
            throw Util.sqlException(e);
        } finally {
            clear(con, null, null);
        }
    }
    public static DBManager newManager(String tag, DataSource d, InputStream sqlDatabaseDefinitionFile, InputStream...sqlDataFiles) throws SQLException, IOException, URISyntaxException{
        DBManager dbmanager = newManager(tag, d);
        log.debug("creando un nuevo gestor de bd para {}", tag);
        newEntityStore(d, sqlDatabaseDefinitionFile, sqlDataFiles);
        return dbmanager;
    }
    public static DBManager getManager(String tag) throws EntityManagerNotSetException{
        DBManager dbManager = managers.get(tag);
        if (dbManager == null){
            dbManager = managers.get(DEFAULTMANAGER);
            if (dbManager == null) throw new EntityManagerNotSetException(tag);
        }
        return dbManager;
    }
    public static DBManager getManager(Class<?> ctag) throws EntityManagerNotSetException, ClassNotRegisteredException{
        DBManager dbManager = null;
        String tag = classAssociations.get(ctag);
        if (tag != null){
            dbManager = managers.get(tag);
            if (dbManager == null){
                dbManager = managers.get(DEFAULTMANAGER);
                if (dbManager == null) throw new EntityManagerNotSetException(tag);
            }            
        } else  throw new ClassNotRegisteredException(ctag);

        return dbManager;
    }

    public static <T> T load(Class<T> classT, Object...params) throws EntityNotFoundException, EntityManagerNotSetException, ClassNotRegisteredException {
        DBManager dbManager = getManager(classT);
        return dbManager.loadEntity(classT, params); 
    }
    public static <T> T load(T object) throws EntityNotFoundException, EntityManagerNotSetException, ClassNotRegisteredException {
        DBManager dbManager = getManager(object.getClass());
        return dbManager.loadEntity(object); 
    }
    private DBManager(String managerid, DataSource dataSource) {
        dbmanagertag  = managerid;
        ds = dataSource;
    }
    public void dropTypes() {
        daosfactories.clear();
        daoshandlers.clear();
        packagesSchemas.clear();
        entitiesByKeyCode.clear();
        index.clear();
    }
    public void registerType (Class<?> cl) throws CannotRegisterException{
        List<Class<?>> classes = getInnerAnnotatedClassesFrom(cl);

        try {
            for (Class<?> type : classes){
                if (!daosfactories.containsKey(type)){
                    ef.generate(type, ds, packagesSchemas, daosfactories, daoshandlers);
                    classAssociations.put(type, dbmanagertag);
                    index.put(cl, new ArrayList<Registrable<? extends DAOEntity>>());
                    log.debug("Despues de generar {}, la relación paquete-esquema es: {}", cl, packagesSchemas);
                }
            }
        } catch(Throwable t){
            log.error("", t);
            throw new CannotRegisterException(t);
        }
    }
    private List<Class<?>> getInnerAnnotatedClassesFrom(
            Class<?> cl) {
        List<Class<?>> innerClasses = new ArrayList<Class<?>>();
        innerClasses.add(cl);
        Class<?>[] declaredClasses = cl.getDeclaredClasses();
        for (Class<?> declaredClass : declaredClasses){
            if (declaredClass.isAnnotationPresent(DBEntity.class)){
                innerClasses.add(declaredClass);
            }
        }
        return innerClasses;
    }

    /**
     * Metodo que permite saber si una clase se registró en este dbmanager.
     * Para que el registro sea efectivo la clase debe marcarse como DBEntity.
     *  
     * @param clentity la clase a comprobar.
     * @return true si la clase se encuentra registrada o false en otro caso.
     */
    public boolean isRegistered(Class<?> clentity) {
        return daosfactories.containsKey(clentity);
    }
    /**
     * 
     * @param entityid
     * @return
     */
    public boolean isRegistered(Class<?> classt, Object...primarykey){
        return getKey(classt, primarykey) != null;
    }
/*
    public <T> T getEntity(int hashcode) throws EntityNotFoundException{

        String key = String.valueOf(hashcode);
        @SuppressWarnings("unchecked")
        T loadedEntity = (T) entitiesbyid.get(key);
        if (loadedEntity == null) throw new EntityNotFoundException(key);
        return loadedEntity;
    }
*/
    public <T> T getEntity(String keyCode) throws EntityNotFoundException{
        @SuppressWarnings("unchecked")
        T loadedEntity = (T) entitiesByKeyCode.get(UUID.fromString(keyCode));
        if (loadedEntity == null) throw new EntityNotFoundException(keyCode);
        return loadedEntity;
    }
    /**
     * Carga una entidad de la base de datos y la registra en la caché de
     * entidades asociada al DBManager.
     *  
     * @param classt tipo de la entidad a recuperar que debe extender el interfaz
     *             Registrable. El tipo debe poseer las anotaciones correspondientes.
     * @param primarykeyparams clave parametros para inicializar una instancia de la clase a 
     *                recuperar que contiene la información necesaria para identificarla
     *                en la DBManager.
     * @return la entidad registrada.
     * 
     * @throws EntityNotFoundException si entity no identificó a una entidad
     *                                  en la BD.
     * @throws ClassNotRegisteredException si la clase de la entidad no estaba
     *                                      registrada.
     *                                  
     */
    @SuppressWarnings("unchecked")
    public <T > T loadEntity(Class<T> classt, Object...primarykeyparams) throws EntityNotFoundException, ClassNotRegisteredException {
        if (!isRegistered(classt)) throw new ClassNotRegisteredException(classt);

        T loadedEntity = (T) entitiesByKeyCode.get(getKey(classt, primarykeyparams));
        if (loadedEntity == null){
            DAOEntityFactory fact = daosfactories.get(classt);
            DAOEntity daoEntity = fact.loadInstance(primarykeyparams);
            Registrable<? extends DAOEntity> r = daoEntity.getRegistrable();
            DAOSQLHandler daoHandler = daoshandlers.get(daoEntity.getClass());
            try {
                addKey(r);
                entitiesByKeyCode.put(r.getKey(), daoEntity);

                daoHandler.load(daoEntity);
                loadedEntity = (T) daoEntity;

            } catch (SQLException e) {
                log.error("", e);
            } finally {
                if (daoHandler.isError()){
                    if (r != null){
                        entitiesByKeyCode.remove(r.getKey());
                        removeKey(r);
                    }
                }
            }
        }
        return loadedEntity;

    }
    @SuppressWarnings("unchecked")
    public <T > T loadEntity(T object) throws EntityNotFoundException, ClassNotRegisteredException {
        Class<? extends Object> classt = object.getClass();
        if (!isRegistered(classt)) throw new ClassNotRegisteredException(classt);
        Object[] primarykey = getPrimaryKeyFromEntity(object);
        T loadedEntity = (T) entitiesByKeyCode.get(getKey(classt, primarykey));
        if (loadedEntity == null){
            DAOEntityFactory fact = daosfactories.get(classt);
            DAOEntity daoEntity = fact.newInstance(object);
            Registrable<? extends DAOEntity> r = daoEntity.getRegistrable();
            DAOSQLHandler daoHandler = daoshandlers.get(daoEntity.getClass());
            try {
                addKey(r);
                entitiesByKeyCode.put(r.getKey(), daoEntity);
                daoHandler.load(daoEntity);
                loadedEntity = (T) daoEntity;
            } catch (SQLException e) {
                log.error("", e);
            } finally {
                if (daoHandler.isError()){
                    if (r != null){
                        entitiesByKeyCode.remove(r.getKey());
                        removeKey(r);
                    }
                }
            }
        }
        return loadedEntity;

    }
    /**
     * Crea una entidad en la base de datos del tipo especificado.
     * 
     * @param <T> clase cuya instancia se va a almacenar en la bd, esta clase
     *             debe encontrarse anotada con @DBEntity.
     * @param entity entidad a crear.
     * 
     * @throws ClassNotRegisteredException si la clase no estaba registrada en
     *                                      el gestor actual. 
     * @throws EntityNotCreatedException si la entidad no pudo ser creada.
     */

    @SuppressWarnings("unchecked")
    public <T> T createEntity(T entity) throws ClassNotRegisteredException, EntityNotCreatedException {
        DAOEntity daoEntity = getDAO(entity);
        DAOSQLHandler daohandler = daoshandlers.get(daoEntity.getClass());
        try {
            daohandler.create(daoEntity);
            Registrable<? extends DAOEntity> r = daoEntity.getRegistrable();
            if (!entitiesByKeyCode.containsKey(r.getKey())){
                addKey(r);
                entitiesByKeyCode.put(r.getKey(), daoEntity);
            }
        } catch (SQLException e) {
            throw new EntityNotCreatedException(e); 
        } catch (EntityNotFoundException e) {
            log.error("", e);
        }
        return (T) daoEntity;
    }

    /**
     * Elimina una entidad de la base de datos.
     * 
     * @param entity entidad a eliminar.
     * @throws EntityNotFoundException 
     * @throws ClassNotRegisteredException si la clase no se registro en este dbmanager.
     */
    public <T > void deleteEntity(T entity) throws EntityNotFoundException, ClassNotRegisteredException {
        DAOEntity daoEntity = getDAO(entity);

        DAOSQLHandler daohandler = daoshandlers.get(daoEntity.getClass());
        try {
            daohandler.remove(daoEntity);
            Registrable<? extends DAOEntity> r = daoEntity.getRegistrable();
            entitiesByKeyCode.remove(r.getKey());
            removeKey(r);
        } catch (SQLException e) {
            log.error("", e);
        }
    }

    /**
     * Ejecuta una consulta que recupera un conjunto de datos de la base de 
     * datos asociada.
     *  
     * @param query consulta a ejecutar.
     * @param data parametros de la consulta.
     * @return una colección de arrays de objetos que son los resultados de
     *          la consulta ejecutada. Cada array tiene la misma longitud que
     *          el número de campos recuperado. o null
     * @throws SQLException si hay un problema en la consulta.
     * @throws ArrayIndexOutOfBoundsException si data es de menor longitud que
     *          el número de parámetros de la consulta.
     */
    public Collection<Object[]> retrieveSetFromQuery (String query, Object... data) throws SQLException, ArrayIndexOutOfBoundsException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Object[]> entitiesSet = new ArrayList<Object[]>();
        try {
            con = ds.getConnection();
            ps = con.prepareStatement(query);
            ParameterMetaData parameterMetaData = ps.getParameterMetaData();
            int parameterCount = parameterMetaData.getParameterCount();
            for (int i = 0; i < parameterCount; i++){
                ps.setObject(i+1, data[i]);
            }
            rs = ps.executeQuery();
            ResultSetMetaData resultSetMetaData = rs.getMetaData();
            int columnCount = resultSetMetaData.getColumnCount();
            Object[] o;
            while (rs.next()){
                o = new Object[columnCount];
                for (int i = 0; i < columnCount; i++){
                    o[i] = rs.getObject(i+1);
                    if (resultSetMetaData.getColumnType(i+1) == java.sql.Types.BLOB){
                        Blob b = ((Blob)o[i]);
                        log.debug("tamaño del blob: {}", b.length());
                        try {
                            o[i] = b.getBytes(1, (int)b.length());
                        } catch (Throwable e){
                            log.warn("", e);
                        }
                    }
                }
                entitiesSet.add(o);
            }
        } finally {
            clear(con, ps, rs);
        }
        return entitiesSet;
    }
    @SuppressWarnings("unchecked")
    public <T> Collection<T> retrieveDataFromQuery (String query, Object... data) throws SQLException, ArrayIndexOutOfBoundsException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<T> entitiesSet = new ArrayList<T>();
        try {
            con = ds.getConnection();
            ps = con.prepareStatement(query);
            ParameterMetaData parameterMetaData = ps.getParameterMetaData();
            int parameterCount = parameterMetaData.getParameterCount();
            for (int i = 0; i < parameterCount; i++){
                ps.setObject(i+1, data[i]);
            }
            rs = ps.executeQuery();
            ResultSetMetaData resultSetMetaData = rs.getMetaData();
            int columnCount = resultSetMetaData.getColumnCount();
            while (rs.next()){
                for (int i = 0; i < columnCount; i++){
                    entitiesSet.add((T)rs.getObject(i+1));
                }
            }
        } finally {
            clear(con, ps, rs);
        }
        return entitiesSet;
    }
    /**
     * Cierra la base de datos asociada al dbmanager y elimina los tipos registrados. 
     */
    public void close() {
        dropTypes();
        Connection con = null;
        try {
            con = ds.getConnection();
            Statement shutdownStatement = con.createStatement();
            shutdownStatement.execute("SHUTDOWN");
            log.info("Closing database {}", dbmanagertag);
        } catch (SQLException e) {
            log.error("", e);
        } finally {
            clear(con, null, null);
        }
         
    }    
    private static void clear(Connection con, PreparedStatement ps, ResultSet rs) {
        if (rs != null){
            try {
                rs.close();
            } catch (SQLException e){}
        }
        if (ps != null){
            try {
                ps.close();
            } catch (SQLException e){}
        }
        if (con != null){
            try {
                con.close();
            } catch (SQLException e){}
        }
    }

    public void executeQuery(String query, Object... data) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = ds.getConnection();
            ps = con.prepareStatement(query);
            ParameterMetaData parameterMetaData = ps.getParameterMetaData();
            int parameterCount = parameterMetaData.getParameterCount();
            for (int i = 0; i < parameterCount; i++){
                ps.setObject(i+1, data[i]);
            }
            ps.executeUpdate();
        } finally {
            clear(con, ps, null);
        }
    }

    private <T > DAOEntity getDAO(T entity) throws ClassNotRegisteredException{
        if (DAOEntity.class.isAssignableFrom(entity.getClass())) return (DAOEntity)entity;

        Class<?> clentity = entity.getClass();
        if (!isRegistered(clentity)) throw new ClassNotRegisteredException(clentity);
        Object[] primarykey = getPrimaryKeyFromEntity(entity);
        DAOEntity loadedEntity = entitiesByKeyCode.get(getKey(clentity, primarykey));
        log.debug("Clave primaria {}, entidad DAO leida {}", Arrays.toString(primarykey), loadedEntity);
        if (loadedEntity == null){
            DAOEntityFactory fact = daosfactories.get(clentity);
            loadedEntity = fact.newInstance(entity);
        }
        return loadedEntity;
    }
    private static <T> Object[] getPrimaryKeyFromEntity(T entity){
        int chunk = 0;
        TreeMap<Integer,Object> param = new TreeMap<Integer, Object>();
        Class<? extends Object> classt = entity.getClass();
        if (DAOEntity.class.isAssignableFrom(classt)){
            return ((DAOEntity)entity).getPrimaryKey();
        }
        Field[] declaredFields = DAOEntityFactory.getAllDeclaredFields(classt);
        for (Field f : declaredFields){
            if (f.isAnnotationPresent(PK.class)){
                PK pk = f.getAnnotation(PK.class);
                try {
                    param.put(chunk + pk.value(), f.get(entity));
                } catch (IllegalArgumentException e) {
                    log.error("", e);
                } catch (IllegalAccessException e) {
                    log.error("", e);
                } finally {
                    if (classt != f.getDeclaringClass()){
                        classt = f.getDeclaringClass();
                        chunk += 10000;
                    }
                }
            }
        }
        return param.values().toArray();
    }
    private UUID getKey(Class<?>cl, Object[]params){
        Registrable<? extends DAOEntity> key = null;
        ArrayList<Registrable<? extends DAOEntity>> list = index.get(cl);
        if (list != null){
            for (Registrable<? extends DAOEntity> r : list){//... esto es horrible ... 
                if (Arrays.deepEquals(params, r.getPrimaryKey())){
                    key = r;
                    break;
                }
            }
        }
        return (key != null) ? key.getKey() : null;
    }
    private void addKey(Registrable<? extends DAOEntity> registrable) {
        Class<?> cl = registrable.getSuperClass();
        ArrayList<Registrable<? extends DAOEntity>> list = index.get(cl);
        if (list == null){
            return;
        }
        list.add(registrable);
    }
    private void removeKey(Registrable<? extends DAOEntity> r){
        ArrayList<Registrable<? extends DAOEntity>> list = index.get(r.getSuperClass());
        if (list != null){
            list.remove(r);
        }
    }

    /*
     * Aquí se almacenan los Registrables de cada instancia de DAO. Estos 
     * objetos permiten calcular un identificador unívoco para un objeto
     * de ese tipo.
     * 
     * Se separa el objeto que almacena el DBManager de la entidad en la base
     * de datos para no depender de posibles cambios en la clave primaria de 
     * dicha entidad.
     */
    private HashMap<Class<?>, ArrayList<Registrable<? extends DAOEntity>>> index = new HashMap<Class<?>, ArrayList<Registrable<? extends DAOEntity>>>();
    
    private static DBEntityFactory ef = DBEntityFactory.getInstance();

    private static HashMap<String, String> packagesSchemas = new HashMap<String, String>();
    
    private static HashMap<String, DBManager> managers = new HashMap<String, DBManager>();

    //almacena las asociaciones clase-dbmanager que la gestiona
    private static HashMap<Class<?>, String> classAssociations = new HashMap<Class<?>, String>();

    private String dbmanagertag = null;

    private HashMap<Class<?>, DAOEntityFactory> daosfactories = new HashMap<Class<?>, DAOEntityFactory>();
    
    private HashMap<Class<?>, DAOSQLHandler> daoshandlers = new HashMap<Class<?>, DAOSQLHandler>();
    
    private HashMap<UUID, DAOEntity> entitiesByKeyCode = new HashMap<UUID, DAOEntity>();

    private DataSource ds;
    
    private static final Logger log = LoggerFactory.getLogger(DBManager.class);
    
    static {
        TextBuilder.load(DBManager.class.getResourceAsStream("/dbmessages/logmessages"));
    }
}
