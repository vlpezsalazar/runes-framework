package es.calipso.framework.storage.exceptions;

import es.calipso.framework.text.TextBuilder;


public class ClassNotRegisteredException extends Exception {

    public ClassNotRegisteredException(Class<?> classt) {
        super(m.get("CLASSNOTREGISTEREDEXCEPTION", classt.getCanonicalName()));
    }

    private static final long serialVersionUID = 4048383760710029345L;
    private static final TextBuilder m = TextBuilder.getSingletonInstance();
}
