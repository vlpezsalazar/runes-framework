package es.calipso.framework.storage.exceptions;

import java.sql.SQLException;

public class EntityNotCreatedException extends Exception {

    public EntityNotCreatedException(SQLException e) {
        super(e);
    }

    /**
     * 
     */
    private static final long serialVersionUID = -4608969217935959593L;

}
