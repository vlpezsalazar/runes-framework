package es.calipso.framework.storage.exceptions;

public class EntityManagerNotSetException extends Exception {

    public EntityManagerNotSetException(String tag) {
        super(tag);
    }

    private static final long serialVersionUID = -1542958861143048822L;

}
