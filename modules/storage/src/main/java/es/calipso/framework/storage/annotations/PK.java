package es.calipso.framework.storage.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Primary Key annotation.
 * 
 * @author Víctor López Salazar
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface PK {
    public int value() default 0;
}
