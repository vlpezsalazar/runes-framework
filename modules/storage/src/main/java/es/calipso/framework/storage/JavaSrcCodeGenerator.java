package es.calipso.framework.storage;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import javassist.CtField;
import javassist.NotFoundException;

class JavaSrcCodeGenerator {
    public static String declareVar(String type, String name, String init) {
        return tempcode1.delete(0, tempcode1.length()).append(type).append(" ").append(name).append(" = ").append(init).toString();
    }
    public static String incVar(String name) {
        return tempcode1.delete(0, tempcode1.length()).append(name).append("++").toString();
    }
    public static String forEachStatement(String type, String varname, String set) {
        return tempcode1.delete(0, tempcode1.length()).append(newline).append("for (").append(type).append(" ").append(varname).append(" : ").append(set).append("){").append(CODEPOINT).append("}").toString();
    }
    public static String castObject(String classcast, String object) {
        return tempcode1.delete(0, tempcode1.length()).append("((").append(classcast).append(")").append(object).append(")").toString();
    }

    public static String switchStatement(String switchvar, @SuppressWarnings("rawtypes") List casevars){
        tempcode1.delete(0, tempcode1.length());
        tempcode1.append(newline).append("switch(").append(switchvar).append("){");
        for (Object o : casevars){
            tempcode1.append(newline).append("case ").append(o).append(": {").append(newline).append(CODEPOINT).append(newline).append("}").append(newline).append("break;");
        }
        tempcode1.append(newline).append("}");
        return tempcode1.toString();
    }
    public static String switchStatement(String switchvar, @SuppressWarnings("rawtypes") Set casevars) {
        tempcode1.delete(0, tempcode1.length());
        tempcode1.append(newline).append("switch(").append(switchvar).append("){");
        for (Object o : casevars){
            tempcode1.append(newline).append("case ").append(o).append(": {").append(newline).append(CODEPOINT).append(newline).append("}").append(newline).append("break;");
        }
        tempcode1.append(newline).append("}");
        return tempcode1.toString();
    }
    public static String codeBlock(){
        return  tempcode1.delete(0, tempcode1.length()).append("{").append(CODEPOINT).append("}").toString();
    }
    public static String throwStatement(String exceptionType, String message) {
        return tempcode1.delete(0, tempcode1.length()).append("throw new ").append(exceptionType).append("(\"").append(message).append("\");").toString();
    }
    public static String ifStatement(String condition, String body){
        return  tempcode1.delete(0, tempcode1.length()).append("if (").append(condition).append(")").append("{").append(body).append("}").toString();
    }
    public static String elseifStatement(){
        return tempcode1.delete(0, tempcode1.length()).append("else if (").append(CODEPOINT).append(")").toString();
    }
    public static String elseStatement(){
        return tempcode1.delete(0, tempcode1.length()).append("else (").append(CODEPOINT).append(")").toString();
    }
    public static String returnStatement(String returnObject) {

        return tempcode1.delete(0, tempcode1.length()).append("return ").append(returnObject).append(";").append(newline).toString();
    }
    public static String initField(String fieldcode, String initcode){
        return tempcode1.delete(0, tempcode1.length()).append(fieldcode).append(" = ").append(initcode).toString();
    }
    public static String accessField(String object, String field){
        return tempcode1.delete(0, tempcode1.length()).append(object).append(".").append(field).toString();
    }
    public static String accessField(String classcast, String object, String field){
        return tempcode1.delete(0, tempcode1.length()).append("((").append(classcast).append(")").append(object).append(").").append(field).toString();
    }
    public static String callMethod(String object, String methodName){
        return tempcode1.delete(0, tempcode1.length()).append(object).append(".").append(methodName).append("()").toString();
    }
    public static String newObject(String type, Object...params) {
        tempcode1.delete(0, tempcode1.length()).append(" new ").append(type).append("(");
        if (params != null && params.length > 0){
            tempcode1.append(params[0]);
            for (int i = 1, n = params.length; i<n; i++) {
                tempcode1.append(",").append(params[i]);
            }
        }
        return tempcode1.append(")").toString();
    }
    public static String newArray(String type, List<Object> params) {
        tempcode1.delete(0, tempcode1.length()).append(" new ").append(type).append("[]{(#w)");
        if (params != null && !params.isEmpty()){
            tempcode1.append(params.get(0));
            params = params.subList(1, params.size());
            for (Object param : params){
                tempcode1.append(",(#w)").append(param);
            }
        }
        return tempcode1.append("}").toString();
    }
    private static String callMethodWithUnboundParams(String object, String methodName, int n) {
        tempcode1.delete(0, tempcode1.length()).append(object).append(".").append(methodName).append("(");
        for (int i = 0; i<n; i++){
            tempcode1.append(CODEPOINT);
            if (i<n-1) tempcode1.append(", ");
        }
        return tempcode1.append(")").toString();
    }
    public static String callMethod(String object, String methodName, List<Object> params){
        tempcode1.delete(0, tempcode1.length()).append(object).append(".").append(methodName).append("(");
        if (params != null && !params.isEmpty()){
            tempcode1.append(params.get(0));
            params = params.subList(1, params.size());
            for (Object param : params){
                tempcode1.append(",").append(param);
            }
        }
        return tempcode1.append(")").toString();
    }
    public static String callMethod(String object, String methodName, Object... params) {
        tempcode1.delete(0, tempcode1.length()).append(object).append(".").append(methodName).append("(");
        if (params != null && params.length > 0){
            tempcode1.append(params[0]);
            for (int i = 1, n = params.length; i<n; i++) {
                tempcode1.append(",").append(params[i]);
            }
        }
        return tempcode1.append(")").toString();
    }
    public static String registerInDBManagerBlock(String fieldType, String varname, String initParam) throws NotFoundException{
        String calldbmanagerload;
        if (initParam.matches("\\s*new.+")){
            calldbmanagerload = endStatement(declareVar(fieldType, varname, JavaSrcCodeGenerator.callMethod(GLOBALDBMANAGER, "load", initParam)));
        } else {
            calldbmanagerload = endStatement(declareVar(fieldType, varname, callMethod(GLOBALDBMANAGER, "load", JavaSrcCodeGenerator.getDotClassOfType(fieldType), JavaSrcCodeGenerator.newArray("Object", Arrays.asList((Object[])initParam.split(","))))));
        }
        return calldbmanagerload;
    }
    public static String getDotClassOfType(String type){
        tempcode1.delete(0, tempcode1.length()).append(type).append(".class");
        return tempcode1.toString();
    }
    public JavaSrcCodeGenerator writeCode(String code) {
        codebuf.delete(0, codebuf.length()).append(code);
        return this;
    }
    public JavaSrcCodeGenerator insertCode(String code){
        codebuf.insert(0, code);
        return this;
    }
    public JavaSrcCodeGenerator appendCode(String code) {
        codebuf.append(code);
        return this;
    }
    public JavaSrcCodeGenerator endStatement(){
        codebuf.append(";").append(newline);
        return this;
    }
    public JavaSrcCodeGenerator insertBeforeCodePoint (String code){
        tempcode.delete(0, tempcode.length());
        tempcode1.delete(0, tempcode1.length()).append(Matcher.quoteReplacement(code)).append(CODEPOINT);
        Matcher m = pcodepoint.matcher(codebuf);
//        log.debug(MessageFormat.format("code to modify: {0}\ncode to insert: {1}", codebuf.toString(), tempcode1.toString()));
        if (m.find()){
            m.appendReplacement(tempcode, tempcode1.toString());
        }
        m.appendTail(tempcode);
        codebuf.replace(0, codebuf.length(), tempcode.toString());
        return this;
    }
    public JavaSrcCodeGenerator replaceCodePoint(String code){
        tempcode.delete(0, tempcode.length());
        Matcher m = pcodepoint.matcher(codebuf);

        if (m.find()){
            m.appendReplacement(tempcode, Matcher.quoteReplacement(code));
        }
        m.appendTail(tempcode);
        codebuf.replace(0, codebuf.length(), tempcode.toString());
        return this;
    }
    public JavaSrcCodeGenerator appendAfterCodePoint(String code){
        tempcode.delete(0, tempcode.length());
        tempcode1.delete(0, tempcode1.length()).append(CODEPOINT).append(code);
        Matcher m = pcodepoint.matcher(codebuf);

        if (m.find()){
            m.appendReplacement(tempcode, tempcode1.toString());
        }
        m.appendTail(tempcode);
        codebuf.replace(0, codebuf.length(), tempcode.toString());
        return this;
    }
    public static String replaceSpecialVars(String code, CtField presentField, String column) throws NotFoundException{
        if (code == null || code.isEmpty()) return ""; 
        StringBuffer tempcode = new StringBuffer();
        Matcher m= p.matcher(code.replaceAll("(\\w+)@keycode", "((es.calipso.framework.storage.daotype.KeyCode)$1).get()"));
//        log.debug(MessageFormat.format("Reemplazando variables de {0}", code));
        while (m.find()){
            String type = m.group(1);
            String v = m.group(2);
            if (type.equals("#")){
                v = tempcode1.delete(0, tempcode1.length()).append("#2.getObject(\"").append(v).append("\")").toString();
            } else if (type.equals("$")){
                if (v.equals("rs")){
                    try {
                        v = getPopulateCallFromType(presentField.getType().getSimpleName(), column);
                    } catch (NotFoundException e) {}
                } else if (v.equals("rs.")){
                    v = "#2.";
                } else if (v.equals("this")){
                    v = "super";
                } else {
                    v = accessField("#0", (v.matches("\\$")) ? presentField.getName() : v);
                }
            }
            if (v == null){
                v = CODEERROR;
            }
//            log.debug(MessageFormat.format("Reemplazando {0} por {1}", m.group(1), v));
            m.appendReplacement(tempcode, v);
        }
        m.appendTail(tempcode);
        m = pself.matcher(tempcode.subSequence(0, tempcode.length()));
        if (m.find()){
            tempcode.delete(0, tempcode.length());
            do {
//              log.debug(MessageFormat.format("Match {0}, group(1) = {1}, group(2) = {2}, group(3) ={3}, group(4) = {4}, group(5) = {5}", m.group(), m.group(1), m.group(2), m.group(3), m.group(4), m.group(5)));
                String initCode = (m.group(4) == null) ? m.group(5) : m.group(4);
                String v = registerInDBManagerBlock(m.group(1), m.group(2), initCode);
//                log.debug(MessageFormat.format("Reemplazando {0} por {1}", m.group(1), v));
                m.appendReplacement(tempcode, v);
            } while (m.find());
            m.appendTail(tempcode);
        }
        return tempcode.toString();
    }
    public static String getPrepareCallFromType(String type){
        String call = null; 
        tempcode1.delete(0, tempcode1.length());
        type = (resolveTypes.containsKey(type)) ? resolveTypes.get(type) : type;
        call = JavaSrcCodeGenerator.callMethodWithUnboundParams("#2", tempcode1.append("set").append(type).toString(), 2);
        return call;
    }
    public static String getPopulateCallFromType(String type, String columnName){
        String call = null; 
        tempcode1.delete(0, tempcode1.length());
        type = (resolveTypes.containsKey(type)) ? resolveTypes.get(type) : type;
        String methodName = tempcode1.append("get").append(type).toString();
        tempcode1.delete(0, tempcode1.length());
        columnName = tempcode1.append("\"").append(columnName).append("\"").toString();
        call = JavaSrcCodeGenerator.callMethod("#2", methodName, columnName);
        return call;
    }
    
    public static String endStatement(String statement){
        tempcode1.delete(0, tempcode1.length());
        tempcode1.append(statement).append(";");
        return tempcode1.toString(); 
    }
    public JavaSrcCodeGenerator replaceCodeVarsToMethodVars(){
        String replacement = codebuf.toString().replaceAll("#", "\\$");
        codebuf.replace(0, codebuf.length(), replacement);
        return this;

    }
    public String flush(){
        String s = codebuf.toString();
        codebuf.delete(0, codebuf.length());
        return s;
    }
    @Override
    public String toString(){
        return codebuf.toString();
    }
    public static JavaSrcCodeGenerator get(){
        return theInstance;
    }
    private StringBuilder codebuf = new StringBuilder();
    public static final JavaSrcCodeGenerator theInstance = new JavaSrcCodeGenerator();
    public static final int PREPAREOPERATION = 0;
    public static final int POPULATEOPERATION = 1;
    static final String CODEPOINT = "#\"codepoint\"#";
    static final String LOCALDBMANAGERFIELDNAME = "dbManager";
    static final String GLOBALDBMANAGER = "es.calipso.framework.storage.DBManager";
    private static final String CODEERROR = "#\"codeerror\"#";
    private static StringBuilder tempcode1 = new StringBuilder();
    private static StringBuffer tempcode = new StringBuffer();
    private static Pattern pcodepoint = Pattern.compile(CODEPOINT);
    private static Pattern p = Pattern.compile("(#|\\$)(rs\\.?|\\$|this|\\w+)", Pattern.MULTILINE);
    private static Pattern pself = Pattern.compile("(\\w+(?:\\.\\w+)+)\\s+(\\w+)(@self)\\s*(?:\\((.+?)\\)\\s*;|=\\s*(.+?)\\s*;)", Pattern.MULTILINE);
//    private static Logger log = Logger.getLogger(JavaSrcCodeGenerator.class);
    private static final String newline = System.getProperty("line.separator");
    private static final HashMap<String, String> resolveTypes = new HashMap<String, String>();
//    private static final Logger log = Logger.getLogger(JavaSrcCodeGenerator.class);

    static {
        resolveTypes.put("Integer", "Int");
        resolveTypes.put("byte[]", "Bytes");
        resolveTypes.put("int", "Int");
        resolveTypes.put("char", "Char");
        resolveTypes.put("short", "Short");
        resolveTypes.put("char", "Char");
        resolveTypes.put("long", "Long");
    }
}
