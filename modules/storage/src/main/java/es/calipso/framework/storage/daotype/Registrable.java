package es.calipso.framework.storage.daotype;

import java.io.Serializable;
import java.util.UUID;

public class Registrable<T extends DAOEntity> implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -868788360739698971L;
    public Registrable(T dao){
        this.dao = dao;
    }
    public void setDAO(T dao){
        this.dao = dao;
    }
    public Object[] getPrimaryKey(){
        return dao.getPrimaryKey();
    }
    public Class<?> getSuperClass() {
        return dao.getSuperClass();
    }
    public UUID getKey(){
        return uuid;
    }

    private T dao;
    private final UUID uuid = UUID.randomUUID();

}
