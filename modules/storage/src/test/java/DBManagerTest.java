import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Properties;
import javax.sql.DataSource;


import org.hsqldb.cmdline.SqlFile;
import org.hsqldb.jdbc.JDBCDataSourceFactory;
import org.junit.Before;
import org.junit.Test;

import es.calipso.framework.storage.DBManager;
import es.calipso.framework.storage.daotype.KeyCode;
import es.calipso.framework.storage.exceptions.CannotRegisterException;
import es.calipso.framework.storage.exceptions.ClassNotRegisteredException;
import es.calipso.framework.storage.exceptions.EntityManagerNotSetException;
import es.calipso.framework.storage.exceptions.EntityNotCreatedException;
import es.calipso.framework.storage.exceptions.EntityNotFoundException;
import es.calipso.framework.storage.test.dao1.BerserkerGameDescriptor;
import es.calipso.framework.storage.test.dao1.SkillTree;
import es.calipso.framework.storage.test.dao1.SkillTreeNode;
import es.calipso.framework.storage.test.dao2.User;

public class DBManagerTest {

    private static final String TESTMANAGER = "TESTMANAGER";
    private static final String TESTMANAGER1 = "TESTMANAGER1";
    private static final String TESTDAO1DB = "/tablesdao1.ddl.sql";
    private static final String TESTDAO2DB = "/tablesdao2.ddl.sql";

    public DBManagerTest() throws Exception {
        Properties p = new Properties();
        p.put("user", "dev.berserker");
        p.put("password", "develop-calypso");
        p.put("url", "jdbc:hsqldb:mem:test");

        ds = JDBCDataSourceFactory.createDataSource(p);
        Connection con = ds.getConnection();
        SqlFile f = new SqlFile(new File(System.class.getResource(TESTDAO2DB).toURI()));  
        f.setConnection(con);
        f.setAutoClose(true);
        f.execute();
        f.closeReader();
        f = new SqlFile(new File(System.class.getResource(TESTDAO1DB).toURI()));
        f.setConnection(con);
        f.setAutoClose(true);
        f.execute();
        f.closeReader();
        
        DBManager.newManager(TESTMANAGER, ds);
    }
    @Before
    public void cleanup() throws EntityManagerNotSetException {
        DBManager manager = DBManager.getManager(TESTMANAGER);
        manager.dropTypes();
    }
    @Test
    public void testGetManagerString() throws EntityManagerNotSetException {
        DBManager.getManager(TESTMANAGER);
    }
    @Test
    public void testNewManager() throws EntityManagerNotSetException {
        DBManager.newManager(TESTMANAGER1, ds);
        DBManager.getManager(TESTMANAGER1);
    }
    @Test
    public void testRegisterType() throws EntityManagerNotSetException, CannotRegisterException {
        DBManager dbmanager = DBManager.getManager(TESTMANAGER);
        dbmanager.registerType(BerserkerGameDescriptor.class);
        assertFalse(dbmanager.isRegistered(SkillTreeNode.class));
        assertTrue(dbmanager.isRegistered(BerserkerGameDescriptor.class));
        
    }

    
    @Test
    public void testGetManagerClassOfQ() throws EntityManagerNotSetException, CannotRegisterException, ClassNotRegisteredException {
        DBManager manager = DBManager.getManager(TESTMANAGER);
        manager.registerType(BerserkerGameDescriptor.class);
        DBManager manager2 = DBManager.getManager(BerserkerGameDescriptor.class);
        assertSame(manager, manager2);
    }

    @Test
    public void testIsRegisteredClass() throws EntityManagerNotSetException, CannotRegisterException {
        DBManager manager = DBManager.getManager(TESTMANAGER);
        assertFalse(manager.isRegistered(BerserkerGameDescriptor.class));
        manager.registerType(BerserkerGameDescriptor.class);       
        assertTrue(manager.isRegistered(BerserkerGameDescriptor.class));
    }

    @Test
    public void testLoadT() throws EntityManagerNotSetException, EntityNotFoundException, ClassNotRegisteredException, SQLException, CannotRegisterException {
        String[] checkedRules = new String[]{"rule1", "rule2", "rule3"};
        Integer[] checkedZones = new Integer[]{0, 1, 2, 3, 4};
        String[] checkedGEHosts = new String[]{"host1", "host2", "host3", "host41"};
        HashMap<String, HashMap<String, Object>> checkedClientRules = new HashMap<String, HashMap<String, Object>>();
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("gameRuleFunction", "attack");
        data.put("serverNeedsToKnow", true);
        data.put("loop", true);
        checkedClientRules.put("rule1", data);
        data = new HashMap<String, Object>();
        data.put("gameRuleFunction", "iii");
        data.put("serverNeedsToKnow", false);
        data.put("loop", true);
        checkedClientRules.put("rule2", data);
        data = new HashMap<String, Object>();
        data.put("gameRuleFunction", "ooo");
        data.put("serverNeedsToKnow", true);
        data.put("loop", false);
        checkedClientRules.put("rule3", data);
        
        createUser(0, "testNick", "test", "test");
        try {
            String bgdname = BerserkerGameDescriptor.class.getCanonicalName();
            String bgname = "es.calipso.games.berserker.BerserkerGame";
            createZones(checkedZones);
            createDescriptor(0, bgdname, bgname,
                    checkedRules, checkedZones, checkedGEHosts, checkedClientRules);

            DBManager manager = DBManager.getManager(TESTMANAGER);
            manager.registerType(BerserkerGameDescriptor.class);
            BerserkerGameDescriptor bgd = DBManager.load(new BerserkerGameDescriptor(0));
            manager.registerType(User.class);
            User us = DBManager.load(new User("test", "test"));

            assertEquals(0, us.getId());
            assertEquals("test", us.getLogin());
            assertEquals("test", us.getPassword());
            assertEquals("testNick", us.getNickname());
            assertEquals(false, us.isPlaying());

            assertEquals(0, bgd.getId());
            assertEquals(bgdname, bgd.getDescriptorClassName());

            assertEquals(bgname, bgd.getGameFactory());
            
            Object[] serverRules = bgd.getServerRules().toArray();
            Arrays.sort(serverRules);
            assertArrayEquals(checkedRules, serverRules);
            
            Object[] masterGEHosts = bgd.getMasterGEHosts().toArray();
            Arrays.sort(masterGEHosts);
            assertArrayEquals(checkedGEHosts, masterGEHosts);

            Object[] gameZones = bgd.getGameZones().toArray();
            Arrays.sort(gameZones);
            assertArrayEquals(checkedZones, gameZones);
            
            HashMap<String,HashMap<String,Object>> clientRules = bgd.getClientRules();
            for (Entry<String, HashMap<String, Object>> clrule : clientRules.entrySet()){
                HashMap<String, Object> clruledata = clrule.getValue();
                for (Entry<String, Object> data1 :  clruledata.entrySet()){
                    assertEquals(MessageFormat.format("checking {0}.{1}", clrule.getKey(), data1.getKey()), checkedClientRules.get(clrule.getKey()).get(data1.getKey()), data1.getValue());                    
                }
            }

        } finally {
            deleteUser(0);
            deleteZones(checkedZones);
            deleteDescriptor(0);
        }
    }

    @Test
    public void testLoadClassOfTInt() throws EntityManagerNotSetException, EntityNotFoundException, ClassNotRegisteredException, SQLException, CannotRegisterException {
        createSkillTree();
        try {
            DBManager manager = DBManager.getManager(TESTMANAGER);
            manager.registerType(SkillTree.class);
            manager.registerType(SkillTreeNode.class);
            
            SkillTree loadedSkillTree = DBManager.load(SkillTree.class, 0);
            SkillTree loadedSkillTree1 = DBManager.load(SkillTree.class, 1);
            SkillTree loadedSkillTree2 = DBManager.load(SkillTree.class, 2);
            
            SkillTree checkedSkillTree = new SkillTree(0);
            checkedSkillTree.addChildSkill(0, "ARR", "walk", 0, 0).addChildSkill(1, "ARR", "run", 0, 200);
            checkedSkillTree.addChildSkill(2, "ABA", "crouch", 0, 0);
            checkedSkillTree.addChildSkill(3, "IZQ", "strafe_left", 0, 0).addChildSkill(4, "IZQ", "jump_left", 0, 200);
            checkedSkillTree.addChildSkill(5, "DER", "strafe_right", 0, 0).addChildSkill(6, "DER", "jump_right", 0, 200);
            checkedSkillTree.addChildSkill(8, "SAL", "jump", 0, 0);

            SkillTree checkedSkillTree1 = new SkillTree(1);
            checkedSkillTree1.addChildSkill(7, "ATA", "attack", 0, 0);
            SkillTree checkedSkillTree2 = new SkillTree(2);
            checkedSkillTree2.addChildSkill(9, "SOC", "salute", 0, 0).addChildSkill(10, "SOC", "salute1", 0, 200);

            assertEquals(checkedSkillTree, loadedSkillTree);
            assertEquals(checkedSkillTree1, loadedSkillTree1);
            assertEquals(checkedSkillTree2, loadedSkillTree2);
        } finally {
            deleteSkillTree();
        }
    }

    @Test
    public void testIsRegisteredPrimaryKey() throws EntityManagerNotSetException, EntityNotFoundException, ClassNotRegisteredException, SQLException, CannotRegisterException {
        createUser(0, "testNick", "test", "test");
        try{
            DBManager manager = DBManager.getManager(TESTMANAGER);
            manager.registerType(User.class);
            User us = manager.loadEntity(new User("test", "test"));
            us.setLogin("loginChanged");
            assertFalse(manager.isRegistered(User.class, "test", "test".getBytes()));
            assertTrue(manager.isRegistered(User.class, "loginChanged", "test".getBytes()));
        } finally{
            deleteUser(0);
        }
    }
 
    @Test
    public void testLoadEntityT() throws EntityManagerNotSetException, EntityNotFoundException, ClassNotRegisteredException, SQLException, CannotRegisterException {
        createUser(0, "testNick", "test", "test");
        try {
            DBManager manager = DBManager.getManager(TESTMANAGER);
            manager.registerType(User.class);
            User us = manager.loadEntity(new User("test", "test"));
    
            assertEquals(0, us.getId());
            assertEquals("test", us.getLogin());
            assertEquals("testNick", us.getNickname());
            assertEquals(false, us.isPlaying());
        } finally {
            deleteUser(0);
        }
    }
    @Test
    public void testGetEntity() throws EntityManagerNotSetException, EntityNotFoundException, ClassNotRegisteredException, SQLException, CannotRegisterException {
        createUser(0, "testNick", "test", "secret");
        createUser(1, "testNick1", "test1", "secreta");
        createUser(2, "testNick2", "test2", "secretb");
        try {
            DBManager manager = DBManager.getManager(TESTMANAGER);
            manager.registerType(User.class);
            
            User us = manager.loadEntity(User.class, "test", "secret".getBytes());
            User us1 = manager.loadEntity(User.class, "test1", "secreta".getBytes());
            User us2 = manager.loadEntity(User.class, "test2", "secretb".getBytes());
            
            assertSame(us, manager.getEntity(((KeyCode)us).get()));
            assertSame(us1, manager.getEntity(((KeyCode)us1).get()));
            assertSame(us2, manager.getEntity(((KeyCode)us2).get()));


        } finally{
            deleteUser(0);
            deleteUser(1);
            deleteUser(2);
        }
    }
    @Test
    public void testLoadEntityClassOfTPrimaryKey() throws EntityManagerNotSetException, EntityNotFoundException, ClassNotRegisteredException, SQLException, CannotRegisterException {
        createUser(0, "testNick", "test", "secret");
        createUser(1, "testNick1", "test1", "secreta");
        createUser(2, "testNick2", "test2", "secretb");
        try {
            DBManager manager = DBManager.getManager(TESTMANAGER);
            manager.registerType(User.class);
            
            User us = manager.loadEntity(User.class, "test", "secret".getBytes());
            User us1 = manager.loadEntity(User.class, "test1", "secreta".getBytes());
            User us2 = manager.loadEntity(User.class, "test2", "secretb".getBytes());
            
            assertEquals(0, us.getId());
            assertEquals("test", us.getLogin());
            assertEquals("testNick", us.getNickname());
            assertEquals("secret", us.getPassword());
            assertEquals(false, us.isPlaying());
            assertEquals(1, us1.getId());
            assertEquals("test1", us1.getLogin());
            assertEquals("testNick1", us1.getNickname());
            assertEquals("secreta", us1.getPassword());
            assertEquals(false, us1.isPlaying());
            assertEquals(2, us2.getId());
            assertEquals("test2", us2.getLogin());
            assertEquals("testNick2", us2.getNickname());
            assertEquals("secretb", us2.getPassword());
            assertEquals(false, us2.isPlaying());

        } finally{
            deleteUser(0);
            deleteUser(1);
            deleteUser(2);
        }
    }
    @Test
    public void testCreateEntity() throws EntityManagerNotSetException, EntityNotFoundException, ClassNotRegisteredException, SQLException, CannotRegisterException, EntityNotCreatedException {
        DBManager manager = DBManager.getManager(TESTMANAGER);
        manager.registerType(User.class);
        User us = new User("testUserCreate", "secret");
        us.setNickname("nickUserCreate");
        us.setPlaying(true);
        us.setPassword("secret");
        manager.createEntity(us);
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        int uid = -1; 
        try {
            con = ds.getConnection();
            st = con.createStatement();
            rs = st.executeQuery("select u.login, u.nickName, u.password, u.id from testdao2.User u where u.login = 'testUserCreate'");
            if (rs.next()){
                String login = rs.getString(1);
                assertEquals("testUserCreate", login);
                String nickname = rs.getString(2);
                assertEquals("nickUserCreate", nickname);
                String password = new String(rs.getBytes(3));
                assertEquals("secret", password);
                uid = rs.getInt(4);
            } else {
                fail();
            }
        } finally {
            if (uid != -1){
                deleteUser(uid);
            }
            closeConnection(con, st, rs);
        }
    }

    @Test
    public void testDeleteEntity() throws EntityManagerNotSetException, ClassNotRegisteredException, EntityNotFoundException, SQLException, CannotRegisterException {
        createUser(-1, "nickUserDelete", "testUserDelete", "secret");
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            DBManager manager = DBManager.getManager(TESTMANAGER);
            manager.registerType(User.class);
            User us = manager.loadEntity(User.class, "testUserDelete", "secret".getBytes());
            manager.deleteEntity(us);
            assertFalse(manager.isRegistered(User.class, "testUserDelete", "secret".getBytes()));
            con = ds.getConnection();
            st = con.createStatement();
            rs = st.executeQuery("select u.id from testdao2.User u where u.id = -1");
            if (rs.next()){
                fail();
            }
        } finally {
            closeConnection(con, st, null);
            deleteUser(-1);
        }
    }

    @Test
    public void testRetrieveSetFromQuery() throws EntityManagerNotSetException, SQLException {
        createUser(-2, "nickUserRetrieve0", "testUserRetrieve0", "secret");
        createUser(-3, "nickUserRetrieve1", "testUserRetrieve1", "secret");
        try {
            DBManager manager = DBManager.getManager(TESTMANAGER);
            Collection<Object[]> retrieveSetFromQuery = manager.retrieveSetFromQuery("select u.id, u.nickname, u.login, u.password from testdao2.User u where u.login = ? or u.login = ?", "testUserRetrieve0", "testUserRetrieve1");
            
            assertEquals(retrieveSetFromQuery.size(), 2);
            for (Object[] retrieved : retrieveSetFromQuery){
                if (retrieved[0].equals(-2)){
                    assertEquals("nickUserRetrieve0", retrieved[1] );
                    assertEquals("testUserRetrieve0", retrieved[2] );
                    assertEquals("secret", new String((byte[])retrieved[3]));
                } else if (retrieved[0].equals(-3)){
                    assertEquals("nickUserRetrieve1", retrieved[1]);
                    assertEquals("testUserRetrieve1", retrieved[2]);
                    assertEquals("secret", new String((byte[])retrieved[3]));
                } else {
                    fail();
                }
            }
        } finally {
            deleteUser(-2);
            deleteUser(-3);
        }
    }

    @Test
    public void testRetrieveDataFromQuery() throws SQLException, EntityManagerNotSetException {
        createUser(-4, "nickUserDataRetrieve0", "testUserDataRetrieve0", "secret");
        createUser(-5, "nickUserDataRetrieve1", "testUserDataRetrieve1", "secret");
        try {
            DBManager manager = DBManager.getManager(TESTMANAGER);
            Collection<Integer> retrieveSetFromQuery = manager.retrieveDataFromQuery("select u.id from testdao2.User u where u.login = ? or u.login = ? order by u.id", "testUserDataRetrieve0", "testUserDataRetrieve1");
            assertEquals(retrieveSetFromQuery.size(), 2);
            int i = -5;
            for (Integer retrieved : retrieveSetFromQuery){
                assertEquals(i++, retrieved.intValue());
            }
        } finally {
            deleteUser(-4);
            deleteUser(-5);
        }
    }

    @Test
    public void testExecuteQuery() throws EntityManagerNotSetException, SQLException {
        DBManager manager = DBManager.getManager(TESTMANAGER);
        manager.executeQuery("insert into testdao2.User (id, nickname, login, password) values (?, ?, ?, ?)", -5 , "nickUserExecuteQuery", "loginUserExecuteQuery", "secret".getBytes());

        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            con = ds.getConnection();
            st = con.createStatement();
            rs = st.executeQuery("select u.id, u.nickname, u.login, u.password from testdao2.User u where u.id = -5");
            if (rs.next()){
                assertEquals(-5 , rs.getInt(1));
                assertEquals("nickUserExecuteQuery", rs.getString(2));
                assertEquals("loginUserExecuteQuery", rs.getString(3));
                assertEquals("secret", new String(rs.getBytes(4)));
                st.executeUpdate("delete from testdao2.User where id = -5");
            } else {
                fail();
            }
        } finally {
            closeConnection(con, st, null);
        }

    }

    @Test
    public void testUpdateMethod()  throws EntityManagerNotSetException, EntityNotFoundException, ClassNotRegisteredException, SQLException, CannotRegisterException, EntityNotCreatedException {
        User us = null;
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        int uid = -1; 
        try {
            DBManager manager = DBManager.getManager(TESTMANAGER);
            manager.registerType(User.class);
            us = new User("testUserCreate", "secret");
            us.setNickname("nickUserCreate");
            us.setPlaying(true);
            us.setPassword("secret");
            us = manager.createEntity(us);
            final String p = "hello_9,9.Xoo!";
            us.setPassword(p);

            con = ds.getConnection();
            st = con.createStatement();
            rs = st.executeQuery("select u.login, u.nickName, u.password, u.id from testdao2.User u where u.login = 'testUserCreate'");
            if (rs.next()){
                String login = rs.getString(1);
                assertEquals("testUserCreate", login);
                String nickname = rs.getString(2);
                assertEquals("nickUserCreate", nickname);
                String password = new String(rs.getBytes(3));
                assertEquals(p, password);
                assertEquals(p, us.getPassword());
                uid = rs.getInt(4);
            } else {
                fail();
            }
            closeConnection(null, null, rs);
            us.setNickname("anotherNick");
            rs = st.executeQuery("select u.login, u.nickName, u.password, u.id from testdao2.User u where u.login = 'testUserCreate'");
            if (rs.next()){
                String login = rs.getString(1);
                assertEquals("testUserCreate", login);
                String nickname = rs.getString(2);
                assertEquals("anotherNick", nickname);
                assertEquals("anotherNick", us.getNickname());
                String password = new String(rs.getBytes(3));
                assertEquals(p, password);
                assertEquals(p, us.getPassword());

            } else {
                fail();
            }
        } finally {
            if (uid != -1){
                deleteUser(uid);
            }
            closeConnection(con, st, rs);
        }
    }

    private void createUser(int id, String nick, String login, String password) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        try {
            con = ds.getConnection();
            st = con.prepareStatement("insert into testdao2.User (id, nickname, login, password) values (?, ?, ?, ?)");
            st.setInt(1, id);
            st.setString(2, nick);
            st.setString(3, login);
//            st.setBytes(4, password.getBytes());
            st.setBlob(4, new ByteArrayInputStream(password.getBytes()));
            st.executeUpdate();
        } finally {
            closeConnection(con, st, null);
        }
        
    }
    private void deleteUser(int id){
        Connection con = null;
        PreparedStatement st = null;
        try {
            con = ds.getConnection();
            st = con.prepareStatement("delete from testdao2.User where id = ?");
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
        } finally {
            closeConnection(con, st, null);
        }
    }

    private void createDescriptor(int id, String gameDescriptorClass, String gameClass,
            String[] checkedRules, Integer[] checkedZones,
            String[] checkedGEHosts, HashMap<String, HashMap<String, Object>> checkedClientRules) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = ds.getConnection();
            ps = con.prepareStatement("insert into testdao2.Games (id, descriptorclassname, gamefactory) values (?, ?, ?)");
            ps.setInt(1, id);
            ps.setString(2, gameDescriptorClass);
            ps.setString(3, gameClass);
            ps.executeUpdate();
            ps.close();
            ps = con.prepareStatement("insert into testdao2.GameRules (ruleclass, gameid, gameRuleFunction, serverNeedsToKnow, loop) values (?, ?, ?, ?, ?)");
            ps.setInt(2, id);
            for (String ruleclass : checkedRules){
                ps.setString(1, ruleclass);
                HashMap<String,Object> clientruledata = checkedClientRules.get(ruleclass);
                ps.setObject(3, clientruledata.get("gameRuleFunction"));
                ps.setObject(4, clientruledata.get("serverNeedsToKnow"));
                ps.setObject(5, clientruledata.get("loop"));
                ps.executeUpdate();
            }
            ps.close();
            ps = con.prepareStatement("insert into testdao2.GameEngineConfig (host, gameid) values (?, ?)");
            ps.setInt(2, id);
            for (String host : checkedGEHosts){
                ps.setString(1, host);
                ps.executeUpdate();
            }
            ps = con.prepareStatement("insert into testdao1.BerserkerGame (gamedescriptorid, zoneid, version) values (?, ?, ?)");
            ps.setInt(1, id);
            ps.setInt(3, 0);
            for (Integer zoneid : checkedZones){
                ps.setInt(2, zoneid);
                ps.executeUpdate();
            }


        } finally {
            closeConnection(con, ps, null);
        }
    }
    private void createSkillTree() throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = ds.getConnection();
            ps = con.prepareStatement("insert into testdao1.SkillTreeNode  (id, action, minTime, maxTime) values (?, ?, ?, ?)");
            Object[][] data = new Object[][]{{1, "run", 0L, 200L},{0, "walk", null, null}, {2, "crouch", null, null},
                        {3, "strafe_left", null, null}, {4, "jump_left", 0L, 200L}, {5, "strafe_right", null, null},
                        {6, "jump_right", 0L, 200L}, {7, "attack", null, null}, {8, "jump", null, null}, {9, "salute", null, null},
                        {10, "salute1", 0L, 200L}};
    
            for (Object[] o : data) {
                for (int i = 0; i < o.length; i++){
                    ps.setObject(i+1, o[i]);
                }
                ps.executeUpdate();
            }

            ps = con.prepareStatement("insert into testdao1.SkillTree (childid, root, movement, type) values (?, ?, ?, ?)");
            data = new Object[][]{{1, 0, "ARR", 0}, {null, 0, "ARR", 0}, {null, 2, "ABA", 0},
                    {null, 3, "IZQ", 0}, {4, 3,  "IZQ", 0}, {null, 5, "DER", 0}, {6, 5, "DER", 0},
                    {null, 8, "SAL", 0}, {null, 7, "ATA", 1}, {null, 9, "SOC", 2}, {10, 9, "SOC", 2}};
    
            for (Object[] o : data) {
                for (int i = 0; i < o.length; i++){
                    ps.setObject(i+1, o[i]);
                }
                ps.executeUpdate();
            }
        } finally {
            closeConnection(con, ps, null);
        }
    }
    private void createZones(Integer[] checkedZones) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = ds.getConnection();
            ps = con.prepareStatement("insert into testdao1.Zone (id, level, parameters, max_clients, model) values (?, 1, '{}', 100, '')");
            for (Integer id : checkedZones){
                ps.setInt(1, id);
                ps.executeUpdate();
            }


        } finally {
            closeConnection(con, ps, null);
        }
        
    }
    private void deleteSkillTree() {
        Connection con = null;
        Statement st = null;
        try {
            con = ds.getConnection();
            st = con.createStatement();
            st.execute("delete from SkillTreeNode");
            st.execute("delete from SkillTree");
        } catch (SQLException e) {
        } finally {
            closeConnection(con, st, null);
        }

        
    }
    private void deleteZones(Integer[] checkedZones){
        Connection con = null;
        PreparedStatement st = null;
        try {
            con = ds.getConnection();
            st = con.prepareStatement("delete from Zone where id = ?");
            for (Integer id : checkedZones){
                st.setInt(1, id);
                st.executeUpdate();
            }

        } catch (SQLException e) {
        } finally {
            closeConnection(con, st, null);
        }
    }
    private void deleteDescriptor(int id) {
        Connection con = null;
        PreparedStatement st = null;
        try {
            con = ds.getConnection();
            st = con.prepareStatement("delete from BerserkerGameDescriptor where groupid = ?");
            st.setInt(1, id);
            st.executeUpdate();
            st.close();
            st = con.prepareStatement("delete from Games where id = ?");
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
        } finally {
            closeConnection(con, st, null);
        }
    }
    private void closeConnection(Connection con, Statement st, ResultSet rs){
        if (rs != null){
            try {
                rs.close();
            } catch (SQLException e) {}
        }
        if (st != null){
            try {
                st.close();
            } catch (SQLException e) {}
        }
        if (con != null){
            try {
                con.close();
            } catch(SQLException e){}
        }
    }

    private DataSource ds;
}
