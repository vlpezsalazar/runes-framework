package es.calipso.framework.storage.test.dao1;

import java.util.ArrayList;
import java.util.HashMap;

import es.calipso.framework.storage.annotations.DBEntity;
import es.calipso.framework.storage.annotations.NamedQuery;
import es.calipso.framework.storage.annotations.Populate;


@DBEntity(queries={
        @NamedQuery(name=MotionGraph.RETRIEVEMOTIONGRAPHTYPES, query="select m.charactertype, m.nodeid from MotionGraphType m where m.version = $version", type = 1),
        @NamedQuery(name=MotionGraph.RETRIEVEMOTIONGRAPH, query="select m.id from MotionGraph m where m.version = $version", type = 1)
})
public class MotionGraph{
    
    public static final String RETRIEVEMOTIONGRAPH = "RETRIEVEMOTIONGRAPH";
    public static final String RETRIEVEMOTIONGRAPHTYPES = "RETRIEVEMOTIONGRAPHTYPES";
    public MotionGraph(int version){
        this.version = version;
        motionTypes = new HashMap<String, ArrayList<Integer>>();
        motionIndexes = new HashMap<Integer, MotionGraph.MotionGraphNode>();
    }

    public int getId() {
        return version;
    }
    public void addMotion(String type, int motion){
        ArrayList<Integer> motionindexes = motionTypes.get(type);
        if (motionindexes == null){
            motionindexes = new ArrayList<Integer>();
            motionTypes.put(type, motionindexes);
        }
        motionindexes.add(motion);
    }
    @DBEntity(queries={
            @NamedQuery(name=MotionGraphNode.RETRIEVEMOTIONGRAPHNODE, query="select $level=m.level, $loop = m.loop, $animation=m.animation  from $MotionGraph m where m.id = $id", type = 1),
            @NamedQuery(name=MotionGraphNode.RETRIEVEMOTIONGRAPHNODECHILDS, query="select mc.action, mc.id from $MotionGraphNodeConstraint mc, $MotionGraph m where mc.groupid = m.childs and m.id = $id", type = 1)})
    public class MotionGraphNode  {
        
        public static final String RETRIEVEMOTIONGRAPHNODE = "RETRIEVEMOTIONGRAPHNODE";
        public static final String RETRIEVEMOTIONGRAPHNODECHILDS = "RETRIEVEMOTIONGRAPHNODECHILDS";
        protected int id;
        protected int level;
        protected boolean loop;
        protected String animation;

        @Populate(query=RETRIEVEMOTIONGRAPHNODECHILDS,
                  init = "MotionGraph.MotionGraphNode.MotionGraphNodeConstraints constraint@self($rs.getInt(2));" +
                         "$this.addChild($rs.getString(1), constraint);")
        private HashMap<String, ArrayList<MotionGraphNodeConstraints>> childs  = new HashMap<String, ArrayList<MotionGraphNodeConstraints>>();
        
        public MotionGraphNode(int nodeid){
            id = nodeid;
        }
        public void addChild(String action, MotionGraphNodeConstraints constraint){
            ArrayList<MotionGraphNodeConstraints> constraints = childs.get(action);
            if (constraints == null){
               constraints = new ArrayList<MotionGraph.MotionGraphNode.MotionGraphNodeConstraints>();
               childs.put(action, constraints);
            }
            constraints.add(constraint); 
        }

        @DBEntity(queries = {@NamedQuery(name=MotionGraphNodeConstraints.RETRIEVEMOTIONGRAPHNODECONSTRAINTS, query="select $constraints = m.constraints, $motionInstructions = m.motionInstructions, $child = m.child from $$ m where m.id = $id", type = 1)})
        public class MotionGraphNodeConstraints  {
            public static final String RETRIEVEMOTIONGRAPHNODECONSTRAINTS = "RETRIEVEMOTIONGRAPHNODECONSTRAINTS";
            protected int id;
            protected String constraints;
            protected String motionInstructions;
            protected int child;

            public MotionGraphNodeConstraints(int constraintgroupid){
                id = constraintgroupid;
            }
            public int getId() {
                return id;
            }
        }
        public int getId() {
            return id;
        }
    }

    protected int version; 
    /*
     * Lista de nodos de inicio por tipo de personaje. 
     */
    @Populate(query = RETRIEVEMOTIONGRAPHTYPES, 
              init= "$this.addMotion($rs.getString(1), $rs.getInt(2));")
    protected HashMap<String, ArrayList<Integer>> motionTypes = new HashMap<String, ArrayList<Integer>>();
    
    /*
     * Indices de animaciones.
     */
    @Populate(query = RETRIEVEMOTIONGRAPH,
            init= "int nodeid = $rs.getInt(1);"+
                  "MotionGraph.MotionGraphNode mgn@self(nodeid);"+
                  "$$.put(nodeid, mgn);")
    protected HashMap<Integer, MotionGraphNode> motionIndexes = new HashMap<Integer, MotionGraphNode>();
}
