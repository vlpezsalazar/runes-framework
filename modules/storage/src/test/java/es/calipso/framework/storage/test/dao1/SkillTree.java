package es.calipso.framework.storage.test.dao1;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import es.calipso.framework.storage.annotations.DBEntity;
import es.calipso.framework.storage.annotations.NamedQuery;
import es.calipso.framework.storage.annotations.PK;
import es.calipso.framework.storage.annotations.Populate;


/**
 * Clase que representa un arbol de habilidades para un tipo concreto de arma
 * o de movimiento p.e. combate cuerpo a cuerpo o con arma a 2 manos. El arbol
 * se lee completo de la base de datos pero se personaliza para el personaje
 * mediante el método setAllowed() cuando es necesario transferirlo al cliente.
 *  
 * @author Víctor López Salazar
 *
 */

@DBEntity(queries={
        @NamedQuery(name = SkillTree.RETRIEVESKILLTREEROOTS, 
                    query = "select s.root, s.movement from $$ s where s.type = $type and s.childid is null", type = 1),
        @NamedQuery(name = "CREATESKILLTREE", 
                    query = "insert into $$ values()", type = 0)
/*
         @NamedQuery(name = "CREATESKILLTREENODES",  
                    iterator = {"$rootNode:i", "$blablabla:j"}, // TODO ESTA PROPIEDAD PERMITE REALIZAR MULTIPLES CONSULTAS A LA BD SOBRE UN ITERADOR DE UNA PROPIEDAD DE LA CLASE
                    query = "insert into $$ values (*i.getValue(), p, *j)",
                    after = "SkillTreeNode sn@self = $[i].getValue();", // TODO ESTO SERÍA EL CÓDIGO A EJECUTAR DESPUES DE UNA CONSULTA SQL. DA LA POSIBILIDAD DE REALIZAR OPERACIONES RECURSIVAS SOBRE LA BD CON OTRO TIPO QUE NO SEA EL ACTUAL (OPERACIONES DE @self)    
                    type = 0)
*/
})
public class SkillTree {
    public static final String RETRIEVESKILLTREEROOTS = "RETRIEVESKILLTREE";
    public SkillTree(){}
    public SkillTree(int type){
        this.type = type;
    }

    public int getId() {
        return type;
    }
    /**
     * Crea una habilidad hija de la actual con los parametros indicados y devuelve dicha habilidad. 
     * @param id identificador único de la habilidad.
     * @param movement movimiento con el que se accede a la habilidad.
     * @param action acción que ejecuta la habilidad.
     * @param minDelay retardo minimo para ejecutar esa habilidad desde la actual.
     * @param maxDelay retardo máximo para ejecutar esa habilidad desde la actual.
     * @return la habilidad hija creada.
     */
/*  TODO AMPLIAR QUERY CON LAS PROPIEDADES SIGUIENTES
    @Query("select blablabla",
    create={"SkillTreeNode"}, //TODO las declaraciones de los tipos que se indican se instancian mediante el método create del dbmanager 
    load ={}, //TODO las declaraciones de los tipos que se indican se instancian mediante el método load del dbmanager  
    delete={""}
    )
*/
    public SkillTreeNode addChildSkill(int id, String movement, String action, long minDelay, long maxDelay){
        SkillTreeNode skn = new SkillTreeNode(id, action, minDelay, maxDelay, skills);
        rootNode.put(movement, skn);
        return skn;
    }
   
    public int getType() {
        return type;
    }
    public void setAllowed(Set<Integer> characterSkills) {
        for (Entry<Integer, SkillTreeNode> nodeentry : skills.entrySet()){
            if (characterSkills.contains(nodeentry.getKey())){
                nodeentry.getValue().setAllowed();
                continue;
            }
            nodeentry.getValue().unSetAllowed();
        }
    }
    @Override
    public boolean equals(Object o) {
        if (!SkillTree.class.isAssignableFrom(o.getClass())){
            return false;
        }
        SkillTree sktree = (SkillTree) o;
        if (this.type != sktree.type) return false;
        for (Entry<String, SkillTreeNode> skchildentry : rootNode.entrySet()){
            SkillTreeNode skchild1 = sktree.rootNode.get(skchildentry.getKey());
            if (skchild1 == null) return false;
            SkillTreeNode skchild = skchildentry.getValue();
            if (!skchild1.equals(skchild))
                return false;            
        }
        return true;
    }
    /**
     * Tipo de arbol: 0 - sin armas, 1 - armas a una mano, 2 - armas a dos manos, 3 - arma a una mano + escudo, 4 - dos armas, 5 a distancia.
     */
    @PK
    protected int type;

    /**
     * Habilidades que forman el árbol: <id_nodo, nodo>
     */
    protected HashMap<Integer, SkillTreeNode> skills = new HashMap<Integer, SkillTreeNode>();

    /**
     * Nodo raiz del árbol: <movimiento, nodo>
     */
    @Populate(query=RETRIEVESKILLTREEROOTS, 
              init="es.calipso.framework.storage.test.dao1.SkillTreeNode sn@self = new es.calipso.framework.storage.test.dao1.SkillTreeNode($rs.getInt(1), $skills);" +
//              	   "$log.debug(MessageFormat.format(\"action = {0}, sn = {1}\", new Object[]{$rs.getString(2), sn}));" +
                   "$$.put($rs.getString(2), sn);")
    protected HashMap<String, SkillTreeNode> rootNode = new HashMap<String, SkillTreeNode>();
}
