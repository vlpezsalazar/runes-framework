package es.calipso.framework.storage.test.dao2;

import es.calipso.framework.storage.annotations.DBEntity;
import es.calipso.framework.storage.annotations.Execute;
import es.calipso.framework.storage.annotations.NamedQuery;
import es.calipso.framework.storage.annotations.PK;
import es.calipso.framework.storage.annotations.Populate;
import es.calipso.framework.storage.annotations.Query;

@DBEntity(queries= {
        @NamedQuery(name = User.CREATEUSER, query = "insert into $$ (nickname, login, password) values ($nickname, $login, $cipheredPassword)", type = 0),
        @NamedQuery(name = User.RETRIEVEUSER, query = "select $id = u.id, $nickname = u.nickname, u.password  FROM $$ u where u.login = $login and u.password = $cipheredPassword", type = {1, 0}),
        @NamedQuery(name = User.REMOVEUSER, query = "delete from $$ where id = $id", type = 2)})
public class User{

    public User(){}
    /*    public User(User us){
    login = us.login;
    nickname = us.nickname;
    password = us.password;
    cipheredPassword = us.cipheredPassword;
    playState = us.playState;
    id = us.id;
    }*/
    public User(String login, String password) {
	    this.login = login;
	    this.nickname = login;
	    setPassword(password);
    }
    public String getLogin() {
        return login;
    }

    public void setId(int id) {
        this.id = id;
    }

    //just to test method call functionality inside queries
    private byte[] cipher(String string){
        return string.getBytes();
    }
    private String decipher(byte[] pass){
        return new String(pass);
    }
    public String getPassword() {
        return password;
    }
/*    private String decypherPassword(){
        CipherManager cipherManager = CipherManager.getInstance();
        Cipher cipher = cipherManager.getDefaultCipherDecrypt();
        String passw = null;
        try {
            cipher.update(password);
           passw = new String(cipher.doFinal());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //NOTHING TO DO
        }
        return passw;
    }*/
    @Query(value="update $$ set password = $cipheredPassword",
            when=Execute.AFTER)
    public void setPassword(String password){
        this.password = password;
        this.cipheredPassword = cipher(password);
    }
    protected void setPassword(byte[] password){
        this.cipheredPassword = password;
        this.password = decipher(password);
    }
    public String getNickname() {
        return nickname;
    }
    @Query("update $$ set nickname = $1")
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    @Query("update $$ set login = $1")
    public void setLogin(String login) {
        this.login = login;
    }

    public int getId() {
        return id;
    }
    public void setPlaying(boolean b) {
        playState = b;
        
    }
    public boolean isPlaying() {
        return playState;
    }

    public static final String CREATEUSER = "CREATEUSER",
                                 RETRIEVEUSER = "RETRIEVEUSER",
                                 SAVEUSER = "SAVEUSER",
                                 UPDATEUSER = "UPDATEUSER", 
                                 REMOVEUSER = "REMOVEUSER";

    private int id;
    @PK
    private String login;
//    @SuppressWarnings("unused")
    @PK(2)
    private byte[] cipheredPassword; 
    private String nickname;
    @Populate(
            query= RETRIEVEUSER,
            init = "$this.setPassword($rs.getBytes(3));"
            )
    private String password;
    private boolean playState = false;
    

    
}