package es.calipso.framework.storage.test.dao2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import es.calipso.framework.storage.annotations.DBEntity;
import es.calipso.framework.storage.annotations.NamedQuery;
import es.calipso.framework.storage.annotations.PK;
import es.calipso.framework.storage.annotations.Populate;

@DBEntity(table={"Games", "GameRules", "GameEngineConfig"},
    queries={
         @NamedQuery(name=GameDescriptor.LOADGAME, query = "select $id, $gameFactory from $$ g where g.descriptorclassname = $descriptorclassname", type=1),
         @NamedQuery(name=GameDescriptor.LOADGAMERULES, query = "select gr.gameRuleFunction, gr.ruleclass, gr.serverNeedsToKnow, gr.loop from $$1 gr where gr.gameid = $id", type=1 ),
         @NamedQuery(name=GameDescriptor.LOADGAMEMASTERGEHOSTS, query = "select g.host from $$2 g where g.gameid = $id", type=1)})
public class GameDescriptor {

    protected GameDescriptor(){}
    protected GameDescriptor(int id, Class<?> childcl){
        this.id = id;
        descriptorclassname = childcl.getCanonicalName();
    }
    public String getDescriptorClassName(){
        return descriptorclassname;
    }

    public int getId() {
        return id;
    }
    public String getGameFactory(){
        return gameFactory;
    }
    public int getGameId(){
        return id;
    }
    public void addGameRule(String rulename, String ruleclass, boolean serverNeedsToKnow, boolean loop){
        HashMap<String, Object> ruleinfo = new HashMap<String, Object>();
        ruleinfo.put("loop", Boolean.valueOf(loop));
        ruleinfo.put("serverNeedsToKnow", Boolean.valueOf(serverNeedsToKnow));
        ruleinfo.put("gameRuleFunction", String.valueOf(rulename));
        rules.put(ruleclass, ruleinfo);
    }
    public Set<String> getServerRules(){
        return rules.keySet();
    }
    public HashMap<String, HashMap<String, Object>> getClientRules(){
        return rules;
    }
    public ArrayList<String> getMasterGEHosts(){
        return masterGEHosts;
    }

    public static final String LOADGAME = "LOADGAME";
    public static final String LOADGAMERULES = "LOADGAMERULES";
    public static final String LOADGAMEMASTERGEHOSTS = "LOADGAMEMASTERGEHOSTS";
    
    /*
     * Esta propiedad contiene el id del juego para este descriptor. 
     */
    @PK
    protected int id;

    /*
     * identificador del juego nombre totalmente cualificado de la clase que
     * extiende a GameDescriptor (descriptor particular del juego)
     */
    @PK
    protected String descriptorclassname;

    /*
     * Esta propiedad contiene el nombre totalmente cualificado de la factoria
     * que sirve para instanciar un juego. 
     */
    protected String gameFactory;

    /*
     * Reglas asociadas a este juego: 
     * <clase de la regla, mapa de <loop : boolean, serverNeedsToKnow: boolean, gameRuleFunction: string>>
     */
    @Populate(query=GameDescriptor.LOADGAMERULES,
              init = "$this.addGameRule($rs.getString(1), $rs.getString(2), $rs.getBoolean(3), $rs.getBoolean(4));")
    protected HashMap<String, HashMap<String, Object>> rules = new HashMap<String, HashMap<String, Object>>();

    @Populate(query=GameDescriptor.LOADGAMEMASTERGEHOSTS,
              init = "$$.add(#host);")
    protected ArrayList<String> masterGEHosts = new ArrayList<String>();
}
