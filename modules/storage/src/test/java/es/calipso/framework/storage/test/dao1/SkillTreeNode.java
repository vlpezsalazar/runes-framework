package es.calipso.framework.storage.test.dao1;

import java.util.HashMap;
import java.util.Map.Entry;

import es.calipso.framework.storage.annotations.DBEntity;
import es.calipso.framework.storage.annotations.NamedQuery;
import es.calipso.framework.storage.annotations.PK;
import es.calipso.framework.storage.annotations.Populate;
/**
 * Nodo del árbol para una rama de habilidades.
 * 
 * @author Víctor López Salazar
 *
 */
@DBEntity(queries = {
        @NamedQuery(name = SkillTreeNode.RETRIEVESKILLTREENODE, query = "select $action = s.action, $minDelay = s.minTime, $maxDelay = s.maxTime from $$ s where s.id = $id", type = 1),
        @NamedQuery(name = SkillTreeNode.RETRIEVESKILLTREENODECHILDS, fail = false, query = "select s.childid, s.movement from $SkillTree s where s.root = $id and s.childid is not null", type = 1)})
public class SkillTreeNode {
    public static final String RETRIEVESKILLTREENODE = "RETRIEVESKILLTREENODE";
    public static final String RETRIEVESKILLTREENODECHILDS = "RETRIEVESKILLTREENODECHILDS";

    public SkillTreeNode(){}
    public SkillTreeNode(int id, HashMap<Integer, SkillTreeNode> register) {
        this.id = id;
        childs = new HashMap<String, SkillTreeNode>();
        register.put(id, this);
        this.register = register;
        allowed = false;
    }
    /*
     * CONSTRUCTOR PARA PRUEBAS.
     */
    public SkillTreeNode(int id, String action, long minDelay, long maxDelay, HashMap<Integer, SkillTreeNode> register){
        this.id = id;
        childs = new HashMap<String, SkillTreeNode>();
        register.put(id, this);
        this.register = register;
        allowed = false;
        this.action = action;
        this.minDelay = minDelay;
        this.maxDelay = maxDelay;
    }

    public void setAllowed() {
        allowed = true;
    }
    public void unSetAllowed() {
        allowed = false;
    }

    public int getId() {
        return id;
    }
    /**
     * Crea una habilidad hija de la actual con los parametros indicados y devuelve dicha habilidad. 
     * @param id identificador único de la habilidad.
     * @param movement movimiento con el que se accede a la habilidad.
     * @param action acción que ejecuta la habilidad.
     * @param minDelay retardo minimo para ejecutar esa habilidad desde la actual.
     * @param maxDelay retardo máximo para ejecutar esa habilidad desde la actual.
     * @return la habilidad hija creada.
     */
    public SkillTreeNode addChildSkill(int id, String movement, String action, long minDelay, long maxDelay){
        SkillTreeNode skn = new SkillTreeNode(id, action, minDelay, maxDelay, register);
        addChild(movement, skn);
        return skn;
    }
    @Override
    public boolean equals(Object o) {
        if (!SkillTreeNode.class.isAssignableFrom(o.getClass())){
            return false;
        }
        
        SkillTreeNode sk1 = (SkillTreeNode) o;

        if (minDelay != sk1.minDelay || id != sk1.id || !action.equals(sk1.action) || maxDelay != sk1.maxDelay || maxChildDelay != sk1.maxChildDelay){
            return false;
        }
        for (Entry<String, SkillTreeNode> skchildentry : childs.entrySet()){
            SkillTreeNode sk1child = sk1.childs.get(skchildentry.getKey());
            if (sk1child == null) return false;
            SkillTreeNode skchild = skchildentry.getValue();
            if (!sk1child.equals(skchild))
                return false;
        }
        return true;
    }
    //setters 
    protected void addChild(String movement, SkillTreeNode child){
        if (child.maxDelay > maxChildDelay){
            maxChildDelay = child.maxDelay;
        }
        childs.put(movement, child);
    }
    /**
     * Identificador de la base de datos para el nodo del árbol.
     */
    @PK
    private int id;
    /**
     * Nombre de la acción de este nodo.
     */
    private String action = "";
    /**
     * habilidad conseguida actualmente por el personaje o no.
     */
    @SuppressWarnings("unused")
    private boolean allowed;
    /**
     * tiempo mínimo de activación que ha de pasar entre la anterior 
     * habilidad y la presente para que esta se ejecute.
     * 
     */
    private long minDelay;
    /**
     * tiempo máximo de activación que puede pasar entre la anterior 
     * habilidad y la presente para que esta se ejecute.
     * 
     */
    private long maxDelay;

    /**
     * tiempo máximo de activación de la habilidad hija que más puede
     * demorarse. 
     */
    private long maxChildDelay = 0;
    /**
     * Otras habilidades que pueden alcanzarse desde la actual empleando un
     * movimiento: <movimiento, nodo_de_habilidad>
     */
    @Populate(query = RETRIEVESKILLTREENODECHILDS, 
              init = "es.calipso.framework.storage.test.dao1.SkillTreeNode sn@self = new es.calipso.framework.storage.test.dao1.SkillTreeNode($rs.getInt(1), $register);" +
                     "$this.addChild($rs.getString(2), (es.calipso.framework.storage.test.dao1.SkillTreeNode) sn);")
    private HashMap<String, SkillTreeNode> childs;
    protected HashMap<Integer, SkillTreeNode> register;        

}