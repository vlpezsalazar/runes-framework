package es.calipso.framework.storage.test.dao1;

import java.util.ArrayList;

import es.calipso.framework.storage.annotations.DBEntity;
import es.calipso.framework.storage.annotations.NamedQuery;
import es.calipso.framework.storage.annotations.Populate;
import es.calipso.framework.storage.test.dao2.GameDescriptor;


@DBEntity(table = "BerserkerGame",
        queries = {
            @NamedQuery(name = BerserkerGameDescriptor.LOADGAMEZONES, query = "select g.zoneid from $$ g where g.gamedescriptorid = $id", type = 1),
            @NamedQuery(name = "LOADVERSION", query = "select $version = g.version from $$ g where g.gamedescriptorid = $id", type = 1)
        })
public class BerserkerGameDescriptor extends GameDescriptor {

    public static final String LOADGAMEZONES = "LOADGAMEZONES";
/*
    public BerserkerGameDescriptor(BerserkerGameDescriptor b){
        super(BerserkerGameDescriptor.class);
        bgdid = b.bgdid;
        gameFactory = b.gameFactory;
        gameId = b.gameId;
        masterGEHosts = b.masterGEHosts;
        rules = b.rules;
        version = b.version;
        zones = b.zones;
    }*/
    public BerserkerGameDescriptor(){}
    public BerserkerGameDescriptor(int id){
        super(id, BerserkerGameDescriptor.class);
    }
    /**
     * Método que devuelve la lista de zonas que se encuentran asociadas a un
     * juego.
     *
     * @return lista de zonas asociadas al juego.
     */
    public ArrayList<Integer> getGameZones() {
        return zones;
    }
    
    public void addGameZone(int zoneid){
        zones.add(zoneid);
    }
    public int getVersion(){
        return version;
    }

    private int version;
    
    @Populate(query=BerserkerGameDescriptor.LOADGAMEZONES, 
              init = "$this.addGameZone($rs.getInt(1));")
    private ArrayList<Integer> zones = new ArrayList<Integer>();

}
