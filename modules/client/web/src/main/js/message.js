/*global $, CalipsoWebClient, Class, console */
/**
 * @author Víctor López Salazar
 */

CalipsoWebClient.Messages = (function(){
    var Messages = {
            rulesPackage : "es.calipso.framework.core.rules",
            messages : []
        }, 
        Message = Class.extend({
            init : function (object, rulesPackage){
                var rule = object.rule,
                    what = object,
                    messagesPackage = "es.calipso.framework.core.communications",
                    type = "GameRequestMessage";
    
    
                if (typeof rule === "undefined"){
                    for (rule in Messages){
                        if (typeof Messages[rule] === "function" && Messages[rule] === this.init.caller){
                            break;
                        }
                    }
                }
                type = messagesPackage + '.' + type;
                rule = ((typeof rulesPackage !== "undefined") ? rulesPackage : Messages.rulesPackage) + '.' + rule;
                
                
                Messages.messages.push(this);
                $.extend(what, {rule: rule, type: type, gameid : Messages.gameid, context : Messages.context, timeStamp: CalipsoWebClient.Scheduler.Timer.getLocalTime()});
                this.getType = function(){
                    return type;
                };
                this.getTimeStamp = function(){
                    return what.timeStamp;
                };
                this.setData = function(data){
                    what = $.extend(data, {rule: rule, type: type, gameid : Messages.gameid, context : Messages.context, timeStamp: CalipsoWebClient.Scheduler.Timer.getLocalTime()});
                    return this;
                };
                this.toString = function(){
    /*                var time = CalipsoWebClient.getTime(),
                        str = $.extend(what, time);*/
                    return JSON.stringify(what);
                };
            }
        });

    Messages.Message = Message;


    Messages.setContext = function(ctx){
        var i, 
            messages = Messages.messages;
        Messages.context = ctx.getBId();
        //para que los mensajes actualicen el contexto.
        for (i in messages){
            messages[i].setData({});
        }
    };

    Messages.setRulesPackage = function(rulesPackage){
        Messages.rulesPackage = rulesPackage;
    };
    /**
     * regla empleada para recibir keycodes de los juegos y sus contextos asociados.
     */
    Messages.InitGame = Message.extend({
        init : function(game){
            Messages.gameid = game;
            this.$super({gameid : game}, "es.calipso.framework.core.rules");
        }
    });
    Messages.EndGame = Message.extend({
        init : function(){
            this.$super({}, "es.calipso.framework.core.rules");
        }
    });

    //creamos tipo de mensaje de accion
    Messages.GetServerTime = Message.extend({
        init: function(){
            this.$super();
        }
    });
    return Messages;
}());