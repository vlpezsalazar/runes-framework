/*global $, GLGE, CalipsoWebClient, Class, Uint8Array, DOMParser, window, Ut, toType */
/**
 * Graphic Engine and Drawable classes.
 * 
 * @author Víctor López Salazar.
 */

                /*
                 * TODO refactorizar Graphics para incluir los modelos 3D en 
                 * su propio paquete y dejar/Crear en Graphics las interfaces
                 * comunes 2D/3D para las GameEntities.
                 */

CalipsoWebClient.Graphics = (function(){
    
 // funcion empleada en Bone para hallar los vertices que son afectados por una
 // articulacion.
    
    function findVertexIds(vertex, mesh, distance){
        var vertexsIds = [],
            i = 0,
            n = mesh.length/3;
        for (; i<n; i++){
            if (GLGE.distanceVec3(vertex, [mesh[i*3],mesh[i*3+1],mesh[i*3+2]])<distance){
                vertexsIds.push(i);
            }
        }
        return vertexsIds;
    }
 // funcion empleada en Bone para hallar los vertices que son afectados por una
 // articulacion.
    function findVertexIdsIntoEnvelope(position, mesh, distance){
        var vertexsIds = [],
            i = 0,
            n = mesh.length/3;
        for (; i<n; i++){
            if (Math.abs(position[0]-mesh[i*3])<distance[0] && Math.abs(mesh[i*3+1]-position[1])<distance[1] && Math.abs(mesh[i*3+2]-position[2])<distance[2]){
                vertexsIds.push(i);
            }
        }
        return vertexsIds;
    }

    function createStatistics(assets){
        var i = 0, 
            objects, 
            type, 
            statistics = {
                total       : 0,
                loaded      : 0,
                error       : 0,
                errorAssets : []
            };
    
        for (type in assets){
            statistics[type] = {};
            statistics[type].total = 0;
            statistics[type].loaded = 0;
    
            objects = assets[type];
            for (i in objects){
                statistics[type].total++;
            }
            statistics.total += statistics[type].total;
        }
        return statistics;
    }
    //funcion empleada en AssetsLoader para crear el callback para la
    //actualización de carga de los recursos
    function updateLoadCreator(loaderCallback, callback, type, id, statistics){
        return function(){
            statistics[type].loaded++;
            statistics.loaded++;
            loaderCallback(type, id, statistics);
            if (statistics.loaded === statistics.total){
                callback();
            } else if (statistics.loaded + statistics.error === statistics.total){
                alert("The following assets couldn't be loaded: " + statistics.errorAssets.join("\n") + "\n The load has been halted. Press F5 to try again.");
            }
        };
    }

    //cargador de imagenes
    function imagePreloader(assets, id, src, assetsLoaderCallback, statistics){
        var image = new Image();
        image.onload = assetsLoaderCallback;
        image.onerror = function(){
            statistics.error++;
            statistics.errorAssets.push(src);
            if (statistics.loaded + statistics.error === statistics.total){
                alert("The following assets couldn't be loaded: " + statistics.errorAssets.join("\n") + "\n The load has been halted. Press F5 to try again.");
            }
        };
        image.src = "images/"+src;
        image.ready = true; //XXX esto se debería hacer dentro del assetsLoaderCallback;
        assets.put(id, image);
    }
    
    var AssetsLoader = Class.extend({
            init : function(){
                //TODO el resto de cargadores de assets: audio, models, scenes
                this.preloaderType = {
                        images : imagePreloader
                };
            },
            addPreloader : function(type, factory){
                this.preloaderType[type] = factory;
            },
            load : function(assets, loaderCallback, callback){
                var id,
                    type,
                    objects,
                    preloader,
                    statistics = createStatistics(assets);
    
                this.assetsCache = {};
    
                for (type in assets){
                    objects = assets[type];
                    preloader = this.preloaderType[type];
                    if (typeof preloader === "function"){
                        for (id in objects){
                            preloader(this, id, objects[id], updateLoadCreator(loaderCallback, callback, type, id, statistics), statistics);
                        }                        
                    }
                }
            },
            put : function(id, object){
                this.assetsCache[id] = object;
            },
            get : function(id){
                return this.assetsCache[id];
            }
        }), 
        Graphics = {
            setGraphics : function(assets, callback){
                var /*motionGraphData = assets.motionGraph, 
                    animationData = assets.motions, 
                    personalizableModels = assets.personalizableModels, */
                    models = assets.models,
                    LoaderDIV = this.Graphics2D.LoaderDIV;
                this.loaderDIV = new LoaderDIV();
                this.assetsLoader = new AssetsLoader();
                
                this.assetsLoader.load(assets, this.loaderDIV.getUpdateCallback(), callback);
                
                CalipsoWebClient.Graphics.setItemsModule(models);
                /*
                if (toType(motionGraphData) === "string"){
                    $.getJSON(motionGraphData, function(data2) {
                        $.getJSON(animationData, function(data1) {
                            console.log('Creando controlador de animaciones...'); 
                            CalipsoWebClient.Graphics.setMotionsModule(data1, data2, CalipsoWebClient.MotionFuncs);
                            if (toType(personalizableModels) === "string"){ //por si no definen modelos personalizables
                                $.getJSON(personalizableModels, function(data){
                                    CalipsoWebClient.Graphics.setPersonalizationModule(data.personalization, data.pieces);
                                    callback();
                                });
                            }
                        });
                    });
                } else {
                    callback();
                }
                */
            },
            getAsset : function(id){
                return this.assetsLoader.get(id);
            },
            putAsset : function(id, obj){
                this.assetsLoader.put(id, obj);
            },
            setPersonalizationModule : function(personalization, pieces){
                this.PersonalizationModule = new CalipsoWebClient.Models.Personalization(personalization, pieces);
            },
            getPersonalizationModule : function(){
                return this.PersonalizationModule;
            },
            setMotionsModule : function(motionsData, motiongraphData, motionfuncs){
                this.MotionsModule = {};
                this.MotionsModule.motions = CalipsoWebClient.Animation.MotionCreator.createMotions(motionsData);
                this.MotionsModule.motionGraph = new CalipsoWebClient.Animation.MotionGraph(motiongraphData, motionfuncs);
            },
            getMotionsModule : function(){
                return this.MotionsModule;
            },
            createModel : function(modelDefinition, callback){
                CalipsoWebClient.Models.ModelCreator.createModel(
                    modelDefinition,
                    this.PersonalizationModule,
                    this.MotionsModule,
                    callback);
            },
            setItemsModule : function(index){
                this.ItemsModule = new CalipsoWebClient.Models.Items(index);
            },
            getDrawable : function(modelid, callback){
                this.ItemsModule.getItemDrawable(modelid, callback); 
            }
        };

    /**
     * Clase camara que utiliza el GraphicEngine.
     * 
     * @param {Array|Object}
     *            position un vector [x,y,z] o un objeto position.x, position.y,
     *            position.z con la posición inicial de la cámara.
     * @param {Array}
     *            upAxis eje vertical de la cámara. Un vector [x,y,z] representando
     *            la dirección del eje vertical de la cámara, p.e.: [0,1,0] indica
     *            el eje Y en el sistema de coordenadas XYZ.
     */
    Graphics.Camera = Class.extend({
        init : function(position, upAxis, callback){
            var innerCamera = new GLGE.Camera(),
                 x=0, 
                 y=0, 
                 z=0,
                 TWOPI = 2*Math.PI,
                 zoomSpeed = 0,
                 zoomLimits = [15, 45], // limites del zoom minimo y máximo
        /*
         * se emplea para establecer la posicion de la/ camara en las
         * animaciones de la misma
         */
                 cameraBlendProperties= {blend : false, isNew : false}, 
                 pointBlendProperties = {isNew : false},  // posicion hacia donde
                                                        // mira la camara
                 objectToFollow, // objeto que debe seguir la cámara.
                 time = 0, // los movimientos de la camara se realizan en time
                           // millis
                 radio = 20, // por defecto una distancia de la camara del objeto a
                             // seguir de 20 unidades
                 dheading = 0, // rotación acumulada de la camara para los
                               // movimientos de la misma cuando sigue a un objeto
                 incheading = 0, // incremento de la rotación en el movimiento de la
                                 // camara cuando sigue a unobjeto.
                 guidePoint = new GLGE.Object("guidePoint"); // el punto que se
                                                             // utiliza para que la
                                                             // cámara mire a una
                                                             // dirección, cuando
                                                             // hace travelling
                                                             // inicializamos la posicion de la camara
            if (typeof position !== "undefined"){
                if (typeof position.x !== "undefined"){
                    x = position.x;
                    y = position.y;
                    z = position.z;
                } else {
                    x = position[0];
                    y = position[1];
                    z = position[2];
                }
            } 
            innerCamera.setLoc(x,y,z);
            // establecemos el eje vertical de la cámara
            if (typeof upAxis !== "undefined"){
                innerCamera.setUpAxis(upAxis);
            } else {
                innerCamera.setUpAxis(GLGE.YUP);
            }
            innerCamera.Lookat([0,0,1]);
            /*
             * innerCamera.addEventListener("animFinished", function(){
             * innerCamera.setAnimation(); //XXX HAY QUE LIMPIAR EL BUFFER DE
             * ANIMACION DESPUES DE UN blendTo SI NO LOS POSICIONAMIENTOS MEDIANTE
             * SETLOC NO FUNCIONAN BIEN cameraBlendProperties.blend = false;
             * console.log('animación finalizada'); });
             */

            guidePoint.setVisible(false);
        
            /*
             * ESTO NO FUNCIONA var positions = [0.0, 0.0, 0.0, 1.0, 1.0, 1.0];
             * guidePoint.setDrawType(GLGE.DRAW_LINES); guidePoint.setPointSize(10);
             * guidePoint.setMesh((new GLGE.Mesh).setPositions(positions));
             */
        
            // función de zoom: actualiza el radio.
            function zoom(){
                radio += zoomSpeed * time / 1000;
                if (radio<zoomLimits[0]) {
                    radio = zoomLimits[0];
                } else if(radio>zoomLimits[1]) {
                    radio = zoomLimits[1];
                }
        // console.log("zoom = ", radio, ", zoomspeed = ", this.zoomSpeed);
            }
            // función de seguimiento de objetos desde una posición relativa
            // al objeto
            function follow(){
                var camerapos,
                    cameraheading,
                    step = {},
                    lookat = innerCamera.getLookat(),
                    /*{number}*/ locx,
                    /*{number}*/ locy,
                    /*{number}*/ locz;
        // console.log('following and looking at', lookat);
                if ( lookat === guidePoint){
                    camerapos = [zoomLimits[0], 8, zoomLimits[0]];
                } else {
                    dheading = (dheading + incheading);
                    if (dheading < 0){
                        dheading = TWOPI - dheading;
                    } else if (dheading > TWOPI){
                        dheading = dheading - TWOPI;
                    }
                    // habría que seleccionar cual es el eje Y
                    cameraheading = objectToFollow.getRotY() + dheading;
                    camerapos =[+radio*Math.cos(cameraheading), 8, -radio*Math.sin(cameraheading)];
                }
        
                locx = +objectToFollow.getLocX() + camerapos[0]; 
                locy = +objectToFollow.getLocY() + camerapos[1];
                locz = +objectToFollow.getLocZ() + camerapos[2];
                if (typeof cameraBlendProperties.LocX !== "undefined"){
                    step.last = [cameraBlendProperties.LocX, cameraBlendProperties.LocY, cameraBlendProperties.LocZ];
                    step.next = [locx, locy, locz];
                }
                
                if (!(isNaN(locx)||isNaN(locy)||isNaN(locz)) 
                        && (locx !== cameraBlendProperties.LocX || 
                                locy !== cameraBlendProperties.LocY ||
                                locz !== cameraBlendProperties.LocZ)){
                    cameraBlendProperties = {LocX: +locx,
                            LocY: +locy,
                            LocZ: +locz,
        // Lookat: lookat,
                            blend : cameraBlendProperties.blend,
                            isNew : true};
                }
        
                return step;                
            }
            function moveGuidePoint(){
                if (incheading !== 0){
                    dheading = (dheading + incheading) % (2*Math.PI);
                    var pointDisp =[-zoomLimits[1]*Math.cos(dheading), 0, +zoomLimits[1]*Math.sin(dheading)],
                        locx = +innerCamera.getLocX() + pointDisp[0], 
                        locy = +innerCamera.getLocY() + pointDisp[1],
                        locz = +innerCamera.getLocZ() + pointDisp[2];
                    if (!(isNaN(locx)||isNaN(locy)||isNaN(locz)) && 
                         (locx !== pointBlendProperties.LocX || 
                          locy !== pointBlendProperties.LocY ||
                          locz !== pointBlendProperties.LocZ)){

                        pointBlendProperties = {LocX: +locx,
                                                LocY: +locy,
                                                LocZ: +locz,
                                                isNew : true};
        /*
         * console.log(first,") guidePoint: ", guidePoint, ", desplazamiento: ",
         * dheading, ", pointDisp:", pointDisp, ", pointBlendProperties",
         * pointBlendProperties);
         */
                    }
                } else if (!pointBlendProperties.blended){
                    guidePoint.blendTo(pointBlendProperties, pointBlendProperties.time);
                    pointBlendProperties.isNew = false;
                }
            }
            this.doTask = function(){
                if (this.zoomSpeed !== 0) {
                    zoom();
                }
                if (this.isLookingAtGuidePoint()){
                    moveGuidePoint();
                    if (pointBlendProperties.isNew){
//                        console.log("pointBlendProperties = ", pointBlendProperties);
                        guidePoint.setLoc(pointBlendProperties.LocX,
                                pointBlendProperties.LocY,
                                pointBlendProperties.LocZ);
                        pointBlendProperties.isNew = false;
                    }
                }

                if (typeof objectToFollow !== "undefined"){
                    var step = follow(), 
                        nextStep;
                    if (cameraBlendProperties.isNew){
                        if (typeof step.last === "undefined" || (GLGE.distanceVec3(step.last, step.next) > 2.6 && !cameraBlendProperties.blend)){
                            // console.log("distancia entre la posicion de
                            // la cámara y la siguiente localización = ",
                            // step, GLGE.distanceVec3(step.last,
                            // step.next));
                            innerCamera.blendTo(cameraBlendProperties, time);
                            cameraBlendProperties.blend = true;
                            cameraBlendProperties.isNew = false;
                        } else {
        // console.log("cameraBlendProperties = ", cameraBlendProperties, ", looking
        // at: ", innerCamera.getLookat());
                            nextStep = step.next;
                            if (cameraBlendProperties.blend){
                                cameraBlendProperties.blend = false;
                                innerCamera.setAnimation();
                            }
                            innerCamera.setLoc(nextStep[0],
                                    nextStep[1],
                                    nextStep[2]);
                            cameraBlendProperties.isNew = false;
                        }
                    }
                } else if (cameraBlendProperties.isNew){
                    innerCamera.blendTo(cameraBlendProperties, time); 
                    cameraBlendProperties.isNew = false;
        // console.log('eeeooo');
                }
                        
        // console.log(cameraBlendProperties, pointBlendProperties, time)
            };
        
            // getter y setters internos
            /**
             * Realiza un zoom continuo de la camara hacia el objeto que se está
             * siguiendo actualmente a una velocidad de speed m/s. El zoom está
             * limitado en un máximo y un mínimo.
             * 
             * @param {Number}
             *            speed velocidad a la que realizar el zoom.
             */
            this.setZoomSpeed = function(speed){
                zoomSpeed = speed;
                console.log("zoomspeed = ", zoomSpeed);
            };
            this.getInnerCamera = function(){
                return innerCamera;
            };
            this.setCameraBlendProperties = function(prop) {
                if (typeof prop === "undefined"){
                    cameraBlendProperties = {
                            isNew : false
                    };
                } else {
                    cameraBlendProperties = prop;
                    cameraBlendProperties.isNew = true;
                }
            };
            this.getCameraBlendProperties = function(){
                return cameraBlendProperties;
            };
            this.isLookingAtGuidePoint = function(){
                return innerCamera.getLookat() === guidePoint;
            };
            this.getGuidePoint = function(){
                return guidePoint;
            };
            this.setGuidePointPosition = function(pos){
                guidePoint.setLoc(pos[0], pos[1], pos[2]);
            };
            this.setGuidePointBlendProperties = function(prop){
                if (typeof prop === "undefined"){
                    pointBlendProperties = {};
                } else {
                    pointBlendProperties = prop;                    
                }
                pointBlendProperties.isNew = true;
            };
            this.getGuidePointBlendProperties = function(){
                return pointBlendProperties;
            };
            this.setObjectToFollow = function(object){
                objectToFollow = object;
            };
            this.setTime = function(t){
                time = t;
            };
            this.stop = function(){
                incheading = 0;
            };
            this.moveLeft = function(){
                incheading = -0.05;
            };
            this.moveRight = function(){
                incheading = +0.05;
            };
            if (typeof callback === "function"){
                callback(this);                
            }
        },
        /**
         * Establece un travelling para la camara en dos parámetros:
         * <ul>
         * <li> Parámetro destPoint: si este parámetro no es null la cámara se se
         * mueve a la posición indicada en el parametro.
         * <li> Parámetro destLookat: si este parámetro no es null la cámara rota la
         * vista desde donde está mirando actualmente hasta donde indica el
         * parámetro.
         * <ul>
         * Este desplazamiento se realiza en un tiempo indicado en el parámetro
         * time.
         * 
         * @param destPoint
         *            punto de destino para mover la cámara.
         * @param destLookat
         *            punto al que va a mirar finalmente la cámara.
         * @param time
         *            tiempo en que se realizará el travelling.
         */
        setTravelling : function(destPoint, destLookat, time){
            var cameraDestination, guidePointDestination;
            if (destPoint !== null){ // propiedades necesarias para que la
                // cámara se mueva al punto destPoint
                cameraDestination = {LocX : destPoint[0], LocY : destPoint[1], LocZ : destPoint[2]};
            }
            
            if (destLookat !== null){// destino del punto guia.
    // console.log("guide position = ", this.getLookat(), ", destination = ",
    // destLookat);
                this.setGuidePointPosition(this.getLookat());
                this.setLookat();// miramos al punto guia
                guidePointDestination = {LocX : destLookat[0], LocY: destLookat[1], LocZ: destLookat[2], time : time};
            }
            this.setFollow(); // eliminamos el objeto a seguir porque la
            // camara va a hacer un travelling
            this.setCameraBlendProperties(cameraDestination);
            this.setGuidePointBlendProperties(guidePointDestination);
            this.setTime(time);
        },
        /**
         * Comprueba si el travelling de la cámara ha finalizado o está en curso.
         * 
         * @returns {Boolean} devuelve true si nunca se estableció ningún travelling
         *          mediante Camera#setTravelling o si este ya ha finalizado o false
         *          en otro caso.
         */
        isTravellingDone : function(){
            var gp = this.getGuidePoint(),
                cam = this.getInnerCamera(),
                cpt = this.getCameraBlendProperties(),
                gpt = this.getGuidePointBlendProperties(),
                travellingDone = true, 
                x, 
                y, 
                z;
            if (typeof gpt !== "undefined" && 
                    typeof gpt.LocX !== "undefined" && 
                    typeof gpt.LocY !== "undefined" && 
                    typeof gpt.LocZ !== "undefined"){
                x = gp.getLocX()-gpt.LocX;
                y = gp.getLocY()-gpt.LocY;
                z = gp.getLocZ()-gpt.LocZ;
                travellingDone = Math.pow((x*x+y*y+z*z), 0.5)<0.1;
            }
            if (typeof cpt !== "undefined" && travellingDone && 
                    typeof cpt.LocX !== "undefined" && 
                    typeof cpt.LocY !== "undefined" && 
                    typeof cpt.LocZ !== "undefined"){
                x = cam.getLocX()-cpt.LocX;
                y = cam.getLocY()-cpt.LocY;
                z = cam.getLocZ()-cpt.LocZ;
                travellingDone = Math.pow((x*x+y*y+z*z), 0.5)<0.1;
            }
            return travellingDone; 
        },
        /**
         * Establece un ojeto al que que la cámara va a seguir a una distancia
         * establecida por el nivel de zoom actual de la camara.
         * 
         * @param object
         *            objeto a seguir.
         */
        setFollow: function(object){
            if (typeof object !== "undefined" && 
                    typeof object.getObjectNode !== "undefined"){
                object = object.getObjectNode();
            }
            this.setTime(200);
            this.setObjectToFollow(object);
            
            if (typeof object === "undefined"){ // si dejamos de seguir a un objeto
                this.setCameraBlendProperties(); // eliminamos las propiedades de
                                                 // animación de la cámara
            }
        },
        /**
         * Devuelve la posición a la que se encuentra mirando la cámara.
         * 
         * @returns {Array} [x,y,z] a la que mira la cámara.
         */
        getLookat : function(){
            var camera = this.getInnerCamera(),
                target = camera.getLookat();
            if (typeof target !== "undefined" && 
                    typeof target.getLocX !== "undefined"){
                target = [target.getLocX(), target.getLocY(), target.getLocZ()];
            }
            return target;
        },
        /**
         * Establece un objetivo (punto o un objeto) al que la camara va a mirar. Si
         * el objetivo se mueve la camara lo seguirá sin variar su posición.
         * 
         * @param {Graphics.DrawableObject}
         *            target objetivo al que mirar.
         */
        setLookat : function(target){
            var camera = this.getInnerCamera();
            if (typeof target !== "undefined"){
                if (typeof target.getObjectNode !== "undefined"){
                    target = target.getObjectNode();
                }
            } else {
                target = this.getGuidePoint();
            }
            camera.setLookat(target);                
        }, 
        /**
         * Devuelve la posición de la camara en un array numérico con el formato:
         * [x,y,z].
         * 
         * @returns {Array} un vector con la posición [x,y,z].
         */
        getPosition : function(){
            var camera = this.getInnerCamera();
            return [camera.getLocX(), camera.getLocY(), camera.getLocZ()];
        },
        /**
         * Establece la posición de la cámara con un vector de posición [x, y, z] o
         * un objeto con {x : {number}, y : {Number}, z: {Number}}.
         * 
         * @param {Array|Object}
         *            position posición de la cámara especificada como un array
         *            [x,y,z] o un objeto position.x, position.y, position.z.
         */
        setPosition : function(position){
            var camera = this.getInnerCamera(),
                x, 
                y, 
                z;
            if (typeof position.x !== "undefined"){
                x = position.x;
                y = position.y;
                z = position.z;
            } else {
                x = position[0];
                y = position[1];
                z = position[2];
            }
            camera.setLoc(x, y, z);
        },
        /**
         * Rota la vista de la camara de dos maneras:
         * <ul>
         * <li> Si la cámara se encuentra siguiendo a un objeto rota alrededor del
         * objeto.
         * <li> TODO Si la cámara no está siguiendo a nadie rota en los ejes XY (si
         * X es el eje horizontal e Y el vertical).
         * <ul>
         * 
         * La rotación sigue una dirección indicada por el parámetro direction. Esta
         * rotación es continua hasta que se indica que se debe detener el
         * movimiento mediante Graphics.Camera.STOPMOVE.
         * 
         * @param {Number}
         *            direction dirección de la rotación:
         *            <ul>
         *            <li> Graphics.Camera.MOVELEFT: gira la vista de
         *            la cámara hacia la izquierda.
         *            <li> Graphics.Camera.MOVERIGHT:gira la vista de
         *            la cámara hacia la derecha.
         *            <li> Graphics.Camera.STOPMOVE: detiene el
         *            movimiento de la camara.
         *            </ul>
         */
        move : function (direction){
    // console.log(direction);
            switch(direction){
            case Graphics.Camera.MOVERIGHT:
                this.moveRight();
                break;
            case Graphics.Camera.MOVELEFT:
                this.moveLeft();
                break;
            case Graphics.Camera.STOPMOVE:
                this.stop();
                break;
            default:
                // NOTHING TO DO;
            }  
        },
        snapshot : function(scene, orix, oriy, width, height){

            var gl = scene.getGLContext(),
                data = new Uint8Array(width * height * 4);

            gl.bindFramebuffer(gl.FRAMEBUFFER, scene.framebuffer);
            gl.readPixels(orix, oriy, width, height, gl.RGBA,gl.UNSIGNED_BYTE, data);
            
            return {
                width: width,
                height : height,
                data :data
            };
        }
    });
    Graphics.Camera.MOVERIGHT = 0;
    Graphics.Camera.MOVELEFT = 1;
    Graphics.Camera.STOPMOVE = 2;

    console.log("Definiendo GraphicEngine.");
    /**
     * Game's Graphic Engine. It draws the scene graph and sets the camera
     * to follow the main character.
     * 
     * @param {Canvas}
     *            canvas the canvas (HTML5) in which the graphics are
     *            depicted.
     */
    Graphics.GraphicEngine = Class.extend({
        init : function(canvas) {
            console.log("dentro de GraphicEngine");
            // primero intentamos utillizar webGL
            var engine = new GLGE.Renderer(canvas),
                viewports = [];
            
            canvas.graphicEngine = this;

            this.getViewPorts = function(){
                return viewports;
            };
            this.getEngine = function(){
                return engine;
            };
            /*
             * Renderiza la escena con los objetos incluidos en la misma hasta el
             * momento.
             */
            this.render = function() {
                if (typeof engine === "undefined"){
                    return true;                
                }
                if (viewports.length > 1){
                    engine.setViewportWidth(this.width);
                    engine.setViewportHeight(this.height);
                    engine.setViewportOffsetX(this.orix);
                    engine.setViewportOffsetY(this.oriy);
                    engine.setScene(this.scene.getObjectNode());
                }
                this.camera.doTask();
                engine.render();
                return this.end;
            };
    /*
     * Este código lo usaba para comprobar si el navegador soporta webgl como de
     * momento no se emplea ninguna alternativa se deja comentado try { this.gl =
     * canvas.getContext("experimental-webgl",props); } catch(e) {} try {
     * if(!this.gl) this.gl = canvas.getContext("webgl",props); } catch(e) {}
     */
        },
        getType : function(){
            return "glge";//TODO comprobar que el navegador soporta webgl y sino devolver el id del engine que usamos para 3d.
        },
        createViewport : function(orix, oriy, width, height, scene, camera){
            var viewport = {
                    orix : orix,
                    oriy : oriy,
                    width : width,
                    height : height,
                    end : false, // utilizado para finalizar la tarea
                    // cuando eliminamos un viewport
                    // (true finalizar, false
                    // continuar).
                    scene : (typeof scene !== "undefined") ? scene : new Graphics.Scene()
                }, 
                viewports = this.getViewPorts(), 
                engine;

            scene.viewport = viewport;
            viewport.task = new CalipsoWebClient.Scheduler.Task(viewport, this.render, true);
            viewports.push(viewport);

            if (typeof camera !== "undefined"){
                viewport.scene.setCamera(camera);    
            } else if (typeof scene.getCamera() === "undefined"){
                viewport.scene.setCamera(new Graphics.Camera());
            } else {
                viewport.camera = scene.getCamera();
            }

            if (viewports.length === 1){
                engine = this.getEngine();
                engine.setViewportWidth(width);
                engine.setViewportHeight(height);
                engine.setViewportOffsetX(orix);
                engine.setViewportOffsetY(oriy);
                engine.setScene(scene.getObjectNode());
            }
            CalipsoWebClient.Scheduler.Scheduler.scheduleGraphicTask(viewport.task);
            // creamos la tarea de renderizado para el viewport
            return viewports.length-1;
        },
        setViewportScene : function(viewportindex, scene){
            var viewports = this.getViewPorts(),
                viewport,
                camera;
            if (typeof viewportindex === "undefined" || viewportindex < 0 || viewports.length <= viewportindex){
                throw new CalipsoWebClient.Exceptions.InvalidViewportIndex(viewportindex);
            }

            viewport = viewports[viewportindex];
            camera = viewport.camera;
            scene.viewport = viewport; 
            viewport.scene = scene;

            if (typeof scene.getCamera() === "undefined"){
                if (typeof camera !== "undefined"){
                    scene.setCamera(camera);
                } else {
                    scene.setCamera(new Graphics.Camera());
                }
            } else {
                viewport.camera = scene.getCamera();
            }

            if (viewports.length === 1){
                this.getEngine().setScene(scene.getObjectNode());
            }
            
        },
        getViewportScene : function(viewportindex){
            var viewports = this.getViewPorts();
            if (viewportindex < 0 || viewports.length <= viewportindex){
                throw new CalipsoWebClient.Exceptions.InvalidViewportIndex(viewportindex);
            }
            
            return viewports[viewportindex].scene; 
        },
        /**
         * Devuelve la camara que usa el GE.
         * 
         * @param {number}
         *            viewportindex indice del viewport
         * @returns {Graphics.Camera} la camara que emplea el motor
         *          gráfico.
         * @throws {CalipsoWebClient.Exceptions.InvalidViewportIndex}
         *             si el indice pasado como parametro no corresponde a ningún
         *             viewport.
         * @see Graphics.Camera
         */
        getViewportCamera : function(/* {number} */viewportindex){
            var viewports = this.getViewPorts();
            if (viewportindex < 0 || viewports.length <= viewportindex){
                throw new CalipsoWebClient.Exceptions.InvalidViewportIndex(viewportindex);
            }
            return viewports[viewportindex].camera;
        },
        removeViewport : function(/*number*/viewportindex){
            var viewports = this.getViewPorts();
            if (viewportindex < 0 || viewports.length <= viewportindex){
                throw new CalipsoWebClient.Exceptions.InvalidViewportIndex(viewportindex);
            }
            viewports[viewportindex].end = true;
            viewports.splice(0, 1, viewportindex);
        }
    });
    Graphics.Document = Class.extend({
        init: function(data, callbacks){
            GLGE.Document.call(this);
            this.data = data;
            this.onLoad = callbacks.onSuccess;
            this.onError = callbacks.onError;
            var parser = new DOMParser();
            
            this.getParser = function (){
                return parser;
            };
            this.getGraphicObject = function(id, type){
                return this.getElement(id, true, type, 0);
            };
            
        },
        load: function(){
            var data = this.data;
            if (this.parseScript(data) === null && !this.parseString(data)){
                this.loadDocument(data);                    
            }
            return this;
        },
        loadDocument : function(url,relativeto){
            this.loadCount++;
            url=this.getAbsolutePath(url,relativeto);
            
            if(this.preloader){
                this.preloader.loadXMLFile(url);
            } else {
                var req = new XMLHttpRequest();
                if(req) {
                    req.docurl=url;
                    req.docObj=this;
                    req.overrideMimeType("text/xml");
                    req.onreadystatechange = function() {
                        if(this.readyState  === 4){
                            if(this.responseXML !== null && (this.status  === 200 || this.status===0)){
                                this.responseXML.getElementById=this.docObj.getElementById;
                                this.docObj.loaded(this.docurl,this.responseXML);
                            } else { 
                                this.docObj.onError((this.responseXML !== null) ? this.status : null);
                            }
                        }
                    };
                    req.open("GET", url, true);
                    req.send("");
                }
            }   
        },
        getElement: function(ele, noerrors, type, index){
            var docele,
                doc;
            if(typeof(ele)==="string"){
                for( doc in this.documents){
                    if(this.documents[doc].xml){
                        if (type === true){
                            docele=this.documents[doc].xml.getElementsByTagName(ele);
                            if(docele){
                                ele=docele[index];
                                break;
                            }
                        } else {
                            docele=this.documents[doc].xml.getElementById(ele);
                            if(docele){
                                ele=docele;
                                break;
                            }
                        }
                    }
                }
            }
            if(typeof(ele)==="string"){
                // if element is still a string at this point there there is
                // an issue
                if(!noerrors) {
                    console.log("Element "+ele+" not found in document");
                }
                return false;
            } else {
                if(this["get"+this.classString(ele.tagName)]){
                    return this["get"+this.classString(ele.tagName)](ele);
                } else {
                    return this.getDefault(ele);
                }
            }
        },
        parseString : function(str) {
            var parser = this.getParser(), 
                imports, 
                xmlDoc, 
                i;
            
            this.rootURL = window.location.toString();
            xmlDoc = parser.parseFromString(str, "text/xml");
            if (xmlDoc.documentElement.nodeName!=="glge") {
                return false;
            }
            xmlDoc.getElementById = this.getElementById;
            this.documents["#script"] = {
                    xml : xmlDoc
            };
            imports = xmlDoc.getElementsByTagName("import");
            for (i = 0; i < imports.length; i++) {
                if (!this.documents[this.getAbsolutePath(imports[i].getAttribute("url"))]) {
                    this.documents[this.getAbsolutePath(imports[i].getAttribute("url"))] = {};
                    this.loadDocument(imports[i].getAttribute("url"));
                }
            }
            if (this.loadCount === 0) {
                this.finishedLoading();
            }
            return true;
        }
    });
    Graphics.Document.augment(GLGE.Document);

    Graphics.Mesh = Class.extend({
        init : function(mesh){
            var a, i;
            GLGE.Mesh.call(this, a, mesh.WINDING_ORDER);
            this.positions = mesh.positions;
            this.framePositions = mesh.framePositions;

            this.tangents = mesh.tangents;
            this.frameTangents = mesh.frameTangents;
            this.normals = mesh.normals;
            this.frameNormals = mesh.frameNormals;
            this.boundingVolume = mesh.boundingVolume;
            this.uv1set = mesh.uv1set;
            this.uv2set = mesh.uv2set;
            this.UV = mesh.UV;
            this.colors = mesh.colors;
            this.loaded = true;
            this.faces = mesh.faces;
            this.faces.GL = false;
            for (i in mesh.buffers){
                this.buffers.push($.extend({}, mesh.buffers[i], {GL:false}));
            }
        }
    });

    Graphics.Mesh.augment(GLGE.Mesh);
    
    Graphics.DrawableObject = Class.extend({
        init : function(data, id, callbacks, eventType) {
            var ready = false,
                error = false,
                doc,
                mesh,
                that = this,
                readyfunc = function(){
                    var objectnode;
                    if (!ready){
                        ready = true;
                        objectnode = that.objectnode;
                        that.setBIdGetter(function(){
                            return this.berserkerid;
                        });
                        if (typeof objectnode.getMesh !== "undefined"){
                            mesh = new Graphics.Mesh(objectnode.getMesh());
//                            console.log(mesh, objectnode.getMesh());
                            objectnode.setMesh(mesh);
                        }
                        if (typeof callbacks !=="undefined"){
                            if (typeof callbacks.onSuccess === "function") {
                                callbacks.onSuccess(that);
                            } else if (typeof callbacks === "function") {
                                callbacks(that);
                            }
                        }
                    }
                };
            
            this.unsetReady = function () {
                ready = false;
            };
            this.setReady = function () {
                ready = true;
            };
            this.isReady = function() {
                return ready;
            };
            this.getMesh = function() {
                return mesh;
            };
            this.getDocument = function() {
                return doc;
            };
            if (typeof data === "string"){
                doc = new Graphics.Document(data, {
                    onSuccess : function(){
                        that.objectnode = doc.getGraphicObject(id, false);
                        if (!that.objectnode){
                            that.objectnode = doc.getGraphicObject(id, true);
                            if (!that.objectnode){
                                ready = true;
                                error = true;
                                if (typeof callbacks !==" undefined" && typeof callbacks.onError === "function"){
                                    callbacks.onError();
                                }
                            }
                        }
                        if (typeof callbacks !== "undefined" && (
                            typeof that.objectnode.meshCache === "undefined" ||
                            typeof that.objectnode.meshCache[that.objectnode.url] !== "undefined")){
                            readyfunc();
                        } else {
                            if (typeof eventType === "undefined"){
                                eventType = "loaded";
                            }
                            that.objectnode.addEventListener(eventType, readyfunc);                            
                        }
                    },
                    onError : function(requestStatus){
                        ready = true;
                        error = true;
                        if (typeof callbacks !==" undefined" && typeof callbacks.onError === "function") {
                            callbacks.onError(requestStatus);
                        }
                    }
                });
                doc.load();
            } else {
                this.objectnode = data;
                readyfunc();
            }
        },
        shaderSet : false,
        getObjectNode : function() {
            return this.objectnode;
        },
        setBId : function(id){
            this.objectnode.berserkerid = id;
        },
        setBIdGetter : function (getter){
            this.objectnode.getBId = getter;
        },
        getBId : function(){
            return this.objectnode.berserkerid;
        },
        /**
         * Set the position of the object.
         * 
         * @param {Array}
         *            pos a vector with the coordinates x,y,z values
         */
        getPosition : function(){
            return this.objectnode.getPosition();
        },
        setPosition : function(x,y,z) {
            if (toType(x) === "array"){
                y = x[1];
                z = x[2];
                x = x[0];
            }
            this.objectnode.setLoc(+x, +y, +z);
        },
        move : function (incpos){
            this.objectnode.move(incpos);
        },
        blendTo : function(params, time){
            this.objectnode.blendTo(params, time);
        },
        setSkeleton : function (bone){
            this.objectnode.setSkeleton(bone.getObjectNode());
        },
        setJoints : function (joints){
            var mesh = this.getMesh();
            if (typeof mesh !== "undefined"){
                mesh.setJoints(joints);
            }
        },
        setInvBindShapeMatrix : function(invShapeMat){
            var mesh = this.getMesh();
            if (typeof mesh !== "undefined"){
                mesh.setInvBindMatrix(invShapeMat);  
            }
        },
        setVertexJoints : function(vertexJointsIds, num){
            var mesh = this.getMesh();
            if (typeof mesh !== "undefined"){
                mesh.setVertexJoints(vertexJointsIds, num);
            }
        },
        setVertexWeights : function (vertexweights, num, pieces){
            var mesh = this.getMesh();
            if (typeof mesh !== "undefined"){
                mesh.setVertexWeights(vertexweights, num, pieces);                
            }
        },
        getScale : function(){
            return this.objectnode.getScale();
        },
        setScale : function(scale){
            this.objectnode.setScale(scale.x, scale.y, scale.z);
        },
        getScaleMatrix : function(){
            return this.objectnode.getScaleMatrix();
        },
        setRotMatrix : function (rotationMatrix){
            this.objectnode.setRotMatrix(rotationMatrix);
        },
        setMaterial : function (material){
            this.objectnode.setMaterial(material.objectnode);
        },
        getRotMatrix : function() {
            return this.objectnode.getRotMatrix();
        },
        setRot : function (rotvec){
            if (rotvec.length >3){
                this.objectnode.setQuat(rotvec[0], rotvec[1], rotvec[2], rotvec[3]);
            } else {
                this.objectnode.setRot(rotvec[0], rotvec[1], rotvec[2]);                    
            }
        },
        setRotY : function(roty){
            this.objectnode.setRotY(roty);
        },
        getRot : function(quat){
            if (quat){
                return [this.objectnode.getQuatX(), this.objectnode.getQuatY(), this.objectnode.getQuatZ(), this.objectnode.getQuatW()];
            } else {
                return [this.objectnode.getRotX(), this.objectnode.getRotY(), this.objectnode.getRotZ()];
            }
        },
        getVertexs: function(){
            var vertex = [], 
                mesh = this.getMesh();
            if (typeof mesh !== "undefined"){
                vertex = mesh.positions;
            }
            return vertex; 
        },
        getVertex : function(id){
            return this.getMesh().positions[id];
        },
        getModelMatrix : function(){
            return this.objectnode.getModelMatrix();
        },
        getTranslateMatrix : function(){
            return this.objectnode.getTranslateMatrix();
        },
        setVertexShaderInjection: function(glsl_shader){
            this.objectnode.setVertexShaderInjection(glsl_shader);
            this.shaderSet = true;
        },
        isShaderInjected : function(){
            return this.shaderSet;
        }, 
        setBuffer : function(bufferName,jsArray,size,exclude){
            this.getMesh().setBuffer(bufferName, jsArray, size, exclude);
        },
        setUniform : function(type, name, value){
            this.objectnode.setUniform("Matrix4fv",name, value);
            
        },
        getGLContext : function(){
            return this.objectnode.gl;
        },
        getFrameRate : function(){
            return this.objectnode.getFrameRate();
        }
    });
    Graphics.Material = Graphics.DrawableObject.extend({
        init : function(data, callbacks){
            if (typeof data === "undefined"){
                data = new GLGE.Material();
            }
            if (typeof callbacks !== "undefined"){
                callbacks.now = true;
            }
            this.$super(data, "material", callbacks);
        },
        setColor : function(color){
            var done = false;
            if (this.objectnode){
                this.objectnode.setColor(color);
                done = true;
            }
            return done;
        }
    });
    Graphics.Group = Graphics.DrawableObject.extend({
        init : function(id){
            this.$super(new GLGE.Group(), id);
            this.setBId(id);
        },
        downloadComplete : function(){
            return this.objectnode.downloadComplete();
        },
        compose : function (drawableObject){
            if (typeof drawableObject !== "undefined"){
                if (typeof drawableObject.getObjectNode !== "undefined"){
                    drawableObject = drawableObject.getObjectNode();
                }
                this.objectnode.addChild(drawableObject);
            } else {
                console.log('Intento de añadir un objeto no definido al grupo:', this);
            }
        },
        remove : function (drawableObject){
            this.objectnode.removeChild(drawableObject.getObjectNode());
        },
        removeAny : function (drawableObjects){
            var obj = this.objectnode;
            $.each(drawableObjects, function(i, e){
                obj.removeChild(e.getObjectNode());
            });
        },
        getName : function(){
            return this.objectnode.getName();
        },
        setName : function(name){
            return this.objectnode.setName(name);
        }
    });
    Graphics.Scene = Graphics.DrawableObject.extend({
        init : function(data, callbacks){
            if (typeof data === "undefined"){
                data = new GLGE.Scene();
            }
            if (typeof callbacks !== "undefined"){
                callbacks.now = true;
            }
            this.$super(data, "scene", callbacks, "downloadComplete");
        },
        setCamera : function (camera){
            if (typeof this.viewport !== "undefined"){
                this.viewport.camera = camera;
            }
            this.camera = camera;
            this.objectnode.camera = camera.getInnerCamera();
        },
        getCamera : function(){
            return this.camera;
        }, 
        pick : function(x, y){
            return this.objectnode.pick(x,y);
        }
    });
    Graphics.Scene.augment(Graphics.Group);
    console.log('definiendo AnimatableObject');
    /**
     * 
     * Class representing an object with a set of animations. The object's
     * animations and model are contained in a MD2 model structure [1].
     * 
     * References:
     * 
     * <dl compact>
     * <dt>[1]
     * <dd>http://en.wikipedia.org/wiki/MD2_%28file_format%29
     * </dl>
     * 
     * @param {String}
     *            serializedObject JSON serialized data with the MD2 model.
     * @param {String}
     *            id the id of the object
     * @param {Graphics.MotionGraph}
     *            motionGraph the motionGraph to control the animation transitions
     * @param {Function}
     *            readyfunc, a function called when the object has been loaded.
     */
    Graphics.AnimatableObject = Graphics.DrawableObject.extend({
        init : function(data, id, animType, motionGraph, callbacks) {
            /*
             * especie o genero del personaje; se utiliza para decidir el
             * conjunto de animaciones que posee.
             */
            var type = animType,
                Scheduler = CalipsoWebClient.getScheduler(),
                animate = this.animate, 
                motionTask = new CalipsoWebClient.Scheduler.Task(this, animate, true),
                animCallbacks = {
                    onSuccess : function(){
//                        Scheduler.scheduleMotionTask(motionTask);
                        if (typeof callbacks !== "undefined" &&
                            typeof callbacks.onSuccess === "function"){
                            callbacks.onSuccess();
                        }
                    }
                };
            this.setMotionTask = function(){
                if (!Scheduler.hasTask(motionTask)){
                    Scheduler.scheduleMotionTask(motionTask);
                }
            };
            if (typeof callbacks !== "undefined" &&
                typeof callbacks.onError !== "undefined"){
                animCallbacks.onError = callbacks.onError;                
            }
            this.$super(data, id, animCallbacks);
            this.change = true;
            this.motionGraph = motionGraph;
            this.defaultState = {
                    id : this.motionGraph.getDefaultMotions(type)[0],
                    constraints : {}
            };

            $.extend(this.defaultState, this.motionGraph.getMotionFromId(this.defaultState.id), {
                childs : null,
                reset : true
            });
            /*
             * Cola de estados de animacion. Empleada en el metodo animate
             * para saber que animaciones se deben ejecutar en cada
             * instante. El componente MotionManager crea MotionStates a
             * partir del método getMotionState().
             */
            this.motionStateQueue = [this.defaultState];

            this.getType = function() {
                return type;
            };
        },
        doAnimation : function(){
            return false; // funcion para sobrecargar en los hijos
        },
        animate : function() {
            try {
                if (!this.change){
                    return false;
                }
                this.change = false;
                var motion = this.motionStateQueue[this.motionStateQueue.length - 1];
                if (typeof motion !== "undefined"){
                    this.doAnimation(motion);
                }
                return false;// FIXME ¿cuando deja de
                // animarse el personaje?
            } catch (e) {
                console.log(e.toString(), e.stack);
            }
        },
        setMotion : function(motion) {
            try {
                if (motion.reset && motion.level < this.motionStateQueue.length - 1) {
                    this.motionStateQueue = this.motionStateQueue.slice(0, motion.level);
                    console.log('setMotion: 1');
                    this.change = true;
                } else {
                    if (this.motionStateQueue.length - 1 <= motion.level) {
                        if (this.motionStateQueue.length > 0 && !this.motionStateQueue[this.motionStateQueue.length - 1].loop) {
                            console.log('AnimatableObject.setMotion(): lastMotion = ', !this.motionStateQueue[this.motionStateQueue.length - 1],', motion = ',motion);
                            this.motionStateQueue.splice(this.motionStateQueue.length - 1,0, motion);
                            console.log('setMotion: 2');
                        } else {
                            console.log('setMotion: 3');
                            this.motionStateQueue.push(motion);
                            this.change = true;
                        }
                    } else {
                        console.log('setMotion: 4');
                        this.motionStateQueue.splice(motion.level, 1, motion);
                    }
                }
                if (motion.level === 0) {
                    this.defaultState = motion;
                }
            } catch (e) {
                console.log(e);
            }
            
        },
        getPresentMotion : function() {
            return (this.motionStateQueue.length > 0) ? this.motionStateQueue[this.motionStateQueue.length - 1]
            : this.defaultState;
        },
        /**
         * Starts the action
         * "action". The action is enqueued in the object's motion queue.
         * 
         * @param {string}
         *            action skill.
         */
        startMotion : function(action, actionData) {
            try {
                var i = this.motionStateQueue.length - 1,
                    presentMotion,
                    motion;
                for (; i > 0; i--){
                    if (this.motionStateQueue[i].loop){
                        break;
                    }
                }
                presentMotion = (i >= 0 && this.motionStateQueue.length > 0) ? this.motionStateQueue[i] : this.defaultState;
                motion = this.motionGraph.getMotionFromAction(presentMotion, action, actionData);
                console.log('AnimatableObject.startMotion(): ', motion);
                this.setMotion(motion);
            } catch (e) {
                console.log(e);
            } finally {
                // NOTHING TO DO
            }
        },
        
        /**
         * Finishes a character's ongoing motion if it is a loop motion. The
         * method's parameter needs to come from a call to the method
         * AnimatableObject.getQueueMotionFromAction();
         * 
         * @param {Object}
         *            queueMotion, an object with the following structure:
         *            {{number} index, {Boolean} loop }.
         */
        endLoopMotion : function(queueMotion) {
            try {
                var i = queueMotion.index, 
                    presentMotion, 
                    motionAction,
                    constraints;
                if (this.motionStateQueue[i].loop) {
                    this.motionStateQueue.splice(i, 1);
                    this.change = true;
                }
                if (i < this.motionStateQueue.length) {
                    console.log('endLoopMotion.i === ', i);
                    presentMotion = this.motionStateQueue[i - 1];
                    for (; i < this.motionStateQueue.length; i++) {
                        motionAction = this.motionStateQueue[i].action;
                        constraints = this.motionStateQueue[i].constraints;
                        presentMotion = this.motionGraph.getMotionFromAction(presentMotion, motionAction, constraints);
                        if (presentMotion.id === this.motionStateQueue[i].id) {
                            break;
                        }
                        this.motionStateQueue.splice(i, 1, presentMotion);
                    }
                }
            } catch (e) {
                console.log(e);
            }
        },
                
        /**
         * The present object's motion is interrupted playing the named
         * interruptMotion and setting a basic motion state for the object.
         * The basic motion state is created according to the actual basic
         * state of the object
         * 
         * @param {string}
         *            interruptMotion the interruption motion to play before
         *            setting the basic state.
         */
        interruptMotion : function(interruptionMotionId) {
            try {
                this.setMotion(this.defaultState);
                var interruptionMotion = this.motionGraph.getMotionFromId(interruptionMotionId);
                this.setMotion(interruptionMotion);
            } catch (e) {
                console.log(e);
            }
        },
        getQueueMotionFromAction : function(action) {
            var i = 1,
                l = this.motionStateQueue.length;
            for ( ; i < l; i++) {
                if (this.motionStateQueue[i].action === action) {
                    break;
                }
            }
            return (i < l) ? {
                index : i,
                loop : this.motionStateQueue[i].loop
            } : false;
        },
        setAnimation : function(anim){
            this.objectnode.setAnimation(anim);
        }
    });
    Graphics.Bone = Graphics.Group.extend({
        init : function(drawableObject, boneDefinition){
            this.$super(boneDefinition.id);
            this.setName("rootBone");
            this.skinningJoints = [this];
            this.invBindShapeMatrix = [this.identMatrix];
            this.invBindShapeBone = GLGE.Mat4(this.identMatrix);
            this.childs = {};
            this.setDrawableObject(drawableObject, boneDefinition);
            this.objectsInBone = [];
        },
        identMatrix : [1,0,0,0, 
                       0,1,0,0,
                       0,0,1,0,
                       0,0,0,1],
        jointVertexShader : // XXX ESTO NO SE USA.
                              ['#ifdef GLGE_VERTEX\n',
                               '    uniform mat4 stitchMat;\n',
                               '    vec4 GLGE_Position(vec4 pos){\n', // pos es la
                                                                        // posición
                                                                        // del
                                                                        // vértice
                                                                        // una vez
                                                                        // hecha la
                                                                        // trasformación
                               '        if (isStitchVertex == 1.0){\n',
                               '            return pos*stitchMat;\n',
                               '        }',
                               '        return pos;\n',
                               '    }\n',
                               '#endif\n'
                               ],
        getSkinJointId : function(skinningJoints){
            var skinJointId = $.inArray(this, skinningJoints);
            if (skinJointId < 0){
                skinJointId = this.skinningJoints.length;
            }
            return skinJointId;
        },
        getDrawableObject : function(){
            return this.drawableObject;
        },
        getInverseBindingMatrix : function(){
            return this.invBindShapeBone;
        },
        setPosition : function(x, y, z) {
            this.$super(x,y,z);
            var pos = this.getTranslateMatrix();
            this.invBindShapeBone[3] = -1*pos[3]; 
            this.invBindShapeBone[7] = -1*pos[7]; 
            this.invBindShapeBone[11] = -1*pos[11];
            console.log("pos = ", pos, "invpos = ", this.invBindShapeBone);
        },
        /**
         * This method finds in the drawable object associated to this bone
         * the vertexs which needs to be in the influence list. The
         * parameters specifies the vertexs to find. If withparent is true,
         * finds the drawable object vertex which are associated with the
         * parent bone's joint. If jointid is true, this method finds the
         * drawable object vertex which are associated with each child
         * joint. If jointid is a string, this method finds the drawable
         * object vertexs which are associated with the joint specified.
         * 
         * The search for the vertexs in the drawable is done with two
         * methods: the envelop and the vertexs list. The envelop specifies
         * a searching distance from the bone position in [x, y, z]. Each
         * drawable object vertex which are
         * 
         * The search results are stored in this.parent.vertexsIds if
         * withparent is true, and in this.childs[jointid].vertexsIds for
         * jointid.
         * 
         * @param {boolean}
         *            withparent if true this methods finds the vertexs of
         *            the drawables object associated with the parent bone.
         * @param {string|boolean}
         *            jointid if true finds the vertexs in the drawable
         *            which are associated with every joint in the child
         *            bones. If it is a string, finds the vertexs in the
         *            drawable associated with the specified joint.
         */
        findVertexPairing : function (/* {boolean} */withparent, /* {string|boolean} */withjoint){
            var dobj = this.getDrawableObject(), 
                parentJoint,
                parentVertexsIds,
                childJoint,
                childVertexsIds,
                childs,
                jointList, 
                jointid = withjoint, 
                idx,
                objectsInBone = [];
            if (withparent && typeof this.parent !== "undefined"){
                parentJoint = this.getJointList()[this.parent.jointid];
                if (typeof parentJoint === "undefined"){
                    throw new CalipsoWebClient.Exceptions.BoneJointUndefined(this.parent.bone, this, this.parent.jointid);
                }
                parentVertexsIds = [];
                if (typeof parentJoint.envelope !== "undefined"){
                    parentVertexsIds = parentVertexsIds.concat(findVertexIdsIntoEnvelope(parentJoint.jointPosition, dobj.getVertexs(), parentJoint.envelope));
                    console.log("sacando vertices de ", this.parent.jointid, " con método de envelope para: ", this.getBId(), parentVertexsIds);
                } 
                
                if (typeof parentJoint.jointVertexs !== "undefined"){
                    $.each(parentJoint.jointVertexs, function(i, vertex){
                        parentVertexsIds = parentVertexsIds.concat(findVertexIds(vertex, dobj.getVertexs(), 0.1));
                    });
//                    console.log("sacando vertices de ", this.parent.jointid, " con método lista de vértices para: ", this.getBId(), parentVertexsIds);
                }
                this.parent.vertexsIds = parentVertexsIds;
            }

            if (typeof jointid === "string"){
                childJoint = this.getJointList()[jointid];
                if (typeof childJoint === "undefined"){
                    throw new CalipsoWebClient.Exceptions.BoneJointUndefined(this, this.childs, jointid);
                }
                
                childVertexsIds = [];
                if (typeof childJoint.envelope !== "undefined"){
                    childVertexsIds = childVertexsIds.concat(findVertexIdsIntoEnvelope(childJoint.jointPosition, dobj.getVertexs(), childJoint.envelope));
                    console.log("sacando vertices de ", jointid, " con método de envelope para ", this.getBId(), ":", childVertexsIds);
                }
                if (typeof childJoint.jointVertexs !== "undefined"){
                    $.each(childJoint.jointVertexs, function(i, vertex){
                        childVertexsIds = childVertexsIds.concat(findVertexIds(vertex, dobj.getVertexs(), 0.1));
                    });
//                    console.log("sacando vertices de ", jointid, " con método de lista de vertices para ", this.getBId(), ":", childVertexsIds);
                }
                this.childs[jointid].vertexsIds = childVertexsIds;
            } else if (jointid){
                childs = this.childs;
                jointList = this.getJointList();
                childVertexsIds = [];
                for (jointid in jointList){
                    childJoint = jointList[jointid];
                    if (typeof childJoint.envelope !== "undefined"){
                        childVertexsIds = childVertexsIds.concat(findVertexIdsIntoEnvelope(childJoint.jointPosition, dobj.getVertexs(), childJoint.envelope));
                        console.log("sacando vertices de ", jointid, " con método de envelope para ", this.getBId(), ":", childVertexsIds);
                    }
                    if (typeof childJoint.jointVertexs !== "undefined"){
                        for (idx in childJoint.jointVertexs){
                            childVertexsIds = childVertexsIds.concat(findVertexIds(childJoint.jointVertexs[idx], dobj.getVertexs(), 0.1));
                        }
                        console.log("sacando vertices de ", jointid, " con método de lista de vertices para ", this.getBId(), ":", childVertexsIds);
                    }
                    if (typeof childs[jointid] !== "undefined"){
                        childs[jointid].vertexsIds = childVertexsIds;
                    }
                }
            }
            
            /*
             * encontrar los vertices del padre que emparejan con los del
             * hijo y viceversa, no se usa por que no se modifican los mesh
             * de los drawables. Se deja aquí por si en un futuro se deciden
             * fusionar estos puntos. var dobjVertexs =
             * this.getDrawableObject().getVertexs(); childDrawableObject =
             * childDrawableObject.getVertexs(); var vertexPairing = {
             * parent : new Array(), child : new Array() };
             * 
             * childVertexList = childVertexList.slice(0); //copiamos el
             * array de hijos para no modificarlo $.each (parentVertexList,
             * function(i, parentVertex){ var minDistance = 10000.0; var
             * minChildVertexId; $.each(childVertexList, function(j,
             * childVertex){ var dist = GLGE.distanceVec3(parentVertex,
             * childVertex); if (dist < minDistance){ minChildVertexId = j;
             * minDistance = dist; } });
             * 
             * var parentVertexsIds = findVertexIds (parentVertex,
             * dobjVertexs, 0.01); var childVertex =
             * childVertexList.splice(minChildVertexId, 1)[0]; var
             * childVertexsIds = findVertexIds (childVertex,
             * childDrawableObject, 0.01);
             * 
             * vertexPairing.child.push({ vertexs : parentVertexsIds,
             * position : childVertex, origin : parentVertex
             * 
             * }); vertexPairing.parent.push({ vertexs : childVertexsIds,
             * position : parentVertex, origin : childVertex }); }); return
             * vertexPairing;
             */
        },
        /**
         * This method creates an influence list which contains the joints
         * influencing each vertex in the bone's drawable.
         */
        createJointsInfluenceList : function (){
            var vertexJointsIds = [],
                jointsWeights = [],
                dobj = this.getDrawableObject(),
                numvertexs = dobj.getVertexs().length/3,
                jointId = 0,
                i;
            for (i = 0; i<numvertexs; i++){
                vertexJointsIds.push(jointId);
                jointsWeights.push(1.0);
            }
            this.vertexsJointList = {
                    /**
                     * Joint list. This list contains for each vertex
                     * <i>numJoints</i> joints's ids which have influence
                     * in it.
                     */
                    joints : vertexJointsIds,
                    /**
                     * The normalized weight for each joint in each vertex.
                     * It contains <i>numJoints</i> weights for each
                     * vertex, which are a number between 0 and 1.0
                     * representing how much a joint influences in the
                     * vertex.
                     */
                    weights : jointsWeights,
                    /**
                     * Number of vertexs in the bone's drawable.
                     */
                    numVertexs : numvertexs,
                    /**
                     * Number of joints per vertex.
                     */
                    numJoints : 1,
                    /**
                     * This method looks for a joint in the joint list
                     * starting at vertexjointstartpos and returns its
                     * position or -1 if it is not found.
                     * 
                     * @param {number}
                     *            joint joint id to search.
                     * @param {number}
                     *            vertexjointstartpos absolute position
                     *            (vertex_number*numJoints + joint_pos) to
                     *            start the search.
                     * @return the joint position.
                     */
                    hasJoint : function (/* {number} */ joint, /* {number} */ vertexjointstartpos){
                        var i = vertexjointstartpos, 
                            max = vertexjointstartpos+this.numJoints;
                        for (; i<max ; i++){
                            if (this.joints[i] === joint){
                                break;
                            }
                        }
                        if (i>=max) {
                            i = -1;
                        }
                        return i;
                    },
                    /**
                     * This methods gets the number of joints in the
                     * jointlist parameter which are already in the joint
                     * list.
                     * 
                     * @param {Array}
                     *            the array which contains the ids to search
                     *            in the joint list.
                     * @return the number of joints in the joint list which
                     *         was in the array passed as parameter.
                     */
                    howManyJointsInList : function(/* {Array} */ jointlist){
                        var numJoints = 0,
                            i = 0,
                            n = jointlist.length;
                        for (; i<n; i++){
                            if (this.hasJoint(jointlist[i], 0)>-1) {
                                numJoints++; 
                            }
                        }
                        return numJoints;
                    }
            };
        },  
        includeJointsinInfluenceList : function (newjoints, newweights, vertexsList){
            var numvertexs = this.vertexsJointList.numVertexs,
                myjoints = this.vertexsJointList.joints,
                myweights = this.vertexsJointList.weights,
                numjoints = newjoints.length,
                i,
                j,
                jointlistindexdesp,
                joint,
                vertexjointpos,
                numJointinVertexsList = this.vertexsJointList.numJoints + numjoints - this.vertexsJointList.howManyJointsInList(newjoints);
            
            if (typeof vertexsList === "undefined"){
                for (i = 0; i<numvertexs; i++){
                    jointlistindexdesp=0;
                    for (j = 0; j<numjoints; j++){
                        joint = newjoints[j];
                        vertexjointpos = this.vertexsJointList.hasJoint(joint, i*numJointinVertexsList+jointlistindexdesp);
                        if (vertexjointpos > -1){
                            myweights.splice(vertexjointpos, 1, newweights[j]);
                        } else {
                            myjoints.splice(i*numJointinVertexsList, 0, joint);
                            myweights.splice(i*numJointinVertexsList, 0, newweights[j]);
                            jointlistindexdesp++;
                        }
                    }
                }
            } else {
                for (i = 0; i<numvertexs; i++){
                    if ($.inArray(i, vertexsList) > -1){
                        jointlistindexdesp=0;
                        for (j = 0; j<numjoints; j++){
                            joint = newjoints[j];
                            vertexjointpos = this.vertexsJointList.hasJoint(joint, i*numJointinVertexsList+jointlistindexdesp);
                            if (vertexjointpos > -1){
                                myweights.splice(vertexjointpos, 1, newweights[j]);
                            } else {
                                myjoints.splice(i*numJointinVertexsList, 0, joint);
                                myweights.splice(i*numJointinVertexsList, 0, newweights[j]);
                                jointlistindexdesp++;
                            }
                        }
                    } else {
                        jointlistindexdesp=0;
                        for (j = 0; j<numjoints; j++){
                            joint = newjoints[j];
                            vertexjointpos = this.vertexsJointList.hasJoint(joint, i*numJointinVertexsList+jointlistindexdesp);
                            if (vertexjointpos < 0){
                                myjoints.splice(i*numJointinVertexsList, 0, joint);
                                myweights.splice(i*numJointinVertexsList, 0, 0.0);
                                jointlistindexdesp++;
                            }
                        }
                    }
                }
            }
            this.vertexsJointList.numJoints = numJointinVertexsList;
        },
        /*
         * @private
         */
        setParentBone : function(parentBone, jointPos, jointOri, jointid){
            if (!(parentBone instanceof Graphics.Bone)) {
                return false;
            }
            this.parent = {
                    bone : parentBone,
                    jointPosition : jointPos,
                    jointid : jointid,
                    jointOrientation : jointOri
            };
            // aquí recalculamos los ids de los joints de influencia para
            // este hueso
            this.findVertexPairing(true, false);
            var skinningJointId = parentBone.getSkinJointId(this.skinningJoints),
                invBindShapeBonei = this.getTranslateMatrix();  
            if (skinningJointId < this.skinningJoints.length){
                this.invBindShapeMatrix.splice(skinningJointId, 1, invBindShapeBonei);
            } else {
                this.skinningJoints.push(parentBone);
                this.invBindShapeMatrix.push(invBindShapeBonei);
            }
            this.fixBone();
        },
        setChildBone : function(childBone, jointid){
            var parentJoint = this.getJointList()[jointid],
                skinningJointId = this.skinningJoints.length,
                deletedBone,
                nj,
                invBindShapeBonei;
            if (typeof parentJoint === "undefined") {
                return false;
            }
    // var group = this.getObjectNode();
            if (typeof this.childs[jointid] !== "undefined"){
    // group.removeChild(this.childs[jointid].getObjectNode());
                this.remove(this.childs[jointid].bone);
                deletedBone = this.childs[jointid].bone;
                delete deletedBone.parent;// eliminar la referencia al
                // padre.
                skinningJointId = deletedBone.getSkinningJointId(this.skinningJoints);
                deletedBone.fixBone();
            }
    // var childGroup = childBone.getObjectNode();
    // childGroup.setName(jointid);
            childBone.setName(jointid);
            this.compose(childBone);
    // group.addChild(childGroup);
            this.childs[jointid] = {
                    bone : childBone
            };

            this.findVertexPairing(false, jointid);
            this.includeJointsinInfluenceList([skinningJointId], [1.0], this.childs[jointid].vertexsIds);
            
            nj = this.vertexsJointList.numJoints;
            this.drawableObject.setVertexJoints(this.vertexsJointList.joints, nj);
            this.drawableObject.setVertexWeights(this.vertexsJointList.weights, nj);
            /*
             * aqui el mesh actua como hijo, por eso se
             * le pasa la lista de identificadores de
             * los vertices del hijo.
             */                
            childBone.setParentBone(this, parentJoint.jointPosition, parentJoint.jointOrientation, jointid); 
            /* esta matriz translacion debería incluir el desplazamiento de los padres*/
            invBindShapeBonei =  childBone.getInverseBindingMatrix();
            if (skinningJointId < this.skinningJoints.length){
                this.skinningJoints.splice(skinningJointId, 1, childBone);
                this.invBindShapeMatrix.splice(skinningJointId, 1, invBindShapeBonei);
            } else {
                this.skinningJoints.push(childBone);
                this.invBindShapeMatrix.push(invBindShapeBonei);
            }
            return true;
        },
        fixBone : function(){
            var parentJoint, 
                parentBone, 
                jointOri, 
                nj, 
                drawableObject = this.getDrawableObject(), 
                mypos, 
                that = this;

            if (typeof this.parent === "undefined"){
                this.setPosition(0,0,0);
                if (!Ut.hasProperties(this.childs)){
                    return;
                }
            } else {
                parentJoint = (typeof this.parent.jointPosition === "undefined") ? [0,0,0] : this.parent.jointPosition;
                parentBone = this.parent.bone;
                jointOri = this.parent.jointOrientation;
                mypos = [parentJoint[0], 
                         parentJoint[1], 
                         parentJoint[2]]; 
                that = this;
                if (typeof jointOri !== "undefined"){
                    console.log("estableciendo orientacion", jointOri, "para", this.boneDefinition.id);
                    this.setRot(jointOri);
                }
                this.setPosition(mypos[0], mypos[1], mypos[2]);
                this.includeJointsinInfluenceList([parentBone.getSkinJointId(this.skinningJoints)], [1.0], this.parent.vertexsIds);
            }
            $.each(this.childs, function(jointid, child){
                 console.log(child.bone);
                 that.includeJointsinInfluenceList([child.bone.getSkinJointId(that.skinningJoints)], [1.0], child.vertexsIds);
            });                
            nj = this.vertexsJointList.numJoints;
            drawableObject.setVertexJoints(this.vertexsJointList.joints, nj);
            drawableObject.setVertexWeights(this.vertexsJointList.weights, nj);
        },
        setDrawableObject : function(drawableObject, boneDefinition) {
            var parentBone, 
                previousInfList;
            if (typeof this.drawableObject !== "undefined"){
                this.remove(this.drawableObject);
            }
            this.drawableObject = drawableObject;
            drawableObject.setInvBindShapeMatrix(this.invBindShapeMatrix);
            drawableObject.setJoints(this.skinningJoints);
            this.compose(drawableObject);
            
            if (typeof boneDefinition !== "undefined"){
                this.boneDefinition = boneDefinition;
                previousInfList = this.vertexsJointList;
                this.createJointsInfluenceList();
    // console.log('nuevo array para ', this.getBId(), this.isStitchVertex);
                this.findVertexPairing(true, true);
                this.fixBone();
/*                if (typeof this.parent !== "undefined"){
                    parentBone =  this.parent.bone;
                    parentBone.invBindShapeMatrix.splice(this.getSkinJointId(parentBone.skinningJoints), 1, this.getInverseBindingMatrix());                
                }*/
                parentBone = this;
                console.log("lista de vertices previa de ", this.getBId(), previousInfList, "actual", this.vertexsJointList);
                //hacemos un fixbone para los hijos
                $.each(this.childs, function(jointid, joint){
                    var jointOri,
                        childBone,
                        mypos;
                    if (typeof boneDefinition[jointid] !== "undefined"){
                        jointOri = boneDefinition[jointid].jointOri;
                        mypos = (typeof boneDefinition[jointid].jointPosition === "undefined") ? [0,0,0] : boneDefinition[jointid].jointPosition;
                        childBone = joint.bone;
                        if (typeof jointOri !== "undefined"){
                            childBone.parent.jointOrientation = jointOri;
                            childBone.setRot(jointOri);
                        }
                        childBone.parent.jointPosition = mypos;
                        childBone.setPosition(mypos[0], mypos[1], mypos[2]); //esto arregla la matriz de traslación inversa incluída en el padre y la de los hijos
                        childBone.invBindShapeMatrix.splice(parentBone.getSkinJointId(childBone.skinningJoints), 1, childBone.getTranslateMatrix());
                        parentBone.invBindShapeMatrix.splice(childBone.getSkinJointId(parentBone.skinningJoints), 1, childBone.getInverseBindingMatrix());
                        console.log(childBone.getBId(), "matriz de traslación inversa: ", childBone.invBindShapeBone, "lista de matrices de traslación inversa", childBone.invBindShapeMatrix);
                    }
                });
                console.log("lista de matrices inversas de traslación para la articulacion actual:", this.invBindShapeMatrix);
            }
        },
        getJointList : function(){
            return this.boneDefinition;
        },
        getChildList : function (){
            var childList = [];
            
            $.each(this.childs, function(i, child){
                childList.push(child.bone);
                childList = childList.concat(child.bone.getChildList());                        
            });
            return childList;
        },
        setAction : function (animation, time, loop){
            this.objectnode.setAction(animation, time, loop);
        },
        addObject : function(object){
            this.objectsInBone.push(object);
            this.compose(object);
        },
        remove : function(object){
            var objectsInBone = this.objectsInBone,
                position = objectsInBone.indexOf(object);
            if (position !== -1){
                this.objectsInBone.splice(position, 1);
                this.$super(object);
            }
        },
        removeAll : function(){
            this.removeAny(this.objectsInBone);
        }
        
    });
        
    /**
     * Skeleton definition.
     * 
     * @param {string}
     *            id skeleton's id.
     * @param {Object}
     *            bonesDefinition an object with the following structure: {
     *            drawableObject_id : { joints: { joint_id: { jointPosition :
     *            [x, y, z], jointVertexs: [vertex_list] }, joint_id: ... },
     *            parentDrawableObject: { jointId: joint_id,
     *            DrawableObjectId: DrawableObject_id } },
     *            DrawableObject_id: ... }
     * @param {Object}
     *            animType animation Type. This is an index to the set of
     *            procedural animations that this skeleton is able to play.
     * @param {motionGraph}
     *            the motionGraph containing the animations.
     * 
     */
    Graphics.Skeleton = Graphics.AnimatableObject.extend({
        init : function (rootBoneId, drawableObject, boneDefinition, animType, motionGraph){
            var scale = drawableObject.getScale();
            // scaleX = "'+ scale['x']+'" scaleY = "'+ scale['y']+'" scaleZ
            // = "'+ scale['z']+'"
            this.$super('<glge><group/></glge>', 'group', animType, motionGraph);
            this.setScale(scale);
            boneDefinition.id = rootBoneId;
            this.rootBone = new Graphics.Bone(drawableObject, boneDefinition);
            this.compose(this.rootBone);
            this.bones = {};
            this.bones[rootBoneId] = this.rootBone;
            this.joints = {};
            drawableObject.setSkeleton(this.rootBone); // establecemos el
            // esqueleto del
            // objeto como el
            // hueso raíz actual
        },
        doAnimation : function (motion){
            if (typeof motion.animation.start !== "undefined"){
                this.rootBone.setAction(motion.animation, motion.time, motion.loop);
            }
        },
        getRootBone : function(){
            return this.rootBone.getBId();
        },
        hasBone : function(boneid){
            return typeof this.bones[boneid] !== "undefined";
        },
        hasJoint : function(jointid){
            return typeof this.joints[jointid] !== "undefined"; 
        },
        getBoneIdbyJoint : function (jointid){
            if (typeof this.joints[jointid] !== "undefined"){
                return this.joints[jointid].child;
            } else if (jointid ==="rootBone"){
                return this.getRootBone();
            }
            return ;
        },
        getBones : function(){
            return this.bones;
        },
        newBone : function(boneid, drawableObject, boneDefinition){
            boneDefinition.id = boneid;
            var bone = new Graphics.Bone(drawableObject, boneDefinition),
                that = this;
            drawableObject.setSkeleton(this.rootBone); // establecemos el esqueleto del objeto como el hueso raíz actual
            
            drawableObject.setBIdGetter(function(){
                return that.getBId();
            });
            this.bones[boneid] = bone;
            return boneid;
        },
        setBoneState : function(boneid, translate, orientation){
            var bone = this.bones[boneid];
            if (typeof bone !== "undefined"){
                if (translate !== null){
                    bone.setPosition(translate[0], translate[1], translate[2]);                    
                }
                //console.log(typeof orientation);
                if (typeof orientation === "array"){
                    bone.setRot(orientation[0], orientation[1], orientation[2], orientation[3]);
                }                    
            }
        },
        getBoneState : function(boneid){
            var bone = this.bones[boneid],
                pos;
            if (typeof bone !== "undefined" ){
                pos = bone.getPosition();
                return {
                    translate : [pos.x, pos.y, pos.z],
                    rotation : bone.getRot(),
                    quaternions : bone.getRot(true)
                };
            }
            return ;
        },
        setBoneRelationship : function(parentBoneid, childBoneid, jointid){
            var childBone = this.bones[childBoneid],
                parentBone = this.bones[parentBoneid];
            if (parentBone.setChildBone(childBone, jointid)){
                if (typeof this.joints[jointid] === "undefined"){
                    this.joints[jointid] = {};
                }
                this.joints[jointid].parent = parentBone;
                this.joints[jointid].child = childBone;
            } else {
                throw new CalipsoWebClient.Exceptions.BoneJointUndefined(parentBone, childBone, jointid);
            }
        },
        addObjectToBone : function(boneid, object, position){
            var bone = this.bones[boneid];
            if (typeof bone !== "undefined"){
                bone.addObject(object);
                object.setPosition(position[0], position[1], position[2]);                    
            }
        },
        removeObjectFromBone : function(boneid, object){
            var bone = this.bones[boneid];
            if (typeof bone !== "undefined"){
                bone.remove(object);
            }
        },
        removeObjectsFromBone : function(boneid){
            var bone = this.bones[boneid];
            if (typeof bone !== "undefined"){
                bone.removeAll();
            }
        },
        rotateJoint : function(jointid, rotation, time){
            var bone = this.joints[jointid];
            if (typeof bone === "undefined"){
                return false;
            }
            bone = this.joints[jointid].child;
            bone.getObjectNode().blendTo({DRotX: rotation[0], DRotY: rotation[1], DRotZ: rotation[2]}, time);
        }
    });
    Graphics.Skeleton.augment(Graphics.Group);

    Graphics.SkeletonCreator = Class.extend({
        init : function (pieces, skeletonsDefinition, motionGraph, onReady) {
            this.bonesdef = {};
            this.skeletons = [];
            var Drawable = Graphics.DrawableObject,
                that = this,
                loadedPieces = 0, numberOfPieces = 0;
            $.each(pieces, function(id, piecedef){
                numberOfPieces++;
                that.bonesdef[id] = {
                        drawable : new Drawable(piecedef.drawable, id, {
                            onSuccess : function() {
    // console.log('loaded:', loadedPieces, 'total', numberOfPieces);
                                if (typeof skeletonsDefinition !== "undefined" && ++loadedPieces === numberOfPieces){
                                    that.createSkeletons(skeletonsDefinition, motionGraph);
                                    if (typeof onReady === "function"){
                                        onReady();
                                    }
                                }
                            }
                        }),
                        joints : piecedef.joints
                };
            });

        },
        createSkeletons : function(skeletonsDefinition, motionGraph){
            var that = this,
                Skeleton = Graphics.Skeleton;
            
            $.each(skeletonsDefinition, function(id, skdef){
                var skel = new Skeleton(skdef.skeleton[0], that.bonesdef[skdef.skeleton[0]].drawable, that.bonesdef[skdef.skeleton[0]].joints, skdef.animtype, motionGraph),
                    i = 0,
                    bone1,
                    bone2;
                that.skeletons.push(skel);
                for (; i<skdef.skeleton.length; i+=3){
                    bone1 = skdef.skeleton[i]; 
                    bone2 = skdef.skeleton[i+1];
                    if (!skel.hasBone(bone1)){
                        skel.newBone(bone1, that.bonesdef[bone1].drawable, that.bonesdef[bone1].joints);                           
                    }
                    if (!skel.hasBone(bone2)){
                        skel.newBone(bone2, that.bonesdef[bone2].drawable, that.bonesdef[bone2].joints);
                    }
                    skel.setBoneRelationship(bone1, bone2, skdef.skeleton[i+2]);
                }
            });
        },
        getSkeletons : function(){
            return this.skeletons;
        }
    });
    Graphics.AnimatedMD2 = Graphics.AnimatableObject.extend({
        init : function(data, animType, motionGraph, callbacks) {
            var that = this,
                animationFinished = function (data) {
                    var l = that.motionStateQueue.length,
                        motion;
                    if (l > 1 && !that.motionStateQueue[l - 1].loop) {
                        motion = that.motionStateQueue.pop();
                        that.finish(motion.action);
//                        GameEngine.actionFinished(that, motion.action);
                        console.log('animationFinished: ', motion);
                        that.change = true;
                    }
                };
            this.$super(data, 'MD2', animType, motionGraph, {
                onSuccess : function(){
                    that.objectnode.addEventListener("md2AnimFinished",
                            animationFinished);
                    that.objectnode.setMD2Animation(that.defaultState.animation, true);
                    that.setMotionTask();
                    callbacks.onSuccess(that);
                },
                onError : callbacks.onError
            });
        },
        doAnimation : function (motion){
            if (this.objectnode.MD2Anim !== motion.animation) {
                console.log('AnimatableObject.animate(): setting ',motion);
                this.objectnode.setMD2Animation(
                        motion.animation, motion.loop);
            }   
        }
    });

    return Graphics; 
}());
