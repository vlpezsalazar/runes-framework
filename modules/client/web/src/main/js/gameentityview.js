/*global CalipsoWebClient, Class, $, toType*/
CalipsoWebClient.GameEntities.GameEntityView = (function(Graphics2D){
    "use strict";
    function getViewReferences (canvas, gameEntity, viewSchema, parentElements, references){

        var iteratorConfig,
            iteratorView, 
            attribute,
            iterator,
            elements,
            source,
            gev,
            j;

        if (typeof references === "undefined"){
            references = {};
        }

        if (typeof viewSchema.source !== "undefined"){
            source = viewSchema.source.split(".");
            if (typeof gameEntity.getStateVar === "function"){
                attribute = gameEntity.getStateVar(source[0]);                
            }
            if (typeof attribute !== "undefined"){
                if (typeof viewSchema.id === "undefined"){ //si es una referencia y no tiene id
                    viewSchema.id = source[0]; // lo establecemos al nombre de la variable de estado de la game entity
                }
                //si existe ese atributo
                if (source.length > 1 && viewSchema.type === "GameEntityView"){
                    viewSchema.source = source[1];   
                    gev = new CalipsoWebClient.GameEntities.GameEntityView(canvas, source[1], attribute, viewSchema.layout);
                    references[viewSchema.id] = {source : source[0], viewSchema : viewSchema, gev : gev};
                    if (typeof parentElements !== "undefined"){
                        parentElements.elements.splice(parentElements.index, 1, gev.getView());
                    }
                } else if (typeof viewSchema.iterator !== "undefined" && toType(attribute) === "array"){
                    elements = [];
                    iterator = viewSchema.iterator;
                    references[viewSchema.id] = {source : viewSchema.source, iterator : true, viewSchema : iterator};

                    if (typeof iterator.type === "GameEntityView" || typeof iterator.source !== "undefined" || typeof iterator.elements !== "undefined"){
                        //si lo que hay en el array son GameEntities
                        iteratorView = (typeof iterator.type === "GameEntityView") ? iterator.source : iterator;
                        iteratorConfig = (typeof iterator.type === "GameEntityView") ? iterator : undefined;

                        for (j in attribute){
                            gev = new CalipsoWebClient.GameEntities.GameEntityView(canvas, iteratorView, attribute[j], iteratorConfig);
                            elements.push(gev.getView());
                        }
                    } else {
                        for (j in attribute){
                            source = attribute[j];
                            elements.push($.extend({}, iterator, {source : source}));
                        }
                    }
                    delete viewSchema.source;
                    viewSchema.elements = elements;
                } else {
                    references[viewSchema.id] = viewSchema.source;
                    viewSchema.source = attribute;
                }
            } else {
                if (viewSchema.type === "GameEntityView"){
                    viewSchema.source = source[1];
                } else {
                    viewSchema.source = source[0];
                }
              //aunque no exista aún la referencia a la GameEntity, la ponemos aquí para cuando sea resuelta
                references[source[0]] = {source : source[0], viewSchema : viewSchema};
                
            }
        } else if (typeof viewSchema.elements !== "undefined"){
            parentElements =  {elements : viewSchema.elements};
            elements = viewSchema.elements;
            for (j in elements){
                parentElements.index = j;
                references = getViewReferences(canvas, gameEntity, elements[j], parentElements, references);
            }
        }
        return references;
    }
    function getSetGameEntityOnEventCallback(view, callback){
        return function(e){
            e.gameEntity = view.gameEntity;
            callback.call(this, e);
        };
    }
    function setViewCallbacks (view, viewSchema){
        var id,
            callbacks,
            stack = [viewSchema];

        while (stack.length > 0){
            viewSchema = stack.pop();
            if (typeof viewSchema.callbacks !== "undefined"){
                callbacks = viewSchema.callbacks;
                for (id in callbacks){
                    callbacks[id] = getSetGameEntityOnEventCallback(view, callbacks[id]);
                }
            }
            if  (typeof viewSchema.elements !== "undefined"){
                stack = stack.concat(viewSchema.elements);
            }
        }
    }

    function getUpdateCallbackOnElement(context, element){
        return function(value){
            element.update(value, context);
        };
    }
    
    function createOrUpdateElementCallback(canvas, view, reference){
        var viewSchemaId = reference.viewSchema.source,
            context = canvas.getContext("2d");

        return function(val){
            if (typeof reference.gev !== "undefined"){
                reference.gev.setGameEntity(val);
            } else {
                reference.gev = new CalipsoWebClient.GameEntities.GameEntityView(canvas, viewSchemaId, val, reference.viewSchema.layout);
                view.addElement(reference.gev.getView());
            }
            if (!view.isInScene()){ //esto se hace para no repintar 2 veces si el elemento forma parte de una escena que tiene su propia tarea de repintado.
                view.paint(context);                
            }
        };
    }
    function createArrayElementsCallback(canvas, view, viewSc){
        var viewSchema = (viewSc.type === "GameEntityView") ? viewSc.source : viewSc,
            layout = (viewSc.type === "GameEntityView") ? viewSc.layout : undefined,
            context = canvas.getContext("2d");

        return function(val){
            var i, 
                gev;

            for (i in val){
                gev = new CalipsoWebClient.GameEntities.GameEntityView(canvas, viewSchema, val[i], layout);
                view.addElement(gev.getView());
            }
            if (!view.isInScene()){ //esto se hace para no repintar 2 veces si el elemento forma parte de una escena que tiene su propia tarea de repintado.
                view.paint(context);                
            }
        };

    }
    function createNewElementCallback(canvas, view, viewSc){
        var viewSchema = (viewSc.type === "GameEntityView") ? viewSc.source : viewSc,
            layout = (viewSc.type === "GameEntityView") ? viewSc.layout : undefined,
            context = canvas.getContext("2d");

        return function(val){
            var gev = new CalipsoWebClient.GameEntities.GameEntityView(canvas, viewSchema, val, layout);
            view.addElement(gev.getView());
            if (!view.isInScene()){ //esto se hace para no repintar 2 veces si el elemento forma parte de una escena que tiene su propia tarea de repintado.
                view.paint(context);                
            }

        };
    }
    function deleteElementCallback(canvas, view){
        var context = canvas.getContext("2d");
        return function(val){
            view.removeElements(val.getBId());
            if (!view.isInScene()){ //esto se hace para no repintar 2 veces si el elemento forma parte de una escena que tiene su propia tarea de repintado. 
                view.paint(context);                
            }

        };
    }

    var GameEntityView = Class.extend({
        /**
         * Constructor de vista de entidad. 
         * 
         * @param canvas el canvas sobre el que se pinta la GameEntityView
         * @param { string | object } viewSchemaId es un id de view de una GameEntity o una definición de vista completa de una GameEntity
         * @param {GameEntity | GameEntityClass } gameEntity es una instancia de GameEntity o la clase de una GameEntity 
         * @param { object } viewConfig configuración de la vista.
         * @param [ string ] screenid información opcional de la pantalla. 
         * @param [ string ] guielementid elemento de la gui.
         */
            init : function(canvas, viewSchemaId, gameEntity, viewConfig, screenid, guielementid){
                var id,
                    config,
                    i,
                    j,
                    elements,
                    element,
                    view,
                    gameEntityCallbacks = {},
                    viewSchema,
                    callbacks,
                    references;

                this.callbacksId = "GameEntityView_" + Date.now();
                this.gameEntityCallbacks = gameEntityCallbacks;

                //Esto lo hacemos por si la GameEntity que nos pasan no es una 
                //instancia, sino una clase. Hay situaciones en las que
                //no poseemos una instancia de GameEntity, pero necesitamos crear 
                //el contenedor de la vista para el momento en que tengamos la
                //instancia.
                if (typeof gameEntity.viewSchema !== "undefined"){
                    view = $.extend(true, {}, (typeof gameEntity.viewSchema[viewSchemaId] !== "undefined") ? gameEntity.viewSchema[viewSchemaId] : viewSchemaId);                    
                } else if (typeof gameEntity.prototype !== "undefined" && typeof gameEntity.prototype.viewSchema !== "undefined"){
                    view = $.extend(true, {}, (typeof gameEntity.prototype.viewSchema[viewSchemaId] !== "undefined") ? gameEntity.prototype.viewSchema[viewSchemaId] : viewSchemaId);
                } else {
                    view = $.extend(true, {}, viewSchemaId);
                }

                this.id = view.id;
                
                console.log(this.id);
                references = getViewReferences(canvas, gameEntity, view);
                setViewCallbacks(this, view);

                this.view = Graphics2D.factory(view, canvas);

                view = this.view;

                for (id in references){
                    if (toType(references[id]) === "object"){
                        viewSchema = references[id].viewSchema;
                        if (references[id].iterator){
                            i = references[id].source;
                            // es un array y los callbacks son para varios eventos
                            gameEntityCallbacks[i] = {
                                    onSet    : createArrayElementsCallback(canvas, view, viewSchema),
                                    onPush   : createNewElementCallback(canvas, view, viewSchema),
                                    onDelete : deleteElementCallback(canvas, view) 
                            };
                        } else if (viewSchema.type === "GameEntityView"){
                            i = references[id].source;
                            gameEntityCallbacks[i] = {
                                    onSet    : createOrUpdateElementCallback(canvas, view, references[id])
                            };
                        }
                    } else {
                        i = references[id];
                        elements = view.getChilds(id);
                        if (elements.length > 0){
                            element = elements[0];
                        }
                        gameEntityCallbacks[i] = {
                                onSet : getUpdateCallbackOnElement(canvas.getContext("2d"), element)
                        };
                    }
                    if (typeof gameEntity.prototype !== "undefined"){
                        if (typeof gameEntity.prototype.updateSchema[i] === "undefined"){
                            gameEntity.prototype.updateSchema[i] = { onSet : {}, onPush : {}, onDelete : {}};
                        }
                        callbacks = gameEntityCallbacks[i];
                        for (j in callbacks){
                            gameEntity.prototype.updateSchema[i][j][this.callbacksId] = callbacks[j];                            
                        }

                    }
                }

                for (j in viewConfig){
                    config = viewConfig[j];
                    id = config.id;
                    elements = view.getChilds(id);
                    if (elements.length > 0){
                        element = elements[0];
                    }
                    if (typeof element !== "undefined"){
                        delete config.id;
                        for (i in config){
                            if (typeof element["set"+i] === "function"){
                                element["set"+i](config[i]);                                
                            } else {
                                console.log("WARNING:", "Error en la configuración de la vista", gameEntity.className, viewSchemaId, "en el componente", guielementid,  ". La propiedad", i, " de la configuración no es una propiedad del elemento", id, " de la vista");
                            }
                        }
                    } else {
                        console.log("WARNING:", "Error en la configuración de la vista", gameEntity.className, viewSchemaId, "en el componente", guielementid,  ". El elemento", id, "no se encontró entre los elementos de la vista");
                    }

                }

                /*
                 * Tenemos en cuenta el caso en que nos pasen una clase de
                 * GameEntity en vez de una instancia.
                 */
                if (typeof gameEntity.prototype === "undefined"){
                    this.setGameEntity(gameEntity);
                }

                
            },
            getView : function(){
                return this.view;
            },
            setGameEntity : function(newGameEntity){
                var callbacksId = this.callbacksId,
                    gameEntityCallbacks = this.gameEntityCallbacks,
                    oldGameEntity = this.gameEntity,
                    eventType,
                    events, 
                    stateVarName;

                if (oldGameEntity !== newGameEntity){
                    if (typeof oldGameEntity !== "undefined"){
                        for (stateVarName in gameEntityCallbacks){
                            oldGameEntity.removeUpdateCallbackOnStateVar(stateVarName, callbacksId);
                        }                    
                    }
                    for (stateVarName in gameEntityCallbacks){
                        events = gameEntityCallbacks[stateVarName];
                        for (eventType in events){
                            newGameEntity.addUpdateCallbackOnStateVar(eventType, stateVarName, callbacksId, events[eventType]);                            
                        }
                    }
                    this.gameEntity = newGameEntity;
                    if (typeof this.id === "undefined"){
                        console.log("estableciendo el id para la vista ", this.view);
                        this.view.setId(this.gameEntity.getBId());                        
                    }
                }
            }
    });
    return GameEntityView;

}(CalipsoWebClient.Graphics.Graphics2D));