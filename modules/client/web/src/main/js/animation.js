 /*global $, CalipsoWebClient, Class, GLGE, Ut*/


/**
 * Paquete de animaciones.
 */
CalipsoWebClient.Animation = (function(){
    
    
    var Animation = {};
    /**
     * Clase que representa un keyframe de una animación. Un keyframe es un instante
     * de una animación que establece la posición de las articulaciones de un
     * esqueleto a la que hay que llegar interpolando.
     * <p>
     * En este keyframe se almacenan los siguientes datos:
     * <p>
     * data = {} Es un conjunto de todas las articulaciones de un esqueleto (jointid =
     * "cuello", "hombroizq", "rodillaizq"...) que han sufrido un cambio y para las
     * que se especifica un estado en este keyframe.
     * <p>
     * El estado se define mediante la posición y la rotación de la articulación
     * (jointid) indicadas como dos vectores:
     * <p>
     * <ul>
     * <li>data[jointid].translate = [x, y, z];
     * <li>data[jointid].rotation = [qx, qy, qz, qw];
     * </ul>
     * <p>
     * En donde translate es un vector con la posición en el espacio de la
     * articulación y rotate es otro vector con la orientación de la articulación
     * establecida mediante un quaternion. Sobre esto último basta saber que es la
     * forma más conveniente de dar una rotación y que hay formas de convertir una
     * matriz de rotación en un quaternion:<br>
     * <a>http://en.wikipedia.org/wiki/Rotation_matrix#Quaternion</a>
     * <p>
     * Los últimos datos que contiene el estado de una articulación son los de
     * animación.
     * <p>
     * 
     * data[jointid].channel[channelid] = {<br>
     * type : type,<br>
     * points : points<br> };<br>
     * 
     * Para cada posible "canal de animación" (channelid):
     * <dl>
     * <dt>LocX
     * <dd>posición x del espacio
     * <dt>LocY
     * <dd>posición y del espacio
     * <dt>LocZ
     * <dd>posición z del espacio
     * <dt>QuatX
     * <dd>rotación en X del espacio.
     * <dt>QuatY
     * <dd>rotación en Y del espacio.
     * <dt>QuatZ
     * <dd>rotación en en Z del espacio.
     * <dt>QuatW
     * <dd>posicion en X del espacio.
     * </dl>
     * 
     * se establece la función de interpolación (type):
     * <dl>
     * <dt>Animation.Motion.STEP = 0
     * <dd> función salto.
     * <dt>Animation.Motion.LINEAR = 1
     * <dd> función lineal de interpolación.
     * <dt>Animation.Motion.BEZIER = 2
     * <dd> función de interpolación mediante curva bezier.
     * </dl>
     */
    Animation.Keyframe = Class.extend({
        /**
         * Constructor de un frame
         */
        init : function(data){
            var empty = true;
            
            this.isEmpty = function(){
                return empty;
            };
            this.setUsed = function(){
                empty = false;
            };
            data = ((data instanceof Animation.Keyframe) ? data.data : data);
            this.data = $.extend({}, data);
        },
        mirror : function(){
            
        },
        /**
         * Establece el estado de una articulación en este keyframe.
         * 
         * @param {string}
         *            jointid id de la articulación
         * @param {Array}
         *            rotationQuat vector de 4 componentes con la
         *            orientación de la articulación expresado como un
         *            quaternion
         * @param {Array }
         *            translateVec vector de 3 componentes con el
         *            desplazamiento de la articulación.
         */
        setJointState : function(/* {string} */jointid, /* {Array} */translateVec, /* {Array} */rotationQuat) {
            this.setUsed();
            var data = this.data,
                p = Animation.Motion.channels;
            data[jointid] = {
                    getChannelValue : function(channel){
                        if (typeof channel === "number"){
                            switch (channel){
                            case p.LOCX :
                            case p.DLOCX :
                                return this.translate[0];
                            case p.LOCY :
                            case p.DLOCY :
                                return this.translate[1];
                            case p.LOCZ :
                            case p.DLOCZ :
                                return this.translate[2];
                            case p.QUATX :
                            case p.ROTX :
                                return this.rotation[0];
                            case p.QUATY :
                            case p.ROTY :
                                return this.rotation[1];
                            case p.QUATZ :
                            case p.ROTZ :
                                return this.rotation[2];
                            default:
                            }
                        }
                        return null;
                    }
            };
            data[jointid].channel = {};
            data[jointid].rotation = rotationQuat;
            data[jointid].translate = translateVec;
        },
        /**
         * Establece la función de animación para un canal determinado en
         * una articulación.
         * 
         * @param {string}
         *            jointid id de articulación.
         * @param {string}
         *            channel canal de animación de la articulación.
         * @param {Number}
         *            type tipo de función de interpolación definido en las
         *            constantes [Motion.STEP|Motion.LINEAR|Motion.BEZIER].
         * @param [optional]
         *            {Array} points vector de 4 flotantes conteniendo 2
         *            puntos de control para una curva Bezier de 3 puntos:
         *            <ul>
         *            <li>points[0] : x - número del frame del primer punto
         *            la curva.
         *            <li>points[1] : y - posición de la que parte la
         *            curva.
         *            <li>points[2] : y - posición del punto de control de
         *            la curva para el x = frame actual.
         *            <li>points[3] : x - frame final para la posición y =
         *            valor del canal para jointid en el keyframe actual
         *            <ul>
         * @see {Animation.Motion}
         */
        setJointChannel : function(/* {string} */jointid, /* {string} */channel, /* {Number} */type, /* [optional] {Array}*/points){
            var data = this.data;
            if (typeof data[jointid] === "undefined") {
                return;
            }
            data[jointid].channel[channel] = {
                    type : type,
                    points : points
            };
        },  
        /**
         * Establece el número de frames en que interpolar desde el anterior
         * 
         * @param {Number}
         *            número de intervalos
         * @see {Animation.Motion}
         */
        setIntervals : function(/* {number} */intervals){
            this.intervals=intervals;
        },
        /**
         * Devuelve el número de frames en que interpolar desde el anterior
         * 
         */
        getIntervals : function(){
            return this.intervals;
        },
        /**
         * Devuelve los datos de la animación en el formato especifico del
         * engine gráfico que se está empleando.
         * 
         * @param frame
         *            numero del keyframe actual en la secuencia.
         * @returns {___anonymous3796_3797}
         */
        getMotionData : function(/*{int}*/frame){
            var /* {Object} */ motion = {};
            if (typeof GLGE !== "undefined"){
                $.each(this.data, function(jointid, jointdata){
                    motion[jointid] = {};
                    $.each(Animation.Motion.channels, function(channelName, channelIndex){
                        var channeldata = jointdata.channel[channelIndex],
                            val = jointdata.getChannelValue(channelIndex), 
                            point;
                        if (val !== null){
                            if ((typeof channeldata !== "undefined") && (channeldata.type !== Animation.Motion.NONE)) {
                                switch (channeldata.type){
                                case Animation.Motion.STEP:
                                    point = new GLGE.StepPoint(frame, val);
                                    break;
                                case Animation.Motion.LINEAR:
                                    point = new GLGE.LinearPoint();
                                    point.setX(frame);
                                    point.setY(val);
                                    break;
                                case Animation.Motion.BEZIER:
                                    if (typeof channeldata.points !== "undefined"){
                                        point = new GLGE.BezTriple();
                                        point.setX1(channeldata.points[0]); point.setY1(channeldata.points[1]);
                                        point.setX2(frame); point.setY2(channeldata.points[2]);
                                        point.setX3(channeldata.points[3]); point.setY3(val);
                                    }
                                    break;
                                }
                                motion[jointid][channelIndex] = point;
                            }/*
                             * else { point = new GLGE.LinearPoint();
                             * point.setX(frame); point.setY(val); }
                             * motion[jointid][channelIndex] = point;
                             */
                        }
                    });
                });
            }/* else {
                // TODO Seleccionar otro engine.
            }*/
            return motion;
        },
        clone : function(){
            return new Animation.Keyframe(this.data);
        }
    });
    /**
     * Representa a una animación completa, una secuencia de keyframes para
     * los que se crea una animación con un numero de frames concreto.<br>
     * 
     * Un frame no es lo mismo que un keyframe. Una animación puede tener 4
     * keyframes y estar finalmente compuesta por 50 frames. El número de
     * frames de la animación da una medida de la suavidad en la
     * interpolación entre keyframes.<br>
     * 
     * La interpolación entre keyframes se realiza empleando las funciones
     * de interpolación STEP, LINEAR Y BEZIER. <br>
     * Para saber como se interpreta que una función de interpolación que
     * toma valores en el plano (x,y) se emplea en un canal que esta
     * definido en R tenemos que asimilar la x de esas funciones al número
     * del frame actual, mientras que la y es el valor que se le asigna al
     * canal y que se establece por el estado en el que se encuentra la
     * articulación en el keyframe para el canal de animación actual. Por
     * ejemplo si este es el keyframe 10 y el estado de la articulación
     * "hombroder" para el canal QuatX es 0.3 y se emplea una función salto,
     * esta se define como un par (10,0.3)
     * 
     * ¿Cómo se interpola entre dos keyframes? Esto se hace cogiendo
     * aquellos 2 keyframes A, B cuyas x para el canal que se está animando
     * actualmente quedan justamente por debajo y por encima del frame
     * actual. Luego se usa el tipo de interpolación que marque el frame B.
     * Por ejemplo en caso que A sea (xa,ya), B sea (xb, yb) y tenga una
     * interpolación lineal, tendríamos que emplear la formula ya + (frame -
     * xa)*(yb-ya)/(xb-xa) para obtener el valor del canal en el frame de
     * animación actual.
     * 
     * Si 2 keyframes A,B consecutivos son definidos como Bezier la
     * interpolación que se realiza es de una Bezier con 4 puntos (ver
     * glge-compiled.js: 4670), en caso de ser Linear-Bezier o viceversa se
     * utiliza una Bezier con sólo 3 puntos.
     * 
     */
    Animation.Motion = Class.extend({
        init : function(keyframeArray){
            if (typeof keyframeArray !== "undefined" ){
                this.frames = keyframeArray;
            } else {
                this.frames = []; // lista de frames.
            }
        },
        /**
         * Establece el nombre de esta animación.
         * 
         * @param {String}
         *            name nombre de la animación.
         */
        setName : function(/* {string} */name){
            this.name = name;
        },
        /**
         * Devuelve el nombre de esta animación.
         */
        getName : function(){
            return this.name;
        },
        /**
         * Añade un conjunto de keyframes desde una posición pos que tienen
         * el estado del keyframe en dicha posición.
         * 
         * @param {Number}
         *            pos indice del keyframe desde el que insertar.
         * @param {Number}
         *            num número de keyframes a insertar.
         */
        newKeyframes : function(/* {Number} */pos, /* {Number} */num){
            if (typeof pos === "undefined"){
                pos = this.frames.length;
            }
            if (!num) {
                num = 1;
            }
            var frames = this.frames,
                framepos = pos, 
                frame, 
                i;
            if (pos >=frames.length){
                pos = frames.length-1;
                framepos = frames.length;
            } else if (pos<0){
                pos = 0;
                framepos = 0;
            }
            frame = frames[pos];
            for (i = 0; i<num; i++){
                frames.splice(framepos, 0, (typeof frame === "undefined") ? new Animation.Keyframe() : frame.clone());
            }
            return framepos;
        },
        getNumberOfKeyframes : function (){
            return this.frames.length;
        },
        /**
         * Elimina un conjunto de keyframes de la animación actual.
         * 
         * @param {Number}
         *            from índice de comienzo de los keyframes a eliminar.
         * @param {Number}
         *            howmany número de keyframes a eliminar.
         */
        removeKeyframes : function(/* {Number} */from, /* {Number} */howmany){
            this.frames.splice(from, howmany);
        },
        getKeyframe : function(i){
            if (i < 0 || this.frames.length <= i) {
                return false;
            }
            return this.frames[i];
        },
        /**
         * Establece el estado de una articulación para un keyframe.
         * 
         * @param {Number}
         *            i indice del keyframe.
         * @param {string}
         *            jointid articulación a establecer.
         * @param {Array}
         *            translatevec vector de 3 reales indicando la posición
         *            de la articulación.
         * @param {Array}
         *            rotationquat vector de 4 reales indicando la
         *            orientación de la articulación.
         */
        setKeyframe : function(/* {Number} */i, /* {string} */jointid, /* {Array} */translatevec, /* {Array} */rotationquat){
            if (i < 0 || this.frames.length <= i) {
                return;
            }
            this.frames[i].setJointState(jointid, translatevec, rotationquat);
        },
        setLinearKeyframe : function(/* {Number} */i, /* {string} */jointid, /* {Array} */translatevec, /* {Array} */rotationquat){
            if (i < 0 || this.frames.length <= i) {
                return;
            }
            this.frames[i].setJointState(jointid, translatevec, rotationquat);
            // this.setKeyframeTypeForJoint(i, jointid,
            // Motion.channels.LOCX, Motion.LINEAR);
            // this.setKeyframeTypeForJoint(i, jointid,
            // Motion.channels.LOCY, Motion.LINEAR);
            // this.setKeyframeTypeForJoint(i, jointid,
            // Motion.channels.LOCZ, Motion.LINEAR);
            var Motion = Animation.Motion,
                Channels = Motion.channels;
            this.setKeyframeTypeForJoint(i, jointid, Channels.ROTX, Motion.LINEAR);
            this.setKeyframeTypeForJoint(i, jointid, Channels.ROTY, Motion.LINEAR);
            this.setKeyframeTypeForJoint(i, jointid, Channels.ROTZ, Motion.LINEAR);
        },
        setKeyframeByTypes : function(/* {Number} */i, /* {string} */jointid, /* {Array} */translatevec, /* {Array} */rotationquat, /* {Array} */translateTypes, /* {Array} */rotationTypes, /* integer */intervals){
            if (i < 0 || this.frames.length <= i) {
                return;
            }
            this.frames[i].setJointState(jointid, translatevec, rotationquat);
            this.frames[i].setIntervals(intervals);
            var Motion = Animation.Motion,
                Channels = Motion.channels;
            this.setKeyframeTypeForJoint(i, jointid, Channels.LOCX, translateTypes[0]);
            this.setKeyframeTypeForJoint(i, jointid, Channels.LOCY, translateTypes[1]);
            this.setKeyframeTypeForJoint(i, jointid, Channels.LOCZ, translateTypes[2]);
            this.setKeyframeTypeForJoint(i, jointid, Channels.DLOCX, translateTypes[3]);
            this.setKeyframeTypeForJoint(i, jointid, Channels.DLOCY, translateTypes[4]);
            this.setKeyframeTypeForJoint(i, jointid, Channels.DLOCZ, translateTypes[5]);
            this.setKeyframeTypeForJoint(i, jointid, Channels.ROTX, rotationTypes[0]);
            this.setKeyframeTypeForJoint(i, jointid, Channels.ROTY, rotationTypes[1]);
            this.setKeyframeTypeForJoint(i, jointid, Channels.ROTZ, rotationTypes[2]);
            // this.setKeyframeTypeForJoint(i, jointid,
            // Motion.channels.QUATW, rotationTypes[3]);
        },
        /**
         * Establece una funcion de animacion para un canal de una
         * articulación en un frame determinado.
         * 
         * @param {Number}
         *            frame numero de frame a establecer.
         * @param {string}
         *            jointid identificador del joint
         * @param {string}
         *            channel canal de animación. Alguno de los definidos en
         *            Motion.channels.
         * @param {Number}
         *            type tipo de animación. Alguna de estas constantes
         *            [Motion.STEP|Motion.LINEAR|Motion.BEZIER].
         * @param bezpoints
         *            vector de 4 flotantes conteniendo 2 puntos de control
         *            para una curva Bezier de 3 puntos:
         *            <ul>
         *            <li>points[0] : x - número del frame del primer punto
         *            la curva.
         *            <li>points[1] : y - posición de la que parte la
         *            curva.
         *            <li>points[2] : y - posición del punto de control de
         *            la curva para el x = frame actual.
         *            <li>points[3] : x - frame final para la posición y =
         *            valor del canal para jointid en el keyframe actual
         *            FIXME los frames en los puntos de control de una
         *            bezier sólo se deben establecer cuanto se conozca el
         *            número de frames de la animación.
         *            <ul>
         */
        setKeyframeTypeForJoint : function (/* {Number} */frame, /* {string} */jointid, /* {Motion.channels} */channel, /* {Number} */type, /* {Array} */bezpoints){
            if (frame < 0 || this.frames.length <= frame) {
                return;
            }
            this.frames[frame].setJointChannel(jointid, channel, type, bezpoints);
        },
        /**
         * Función que duplica un conjunto de keyframes pero cambiando las
         * articulaciones del lado izquierdo (aquellas que acaben por izq) a
         * las del lado derecho, sustituyendo estas por der.
         * 
         * @param {Number}
         *            start keyframe de inicio.
         * @param {Number}
         *            end keyframe de fin.
         */
        mirrorKeyframes : function(/* {Number} */start, /* {Number} */end){
    // TODO
        },
        /**
         * Devuelve un nuevo objeto Motion que incluye a los keyframes desde
         * from hasta to.
         * 
         * @param {Number}
         *            from keyframe origen
         * @param {Number}
         *            to keyframe destino
         */
        slice : function (/* {Number} */from, /* {Number} */to){
            return new Animation.Motion(this.frames.slice(from, to));
        },
        /**
         * Procesa los frames de la animación y los transforma a los objetos
         * que emplee el engine gráfico que se esté usando.
         * 
         * @param {Number}
         *            totalFrames numero total de frames que va a tener la
         *            animación
         * @returns la animación en un formato adecuado para emplearlo con
         *          el engine que estemos empleando actualmente.
         */
        getMotion : function(){
            var motion,
                channel = {},
                length = this.frames.length,
                totalFrames = 0;
            
            if (typeof GLGE !== "undefined"){
                motion = new GLGE.Action();
                $.each(this.frames, function(i, frame){
                    totalFrames += frame.getIntervals();
                    console.log("totalFrames "+totalFrames);
                    var joints = frame.getMotionData(totalFrames);
                    $.each(joints, function(jointid, channels){
                        if (typeof channel[jointid] === "undefined"){
                            var animationVector = new GLGE.AnimationVector(),
                                actionChannel = new GLGE.ActionChannel();
                            channel[jointid] = { animationVector : animationVector };
                            actionChannel.setAnimation(animationVector);
                            actionChannel.setTarget(jointid);
                            motion.addActionChannel(actionChannel);
                        }
                        $.each(channels, function(ch, point){
                            var actionCurve = channel[jointid][ch], 
                                animationVector = channel[jointid].animationVector;
                            if (typeof actionCurve === "undefined"){
                                actionCurve = new GLGE.AnimationCurve();
                                actionCurve.setChannel(Animation.Motion.channels.GLGE[ch]);
                                channel[jointid][ch] = actionCurve;
                                animationVector.addAnimationCurve(actionCurve);
                            }
                            actionCurve.addPoint(point);
                        });
                    });
                });
                motion.setFrames(totalFrames);
            }/* else {
                // TODO EMPLEAR OTRO ENGINE PARA 3D QUE NO UTILICE WEBGL
            }*/
            return motion;
        }
    });
    
    /**
     * Esta constante define la función de animación salto entre frame y frame.
     */
    Animation.Motion.NONE = 9;
    /**
     * Esta constante define la función de animación salto entre frame y frame.
     */
    Animation.Motion.STEP = 0;
    /**
     * Esta constante define la función de interpolación lineal entre frame y frame.
     */
    Animation.Motion.LINEAR = 1;
    /**
     * Esta constante define la función de interpolación mediante una curva bezier
     * entre frame y frame..
     */
    Animation.Motion.BEZIER = 2;
    /**
     * Canales de animación.
     */
    Animation.Motion.channels = {
            LOCX : 0,
            LOCY : 1,
            LOCZ : 2,
            QUATX : 3,
            QUATY : 4,
            QUATZ : 5,
            ROTX : 6,
            ROTY : 7,
            ROTZ : 8,
            DLOCX : 9,
            DLOCY : 10,
            DLOCZ : 11,
            GLGE : ["LocX","LocY","LocZ", "QuatX","QuatY","QuatZ", "RotX", "RotY", "RotZ","DLocX","DLocY","DLocZ"]
    };
    
    Animation.MotionCreator = {
        createMotions : function(/* {array} */motionsData){
            var motions = {},
                motion,
                name,
                frameIndex,
                frames,
                frame,
                intervals,
                jointid,
                jointState;
            for (name in motionsData) {
                motion = new Animation.Motion();
                motions[name] = motion;
                motion.setName(name);
                frames = motionsData[name];
                motion.newKeyframes(0, frames.length);
                for (frameIndex in frames) {
                    frame = frames[frameIndex];
                    intervals = 50;
                    for (jointid in frame){
                        if (jointid === "intervals") {
                            intervals = frame[jointid];
                        } else {
                            jointState = frame[jointid];
                            motion.setKeyframeByTypes(frameIndex, jointid, jointState.tra, jointState.rot, jointState.tty, jointState.rty, intervals);                                
                        }
                    }
                }
            }
            return motions;
        }
    };

    function test(actionparams){
        var testPassed = true, 
            i, 
            innertest, 
            constraints = this.constraints;
        for (i in constraints){
            innertest = constraints[i];
            if (!innertest.test(actionparams)){
                testPassed = false;
                break;
            }
        }
        return testPassed;
    }

    function lessequalsthan(actionparams){
        return actionparams[this.parameter] <= this.value;
    }
    function lessthan(actionparams){
        return actionparams[this.parameter] < this.value;
    }
    function greatherthan(actionparams){
        return actionparams[this.parameter] > this.value;
    }
    function greatherequalsthan(actionparams){
        return actionparams[this.parameter] >= this.value;
    }
    function equals(actionparams){
        return actionparams[this.parameter] === this.value;
    }

    function parseConstraint(constraintDefinition){
        var expression = /\s*(\w+)\s*(==|<=|>=|<|>)\s*((\+|-)?\w+)\s*/, 
            match,
            newConstraint, 
            constraintFunction;
        
        match = expression.exec(constraintDefinition);
        if (match !== null){
            if (match[2] === "=="){
                constraintFunction = equals;
            } else if (match[2] === "<="){
                constraintFunction = lessequalsthan;
            } else if (match[2] === ">="){
                constraintFunction = greatherequalsthan;
            } else if (match[2] === "<"){
                constraintFunction = lessthan;
            } else if (match[2] === ">"){
                constraintFunction = greatherthan;
            } 
            newConstraint = {
                    parameter : match[1], 
                    test : constraintFunction,
                    value : match[3]
            };
        }
        return newConstraint;
    }
    Animation.MotionGraph = Class.extend({
        init : function(/* {object} */motiondata) {
            var i,
                ii,
                iii,
                actionName,
                contraints,
                constraint,
                processedConstraints,
                motions = motiondata.motionIndexes, 
                checkNode,
                childs,
                child;
            this.motionGraph = motiondata;
            
            for (i in motions){
                childs = motions[i].childs;
                for (actionName in childs){
                    checkNode = childs[actionName];
                    for (ii in checkNode){
                        processedConstraints = [];
                        contraints = checkNode[ii].constraints;
                        for (iii in contraints){
                            constraint = parseConstraint(contraints[iii]);
                            if (typeof constraint !== "undefined"){
                                processedConstraints.push(constraint);                                
                            }
                        }
                        checkNode[ii].constraints = processedConstraints;
                        checkNode[ii].test = test;
                    }
                }
            }
        },
        getMotionFromAction : function(presentMotion, action, actionparams) {
            var motionGraph = this.motionGraph,
                motion = false,
                checkChildNode = null,
                i,
                motionNode,
                newmotion;
            if (motionGraph.motionIndexes[presentMotion.id].childs) {
                motionNode = motionGraph.motionIndexes[presentMotion.id].childs[action];
                // console.log('MotionGraph.getMotionFromAction(): ',
                // motionNode);
                if (motionNode) {
                    // console.log('2: motionNode = ', motionNode);
                    if (motionNode.length === 0) {
                        throw new CalipsoWebClient.Exceptions.IncorrectMotionGraph(
                                presentMotion.id, action);
                    }   
                    for (i in motionNode){
                        checkChildNode = motionNode[i];
                        if (checkChildNode.test(actionparams)){
                            newmotion = motionGraph.motionIndexes[checkChildNode.child];
                            // console.log('4, newmotion.childs = ',
                            // newmotion.childs);
                            motion = {
                                id : checkChildNode.child,
                                animation : newmotion.animation,
                                level : newmotion.level,
                                loop : newmotion.loop || (typeof newmotion.childs !== "undefined" && Ut.hasProperties(newmotion.childs)),
                                reset : (typeof newmotion.reset !== "undefined") ? newmotion.reset: false,
                                action : action,
                                constraints : actionparams
                            };
                            break;
                        }
                    }
                } else {
                    throw new CalipsoWebClient.Exceptions.UndefinedTransitionException(
                            presentMotion.id, action);
                }
            }
            console.log('MotionGraph.getMotionFromAction(', presentMotion, ', ', action, ', ', actionparams, ') = ', motion);
            return motion;
        },
        getMotionFromId : function(motionid) {
            var motion = this.motionGraph.motionIndexes[motionid];
            if (!motion){
                throw new CalipsoWebClient.Exceptions.UndefinedMotion(
                        motionid);
            }
            return {
                id : motionid,
                animation : motion.animation,
                level : motion.level,
                loop : typeof motion.childs !== "undefined" && Ut.hasProperties(motion.childs),
                reset : (motion.reset) ? motion.reset : false
            };
        },
        getDefaultMotions : function(type) {
            return this.motionGraph.motionTypes[type];
        }
    });
    return Animation;
}());
