/*global $, Class, toType, console, window */        
/**
 * Biblioteca javascript para el cliente web de Berserker.
 * 
 * Author: Víctor López Salazar
 */

/**
 * Game's Global Namespace.
 */

var CalipsoWebClient = (function() {
    var resources = "resources/",
        port = window.location.port,
        gamesrv = "/game.do/data",
        timesrv = "/time.do",
        audioManager,
        communications,
        scheduler,
        timer,
        screens,
        communicationTask,
        communicationsQueue = []; //cola de comunicacion de las reglas

    function sendToServer(){
        var message;
        while (communicationsQueue.length > 0){
            message = communicationsQueue.pop();
            console.log("INFO", "Enviando mensaje ", message);
            communications.sendMessage(message);
        }
    }
    function processParameters(parameters){
        var obj, i;
        for (i in parameters){
            obj = parameters[i];
            if (toType(obj) === "object" && typeof obj.reference !== "undefined"){
                parameters[i] = CalipsoWebClient.GameEntities.get(obj.reference);
            }
        }
        return parameters;
    }
    function getCallback(methodDef, callback){
        if (typeof callback !== "undefined" && typeof callback.total === "undefined"){
            callback.total = 1;
            callback.present = 0;
        }

        return function(obj){
            /*
             * XXX obtener entity puede fallar si methodDef.o fue instanciado 
             *     anteriormente pero aún no finalizó.
             */
            var entity = CalipsoWebClient.GameEntities.get(methodDef.o) || CalipsoWebClient.GUI.getScreen(methodDef.o), 
                method = entity[methodDef.m],
                params = (toType(methodDef.p) === "array") ? methodDef.p : [methodDef.p];

            params.push(obj);
            method.apply(entity, processParameters(params));
            if (typeof callback !== "undefined"){
                callback.present += 1;
                if (callback.total <= callback.present){
                    callback();
                }
            }
        };
    }
    /**
     * Update the world state, which means the entities and
     * characters in this particular instance.
     * 
     * @param {Array} wurset this array contains entities ids
     * and methods pertaining to each entity that should be
     * executed to update the world state. 
     */
    function update(wurset, callback) {
        var entity,
            entityid,
            methods,
            methodid, 
            methodDef,
            method,
            gamecallback, 
            GameEntities =  CalipsoWebClient.GameEntities;
        
        try {
            for (entityid in wurset) {

                if (entityid === "Client"){
                    entity = CalipsoWebClient;
                } else if (CalipsoWebClient.GUI.isScreen(entityid)){
                    entity = CalipsoWebClient.GUI.getScreen(entityid);
                } else {
                    entity = GameEntities.get(entityid);
                }

                if (typeof entity !== "undefined"){
                    methods = wurset[entityid];
                    //el for se hace en dos partes para permitir generar callbacks 
                    //que incluyan al callback de la llamada a update y que este  
                    //sólo se ejecute al final.
                    for (methodid in methods){
                        methodDef = methods[methodid];
                        method = entity[methodDef.m];
                        if (typeof method === "function"){
                            if (toType(methodDef.p) !== "array"){
                                methodDef.p = [];
                            }
                            if (typeof methodDef.callback !== "undefined"){
                                gamecallback = getCallback(methodDef.callback, callback);
                                methodDef.p.push(gamecallback);
                            }
//                            method.apply(entity, processParameters(methodDef.p));
                        } else {
                            console.log("INFO", "World.update(): llamada a", entityid, ":", entity, ".", methodDef.m, "(", methodDef.p, ") - la entidad no contenía el método.");
                        }
                    }
                    for (methodid in methods){
                        methodDef = methods[methodid];
                        method = entity[methodDef.m];
                        if (typeof method === "function"){
                            method.apply(entity, processParameters(methodDef.p));
                        }
                    }
                } else {
                    console.log("INFO", "World.update(): no existe la entidad con id: ", entityid);
                }
            }
            if (typeof callback !== "undefined" && typeof callback.total === "undefined"){
                callback();
            }
        } catch (e){
            console.log(e);
        }
    }

    function createGame(gameData){
        var gameEntityDefinitions = gameData.Client[0].p[0],
            guiData = gameData.gui;

        CalipsoWebClient.GameEntities.initializeGameEntities(gameEntityDefinitions, function createGUI(){
            console.log("INFO:", "Creando la GUI...");
            CalipsoWebClient.GUI.create(screens, guiData);
            //TODO ¿Cual es la primera pantalla?
        });
    }

    //protegemos la ventana de eventos de cierre, para enviar al servidor un EndGame
    $(window).bind("beforeunload", function() {
        communications.sendMessage(new CalipsoWebClient.Messages.EndGame());
    });

    return {
        init : function(/*[number]*/serverPort, /*object*/context) {
            var Graphics= CalipsoWebClient.Graphics, 
                /*object*/gameEntitiesDefinitions, 
                /*object*/GUIDefinition, 
                /*object*/graphicsInitialization;

            if (typeof context === "undefined"){
                context = serverPort;
            } else {
                port = serverPort;
            }

            graphicsInitialization = context.Assets;
            GUIDefinition = context.GUI;
            gameEntitiesDefinitions = context.GameEntities;


            gamesrv = "http://" + window.location.hostname + ":" +port + gamesrv;
            screens = GUIDefinition;
            console.log("INFO", "Inicializando Scheduler...");
            scheduler = CalipsoWebClient.Scheduler.Scheduler;
            console.log("INFO", "Inicializando gestor de audio...");
            audioManager = new CalipsoWebClient.Audio.AudioManager();
            console.log("INFO", "Inicializando gestor de comunicaciones...");
            communications = CalipsoWebClient.Communications.Messenger;
            communications.init();
            console.log("INFO", "Estableciendo timer...");
            timer = CalipsoWebClient.Scheduler.Timer;
            console.log("INFO", "Arrancando Scheduler...");
            communicationTask = new CalipsoWebClient.Scheduler.Task(this, sendToServer);
            CalipsoWebClient.Scheduler.Scheduler.scheduleCommunicationTask(communicationTask);
            scheduler.start();
            console.log("INFO", "Creando clases de juego...");
            CalipsoWebClient.GameEntities.createGameEntityClasses(gameEntitiesDefinitions);
            if (typeof graphicsInitialization === "undefined"){
                graphicsInitialization = {};
            }
            Graphics.setGraphics(graphicsInitialization,
                    function(){
                        console.log("INFO", "Enviando mensaje de inicialización al servidor...");
                        communications.setGameChannelCallback(update);
                        communications.sendMessage(context.Server.getInitGameMessage(), createGame);
                    });
        },
        initializeGameEntities : function(/*array*/definitions, callback){
            try {
                CalipsoWebClient.GameEntities.initializeGameEntities(definitions, callback);                
            } catch (e){
                console.log(e);
            }
        },
        newMessage : function(mesg){
            communications.sendMessage(mesg);
        },
        getGameServer : function() {
            return gamesrv;
        },
        getServer : function() {
            return resources;
        },
        getTimeServer : function() {
            return timesrv;
        },
        getURL : function(file) {
            return resources + file;
        },
        getAudioManager : function() {
            return audioManager;
        },
        getCommunicationsManager : function (){
            return communications;
        },
        pushMessage : function(message){
            console.log("INFO:", "Estableciendo mensaje");
            communicationsQueue.push(message);
        },
        getScheduler : function () {
            return scheduler;
        },
        getTime : function() {
            return timer.getPresentTime();
        },
        setContext : function(contextid){
            var context = CalipsoWebClient.GameEntities.get(contextid);
            if (typeof context !== "undefined"){
                CalipsoWebClient.Messages.setContext(context);                
            } else {
                console.log("WARNING:", "CalipsoWebClient.setContext(", contextid, ") - no existe una GameEntity con ese id.");
            }
            return this;
        },
        setServerTime : function(time){
            timer.setServerTime(time);
        }
    };
}());
