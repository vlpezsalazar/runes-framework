/*global CalipsoWebClient, Class */
/**
 * Exception package for Berserker
 */

CalipsoWebClient.Exceptions = (function() {
    var Exceptions = {}, 
        Exception = Class.extend({
            init : function(message, pkg) {
                var type;
                if (typeof pkg === "undefined"){
                    pkg = Exceptions;
                } 
                for (type in pkg) {
                    if (pkg[type].prototype.constructor === this.constructor) {
                        break;
                    }
                }
                this.type = type;
                this.message = message;
            },
            toString : function() {
                var exmsg = this.type
                + ((typeof this.message === "undefined") ? ('')
                        : (': ' + this.message));
                return exmsg;
            }
        });

    Exceptions.ActionNotDefined = Exception.extend({});

    Exceptions.IncorrectTypeException = Exception.extend({
        init : function(incorrectObject, typeExpected) {
            this.$super("IncorrectTypeException: the type was "
                    + typeof incorrectObject + " and the expected type was "
                    + typeExpected);
        }
    });

    Exceptions.IncorrectMotionGraph = Exception.extend({ 
        init : function(nodeid, action) {
            this.$super("IncorrectMotionException: The MotionGraph has an invalid node format on node: "
                    + nodeid
                    + ", there are no childs for the action "
                    + action);
        }
    });
    Exceptions.UndefinedMotionException = Exception.extend({
        init: function(nodeid) {
            this.$super("UndefinedMotionException: The MotionGraph doesn't contains a motion with id "
                    + nodeid);
        }
    });
    Exceptions.UndefinedTransitionException = Exception.extend({
        init : function(nodeid, action) {
            this.$super("UndefinedTransitionException: The MotionGraph transition "
                    + action + " doesn't exist in the motion with id "
                    + nodeid);
        }
    });
    /**
     * This exception is employed to mark an incomplete object in a method
     * call. Each method checks if an objects' parameter has got the fields
     * needed and throws this exception if it doesn't have them.
     */
    Exceptions.UndefinedTagInData = Exception.extend({
        init : function(tag) {
            this.$super("Game data doesn't contain any '" + tag
                    + "' tag");
        }
    });


    Exceptions.UndefinedSound = Exception.extend({
        init : function(soundid) {
            this.$super("Sound id not defined " + soundid);
        }
    });

    Exceptions.NoFreeChannels = Exception.extend({});
    Exceptions.ZoneIdNotPresent = Exception.extend({
        init : function(zoneid) {
            this.$super("The zone with id " + zoneid
                    + "' has not been created.");
        }
    });
    Exceptions.CameraTargetNotSet = Exception.extend({
        init : function() {
            this.$super("The camera target has not been set for the travelling task.");
        }
    });
        
    Exceptions.BoneJointUndefined = Exception.extend({
        init : function(parent, child, joint) {
            this.$super("The joint " + joint + " is undefined between " +parent+ " and " + child);
        }
    });
        
    Exceptions.InvalidViewportIndex = Exception.extend({
        init : function(viewportindex){
            this.$super("The viewportindex: " + viewportindex + " is invalid");
        }
    });
    Exceptions.TypeException = Exception.extend({
        init : function(property, type){
            this.$super("Error trying to set the property "+ property + " of a GameEntity's state: types are not the same");
        }
    });
    Exceptions.ModelNotInitialized = Exception.extend({
        init : function(gameEntityName){
            this.$super("The GameEntity " + gameEntityName + " needs a graphic model's initialization data and it hasn't been provided.");
        }
    });
    Exceptions.ModelTypeNotDefinedForBone = Exception.extend({
        init : function(boneid, data){
            this.$super("El hueso " + boneid + " no tiene definición para el modelo " + JSON.stringify(data), Exceptions);
        }
    });
    return Exceptions;
}());