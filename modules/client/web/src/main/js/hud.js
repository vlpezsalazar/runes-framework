/*global $, CalipsoWebClient, Class, console */

CalipsoWebClient.HUD = (function(){
    var HUD = {};

    /**
     * @param {DOM element} parent elemento padre donde se incluirá este inventario.
     * @param {object} inventoryDefinition definición del inventario con formato,
     *                  p.e.: {0: 'weapons', 1: 'tools'}.
     */
    HUD.Inventory = Class.extend({  
        init : function(parent, definition){
            var that = this,
                knapsack_items = $("<div/>", {
                                      id : "inventory",
                                      "class" : "inventory"
                                   }),
                knapsack_itemTypes = $("<ul/>", {
                    id : "#item_labels"
                }),
                itemTypes = {}, //representación gráfica de los items que tiene la mochila agrupados por tipos.
                owner, // contenedor actual de los objetos que se representan.
                itemsCallback,
                inventoryDefinition = definition;
            
            if ($("#"+parent).find("#inventory").length > 0){ //CUANDO CREAMOS 2 INVENTARIOS CON EL MISMO PADRE HAY PROBLEMAS (supongo que por jquery...)
                throw new Error("El padre ya contiene un elemento dom con id='inventory'");
            }
            function removeItem(){
                var itemid = this.id.split("|"),
                    items = itemTypes[itemid[0]],
                    itemsOf = owner[itemid[0]],
                    i;
                items.DOM.find("#" + itemid[1]).remove();
                for (i in itemsOf){
                    if (itemsOf[i].getBId() === itemid[1]){
                        itemsOf.splice(i, 1);
                        break;
                    }
                }
                
            }

            //funcion utilizada dentro de cada grupo de itemTypes para añadir un item
            function addItem(item){
                var itemList = this.DOM,
                    itemdiv = $("<div/>", {
                        id : item.getBId(),
                        text: item.getDescription(),
                        "class" : "ui-selectee", 
                        click : function(e){
                            itemsCallback(item);
                        }
                    }),
                    trashbin = $("<img/>", { 
                        id : item.getType()+"|"+item.getBId(),
                        "class" : "ui-li-icon",
                        src : "resources/img2d/trash-icon-20x20.png",
                        click : removeItem
                    });
                itemList.append(itemdiv);
                itemList.append(trashbin);
            }
            /**
             * Permite configurar las etiquetas del inventario y definir un conjunto de items que aparecerán en él.
             * @param {object} inventory items del inventario con formato {0:[{id:0, type:0, description:'sword'}]
             * @param {callback} funciones a llamar cuando se pulsa en un item del inventario.
             */
            this.setInventory = function(inventory, callback){
                var itemType,
                    items,
                    item,
                    dom,
                    i;

                itemsCallback = callback;
                owner = inventory;
                knapsack_itemTypes.empty();
                knapsack_items.empty();

                knapsack_items.append(knapsack_itemTypes);
                //inicializa los grupos de items en el inventario.
                for (itemType in inventoryDefinition){
                    items = inventory[itemType];
                    $("<li>").append(
                            $("<a/>",
                              {href : "#" + itemType,
                               text : inventoryDefinition[itemType]})).appendTo(
                                      knapsack_itemTypes);

                    dom = $("<div/>",{
                        id : itemType,
                        style : "overflow: auto; width: 400px; height: 500px;"
                    });

                    itemTypes[itemType] = {
                        DOM : dom,
                        add : addItem
                    };

                    dom.appendTo(knapsack_items);

                    for (i in items){
                        itemTypes[itemType].add(items[i]);
                    }
                }
                knapsack_items.tabs();
                knapsack_items.resizable({
                    alsoResize: "div.inventory > div.ui-tabs-panel"
                });
                inventory.addItem = function(/*Entities.Item*/item){
                    var items = this[item.getType()];
                    if (typeof items === "array"){
                        items.push(item);
                        if (owner === this){
                            itemTypes[itemType].add(item);
                        }
                    }
                };
            };
            this.switchVisibility = function(){
                knapsack_items.toggle('fast');
            };
            $("#"+parent).append(knapsack_items);
            knapsack_items.draggable({containment: "parent"});
            knapsack_items.hide();
        }
     });

    HUD.PortraitLifeAndEnergyDisplay = Class.extend({
        init : function(portraitImgSrc){
            var fragment = $("<div><canvas width=\"800\" height=\"600\"></div>"),
                canvas = fragment.find("canvas").get(), 
                context = canvas.getContext("2d"),
                portraitImage = new Image(),
                lifeGrdRed = context.createLinearGradient(0,0,0,100),
                energyGrdYellow = context.createLinearGradient(0,0,0,100),
                barsLength = 120;

            lifeGrdRed.addColorStop(0,"darkred");
            lifeGrdRed.addColorStop(1,"red");
            energyGrdYellow.addColorStop(0, "darkyellow");
            energyGrdYellow.addColorStop(1, "yellow");

            this.lifeGrdRed = lifeGrdRed; 
            this.energyGrdYellow = energyGrdYellow;
            this.barsLength = barsLength;
            this.context = context;
            this.canvas = canvas;
            this.lifeGrdRed = lifeGrdRed;
            this.energyGrdYellow = energyGrdYellow;

            portraitImage.src = portraitImgSrc;
            portraitImage.onLoad = function(){
                context.drawImage(portraitImage, 40, canvas.heigth-20, 100, barsLength);
                context.fillStyle = lifeGrdRed;
                context.fillRect(140, canvas.heigth-140, 10, barsLength);
                context.fillStyle = energyGrdYellow;
                context.fillRect(150, canvas.heigth-140, 10, barsLength);
            };
            this.maxLife = 1;
            this.presentLife = 1;
            this.maxEnergy = 1;
            this.presentEnergy = 1;
        },
        setMaxLife : function(newMax){
            this.maxLife = newMax;
        },
        setMaxEnergy : function(newMax){
            this.maxEnergy = newMax;
        },
        /**
         * Actualiza el dibujo de la barra de vida al porcentaje que marca
         * presentLife con respecto de maxLife.
         */
        updateLife : function(/*int*/presentLife, /*[int]*/maxLife){
            var lifeperc, 
                unitsToTotal,
                context = this.context,
                canvas = this.canvas;

            if (maxLife !== "undefined"){
                this.maxLife = maxLife; 
            }

            lifeperc = presentLife/this.maxLife; 

            if (presentLife < this.presentLife){
                unitsToTotal = this.barsLength*(1.0-lifeperc);
                //pintamos el porcentaje de trasparente que corresponde
                context.clearRect(140, canvas.heigth-40-unitsToTotal, 10, unitsToTotal);
            } else {
                //pintamos el porcentaje de rojo que corresponde
                context.fillStyle = this.lifeGrdRed;
                context.fillRect(140, canvas.heigth-140, 10, this.barsLength*lifeperc); //vida
            }
            this.presentLife = presentLife;
        },
        updateEnergy : function(presentEnergy, maxEnergy){
            var energyperc, 
                unitsToTotal,
                context = this.context,
                canvas = this.canvas; 

            if (maxEnergy !== "undefined"){
                this.maxEnergy = maxEnergy;
            }

            energyperc = presentEnergy/this.maxEnergy;

            if (presentEnergy < this.presentEnergy){
                unitsToTotal = this.barsLength*(1.0-energyperc);
                //pintamos el porcentaje de trasparente que corresponde
                context.clearRect(150, canvas.heigth-40-unitsToTotal, 10, unitsToTotal);
            } else {
                //pintamos el porcentaje de rojo que corresponde
                context.fillStyle = this.lifeGrdYellow;
                context.fillRect(150, canvas.heigth-140, 10, this.barsLength*energyperc); //vida
            }
            this.presentEnergy = presentEnergy;
        }
    });

    return HUD;
}());