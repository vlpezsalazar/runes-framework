/*global $, CalipsoWebClient, Class*/

CalipsoWebClient.Communications = (function(){
    var Communications = {};
    Communications.Messenger = {
        init : function(gamecallback) {
            var response, 
                channel,
                ready = false,
                unsentMessages = [],
                messagesCallbacks = {},
                messagesTask = new CalipsoWebClient.Scheduler.Task(this, function(){
                    var worktodo = (unsentMessages.length > 0), 
                        mesg;
                    if (ready && worktodo){
                        mesg = unsentMessages.shift();
                        channel.push(mesg);
                    }
                    return !worktodo;
                });
            console.log("INFO:", 'definiendo communication');
            function innerCallback(presentresponse) {
                var time, 
                    presentCallback;
//                console.log("TRACE:", 'respuesta recibida: ', presentresponse);
                if (presentresponse.state === 'messageReceived'){
                    response = JSON.parse(presentresponse.responseBody);
                    time = response.time;
                    if (typeof time !== "undefined") {
                        CalipsoWebClient.setServerTime(time);
                    }
                    time = response.timeStamp;
                    if (typeof time !== "undefined"){
                        delete response.timeStamp;
                    }
                    presentCallback = messagesCallbacks[time];
                    if (typeof messagesCallbacks[time] === "function"){
                        if (presentCallback.after){
                            gamecallback(response, presentCallback);
                        } else {
                            presentCallback(response);                            
                        }
                        delete messagesCallbacks[time];
                    } else if (typeof gamecallback === "function") {
                        gamecallback(response);
                    }                        
                }
            }

            console.log("INFO:", 'creando canal de usuario');

            channel = $.atmosphere.subscribe({
                url : CalipsoWebClient.getGameServer(),
                contentType : "application/json",
                logLevel : 'debug',
                transport : 'sse',
                fallbackTransport : 'websocket',
//                trackMessageLength : true,
                onMessage : innerCallback,
                onOpen : function(response){
                    ready = true;
                    console.log('connection established:', response);
                },
                onReconnect : function(request, response){
                    ready = true;
                    console.log("onReconnect(): ", request, response);
                },
                onClose : function(request, response){
                    ready = false;
                    console.log("onClose(): ", request, response);
                }
                
            });
            messagesTask.setDone();

            this.login = function(msg, callback){
                var that = this;
                $.ajax(CalipsoWebClient.getLoginServer() , {
                    success : function(){
                        that.sendMessage(msg, callback);
                    }
                });
            };

            this.sendMessage = function(mesg, callback) {
                if (typeof callback === "function"){
                    messagesCallbacks[mesg.getTimeStamp()] = callback;                    
                }
                if (!ready || unsentMessages.length > 0) {
                    if (messagesTask.isDone()){
                        messagesTask.setDone(false);
                        CalipsoWebClient.getScheduler().scheduleGameTask(messagesTask);
                    }
                    unsentMessages.push(mesg.toString());
                } else {
                    channel.push(mesg.toString());
                }
            };

            this.setGameChannelCallback = function(callback) {
                gamecallback = callback;
            };

/*            this.sync = function() {
                timeChannel.push({
                    data : '', 
                    onMessage : function(presentresponse){
                        if (presentresponse.state === 'messageReceived'){
                            timeChannel.push({data : '',
                                onMessage : function(presentresponse){
                                    if (presentresponse.state === 'messageReceived'){
                                        response = JSON.parse(presentresponse.responseBody);
                                        time = response.time;
                                        if (typeof time !== "undefined") {
                                            CalipsoWebClient.setServerTime(time);
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            };*/
        }
    };
    return Communications;
}());