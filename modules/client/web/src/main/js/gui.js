/*global CalipsoWebClient, $, Class, toType */
CalipsoWebClient.GUI = (function(GameEntities, CanvasController, Class, GraphicEngine, Scene, HUD, Graphics, Graphics2D, GameEntityView){

    function newGameEntityView(HTMLElement, screen, guielementid, screenComponent, gereference, attributeOrViewId){
        var defGameEntities, 
            type,
            ge = screen.gameEntities[gereference],
            value;
        if (typeof screenComponent.type !== "undefined"){
            if (screenComponent.type === "GameEntityView"){
                if (typeof ge === "undefined"){
                    defGameEntities = screen.definition.gameEntities;
                    type = (typeof defGameEntities[gereference].type !== "undefined") ? defGameEntities[gereference].type : defGameEntities[gereference];
                    ge = GameEntities.gameEntityClasses[type];
                }
                value = screen.setGameEntityView(guielementid + attributeOrViewId, new GameEntityView(HTMLElement, attributeOrViewId, ge, screenComponent.layout, screen.getId(), guielementid));
                screen.setOnChangeHandler(screen.gameEntitiesViews, guielementid + attributeOrViewId, gereference, function(element){
                    this.setGameEntity(element);
                });

            } else {
                if (typeof ge !== "undefined"){
                    screenComponent.source = ge.getStateVar(attributeOrViewId);
                    ge.addUpdateCallbackOnStateVar("onSet", attributeOrViewId, screen.getId()+guielementid+attributeOrViewId,
                            function (e){
                        value.update(e);
                    });
                }
                value = Graphics2D.factory(screenComponent, HTMLElement);
                screen.setOnChangeHandler(screen.rendering, guielementid, gereference, function(element){
                    var ge = screen.gameEntities[gereference];
                    if (typeof ge !== "undefined"){
                        ge.removeUpdateCallbackOnStateVar(attributeOrViewId, screen.getId()+guielementid+attributeOrViewId);                        
                    }
                    element.addUpdateCallbackOnStateVar("onSet", attributeOrViewId, screen.getId()+guielementid+attributeOrViewId, function (e){
                        value.update(e);
                    });
                    value.update(element.getStateVar(attributeOrViewId));
                });
            }
        } else {
            value = attributeOrViewId;
        }
        return value;
    }

    function parseElements(DOMParent, screen, guielementid, elements){
        var id, 
            type,
            source,
            screenComponent, 
            HTMLElement;
        console.log("Parseando referencias a Game Entities en elemento del GUI: ", guielementid);
        for (id in elements){
            screenComponent = elements[id];
            type = toType(screenComponent);
            if ( type === "object" && toType(screenComponent.type) !== "undefined" ){
                source = screenComponent.source;
            } else if (type === "string"){
                source = screenComponent;
            }
            if (source.indexOf("/") === -1){
                if (source.indexOf(".") !== -1){
                    source = source.split(".");
                    if (typeof screenComponent.id !== "undefined"){
                        HTMLElement = $(DOMParent).find("#" + screenComponent.id).get(0);
                    }
                    elements[id] = newGameEntityView((typeof HTMLElement !== "undefined") ? HTMLElement : DOMParent, screen, guielementid, screenComponent, source[0], source[1]);
                }
            }
        }
        return elements;
    }

    function getScreenEntityCallback(that, statevar){
        return function(value){
            that.setScreenEntity(statevar, value);
        };
    }
    function createHandler(that, handler){
        return function(e){
            handler.call(that, e);
        };  
    }

    function createHandlers(that, handlers){
        var id;
        for (id in handlers){
            handlers[id] = createHandler(that, handlers[id]);
        }
        return handlers;
    }
    
        /**
         * Una pantalla representa un conjunto de elementos de la gui del usuario
         * diferenciados del resto y que se encuentran bajo un epígrafe común.
         * Su representación en elementos nativos web es un div que contiene
         * distintos elementos del DOM (botones, imagenes, canvas, otras pantallas,
         * o elementos del hud. Para generar una pantalla es necesario declarar
         * en el html de inicio un div con id igual al nombre de la pantalla que 
         * se desea declarar y en un fichero javascript, una definición de
         * pantalla que es un objeto javascript con el siguiente formato:
         * 
         * {
         *    gameEntities: {
         *       nombre : "tipo"
         *    }, 
         *    guiHandlers: {
         *       id_del_dom : { 
         *          tipo_evento : function(event){
         *             ...
         *          }
         *       }
         *    }
         * }
         * 
         * A partir de este momento, todos los elementos del DOM que se 
         * declaren dentro del div en el html formarán parte de la pantalla y
         * pasarán a estar controlados por los manejadores de eventos declarados
         * en guiHandlers cuyo id coincida con el id del elemento incluido en
         * el div. Por ejemplo si en el div hay un botón con id boton1, para
         * controlar el click del ratón sobre ese botón declaramos un manejador
         * para el evento de la siguiente forma:
         * {
         *    ... 
         *    guiHandlers : {
         *       boton1 : {
         *          click : function(event){
         *          
         *          }
         *       }
         *    }
         * }
         * 
         * Una pantalla también posee un conjunto de GameEntities que se denominan
         * "GameEntities activas" y que recibirán eventos generados por el usuario.
         * Estos eventos tienen 2 fuentes, a traves de elementos del DOM pertenecientes
         * a la pantalla o eventos generados en el canvas. Estos últimos eventos
         * son los que directamente recibirán las GameEntities activas. Cuando
         * una GameEntity declara un elemento userEvents y forma parte de las
         * entidades activas de una pantalla que contiene un canvas, estos
         * GameEntities se insertan dentro del controlador del canvas y reciben
         * los eventos que han declarado manejar en la sección userEvents.
         * 
         * Cuando una GameEntity activa es declarada con un modelType que hereda
         * de Scene, Se crea un viewport en ese canvas que visualizará
         * la escena. Las propiedades del viewport se pueden controlar declarando
         * para el tipo del GameEntity un objeto de la siguiente manera:
         *
         * //Definimos una pantalla que tendrá una declaración de un div con
         * //id = "GameScreen"
         * var GameScreen = {
         *    gameEntities : {
         *        userCharacter : "Character",
         *        presentZone : { //Este GameEntity es derivado de Scene y va a ser la escena que
         *                        //visualiza el GraphicEngine su tipo es un objeto con los parámetros:
         *          type : "Zone" //tipo de GameEntity cuyo modelType debe ser derivado de Scene
         *          orix : 10,    //origen en x del viewport
         *          oriy : 10,    //origen en y del viewport
         *          width : 640,  // ancho del viewport
         *          height : 480, // alto del viewport
         *        },
         *        presentCamera : "MainCamera[presentZone.camera]" // [] indica una referencia a una 
         *                                                       // variable de estado de una de 
         *                                                       // las gameEntities que hay en 
         *                                                       // la pantalla y con el mismo 
         *                                                       // tipo.
         *    }
         *    ...
         * }
         *
         * Si no se indican propiedades para el viewport se heredarán las que
         * se hayan establecido para el canvas.
         * 
         * Inicialización y Referencias a otras variables.
         * 
         * Las GameEntities que forman parte de la definición de una pantalla
         * se inicializan de 3 maneras distintas. 
         * 
         * 1. Con la GameEntity a la que hace referencia el id que envía el
         * servidor para las gameEntities de la pantalla.
         * 2. Con un id especificado de forma concreta en el tipo siguiendo el
         * formato "tipo:id" para el tipo de variable de la sección gameEntities.
         * 3. Mediante una referencia a una variable de estado de una GameEntity
         * inicializada previamente por cualquiera de los dos métodos anteriores.
         * Esto se realiza siguiendo el formato: "tipo[varname.statevarname]". 
         * Un ejemplo de esto es el tipo de la variable presentCamera anterior.
         * Aunque no es recomendable, es posible incluir referencias anidadas,
         * es decir referencias a variables que son referencias. La pantalla
         * se encarga de gestionar estas referencias cuando una GameEntity
         * que es referenciada cambia. 
         * 
         * 
         */
    var Screen = Class.extend({
            init : function(divid, definition, initdata){
                var dge = definition.gameEntities,
                    ige = initdata,
                    cge = definition.rendering,
                    id,
                    type,
                    gameEntity,
                    guiHandlers = definition.guiHandlers,
                    reference,
                    referencedBy,
                    clientEntityIndex,
                    clientEntityId,
                    canvas,
                    div,
                    divHandlers,
                    onChange,
                    onChangeHandler,
                    entitiesToResolve = [], 
                    screenComponent, 
                    store,
                    parent,
                    end;
                this.id = divid;
                this.div = $("#"+divid);
                this.definition = definition;
                this.referencedBy = {};
                this.propertyReferences = {};
                this.onChangeHandlers = {};
                this.gameEntities = {};
                this.screens = {};
                this.gameEntitiesViews = {};
                this.rendering = {};
                this.scenes = [];
                div = this.div;

                if (typeof cge !== "undefined"){
                    for (id in cge){
                        if (cge[id].context === "3d"){
                            canvas = div.children("#" + id).get(0);
                            if (typeof canvas === "undefined"){ //comprobamos si se trata de un elemento global
                                canvas = $("#" + id).get(0);
                            }
                            if (typeof canvas !== "undefined"){
                                console.log("Encontrado canvas 3d'", id, "' en pantalla '", divid);
                                if (typeof canvas.graphicEngine !== "undefined"){
                                    console.log("INFO:", "Empleando motor 3D previamente inicializado...");
                                    this.graphicEngine = canvas.graphicEngine;
                                    this.canvas = canvas; // solo guardamos el canvas de 3d, consecuentemente por cada pantalla sólo podemos tener 1 canvas en 3d.
                                    console.log("INFO:", "Empleando controlador previamente inicializado...");
                                    this.controller = canvas.controller;
                                } else {
                                    console.log("INFO:", "Inicializando motor gráfico...");
                                    this.graphicEngine = new GraphicEngine(canvas);
                                    this.canvas = canvas; // solo guardamos el canvas de 3d, consecuentemente por cada pantalla sólo podemos tener 1 canvas en 3d.
                                    console.log("INFO:", "Inicializando controlador del canvas 3d...");
                                    this.controller = new CanvasController(canvas);
                                }
                                delete cge[id];
                            } else {
                                console.log("WARNING - Screen ", divid, ": No existe ningún elemento con id", id, "declarado en index.html");
                            }
                        }
                    }
                }

                for (id in dge){ //primero resolvemos todas las referencias y los
                    type = (typeof dge[id] === "object") ? dge[id].type : dge[id];
                    end = type.length;
                    clientEntityIndex = type.indexOf(":");
                    if (clientEntityIndex === -1){
                        clientEntityIndex = type.indexOf("[");
                        if (clientEntityIndex > -1){
                            reference = true;
                            end--;
                        }
                    }

                    if (clientEntityIndex > -1){
                        clientEntityId = type.substring(clientEntityIndex+1, end).split(".");
                        if (reference){
                            referencedBy = this.referencedBy[clientEntityId[0]];
                            if (typeof referencedBy === "undefined"){
                                referencedBy = [];
                                this.referencedBy[clientEntityId[0]] = referencedBy;
                            }
                            referencedBy.push({entity : id, property : clientEntityId[1]});
                            reference = false;
                        } else {
                            ige[id] = clientEntityId;
                            entitiesToResolve.push(id);
                        }
                        type = type.substring(0, clientEntityIndex); 
                    } else {
                        entitiesToResolve.push(id);
                    }
                    if (typeof dge[id] === "object"){
                        dge[id].type = type;
                        onChange = dge[id].onChange;
                        if (typeof onChange !== "undefined"){
                            for (onChangeHandler in onChange){
                                this.setOnChangeHandler(this.gameEntities, id, onChangeHandler, onChange[onChangeHandler]);
                            }
                        }
                    } else {
                        dge[id] = {type : type};
                    }
                }
                for (id in entitiesToResolve){ //resolvemos las GameEntities y sus referencias.
                    id = entitiesToResolve[id];
                    clientEntityId = ige[id];
                    if (toType(clientEntityId) === "array"){
                        if (clientEntityId.length === 1){
                            clientEntityId = clientEntityId[0];
                            store = GameEntities.cache;
                        } else {
                            store = GameEntities.cache[clientEntityId[0]];
                            clientEntityId = clientEntityId[1];
                            if (typeof store !== "undefined"){
                                store.addUpdateCallbackOnStateVar("onSet", clientEntityId, "screen"+divid, getScreenEntityCallback(this, id));
                                store = store.state;
                            }
                        }
                        if (typeof store !== "undefined"){
                            gameEntity = store[clientEntityId];
                        }
                    }
                    if (typeof gameEntity !== "undefined"){
                        this.setScreenEntity(id, gameEntity);
                    }
                }

//inicializamos los componentes de las vistas
                for (id in cge){
                    parent = div.find("#" + id).get(0);
                    if (typeof parent === "undefined"){ //comprobamos si se trata de un elemento global
                        canvas = $("#" + id).get(0);
                    }
                    if (typeof parent !== "undefined"){
                        if (typeof cge[id].type !== "undefined"){
                            type = cge[id].type;
                        } else if (typeof cge[id].context !== "undefined"){
                            cge[id].type = "Scene";
                        } 
                        if (typeof cge[id].type !== "undefined") {
                            parseElements(parent, this, id, cge[id].elements);
                            screenComponent = Graphics2D.factory(cge[id], parent);
                            if (typeof screenComponent !== "undefined"){
                                this.rendering[id] = screenComponent;
                                screenComponent = undefined;
                            }
                        } else {
                            parent = $(parent);
                            this.rendering[id] = parent;
                            divHandlers = cge[id].callbacks;
                            parent.on(createHandlers(this, divHandlers));

                        }
                    } else {
                        console.log("WARNING - Screen ", divid, ": No existe ningún elemento con id", id, "declarado en index.html");
                    }
                }

                if (typeof guiHandlers !== "undefined"){
                    if (typeof guiHandlers.hide !== "undefined"){
                        this.hide = guiHandlers.hide;
                    }
                    if (typeof guiHandlers.show !== "undefined"){
                        this.show = guiHandlers.show;
                    }
                    div.on(createHandlers(this, guiHandlers));
                }

                if (definition.visible){
                    this.show();    
                } else {
                    this.hide();
                }
            },
            getId : function(){
                return this.id;
            },
            getElement : function(id){
                return this.rendering[id];
            },
            setOnChangeHandler : function(context, elementid, gameEntityId, handler){
                var onChangeHandlers = this.onChangeHandlers[gameEntityId];
                if (typeof onChangeHandlers === "undefined"){
                    onChangeHandlers = [];
                    this.onChangeHandlers[gameEntityId] = onChangeHandlers;
                }
                onChangeHandlers.push({context : context, entity: elementid, handler : handler});

            },
            setGameEntityView : function(id, gev){
                this.gameEntitiesViews[id] = gev;
                return gev.getView();
            },
            getScreenEntity : function(varname){
                return this.gameEntities[varname];
            },
            setScreenEntity : function(varname, entity){
                var controller = this.controller,
                    gameEntities = this.gameEntities,
                    defGameEntities = this.definition.gameEntities, 
                    type = (typeof defGameEntities[varname].type !== "undefined") ? defGameEntities[varname].type : defGameEntities[varname],
                    sceneVis,
                    onChangeHandlers,
                    reference,
                    property,
                    context,
                    i;

                type = Graphics[type] || HUD[type] || GameEntities.gameEntityClasses[type] || type;

                if (typeof type !== "undefined" && 
                    gameEntities[varname] !== entity && 
                    ((typeof type === "function" && entity instanceof type) || 
                     toType(entity)  === type)){
                    if (typeof controller !== "undefined"){
                        if (typeof gameEntities[varname] !== "undefined"){
                            controller.removeGameEntity(gameEntities[varname]);                            
                        }
                        if (typeof entity.userEvents !== "undefined"){
                            controller.addGameEntity(entity);                            
                        }
                    }
                    if (typeof this.graphicEngine !== "undefined" && 
                        entity instanceof Scene){
                        if (this.scenes.indexOf(varname) === -1){
                            this.scenes.push(varname);
                        }
                        
                        sceneVis = defGameEntities[varname];
                        try {
                            this.graphicEngine.setViewportScene(sceneVis.viewport, entity);
                        } catch (e){
                            if (typeof this.canvas !== "undefined"){
                                if (typeof sceneVis.orix === "undefined"){
                                    sceneVis.orix = 0;
                                }
                                if (typeof sceneVis.oriy === "undefined"){
                                    sceneVis.oriy = 0;
                                }
                                if (typeof sceneVis.width === "undefined"){
                                    sceneVis.width = this.canvas.width;
                                }
                                if (typeof sceneVis.height === "undefined"){
                                    sceneVis.height = this.canvas.height;
                                }
                                sceneVis.viewport = this.graphicEngine.createViewport(sceneVis.orix, sceneVis.oriy, sceneVis.width, sceneVis.height, entity);
                                console.log("Creado viewport", sceneVis.viewport);
                            }
                        }
                    }
                    reference = this.referencedBy[varname];
                    //si hay referencias a esta entidad y la entidad es un GameEntity
                    if (typeof reference !== "undefined" && typeof entity.className !== "undefined"){
                        for (i in reference){
                            if (typeof gameEntities[varname] !== "undefined"){
                                gameEntities[varname].removeUpdateCallbackOnStateVar("onSet", reference[i].property, "screen");
                            }
                            entity.addUpdateCallbackOnStateVar("onSet", reference[i].property, "screen"+this.id, getScreenEntityCallback(this, reference[i].entity));
                            property = entity.state[reference[i].property];
                            if (typeof property !== "undefined"){
                                this.setScreenEntity(reference[i].entity, property);
                            }
                        }
                    }
                    onChangeHandlers = this.onChangeHandlers[varname];
                    if (typeof onChangeHandlers !== "undefined"){
                        for (i in onChangeHandlers){
                            context = onChangeHandlers[i].context;
                            if (typeof context[onChangeHandlers[i].entity] !== "undefined"){
                                onChangeHandlers[i].handler.call(context[onChangeHandlers[i].entity], entity);                                
                            }
                        }
                    }
                    if (typeof gameEntities[varname] !== "undefined"){
                        gameEntities[varname].screen = undefined;
                    }
                    gameEntities[varname] = entity;
                    entity.screen = this;
                }
            },
            show : function(effect, options, time, callback){
                var i,
                    varname,
                    defGameEntities = this.definition.gameEntities,
                    sceneVis,
                    gameEntities = this.gameEntities,
                    graphicEngine = this.graphicEngine,
                    scenes = this.scenes;

                if (typeof graphicEngine !== "undefined"){
                    for (i in scenes){
                        varname = scenes[i];
                        sceneVis = defGameEntities[varname];
                        try{
                            graphicEngine.setViewportScene(sceneVis.viewport, gameEntities[varname]);                            
                        } catch (e){
                            sceneVis.viewport = graphicEngine.createViewport(sceneVis.orix, sceneVis.oriy, sceneVis.width, sceneVis.height, gameEntities[varname]);
                        }

                    }
                }
                this.div.show(effect, options, time, callback);
            },
            hide : function(effect, options, time, callback){
                this.div.hide(effect, options, time, callback);
            }
        }),
        GUI = {
            screens : {},
            create : function(guiDefinition, initData){
                var id, 
                    dummy = {},
                    screenid,
                    screenDef,
                    screens = this.screens;

                if (typeof initData === "undefined"){
                    initData = dummy;
                }
                for (id in guiDefinition){
                    if (typeof screens[id] === "undefined"){ //cuando una pantalla se inicializa por segunda vez, pu
                        screens[id] = new Screen(id, guiDefinition[id], (typeof initData[id] === "undefined") ? dummy : initData[id]);
                    }
                }
                for (id in guiDefinition){
                    screenDef = guiDefinition[id].screens;
                    if (typeof screenDef !== "undefined"){
                        for (screenid in screenDef){
                            try {
                                screens[id].screens[screenid] = screens[screenDef[screenid]];
                            } catch (e){
                                console.log(e.toString(), e.stack);
                            }
                        }
                    }
                }
            },
            getScreen : function(screenid){
                return this.screens[screenid];
            },
            show : function (screenid){
                var id,
                    screens = this.screens,
                    screen = screens[screenid];
                for (id in screens){
                    screens[id].hide();
                }
                if (typeof screen !== "undefined"){
                    screen.show();
                }
            },
            isScreen : function(screentype){
                return typeof this.screens[screentype] !== "undefined";
            }
        };

    return GUI;
}(CalipsoWebClient.GameEntities, CalipsoWebClient.Controller.CanvasController, Class, CalipsoWebClient.Graphics.GraphicEngine, CalipsoWebClient.Graphics.Scene, CalipsoWebClient.HUD, CalipsoWebClient.Graphics, CalipsoWebClient.Graphics.Graphics2D, CalipsoWebClient.GameEntities.GameEntityView));
