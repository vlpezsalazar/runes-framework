/*global CalipsoWebClient, $, Class, toType*/
CalipsoWebClient.GameEntities = (function(Task, Scheduler, Message, Graphics2D){
/*jslint regexp: false */
    var template = /(object|array)\s*<\s*(string|number|@?.+)(?:\s*,\s*(@?.+)\s*)?>/,
        pat = /_(\w+)_/,
        GameEntityMethods,
        GameEntities, 
        State;
/*jslint regexp: true */

    function getGameEntityCallback(gameEntity, property){
        return function(obj){
            gameEntity.setStateVar(property, obj);
        };
    }
    
    function resolveReferencesAndStoreInto(container, method, state, statevarName, mustSet, referenceType, instanceDefinition){
        var presentType = referenceType.substring(1, referenceType.length),
            gameEntity,
            i;

        if (typeof GameEntities.gameEntityClasses[presentType] !== "undefined"){
            for (i in instanceDefinition){
                gameEntity = GameEntities.get(instanceDefinition[i]);
                if (typeof gameEntity !== "undefined" && 
                    gameEntity.className === presentType){
                    method.call(container, state, statevarName, mustSet, i, gameEntity);
                }
            }
        }
        return container;
    }

    function getPushCallback(gameEntity, state, statevarName, mustSet, array, callback){
        return function (obj){
            if (mustSet){
                state.push(statevarName, obj);
            } else {
                array.push(obj);
            }
        };
    }
    function getPutCallback(gameEntity, state, statevarName, mustSet, object, callback){
        return function (obj, i){
            if (mustSet){
                state.put(statevarName, i, obj);
            } else {
                object[i] = obj;
            }
        };
    }
    /**
     * Función para insertar dentro de un array usada en parseComplexType. 
     */
    function push(state, statevarName, mustSet, i, obj){
        if (mustSet){
            state.push(statevarName, obj);
        } else {
            this.push(obj);
        }
    }

    /**
     * Función para insertar dentro de un objeto usada en parseComplexType. 
     */    
    function put(state, statevarName, mustSet, i, obj){
        if (mustSet){
            state.put(statevarName, i, obj);
        } else {
            this[i] = obj;
        }
    }
    
    function parseComplexType(gameEntity, state, statevarName, mustSet, type, instanceDefinition, whenLoadedCallback){ //versión recursiva
        var i,
            instance,
            presentType,
            presentInstance = instanceDefinition,
            complexType = template.exec(type),
            mycallback,
            container,
            gameEntityTypeIndex,
            reference;

        if (complexType !== null){
            presentType = complexType[1];
            if (toType(presentInstance) === presentType){
                if (presentType === "array"){
                    gameEntityTypeIndex = 2;
                    if (mustSet){
                        container = state[statevarName];                        
                    } else {
                        container = [];
                    }
                    if (complexType[gameEntityTypeIndex].charAt(0) === "@"){
                        reference = true;
                        mycallback = push;
                    } else {
                        reference = false;
                        mycallback = getPushCallback(gameEntity, state, statevarName, mustSet, container);
                    } 
                } else if (presentType === "object"){
                    gameEntityTypeIndex = 3;
                    if (mustSet){
                        container = state[statevarName];                        
                    } else {
                        container = {};
                    }
                    if (complexType[gameEntityTypeIndex].charAt(0) === "@"){
                        reference = true;
                        mycallback = put;
                    } else {
                        reference = false;
                        mycallback = getPutCallback(gameEntity, state, statevarName, mustSet, container);
                    }
                }
                presentType = complexType[gameEntityTypeIndex];
                if (reference){ //si lo que sigue es una referencia
                    instance = resolveReferencesAndStoreInto(container, mycallback, state, statevarName, mustSet, presentType, presentInstance);
                } else {
                    instance = container;
                    // si es una clase de GameEntity
                    if (typeof GameEntities.gameEntityClasses[presentType] !== "undefined"){
                        for (i in presentInstance){
                            GameEntities.newGameEntity(presentInstance[i], mycallback, i, whenLoadedCallback);
                        }
                    } else if (template.test(presentType)){
                        for (i in presentInstance){
                            mycallback(parseComplexType(gameEntity, state, statevarName, false, presentType, presentInstance[i], whenLoadedCallback), i);
                        }
                    } else { //tipo simple
                        instance = presentInstance;
                    }
                }
            }
        }
        return instance;
    }
    function setInstanceIntoState(gameEntity, state, statevar, callback){
        return function(value){
            state.set(statevar, value);
//            callback(gameEntity);
        };
    }
    function endAnimation(actionMotion){
        var queueMotion = this.getQueueMotionFromAction(actionMotion);
        this.setAnimation();//para el nodo global
        if (queueMotion && queueMotion.loop){
            this.endLoopMotion(queueMotion);
            //console.log('Finishing movement for ', this.getBId());
        }
    }
    function createBufferedAction(actionName, actionMotion, actionFunction) {
        var action = function(){
            var task = this.actions[actionName],
                buffer = task.buffer,
                end = true;
            
            if (buffer.length > 0){
                end = actionFunction.apply(this, buffer[0]);
                if (end){
                    buffer.shift();
                }
                end = (buffer.length === 0);
                if (end){
                    endAnimation.call(this, actionMotion);
                }
            }
            return end;
        };
        action.motionName = actionMotion;
        return action; 
    }
    
    function createBufferedActionNoAnimation(actionName, actionFunction) {
        var action = function(){
            var task = this.actions[actionName],
                buffer = task.buffer,
                end = true;

            if (buffer.length > 0){
                end = actionFunction.apply(this, buffer[0]);
                if (end){
                    buffer.shift();
                }
                end = (buffer.length === 0);
            }
            return end;
        };
        return action; 
    }
    
    function createNormalAction(actionName){
        var action = function(){
            var task = this.actions[actionName];
            task.setActualParameters(arguments);
            if (task.isDone()) {
                //console.log('disparando regla: ', actionName, ' para ', this.getBId());
                this.startMotion(actionName, (arguments.length >0) ? $.extend({}, arguments[0], this.state) : this.state);
                task.setDone(false);
                Scheduler.scheduleGameTask(task);
            }
        };
        action.name = actionName;
        return action;
    }
    function createGraphicUnrelatedAction(actionName){
        var action = function(){
            var task = this.actions[actionName];
            task.setActualParameters(arguments);
            if (task.isDone()) {
                //console.log('disparando regla: ', actionName, ' para ', this.getBId());
                task.setDone(false);
                Scheduler.scheduleGameTask(task);
            }
        };
        return action;
    }
    function putArgumentsInBuffer(actionName, actionMotion){
        var action = function(){
            var task = this.actions[actionName],
                buffer = task.buffer;
            buffer.push(arguments);
            if (task.isDone()) {
                //console.log('disparando regla: ', actionName, ' para Character: ', this.getBId());
                task.setDone(false);
                this.startMotion(actionMotion, (arguments.length >0) ? $.extend({}, arguments[0], this.state) : this.state);
                Scheduler.scheduleGameTask(task);
            }
        };
        return action;
    }
    function putArgumentsInBufferNoAnimation(actionName, actionMotion){
        var action = function(){
            var task = this.actions[actionName],
                buffer = task.buffer;
            buffer.push(arguments);
            if (task.isDone()) {
                //console.log('disparando regla: ', actionName, ' para Character: ', this.getBId());
                task.setDone(false);
                Scheduler.scheduleGameTask(task);
            }
        };
        return action;
    }    
    function createActions(/*object*/actionMethods, type){
        var actionName,
            bufferedAction,
            actions = {
                buffered : {}
            };

        if (typeof type !== "undefined" && type.isChildOf(CalipsoWebClient.Graphics.AnimatableObject)){
            for (actionName in actionMethods){
                bufferedAction = actionName.match(pat);
                if (bufferedAction !== null){
                    actions.buffered[actionName] = createBufferedAction(actionName, bufferedAction[1], actionMethods[actionName]);
                    actions[actionName] = putArgumentsInBuffer(actionName, bufferedAction[1]);
                } else {
                    actions[actionName] = createNormalAction(actionName);
                }
            }
        } else {
            for (actionName in actionMethods){
                bufferedAction = actionName.match(pat);
                if (bufferedAction !== null){
                    actions.buffered[actionName] = createBufferedActionNoAnimation(actionName, actionMethods[actionName]);
                    actions[actionName] = putArgumentsInBufferNoAnimation(actionName, bufferedAction[1]);
                } else {
                    actions[actionName] = createGraphicUnrelatedAction(actionName);
                }
            }
        }
        return actions;
    }
    function onFinishLoadGameEntitiesCall(minimum, callback){
        var loadedAndReferencesSet = 0,
            gameEntities = GameEntities.cache;
        return function(instance){
            var i;

            try {
                loadedAndReferencesSet+=1;
                if (loadedAndReferencesSet >= minimum && loadedAndReferencesSet === GameEntities.loaded){
                    for (i in gameEntities){
                        console.log("DEBUG:", "Inicializando", gameEntities[i]);
                        gameEntities[i].initialize();
                    }
                    console.log("DEBUG:", "Finalizada inicialización, callback = ", callback);
                    if (typeof callback === "function"){
                        console.log("DEBUG:", "Llamando a callback");
                        callback(instance);                    
                    }                    
                }
            } catch (e){
                console.log(e);
            }
        };
    } 
    function createInitStateCallback(obj, id, stateSchema, state, callback, whenLoadedCallback){
        return function(obj){
            obj.state = new State(obj, stateSchema, state, callback, whenLoadedCallback);
//            if (typeof obj.setBId === "undefined"){
                obj.setBId = function(id){
                    this.id = id; //FIXME cambiar el nombre de "berserkerid" por cid (calipso id) aquí, en los drawableObjects y allí donde se use 
                };
//            }
//            if (typeof obj.getBId === "undefined"){
                obj.getBId = function(){ // FIXME cambiar el nombre de getBId por getCId aquí y en los drawableObjects y allí donde se use
                    return this.id;
                };
//            }
            obj.setBId(id);
            callback(obj);
        };
    }
    function whenDoneFinishAnimationCallback(/*function*/f, actionMotion, serverMessage, messageData){
        var actionFunction;
        if (typeof messageData !== "undefined"){
            actionFunction = function (){
                var end = f.apply(this, arguments),
                    queueMotion;
                //console.log("whendonefinish...");
                if (end && this.modelType.isChildOf(CalipsoWebClient.Graphics.AnimatableObject)){
                    this.setAnimation();//para el nodo global
                    //console.log('Finishing movement for ', this);
                    queueMotion = this.getQueueMotionFromAction(actionMotion);
                    if (queueMotion && queueMotion.loop){
                        this.endLoopMotion(queueMotion);
                    }
                }
                serverMessage.setData(messageData);
                //console.log("enviando mensaje al servidor desde whendonefinish...");
                CalipsoWebClient.pushMessage(serverMessage);                
                return end;
            };
        } else {
            actionFunction = function (){
                var end = f.apply(this, arguments),
                    queueMotion;
                //console.log("whendonefinish without message...");
                if (end && this.modelType.isChildOf(CalipsoWebClient.Graphics.AnimatableObject)){
                    this.setAnimation();//para el nodo global
                    //console.log('Finishing movement for ', this);
                    queueMotion = this.getQueueMotionFromAction(actionMotion);
                    if (queueMotion && queueMotion.loop){
                        this.endLoopMotion(queueMotion);
                    }
                }
                return end;
            };            
        }
        return actionFunction;
    }
//     //caller y calle de los metodos de estado de la GameEntity no funcionan si está en modo estricto
    //var template = /(array)\s*<\s*(@?.+)\s*>|(object)\s*<\s*(string|number)\s*,\s*(@?.+)\s*>/,
    State = Class.extend({
                    init : function(gameEntity, stateSchema, state, callback, whenLoadedCallback){
                        var statevarName,
                            statevar,
                            statevarType,
                            type,
                            complexTypes,
                            typeProperties;

                        this.schema = stateSchema;
                        this.gameEntity = gameEntity;
                        //inicialización del esquema
                        for (statevarName in stateSchema){
                            typeProperties = stateSchema[statevarName];
                            if (toType(typeProperties) === "object"){
                                type = typeProperties.type;
                            } else {
                                stateSchema[statevarName] = {
                                    type : typeProperties
                                };
                                type = typeProperties;
                                typeProperties = stateSchema[statevarName];
                            }
                            statevar = (typeof state[statevarName] !== "undefined") ? state[statevarName] : typeProperties.init;
                            if (type.charAt(0) === "@" || type.indexOf(":") > -1){
                                GameEntities.setGameEntityToBeResolved(gameEntity);
                                this.gameEntity.referencesToBeResolved.push(statevarName);
                                this[statevarName] = statevar; //si el id de la referencia nos viene del servidor lo recordamos aquí para resolverlo luego.
                            } else if (template.test(type)){//objeto complejo con tipo array<XXXX> o object<string|number, XXXX>
                                complexTypes = template.exec(type);
                                typeProperties.primaryType = complexTypes[1];
                                typeProperties.secondaryType = complexTypes[2];
                                if (typeProperties.primaryType === "object") {
                                    this[statevarName] = {};
                                    typeProperties.terciaryType = complexTypes[3];
                                } else {
                                    this[statevarName]=[];
                                }
                                if (typeof statevar !== "undefined"){
                                    this[statevarName] = parseComplexType(gameEntity, this, statevarName, true, type, statevar, whenLoadedCallback);
                                }
                            } else {
                                typeProperties.primaryType = type;
                                if (typeof GameEntities.gameEntityClasses[type] !== "undefined"){
                                    if (GameEntities.gameEntityClasses[type].prototype.client){
                                        GameEntities.newGameEntity({
                                            className : type,
                                            model : typeProperties.model,
                                            id : ((typeof typeProperties.id !== "undefined") ? typeProperties.id : statevarName),
                                            state : ((typeof typeProperties.init !== "undefined") ? typeProperties.init : {})
                                        }, setInstanceIntoState(gameEntity, this, statevarName, callback), undefined, whenLoadedCallback);
                                    } else if (typeof statevar !== "undefined"){
                                        GameEntities.newGameEntity(statevar, setInstanceIntoState(gameEntity, this, statevarName, callback), undefined, whenLoadedCallback);
                                    }
                                } else {
                                    statevarType = toType(statevar);
                                    if (statevarType === "undefined"){
                                        if (type === "object") {
                                            this[statevarName] = {};
                                        } else if (type === "array"){
                                            this[statevarName]=[];
                                        }
                                    } else if ( statevarType === type) {
                                        this.set(statevarName, statevar);
                                    }
                                }
                            }
                        }
                    },
                    resolveReference : function(statevarName){
                        var stateSchema = this.schema, 
                            references = this.gameEntity.referencesToBeResolved, 
                            statevar,
                            reftype,
                            type,
                            whereid,
                            whereproperty,
                            property,
                            typeProperties,
                            reference,
                            referenceIndex,
                            end,
                            complexTypes,
                            store = GameEntities.cache;

                        referenceIndex = references.indexOf(statevarName);
                        if (referenceIndex > -1){
                            references.splice(referenceIndex, 1);
                            typeProperties = stateSchema[statevarName];
                            reftype = typeProperties.type;
                            whereid = reftype.indexOf(":");
                            end = reftype.length;
                            if (whereid === -1){
                                whereid = reftype.indexOf("[");
                                if (whereid > -1) {
                                    end--;
                                    store = this;
                                }
                            }

                            type = reftype.substring((reftype.charAt(0) === "@") ? 1 : 0, (whereid>-1) ? whereid : reftype.length);
                            if (template.test(type)){//objeto complejo con tipo array<XXXX> o object<string|number, XXXX>
                                complexTypes = template.exec(type);
                                typeProperties.primaryType = complexTypes[1];
                                typeProperties.secondaryType = complexTypes[2];
                                if (typeProperties.primaryType === "object") {
                                    typeProperties.terciaryType = complexTypes[3];
                                }
                            } else {
                                typeProperties.primaryType = type;
                            }
                            if (whereid > -1){
                                whereproperty = reftype.indexOf(".");
                                if (whereproperty > -1){
                                    property = reftype.substring(whereproperty+1, end);
                                    statevar = store[reftype.substring(whereid+1, whereproperty)];
                                } else {
                                    statevar = store[reftype.substring(whereid+1, reftype.length)];
                                }
                            } else {
                                statevar = store[this[statevarName]];
                            }
                            if (whereproperty > -1){
                                reference = statevar.getStateVar(property);
                                if (typeof reference === "undefined"){
                                    reference = statevar.state.resolveReference(property);
                                }
                                if (typeof reference !== "undefined" || typeof statevar.stateSchema[property] !== "undefined"){
                                    statevar.addUpdateCallbackOnStateVar("onSet", property, "reference" + this.gameEntity.getBId(), getGameEntityCallback(this.gameEntity, property));
                                }
                            } else {
                                reference = statevar;
                            }
                        }
                        return reference;
                    },
                    set : function(property, value){
                        var type = (typeof value.className !== "undefined") ? value.className : toType(value),
                            type1 = this.schema[property].primaryType,
                            callback,
                            callbackFunctions =  this.gameEntity.updateSchema[property],
                            previousValue = this[property],
                            onDelete,
                            onSet,
                            onPush,
                            f,
                            communications = this.gameEntity.communications[property],
                            i;

                        if (type1 === type){
                            if (typeof callbackFunctions !== "undefined"){
                                onSet = callbackFunctions.onSet;
                                onDelete = callbackFunctions.onDelete;
                                onPush = callbackFunctions.onPush;
                                /*
                                 * si lo que estamos estableciendo es un array o un objeto
                                 * aplicamos los callbacks a cada una de los elementos
                                 * del array o las propiedades del objeto.
                                 */
                                if (type === "array" || type === "object"){
                                    for (f in onDelete){
                                        callback = onDelete[f];
                                        for (i in previousValue){
                                            callback.call(this.gameEntity, previousValue[i]);
                                        }
                                    }
                                    for (f in onPush){
                                        callback = onPush[f];
                                        for (i in value){
                                            callback.call(this.gameEntity, value[i]);
                                        }
                                    }                                        

                                }
                                for (f in onSet){
                                    onSet[f].call(this.gameEntity, value);
                                }
                            }
                            if (typeof communications !== "undefined"){
                                //console.log("Estableciendo propiedad", property, "para enviar al servidor (communications = ", communications, ")");
                                for (i in communications){
                                    communications[i][property] = value;
                                }
                            }
                            this[property] = value;
                        } else {
                            throw new Error("La propiedad  " + property + " no es del tipo " + type + " que se intentó asignar");
                        }
                    },
                    push : function(property, value){
                        var type = (typeof value.className !== "undefined") ? value.className : toType(value),
                            onPush,
                            callbackFunctions =  this.gameEntity.updateSchema[property],
                            communications = this.gameEntity.communications[property],
                            i,
                            f;

                        if (this.schema[property].primaryType === "array" && 
                            this.schema[property].secondaryType === type){
                            this[property].push(value);
                            if (typeof callbackFunctions !== "undefined"){
                                onPush = callbackFunctions.onPush;
                                for (f in onPush){
                                    onPush[f].call(this.gameEntity, value);                                                     
                                }
                            }
                            if (typeof communications !== "undefined"){
                                for (i in communications){
                                    communications[i][property] = value;
                                }
                            }
                        } else {
                            throw new Error("La propiedad  " + property + " no es del tipo " + type + " que se intentó asignar");
                        }
                    },
                    put : function(property, id, value){
                        var type = (typeof value.className !== "undefined") ? value.className : toType(value),
                            previousValue,
                            onPush,
                            onDelete,
                            callbackFunctions =  this.gameEntity.updateSchema[property],
                            communications = this.gameEntity.communications[property],
                            i,
                            f;
                        
                        if (this.schema[property].primaryType === "object" /*&& (
                            typeof this.schema[property].terciaryType === "undefined" ||  
                            this.schema[property].secondaryType === toType(id) &&
                            this.schema[property].terciaryType === type))*/){
                            previousValue = this[property][id];
                            if (typeof callbackFunctions !== "undefined"){
                                onPush = callbackFunctions.onPush;
                                if (typeof(previousValue !== "undefined")){
                                    for (f in onDelete){
                                        onDelete[f].call(this.gameEntity, previousValue);
                                    }                                    
                                }
                                for (f in onPush){
                                    onPush[f].call(this.gameEntity, value);                                                     
                                }
                            }
                            if (typeof communications !== "undefined"){
                                for (i in communications){
                                    communications[i][property] = value;
                                }
                            }
                            this[property][id] = value;
                        } else {
                            throw new Error("La propiedad  " + property + " no es del tipo " + type + " que se intentó asignar");
                        }
                    },
                    remove : function (/*string*/property, /*[string|number]*/id){
                        var callbackFunctions =  this.gameEntity.updateSchema[property],
                            propType = this.schema[property].primaryType,
                            type,
                            previousValue,
                            onDelete,
                            f;

                        if (typeof this.schema[property] !== "undefined"){
                            if (propType === "array"){
                                type = (typeof id.className !== "undefined") ? id.className : toType(id);
                                if (type === this.schema[property].secondaryType){
                                    id = this[property].indexOf(id);
                                }
                                if (!isNaN(parseFloat(id)) && isFinite(id)){ //test to check for a number
                                    if (id > -1 && this[property].length > id){
                                        previousValue = this[property][id];
                                        this[property].splice(id, 1);
                                    }
                                }
                            } else if (propType === "object"){
                                previousValue = this[property][id];
                                delete this[property][id];
                            } else {
                                previousValue = this[property];
                                delete this[property];
                            }
                            if (typeof callbackFunctions !== "undefined" && typeof previousValue !== "undefined"){
                                onDelete = callbackFunctions.onDelete;
                                /*
                                 * si lo que estamos estableciendo es un array o un objeto
                                 * aplicamos los callbacks a cada una de los elementos
                                 * del array o las propiedades del objeto.
                                 */
                                for (f in onDelete){
                                    onDelete[f].call(this.gameEntity, previousValue);
                                }
                            }
                            /*m
                            if (typeof communications !== "undefined"){
                                communications = communications[rule];
                                communications.data[property] = undefined;
                                CalipsoWebClient.pushMessage(communications.message);
                            }
                            */
                        } else {
                            throw new Error("La propiedad  " + property + " no es del tipo " + type + " que se intentó asignar");
                        }
                    }
    });
    GameEntityMethods = {
        init : function(id, state, model, callback, whenLoadedCallback){
            var actionName,
                i,
                commActionList,
                commByAction = {},
                property,
                data,
                actions = this.actionMethods,
                ruleName,
                message,
                communications = this.communications,
                messageData,
                initState = createInitStateCallback(this, id, this.stateSchema, state, callback, whenLoadedCallback),
                classMethods = ["getBId", "setBId"];

            this.referencesToBeResolved = []; //nombres de variables de estado que son referencias y que se deben resolver posteriormente cuando estas estén instanciadas.
            this.properties = {}; //propiedades temporales se emplean p.e. en berserker.menu para guardar el indice de posición de una entidad personaje
            this.behaviours = {};
            this.actions = {};
            this.finish = {};
            this.communications = {};
            //necesario para crear variables frescas dentro de cada categoría y que no se modifiquen las de todas las instancias;
            this.updateSchema = $.extend(true, {}, this.updateSchema);


            for (property in communications){
                commActionList  = communications[property];
                this.communications[property] = [];
                for (i in commActionList){
                    actionName = commActionList[i];
                    data = commByAction[actionName];
                    ruleName = actions[actionName].name;
//                    console.log("actionName =", actionName, "data =", data, "ruleName =", ruleName, "actions =", actions);
                    if (ruleName !== ""){
                        if (typeof data === "undefined"){
                            data = {rule : ruleName};
                            commByAction[actionName] = data;
                        }
                        this.communications[property].push(data);
                    }
                }
            }
//            console.log("commByAction = ", commByAction);
            for (actionName in actions){
                if (typeof this.buffered[actionName] !== "undefined"){
                    this.actions[actionName] = new CalipsoWebClient.Scheduler.Task(this, this.buffered[actionName]);
                    this.actions[actionName].buffer = [];
                    this.actions[actionName].setDone();//onFinish = endAnimation(this, actionName);
                } else {
                    messageData = commByAction[actionName];
                    if (typeof messageData !== "undefined"){
                        messageData.ge = id;
                        message = new Message(messageData);
                    }
                    //console.log("action:", actionName, "messageData:", messageData);
                    this.actions[actionName] = new CalipsoWebClient.Scheduler.Task(this, whenDoneFinishAnimationCallback(actions[actionName], actionName, message, messageData));
                    this.actions[actionName].setDone();
                }
                classMethods.push(actionName);
            }
            if (typeof model !== "undefined"){
                if (model instanceof this.modelType){
                    this.decorator(model, classMethods);
                    this.objectnode = model.objectnode;
                    initState(this);
                } else {
                    if (toType(model) === "array"){
                        model.push(initState);
                    } else {
                        model = [model, initState];
                    }
                    this.$super.apply(this, model);
                }
            } else if (typeof this.modelType === "undefined"){
                initState(this);
            } else {
                throw new Error("La GameEntity " + this + " no contenía datos para inicializar el modelo 3D.");
            }
        },
        initialize : function(){
            var references = this.referencesToBeResolved,
                behavioursDefinitions,
                behaviourid,
                statevarName,
                reference,
                i,
                Behaviour = CalipsoWebClient.Behaviours.Behaviour;
            try {
                if (!this.initialized){
                    for (i=0; i<references.length; ){
                        statevarName = references[i];
                        reference = this.state.resolveReference(statevarName);
                        if (typeof reference !== "undefined"){
                            this.state.set(statevarName, reference);
                        } else {
                            i++;
                            console.log("WARNING:", "La referencia a", statevarName, "no ha podido ser resuelta para la GameEntity", this.className, ":", this.getBId());
                        }

                    }
                    if (references.length === 0){
                        if (typeof this.initMethod !== "undefined"){
                            this.initMethod(this.state);
                        }
                        behavioursDefinitions = this.behavioursSchema;
                        for (behaviourid in behavioursDefinitions){
                            this.behaviours[behaviourid] = new Behaviour(behavioursDefinitions[behaviourid], this);    
                        }
                        this.initialized = true;
                    } else {
                        console.log("WARNING: QUEDAN REFERENCIAS SIN RESOLVER PARA LA GAME ENTITY ", this.className, ":", this.getBId());
                    }
                }                
            } catch (e){
                console.log(e);
            }

        },
        runBehaviour : function(/*string*/ beaviourid){
            if (typeof this.behaviours[beaviourid] !== "undefined"){
                this.behaviours[beaviourid].run();
            }
        },
        hasView : function(/*string*/ viewId){
            return (typeof this.viewSchema[viewId] !== "undefined");
        },
        getViewSchema : function(/*string*/ viewId){
            return this.viewSchema[viewId];
        },
        /**
         * This method finishes an action. 
         * @param action name of the action to finish.
         */
        finishAction : function(/*string*/action){
            var actions = this.actions,
                Graphics = CalipsoWebClient.Graphics,
                task, 
                buffer;
            if (actions.hasOwnProperty(action)) {
                //console.log(this, 'finalizando acción: ', action);
                task = actions[action];
                if (typeof this.buffered[action] !== "undefined"){
                    buffer = task.buffer;
                    buffer.splice(0, buffer.length);
                    task.setDone();
                    if (typeof this.modelType !== "undefined" && this.modelType.isChildOf(Graphics.AnimatableObject)){
                        endAnimation.call(this, this[action].motionName);
                    }
                }  else {
                    task.setDone();
                    if (typeof this.modelType !== "undefined" && this.modelType.isChildOf(Graphics.AnimatableObject)){
                        endAnimation.call(this, action);
                    }
                }
            }
        },

        removeUpdateCallbackOnStateVar : function(varName, callbackName){
            var callbacks = this.updateSchema[varName];
            if (typeof callbacks !== "undefined"){
                if (typeof callbacks.onSet[callbackName] !== "undefined"){
                    delete callbacks.onSet[callbackName];
                } else if (typeof callbacks.onPush[callbackName] !== "undefined"){
                    delete callbacks.onPush[callbackName];
                } else if (typeof callbacks.onDelete[callbackName] !== "undefined"){
                    delete callbacks.onDelete[callbackName];
                }
            }
        },
        addUpdateCallbackOnStateVar : function(callbacktype, varName, callbackName, callback){
            var callbacks = this.updateSchema[varName];
            if (typeof callbacks === "undefined"){
                callbacks = {
                        onSet : {},
                        onPush :{},
                        onDelete : {}
                };
                this.updateSchema[varName] = callbacks;
            }
            callbacks[callbacktype][callbackName] = callback;
        },
        getGraphicType : function(){
            return this.modelType;
        },
        getGraphicModel : function(){
            var model;
            if (typeof this.modelType !== "undefined"){
                model = (typeof this.decoratorObject !== "undefined") ? this.decoratorObject : this; 
            } 
            return model;
        },
        putProperty : function(propertyName, value){
            this.properties[propertyName] = value;
        },
        getProperty : function(propertyName){
            return this.properties[propertyName];
        },
        getStateVar : function(propertyName){
            return this.state[propertyName];
        },
        setStateVar : function(propertyName, obj){
            this.state.set(propertyName, obj);
        },
        pushStateVar : function(propertyName, obj){
            this.state.push(propertyName, obj);
        }
    };

    GameEntities = {
            //para resolver a que id del servidor se refieren los singletonIds definidos en el cliente
            alias : {},
            cache : {},
            // variable temporal para cada vez que se llama al
            // método initializeGameEntities
            loaded : 0, 
            gameEntityClasses : {},
            /*
             * Al crear las GameEntities, se dejan las referencias (tipos con @)
             * de estas sin resolver pues puede que si hacen referencia a otra
             * GameEntity, esta aún no haya sido creada. Esta variable, se
             * se utiliza para almacenar qué gameEntities tienen referencias
             * por resolver. 
             * 
             */
            referencesToBeResolved : [],
            /**
             * 
             * This allows the creation of a game entity class.
             * 
             * @param {string} classSchema an object containing the following
             *  fields:
             *  <ul> {string} modelType: type of Graphic class this entity has.
             *  <li> {object} stateSchema: definition of the fields in the 
             *      class name:type.
             *  <li> {object} actions : actions which controls the behavior of the
             *       class.
             *  <li> {object} methods : class methods.
             *  </ul>
             * 
             */
            createGameEntityClasses : function(/*object*/classesSchemas){
                var GameEntityClass,
                    updateSchemaProperty,
                    GraphicType, 
                    classSchema,
                    prop,
                    className,
                    modelCallback,
                    Behaviours = CalipsoWebClient.Behaviours;

                for (className in classesSchemas){
                    classSchema = classesSchemas[className];
                    //console.log("creando", className, "=", classSchema);
                    GraphicType = CalipsoWebClient.Graphics[classSchema.modelType];
                    classSchema.updateSchema = {};
                    classSchema.communications = {};
                    for (updateSchemaProperty in classSchema.stateSchema){
                        prop = classSchema.stateSchema[updateSchemaProperty];
                        if (typeof prop === "object"){
                            if (typeof GraphicType !== "undefined" && typeof prop.modelCallback !== "undefined"){
                                modelCallback = prop.modelCallback;
                                classSchema.updateSchema[updateSchemaProperty] = {};
                                if (typeof modelCallback === "object"){
                                    if (typeof modelCallback.onSet !== "undefined"){
                                        classSchema.updateSchema[updateSchemaProperty].onSet = {
                                                modelCallback : GraphicType.prototype[modelCallback.onSet]
                                        };
                                    } else {
                                        classSchema.updateSchema[updateSchemaProperty].onSet = {};
                                    }
                                    if (typeof modelCallback.onPush !== "undefined"){
                                        classSchema.updateSchema[updateSchemaProperty].onPush = {
                                                modelCallback : GraphicType.prototype[modelCallback.onPush]
                                        };
                                    } else {
                                        classSchema.updateSchema[updateSchemaProperty].onPush = {};
                                    }
                                    if (typeof modelCallback.onDelete !== "undefined"){
                                        classSchema.updateSchema[updateSchemaProperty].onDelete = {
                                                modelCallback : GraphicType.prototype[modelCallback.onDelete] 
                                        };
                                    } else {
                                        classSchema.updateSchema[updateSchemaProperty].onDelete = {};
                                    }
                                } else {
                                    classSchema.updateSchema[updateSchemaProperty].onSet =  {
                                            modelCallback : GraphicType.prototype[modelCallback]
                                    };
                                }
                            }

                            //console.log("prop.notifyServerWith = ", prop.notifyServerWith);
                            
                            if (typeof prop.notifyServerWith !== "undefined"){
                                classSchema.communications[updateSchemaProperty] = prop.notifyServerWith;
                            }
                        }
                    }
                    if (typeof classSchema.methods !== "undefined" &&
                        typeof classSchema.methods.init !== "undefined"){
                        classSchema.methods.initMethod = classSchema.methods.init;
                        delete classSchema.methods.init;
                    }

                    GameEntityClass = ((typeof GraphicType !== "undefined") ? GraphicType : Class).extend($.extend({
                            modelType           : GraphicType,
                            singletonId         : classSchema.singletonId,
                            className           : className,
                            client              : classSchema.client,
                            userEvents          : classSchema.userEvents,
                            behavioursSchema    : Behaviours.defineBehaviours(classSchema.behaviours, classSchema.stateSchema),
                            actionMethods       : classSchema.actions,
                            updateSchema        : classSchema.updateSchema,
                            communications      : classSchema.communications,
                            stateSchema         : classSchema.stateSchema,
                            viewSchema          : (typeof classSchema.views !== "undefined") ? classSchema.views : {} 
                        }, GameEntityMethods, classSchema.methods, createActions(classSchema.actions, GraphicType)));
                    this.gameEntityClasses[className] = GameEntityClass;
                }
            },
            initializeGameEntities : function(/*array*/definitions, callback){
                var i, 
                    onEndCallback;
                if (toType(definitions) !== "array"){
                    definitions = [definitions];
                }
                onEndCallback = onFinishLoadGameEntitiesCall(definitions.length, callback);
                this.loaded = 0;
                for (i in definitions){
                    this.newGameEntity(definitions[i], undefined, undefined, onEndCallback);
                }
            },
            newGameEntity : function(/*object*/definition, /*function*/callback, callbackparams, whenLoadedCallback){
                var Graphics = CalipsoWebClient.Graphics,
                    GameEntityClass = this.gameEntityClasses[definition.className],
                    dummy,
                    gt = (typeof GameEntityClass.prototype.modelType !== "undefined") ? GameEntityClass.prototype.modelType : Class,
                    /*
                     * Para los ids, en la caché usamos el del singletonId
                     * definido para la GameEntity si existe o los que llegan
                     * desde el servidor, mientras que para las GameEntity
                     * utilizamos siempre los que llegan del servidor.
                     */ 
                    id = (typeof GameEntityClass.prototype.singletonId !== "undefined") ? GameEntityClass.prototype.singletonId : definition.id;

                GameEntities.loaded++;
                if (typeof GameEntities.cache[id] !== "undefined" ){
                    if (typeof callback === "function"){
                        callback(GameEntities.cache[id], callbackparams);
                    }
                    if (typeof whenLoadedCallback === "function" && whenLoadedCallback !== callback){
                        whenLoadedCallback(GameEntities.cache[id]);
                    }
                } else {
                    if (gt.isChildOf(Graphics.AnimatableObject)){
                        Graphics.createModel(
                                definition.model,
                                function(model) {
                                    dummy = new GameEntityClass(definition.id, definition.state, model, function(instance){
                                        GameEntities.cache[id] = instance;
                                        if (typeof callback === "function"){
                                            callback(instance, callbackparams);
                                        }
                                        if (typeof whenLoadedCallback === "function" && whenLoadedCallback !== callback){
                                            whenLoadedCallback(instance);
                                        }
                                    }, whenLoadedCallback);
                                });
                    
                    } else if (gt.isChildOf(Graphics.DrawableObject) && !gt.isChildOf(Graphics.Scene)){
                        Graphics.getDrawable(definition.model, function(model){
                            dummy = new GameEntityClass(definition.id, definition.state, model, function(instance){
                                GameEntities.cache[id] = instance;
                                if (typeof callback === "function"){
                                    callback(instance, callbackparams);
                                }
                                if (typeof whenLoadedCallback === "function" && whenLoadedCallback !== callback){
                                    whenLoadedCallback(instance);
                                }

                            }, whenLoadedCallback);
                        });
                    } else {//if (typeof gt === "undefined" || gt.isChildOf(Graphics.Scene)){
                        dummy = new GameEntityClass(definition.id, definition.state, definition.model, function(instance){
                            GameEntities.cache[id] = instance;
                            if (typeof callback === "function"){
                                callback(instance, callbackparams);
                            }
                            if (typeof whenLoadedCallback === "function" && whenLoadedCallback !== callback){
                                whenLoadedCallback(instance);
                            }
                        }, whenLoadedCallback);
                    }
                    
                    
                    if (typeof GameEntityClass.prototype.singletonId !== "undefined"){
                        this.alias[definition.id] = id;
                    }
                }
            },
            setGameEntityToBeResolved : function(gameEntity){
                var references = this.referencesToBeResolved;
                if (references.indexOf(gameEntity) < 0){
                    references.push(gameEntity);
                }
            },
            get : function(id){
                var alias = this.alias[id];
                return this.cache[id] || this.cache[alias];
            }
        };
    return GameEntities;
}(CalipsoWebClient.Scheduler.Task, CalipsoWebClient.Scheduler.Scheduler, CalipsoWebClient.Messages.Message, CalipsoWebClient.Graphics.Graphics2D));
