/*global $, CalipsoWebClient, Class, console, requestAnimationFrame, window */

/**
 *  
 * Scheduler package. It holds classes to control and schedule tasks.
 * 
 * @author Víctor López Salazar.
 */

/**
 * Package's object. It holds the following package classes:
 * <ul>
 * <li> Timer
 * <li> Task
 * <li> TaskSequence
 * <li> Scheduler
 * <ul>
 */
CalipsoWebClient.Scheduler = (function(){
    var Scheduler = {};
    // console.log('en scheduler');
    if (!Date.now) {
        Date.now = function now() {
            return +new Date();
        };
    }
    // console.log('definiendo timer');
    
    Scheduler.Timer = (function() {
        var basetime = Date.now(),
        /**
         * This time is the base time for all the clients. It is used to prevent
         * time lags and synchronize the game time in each client. It is the
         * difference between the time in the server and the time in the client:
         * 
         * basetime = client_time - server_time
         * 
         * So the time = basetime
         */
            serverTime = null;
        return {
            getPresentTime : function() {
                var time = +Date.now() - basetime;
                return {
                    quantum : (serverTime !== null) ? serverTime + (time / 400) : null,
                    time : time % 400
                };
            },
            setServerTime : function(time) {
                basetime = +Date.now();
                serverTime = +time;
            },
            getLocalTime : function() {
                return Date.now();
            }
        };
    }());
    
    // console.log('Scheduler: Timer definido');
    
    /**
     * The Task class define a task to be scheduled by the Scheduler. This class
     * will have many instances and will be the base class for the Task hierarchy,
     * so the class definition is done by its prototype.
     * 
     * @constructor
     * 
     * @param {Object} object
     *            object which contains the reference to the function. It will be
     *            the context for the func function.
     * @param {Function} func
     *            task to do. It should return true when the task is finished or it
     *            can not be done and false in any other case.
     * @param [{Date}] date
     *            specifies when the task should start.
     */
    /*
     * Class.create('Task', function(object, func, date){ this.object = object;
     * this.executionDate = date; this.id = Scheduler.Task.id++;
     * this.fn = func; this.done = false; }, { setDate : function(date){
     * this.executionDate = date; },
     * 
     * isReady : function(){ if (!this.executionDate) return true; return
     * this.executionDate < Scheduler.Timer.getLocalTime(); }, isDone :
     * function(){ return this.done; },
     * 
     * setDone : function(done){ if (done == undefined) done = true; this.done =
     * done; }, doIt :function(){ if (!this.done){ this.done =
     * this.fn.call(this.object); } }, getId : function(){ return this.id; } });
     */
    Scheduler.Task = Class.extend({
        init : function(object, func, date, resilent) {
            this.object = object;
            if (typeof date === "boolean"){
                this.resilent = date;
            } else {
                this.executionDate = date;
                this.resilent = resilent;
            }
            this.id = Scheduler.Task.id++;
            this.fn = func;
            this.done = false;
        },
        setDate : function(date) {
            this.executionDate = date;
        },
        /**
         * Checks if the task is ready for execution (if it has reached the
         * execution date).
         * 
         * @returns {Boolean} true if the tasks is ready, false in any other case.
         */
    
        isReady : function() {
            if (!this.executionDate){
                return true;            
            }
            return this.executionDate < Scheduler.Timer
                    .getLocalTime();
        },
        /**
         * Checks if the task is done (if the method doIt() has been run).
         * 
         * @returns {Boolean} true if the tasks has been done, false in any other
         *          case.
         */
        isDone : function() {
            return this.done;
        },
        /**
         * This method sets the done flag to true or false. A call without
         * parameters is the same as setDone(true).
         * 
         * @param {Boolean}
         *            done, the value to set in the done flag.
         */
        setDone : function(done) {
            if (typeof done === "undefined"){
                done = true;
            }
            this.done = done;
        },
        setActualArguments : function(){
            this.fnArguments = arguments;
        },
        setActualParameters : function(/*array*/parameters){
            this.fnArguments = parameters;
        },
        /**
         * Calls the function associated to the task if it's not done yet.
         */
        doIt : function() {
            try {
                if (!this.done) {
                    this.done = this.fn.apply(this.object, this.fnArguments);
                }
            } catch (e) {
                console.log(e.toString(), e.stack);
                if (!this.resilent){
                    this.done = true;
                }
            }
        },
        /**
         * Returns the id assigned to this task.
         * 
         * @returns {Number} the id assigned to the task.
         */
        getId : function() {
            return this.id;
        }
    });
    Scheduler.Task.id = 0;
    
    // console.log('dentro de Scheduler: task definidas');
    /**
     * This class represents a sequence of tasks which has to be done in order. For
     * each doIt call a complete iteration of the current task in the sequence is
     * executed and the method returns. Subsequent calls to the method do the same
     * until the task is done. When this happens the task is deleted, the next task
     * is retrieved and the cycle starts over again.
     * 
     * A TaskSequence has some peculiarities in the ready and done flags. The ready
     * flag changes as the tasks finishes adopting the ready state of the current
     * task. The done flag only is true if all the tasks in the sequence have
     * finished.
     * 
     * @extends {Scheduler.Task}
     */
    Scheduler.TaskSequence = Scheduler.Task.extend({
        init : function(loop) {
            this.$super(null, null);
            this.tasksequence = [];
            this.loop = loop;
        },
        getNumberOfTasks : function() {
            return this.tasksequence.length;
        },
        /**
         * Adds new tasks to the sequence.
         * 
         * @throws {Berserker.Exception.IncorrectTypeException}
         * @param {Berserker.Scheduler.Task}
         *            task to add to the sequence
         */
        addTask : function() {
            var i, task;
            for (i in arguments){
                task = arguments[i];
                if (!(task instanceof Scheduler.Task)){
                    throw new CalipsoWebClient.Exception.IncorrectTypeException(task,
                            "Berserker.Scheduler.Task");
                }
                this.tasksequence.push(task);
            }
            return this;
        },
        next : function(/*object*/who, /*function*/what, /*array of arguments*/how){
            var task = new Scheduler.Task(who, what);
            task.setActualParameters(how);
            this.tasksequence.push(task);
            return this;
        },
        /**
         * 
         */
        finish : function() {
            var tasksequence = this.tasksequence, task;
            for (task in tasksequence) {
                tasksequence[task].setDone();
            }
        },
        clear : function () {
            this.tasksequence = [];
        },
        /**
         * Checks if the current task in the sequence is ready for execution (if it
         * has reached the execution date).
         * 
         * @returns {Boolean} true if the task is ready, false in any other case.
         */
        isReady : function() {
            if (this.tasksequence.length === 0){
                return false;
            }
            return this.tasksequence[0].isReady();
        },
        /**
         * Checks if the tasks's complete sequence has been done.
         * 
         * @returns {Boolean} true are not more task in the sequence, false in any
         *          other case.
         */
        isDone : function() {
            if (!this.loop && this.tasksequence.length === 0){
                return true;
            }
            return false;
        },
        /**
         * Sets the done flag to true in the currenTask.
         */
        setDone : function() {
            if (this.tasksequence.length === 0){
                return;
            }
            this.tasksequence[0].setDone();
        },
    
        /**
         * Calls the function associated to the current task and controls the
         * execution of the sequence, removing tasks from it when they are done.
         */
        doIt : function() {
            if (this.tasksequence.length > 0) {
                var currentTask = this.tasksequence[0];
                currentTask.doIt();
                if (currentTask.isDone() && this.tasksequence.length > 0) {
                    this.tasksequence.shift();
                    if (this.tasksequence.length === 0 && typeof this.onFinish === "function"){
                        this.onFinish();
                    }
                }
            }
        }
    });

    // console.log('Scheduler: tasksequence definida');
    
    /**
     * This class controls the global game loop. The other classes uses the schedule
     * method to add new tasks to a queue which are executed in turn cyclically.
     * This class allows to set just one timer to execute the game tasks so there is
     * no need to spread window.setTimeOut or another asynchronous time event
     * methods over the game classes.
     * 
     * The queue is an associative map which contains pairs of task's ids and
     * {Scheduler.Task} tasks.
     */
    Scheduler.Scheduler = (function(){
            /**
             * @private
             */
            var gameTasksQueue = {
                length : 0,
                tasks : {},
                addTask : function(t) {
                    this.length++;
                    this.tasks[t.getId()] = t;
                    return t.getId();
                },
                removeTask : function(id) {
                    var exists = typeof this.tasks[id] !== "undefined";
                    if (this.length > 0 && exists) {
                        this.length--;
                        delete this.tasks[id];
                    }
                    return exists;
                },
                isEmpty : function() {
                    return this.length === 0;
                },
                flush : function() {
                    var i, tasks = this.tasks;
                    for ( i in tasks) {
                        if (tasks.hasOwnProperty(i)){
                            delete tasks[i];
                        }
                    }
                    this.length = 0;
                }
            },
            /**
             * @private
             */
            eventTasksQueue = $.extend(true, {}, gameTasksQueue),
            /**
             * @private
             */
            motionTasksQueue = $.extend(true, {}, gameTasksQueue),
        
            /**
             * @private
             */
            graphicTasksQueue = $.extend(true, {}, gameTasksQueue),
            
            /**
             * @private
             */
            communicationTasksQueue = $.extend(true, {}, gameTasksQueue),
            /**
             * @private
             */
            taskQueues = [ eventTasksQueue, gameTasksQueue, communicationTasksQueue, motionTasksQueue,
                    graphicTasksQueue ],
        
            /**
             * @private
             */
            ready = false;
        
            function myRequestAnimationFrame(func) {
                var raf = requestAnimationFrame || window.mozRequestAnimationFrame
                        || window.webkitRequestAnimationFrame
                        || window.msRequestAnimationFrame;
                if (typeof raf === "undefined") {
                    window.setTimeout(func, 20);
                } else {
                    raf(func);
                }
            }
        
            function mainLoop() {
                if (ready) {
                    var task, currentQueue, i, taskid, currentQueueTasks;
                    for ( i = 0; i < taskQueues.length; i++) {
                        currentQueue = taskQueues[i];
        
                        if (!currentQueue.isEmpty()) {
                            currentQueueTasks = currentQueue.tasks;
                            for ( taskid in currentQueueTasks) {
                                task = currentQueueTasks[taskid];
                                if (task.isReady() && !task.isDone()) { // si la tarea está lista para ejecutarse (en caso que la tarea tenga alguna restricción de tiempo)
                                    task.doIt(); // ejecutamos la tarea
                                }
                                if (task.isDone()) { // si la tarea ha terminado y no es
                                                        // cíclica
                                    currentQueue.removeTask(taskid); // la eliminamos.
                                }
                            }
                        }
                    }
                    myRequestAnimationFrame(mainLoop);
                }
            }
            return {
                getNumberOfGameTasks : function() {
                    return gameTasksQueue.length;
                },
                getNumberOfMotionTasks : function() {
                    return motionTasksQueue.length;
                },
                getNumberOfGraphicTasks : function() {
                    return graphicTasksQueue.length;
                },
                getNumberOfEventTasks : function() {
                    return eventTasksQueue.length;
                },
                listTasks : function() {
                    var list = '', i, taskq, j;
                    for ( i = 0; i < taskQueues.length; i++) {
                        taskq = taskQueues[i].tasks;
                        for ( j in taskq) {
                            list += j + ', ';
                        }
                    }
                    return list;
                },
                hasTask : function(task){
                    var taskid = task.getId(),
                    i, 
                    queue, 
                    found = false;
                    for (queue in taskQueues){
                        if (taskQueues[queue].tasks.hasOwnProperty(taskid)){
                            found = true;
                            break;
                        }
                    }
                    return found;
                },
                /**
                 * Adds a new task to schedule.
                 * 
                 * @param {Berserker.Controller.Task}
                 *            task to add.
                 * 
                 * @returns {String} a string with the id assigned to the added task.
                 */
                scheduleCommunicationTask : function(task) {
                    return communicationTasksQueue.addTask(task);
                },
                /**
                 * Deletes the scheduled task with taskId id.
                 * 
                 * @param {String}
                 *            the task id as returned by the schedule method.
                 * @returns {Boolean} true if the tasks existed and was removed without
                 *          problems or false in any other case
                 */
                removeCommunicationTask : function(taskId) {
                    if (taskId instanceof Scheduler.Task) {
                        taskId = taskId.getId();
                    }
                    return communicationTasksQueue.removeTask(taskId);
                },
                /**
                 * Adds a new task to schedule.
                 * 
                 * @param {Berserker.Controller.Task}
                 *            task to add.
                 * 
                 * @returns {String} a string with the id assigned to the added task.
                 */
                scheduleGameTask : function(task) {
                    return gameTasksQueue.addTask(task);
                },
                /**
                 * Deletes the scheduled task with taskId id.
                 * 
                 * @param {String}
                 *            the task id as returned by the schedule method.
                 * @returns {Boolean} true if the tasks existed and was removed without
                 *          problems or false in any other case
                 */
                removeGameTask : function(taskId) {
                    if (taskId instanceof Scheduler.Task) {
                        taskId = taskId.getId();
                    }
                    return gameTasksQueue.removeTask(taskId);
                },
                /**
                 * Adds a new task for schedule.
                 * 
                 * @param {Berserker.Controller.Task}
                 *            task to add.
                 * 
                 * @returns {String} a string with the id assigned to the added task.
                 */
                scheduleMotionTask : function(task) {
                    return motionTasksQueue.addTask(task);
                },
                /**
                 * Deletes the scheduled task with taskId id.
                 * 
                 * @param {String}
                 *            the task id as returned by the schedule method.
                 * @returns {Boolean} true if the tasks existed and was removed without
                 *          problems or false in any other case
                 */
                removeMotionTask : function(taskId) {
                    if (taskId instanceof Scheduler.Task) {
                        taskId = taskId.getId();
                    }
                    return motionTasksQueue.removeTask(taskId);
                },
                /**
                 * Adds a new task for schedule.
                 * 
                 * @param {Berserker.Controller.Task}
                 *            task to add.
                 * 
                 * @returns {String} a string with the id assigned to the added task.
                 */
                scheduleGraphicTask : function(task) {
                    return graphicTasksQueue.addTask(task);
                },
                /**
                 * Deletes the scheduled task with taskId id.
                 * 
                 * @param {String}
                 *            the task id as returned by the schedule method.
                 * @returns {Boolean} true if the tasks existed and was removed without
                 *          problems or false in any other case
                 */
                removeGraphicTask : function(taskId) {
                    if (taskId instanceof Scheduler.Task) {
                        taskId = taskId.getId();
                    }
                    return graphicTasksQueue.removeTask(taskId);
                },
                /**
                 * Adds a new task for schedule.
                 * 
                 * @param {Berserker.Controller.Task}
                 *            task to add.
                 * 
                 * @returns {String} a string with the id assigned to the added task.
                 */
                scheduleEventTask : function(task) {
                    return eventTasksQueue.addTask(task);
                },
                /**
                 * Deletes the scheduled task with taskId id.
                 * 
                 * @param {String}
                 *            the task id as returned by the schedule method.
                 * @returns {Boolean} true if the tasks existed and was removed without
                 *          problems or false in any other case
                 */
                removeEventTask : function(taskId) {
                    if (taskId instanceof Scheduler.Task) {
                        taskId = taskId.getId();
                    }
                    return eventTasksQueue.removeTask(taskId);
                },
                /**
                 * Start the scheduling and execution of the tasks in the task queue.
                 */
                start : function() {
                    if (!ready) {
                        ready = true;
                        mainLoop();
                    }
                },
                /**
                 * Stops scheduling task.
                 */
                stop : function() {
                    ready = false;
                }
            };
    }());
    return Scheduler;
}());
