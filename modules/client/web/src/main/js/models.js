/*global $, CalipsoWebClient, Class, ObjectCache*/

/**
 * Este módulo carga y crea definiciones de esqueletos. 
 */

CalipsoWebClient.Models = (function(){
    var Models = {};
    function setBoneMaterial(bone){
        return function(material){
            var dw = bone.getDrawableObject();
            dw.setMaterial(material);
        };
    }
    function setBoneDrawable(model, bone, boneDefinition, rootBone){
        return {
            onSuccess: function(drawable){
                if (bone === rootBone){
                    model.setScale(drawable.getScale());
                }
                bone.setDrawableObject(drawable, boneDefinition);
                drawable.setSkeleton(rootBone);
            }
        };
    }
    /**
     * Cache de "drawables" de los items.
     */
    Models.Items = Class.extend ({
        init : function(/*string*/remoteIndexFile){
            var that = this;
            
            function factory(dwid, data, callbacks){
                return new CalipsoWebClient.Graphics.DrawableObject(data, dwid, callbacks);
            }
            function setCache(drawable){
                console.log("en items con this.id = ", this.id);
                this.cache.setObject(this.id, drawable);                                
            }
            function preload(cache){
                var objectCallback = {
                        cache : cache,
                        onSuccess: setCache
                };

                $.getJSON(remoteIndexFile, function(data){
                    var id;
                    for (id in data){
                        if (id === "equipmentBones"){
                            that.equipmentBones = data[id];
                        } else {
                            objectCallback.id = id;
                            factory(id, data[id], objectCallback);
                        }
                    }
                });
            }
            if (typeof remoteIndexFile !== "undefined"){
                this.drawableCache = new ObjectCache(factory, preload);                
            } else {
                this.drawableCache = new ObjectCache(factory);
            }
        },
        getItemDrawable : function(id, callback){
            var model;
            /*
             * EXPLICACIÓN: las gameentities en berserker tienen un id de tipo
             * string que contiene un nombre, haciendo referencia a un modelo 
             * definido en un archivo items.json. El formato de este archivo es 
             * id : "<el xml de un objeto en glge>". En batallas navales
             * se decidió que en el atributo model de cada gameentity fuera el
             * nombre del archivo xml que define al modelo y por tanto ese mismo
             * es el id del modelo.
             * 
             * XXX Sería conveniente ver si es necesario definir un archivo 
             * preload.json para cada tipo de juego en donde se indiquen
             * los modelos a cargar. 
             * 
             * De esta manera cuando se precargan el segundo parámetro es null
             * pues la definición se carga desde el archivo json y, cuando no
             * el segundo parámetro es el nombre del archivo que contiene dicha
             * definición. 
             */
            if (this.drawableCache.preloaded){
                this.drawableCache.getObject(id, null, true, callback);
            } else {
                this.drawableCache.getObject(id, id, true, callback);
            }
            return model;
        },
        getEquipmentBones : function(){
            return this.equipmentBones;
        }
    });

    Models.Personalization = Class.extend({
        init : function(personalization, pieces){// predefined personalization models for each race.
            var that = this;
            this.personalization = personalization;
            this.pieces = pieces;
          //this object stores the previously loaded hair models
            this.hairCache = new ObjectCache(function(dwid, data, callbacks){
                var hair;
                hair = new CalipsoWebClient.Graphics.DrawableObject(data, 'MD2', callbacks);
            }); 
            //body pieces
            this.drawableCache = new ObjectCache(function(cacheObjectid, data, callbacks){
                try {
                    var boneid = cacheObjectid.split(".")[0],
                    drawable = new CalipsoWebClient.Graphics.DrawableObject(that.getBoneDefinition(boneid, data).drawable, 'MD2', {
                        onSuccess : function(drawable){
                            that.getBoneMaterial(boneid, data, data.fromCache, {
                                onSuccess : function(material){
                                    drawable.setMaterial(material);
                                    callbacks.onSuccess(drawable);
                                },
                                onError : callbacks.onError
                            });
                        },
                        onError : callbacks.onError
                    });                    
                } catch (e){
                    console.log(e.toString(), e.stack);
                    if (typeof callbacks.onError !== "undefined"){
                        callbacks.onError();                        
                    }
                }
            });
            this.materialCache = new ObjectCache(function (cacheObjectid, data, callbacks){
                try{
                    var boneid = cacheObjectid.split(".")[0],
                    materialtype = data.materials[that.getBoneGroup(data.race, boneid)],
                    material = new CalipsoWebClient.Graphics.Material(that.getBoneDefinition(boneid, data).materials[materialtype], {
                        onSuccess : function(material){
                            if (/*typeof material !== "undefined" &&*/ material.setColor(data.colors[0])){
                                callbacks.onSuccess(material);                    
                            }
                        },
                        onError : callbacks.onError
                    });
                } catch (e){
                    console.log(e.toString(), e.stack);
                    if (typeof callbacks.onError !== "undefined"){
                        callbacks.onError();                        
                    }
                }
/*                if (material.setColor(data.colors[0])){
                    callbacks.onSuccess(material);                    
                }*/
            });
        },
        getSkeleton : function(race){
            race = this.personalization[race];
            return [].concat(race.body, race.face);
        },
        getBoneGroup : function(race, boneid){
            var boneType = 0; //por defecto es del cuerpo
            if (this.isFaceBone(race, boneid)){
                boneType = 1;
            }
            return boneType;
        },
        getFaceBones : function(race, withJoints){
            return ((withJoints) ?  this.personalization[race].face : this.personalization[race].face.filter(function(val, i, array){
                return (i>0) && ((i+1) % 3 !== 0) && array.indexOf(val)===i;
            }));
        },
        isFaceBone : function(race, boneid){
            //esta comparación con '> 0' es para que no devuelva true en el hueso que
            //pertenece al cuerpo con el que engancha la cabeza que siempre es el 0.
            return this.getFaceBones(race, true).indexOf(boneid) > 0;
        },
        getBodyBones : function(race, withJoints){
            return ((withJoints) ? this.personalization[race].body : this.personalization[race].body.filter(function(val, i, array){
                            return ((i+1) % 3 !== 0) && array.indexOf(val) === i;
                        }));
        },
        isBodyBone : function(race, boneid){
            return this.getBodyBones(race, true).indexOf(boneid) !== -1;
        },
        getHair : function(race, hairStyle, hairColor, fromCache, callback){
            var that = this;
            this.hairCache.getObject(race + "." + hairStyle, this.personalization[race].hair.hairStyles[hairStyle], fromCache, function(hairfeat){
                hairfeat.hairBoneId = that.getHairBone(race);
                var hairMaterial = hairfeat.drawable.getMaterial();
                hairMaterial.setColor(hairColor);
                callback(hairfeat);
            });
        },
        getHairBone : function(race){
            return this.personalization[race].hair.hairBoneId;
        },
        getNumberOfHairStyles : function(race){
            return this.personalization[race].hair.hairStyles.length;
        },
        getBoneDrawable : function(boneid, data, fromCache, callback){
            var race = data.race, 
                sex = data.sex,
                type = data.model[this.getBoneGroup(race, boneid)],
                dwid = boneid + "." + race + "." + sex + "." + type;
            this.drawableCache.getObject(dwid, $.extend({fromCache : fromCache}, data), fromCache, callback);
        },
        getBoneMaterial : function(boneid, data, fromCache, callback){
            var race = data.race,
                sex = data.sex,
                boneGroup = this.getBoneGroup(race, boneid),
                bonetype = data.model[boneGroup],
                materialtype = data.materials[boneGroup],
                matid = boneid + "." + race + "." + sex + "." + bonetype + "." + materialtype;
            this.materialCache.getObject(matid, data, fromCache, callback);
    
        },
        getBoneDefinition: function (boneid, data){
            var race = data.race, 
                sex = data.sex,
                totype = this.pieces[boneid][race][sex],
                bonedef;
            if (this.isFaceBone(race, boneid)){
                bonedef = totype[+data.model[1]]; //Con esto sacamos la definición para el tipo del cuerpo
            } else {
                bonedef = totype[+data.model[0]]; // sacamos la definición para la cabeza
            }
            if (typeof bonedef === "undefined") {
                throw new CalipsoWebClient.Exceptions.ModelTypeNotDefinedForBone(boneid, data); 
            }
            return bonedef;
            
        },
        getNumberOfBoneDefinitions : function (boneid, data){
            var race = data.race, sex = data.sex;
            return this.pieces[boneid][race][sex].length;
        },
        nextFace : function(model){
            var skeletonType = model.getModelDefinition(),
                bones = this.getFaceBones(model.getType(), false),
                numberOfModels = this.getNumberOfBoneDefinitions(bones[1], skeletonType);
            skeletonType.model[1]++;
            skeletonType.model[1] %= numberOfModels;
            this.changeSkeletonGroup(model, bones, skeletonType);
        },
        nextBody : function(model){
            var skeletonType = model.getModelDefinition(),
                bones = this.getBodyBones(model.getType(), false),
                numberOfModels = this.getNumberOfBoneDefinitions(bones[0], skeletonType);
            skeletonType.model[0]++;
            skeletonType.model[0] %= numberOfModels;
            this.changeSkeletonGroup(model, bones, skeletonType);
        },
        nextHairStyle : function(model) {
            var modelDefinition = model.getModelDefinition(),
                that = this;
            //XXX ESTO PUEDE QUE NO FUNCIONE PORQUE AL CREAR INICIALMENTE LOS MODELOS NO SE EMPLEA LA CACHE DE OBJETOS DE 
            //PERSONALIZATION Y EL PELO ES UN OBJETO INDEPENDIENTE INTRODUCIDO EN EL HUESO DE LA CABEZA
            this.getHair(model.getType(), modelDefinition.model[2], modelDefinition.color[1], true, function(hairBoneFeat){
                model.removeObjectFromBone(hairBoneFeat.hairBoneId, hairBoneFeat.drawable);
            });
            modelDefinition.model[2] = this.skeletonType.model[2]++ % this.getNumberOfHairStyles(model.getType());
            this.getHair(model.getType(), modelDefinition.model[2], modelDefinition.color[1], true, function(hairBoneFeat){
                try {
                    model.addObjectToBone(hairBoneFeat.hairBoneId, hairBoneFeat.drawable, that.getBoneDefinition(hairBoneFeat.hairBoneId, modelDefinition).hairPosition);                    
                } catch (e){
                    console.log(e.toString(), e.stack);
                }

            });
        },
        changeSex : function(model, sex){
            var skeletonType = model.getModelDefinition();
            skeletonType.sex = sex;
            model.changeSkeletonGroup(this.getSkeleton(model.getType()), skeletonType);
        },
        nextFaceMaterial : function(model) {
            try {
                var bonelist = this.getFaceBones(model.getType(), false),
                    modelDefinition = model.getModelDefinition(),
                    bones = model.getBones(),
                    numberOfMaterials = this.getBoneDefinition(bonelist[0], modelDefinition).materials.length,
                    i;

                modelDefinition.materials[1]++;
                modelDefinition.materials[1] %= numberOfMaterials;
    
                for (i in bonelist){
                    this.getBoneMaterial(bonelist[i], modelDefinition, true, setBoneMaterial(bones[bonelist[i]]));
                }
            } catch (e){
                console.log(e.toString(), e.stack);
            }
        },
        nextBodyMaterial : function(model) {
            try {
                var bonelist = this.getBodyBones(model.getType(), false),
                    modelDefinition = model.getModelDefinition(),
                    bones = model.getBones(),
                    numberOfMaterials = this.getBoneDefinition(bonelist[0], modelDefinition).materials.length,
                    i;
                modelDefinition.materials[0]++;
                modelDefinition.materials[0] %= numberOfMaterials;
                for (i in  bonelist){
                    this.getBoneMaterial(bonelist[i], modelDefinition, true, setBoneMaterial(bones[bonelist[i]]));
                }
            } catch (e){
                console.log(e.toString(), e.stack);
            }
        },
        setHairColor : function(model, color){//XXX Esto posiblemente falle
            var skeletonType = model.getModelDefinition(),
                hairBoneId = this.getHairBone(this.skeletonType.race),
                dw = this.bones[hairBoneId].getDrawableObject(),
                mat = dw.getMaterial();
            mat.setColor(color);
            skeletonType.color.splice(1, 1, color);
        },
        changeSkeletonGroup : function(/*Models.Model*/model, /*{Array<string>}*/bones, /*{object}*/bonestype){
            var boneid,
                i, 
                length,
                modelbones = model.getBones(),
                boneDefinition;
            //se inicializa i = group porque es adecuado para coger el hueso de la cara o el 
            //cuerpo que le corresponde (si bonestype === 1 es el hueso de la cara que está en la 
            //2º posición del array 'bones')
            
            for (i in bones){
                boneid = bones[i];
                try{
                    boneDefinition = this.getBoneDefinition(boneid, bonestype);
                    if (model.hasBone(boneid)){
                        this.getBoneDrawable(boneid, bonestype, true, setBoneDrawable(model, modelbones[boneid], boneDefinition.joints, model.rootBone));
                    }                    
                } catch(e){
                    console.log(e.toString(), e.stack);
                }
            }
        }
    });
    Models.Model = CalipsoWebClient.Graphics.Skeleton.extend({
        init : function (rootBoneId, drawableObject, boneDefinition, modelDefinition, motionsModule, personalizationModule){
            var index;
            this.$super(rootBoneId, drawableObject, boneDefinition, modelDefinition.race, motionsModule.motionGraph);
            this.skeletonType = modelDefinition;
            this.motions = {};
            this.personalization = personalizationModule;
            for (index in motionsModule.motions){
                this.motions[index] = motionsModule.motions[index].getMotion();
            }
        },
        doAnimation : function(motion){
            var animation = this.motions[motion.animation]; 
            if (typeof animation.start !== "undefined"){
                this.rootBone.setAction(animation, motion.time, motion.loop);
            }
        },
        setBasicModel : function(){
            var skeletonType = this.skeletonType,
                bones = this.getBones(),
                personalization = this.personalization,
                boneid,
                boneDefinition;

            skeletonType.model[0] = 0;
            skeletonType.model[1] = 0;
            skeletonType.materials[0] = 0;
            skeletonType.materials[1] = 0;

                //se inicializa i = group porque es adecuado para coger el hueso de la cara o el 
                //cuerpo que le corresponde (si bonestype === 1 es el hueso de la cara que está en la 
                //2º posición del array 'bones')
        
            for (boneid in bones){
                try{
                    boneDefinition = personalization.getBoneDefinition(boneid, skeletonType);
                    personalization.getBoneDrawable(boneid, skeletonType, true, setBoneDrawable(this, bones[boneid], boneDefinition.joints, this.rootBone));
                       
                } catch(e){
                    console.log(e.toString(), e.stack);
                }
            }
        },
        nextBody : function(){
            this.personalization.nextBody(this);
        },
        nextBodyMaterial : function(){
            this.personalization.nextBodyMaterial(this);
        },
        nextFaceMaterial : function(){
            this.personalization.nextFaceMaterial(this);
        },
        getModelDefinition : function(){
            return this.skeletonType;
        },
        setSkinColor : function(color){
            this.skeletonType.color.splice(0, 1, color);
            var bones = this.bones, 
                dw, 
                mat, 
                boneid;
            for (boneid in bones){
                dw = bones[boneid].getDrawableObject();
                mat = dw.getMaterial();
                mat.setColor(color);
            }
        }
    });
        /*
        "characterid" : { // esto sería lo que enviaría el servidor cuando recupera un personaje ya personalizado.
            type : 0 - skeleton | 1 - MD2
            race: "human", //raza del personaje
            sex: "male", //sexo del personaje*/
    //        model: [/*tipo de cuerpo*/0,/*tipo de cara*/0,/*tipo de pelo*/0], //cuerpo del personaje o archivo md2 que lo contiene
    //        materials: [/*material del cuerpo*///"materialtatoo", /*material de la cara*/"scars"], //materiales que se aplican al cuerpo y la cara
    //        color: [/*color de la cara y cuerpo*/23, /*color del pelo*/0]
    //    }
    
    Models.ModelCreator  = {
        createModel : function(modelDefinition, personalization, motionsModule, callbacks){
            var that = this,
                AnimatedMD2 = CalipsoWebClient.Graphics.AnimatedMD2,
                race = modelDefinition.race,
                type = modelDefinition.modelType,
                model, 
                sex = modelDefinition.sex,
                boneList,
                bonedef;

            this.personalization = personalization;

            if (type === 0){ //es una definición de un modelo mediante un esqueleto.
                try {
                    boneList = personalization.getSkeleton(race);
                    bonedef = personalization.getBoneDefinition(boneList[0], modelDefinition);
                    personalization.getBoneDrawable(boneList[0], modelDefinition, false, {
                        onSuccess : function(drawable){
                            model = new Models.Model(boneList[0], drawable, bonedef.joints, modelDefinition, motionsModule, personalization);
                            that.addBonesToSkeleton(model, boneList, modelDefinition, function(){ //TODO AÑADIR UN EJEMPLO PARA EL PELO
        /*                        personalization.getHair(race, modelDefinition.model[2], modelDefinition.colors[1], false, function(hairBoneFeat){
                                    model.addObjectToBone(hairBoneFeat.hairBoneId, hairBoneFeat.drawable,
                                            personalization.getBoneDefinition(hairBoneFeat.hairBoneId, modelDefinition).hairPosition);*/
                                    if (typeof callbacks !== "undefined"){
                                        if (typeof callbacks === "function"){
                                            model.setMotionTask();
                                            callbacks(model);
                                        } else {
                                            callbacks.onSuccess(model);
                                        }
                                    }
        //                        });
                            });
                        }
                    });
                } catch(e){
                    console.log(e.toString(), e.stack);
                }
            } else {
                model = new CalipsoWebClient.Graphics.AnimatedMD2(modelDefinition.model, race, motionsModule.motionGraph, {
                    onSuccess : (typeof callbacks === "function") ? callbacks : callbacks.onSuccess, 
                    onError : callbacks.onError
                });
            }
        },
        addBonesToSkeleton : function(skeleton, bonelist, boneData, signalReady){
            var that = this,
                numberOfRelationships = bonelist.length/3, //numero de relaciones entre huesos
                personalization = this.personalization,
                numberOfBoneRelationships = 0,
                bone1Exists,
                bone2Exists,
                jointid,
                joints = [],
                bone1,
                bone2,
                dw1,
                dw2,
                i, 
                done = false;

            function newBoneAndRelationShip(createBone, parentBone, childBone, jointid){
                return{
                    onSuccess : function(dw){
                        try {
                            if (!skeleton.hasBone(createBone)){
                                console.log("createdbone = ", createBone, numberOfBoneRelationships);
                                skeleton.newBone(createBone, dw, personalization.getBoneDefinition(createBone, boneData).joints);
                            }
                            if (skeleton.hasBone(parentBone) && 
                                skeleton.hasBone(childBone) && 
                                !skeleton.hasJoint(jointid)){
                                numberOfBoneRelationships++;
                                console.log("created relationship = ", parentBone, childBone, jointid, numberOfBoneRelationships);
                                skeleton.setBoneRelationship(parentBone, childBone, jointid);
                            }
                            if (!done &&
                                numberOfBoneRelationships === numberOfRelationships && 
                                typeof signalReady === "function"){
                                done = true;
                                signalReady();
                            }
                        } catch(e){
                            console.log(e.toString(), e.stack);
                        }
                    },
                    onError : function(){
                        if (joints.indexOf(jointid)===-1){
                            numberOfRelationships--;
                            joints.push(jointid);
                        }
                    }
                };
            }
            for (i = 0; i<bonelist.length; i+=3){
                bone1Exists = true;
                bone2Exists = true;
                bone1 = bonelist[i];
                bone2 = bonelist[i+1];
                jointid = bonelist[i+2];
                personalization.getBoneDrawable(bone1, boneData, false, newBoneAndRelationShip(bone1, bone1, bone2, jointid));
                personalization.getBoneDrawable(bone2, boneData, false, newBoneAndRelationShip(bone2, bone1, bone2, jointid));
            }
        }
    };
    return Models;
}());