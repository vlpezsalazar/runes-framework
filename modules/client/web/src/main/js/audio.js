/*global $, CalipsoWebClient, Class */

var Audio;
/**
 * Sound Manager.
 */

CalipsoWebClient.Audio = (function(){
    var MUSIC_CHANNEL = -1, 
        ALL_CHANNELS = -2,
        FX_CHANNELS = -3,
        Audiopkg = {};
    /**
     * Audio Manager's object which lets to play game sounds. On creation, the maximum number of
     * channels are set to 16 plus one music channel.
     * 
     * Example: <code>
     * var sounds = {dog: '/resources/dog.ogg', cat: '/resources/cat.ogg'}; 
     * var audio = CalipsoWebClient.Sound.AudioManager;
     * 
     * audio.load(sounds);
     * 
     * audio.play('dog');
     * audio.play('cat');
     * </code>
     * 
     * @param soundsdesc
     *            an object with the id and the game sounds URLs
     */
    Audiopkg.AudioManager = Class.extend({
        init : function() {
            var i;
            console.log("definiendo Audio Manager...");
            this.sounds = {};
            console.log("definiendo cargador de audio...");
            this.audioLoader = new Audio();
            console.log("definiendo canales de sonido...");
            this.musicChannel = new Audio();
            this.fxchannels = [];
            for ( i = 0; i < 16; i++) {
                this.fxchannels.push(new Audio());
            }
            console.log("definiendo métodos del gestor de audio...");
            this.assignMusicChannel = function(soundid) {
                var url = this.sounds[soundid];
                if (typeof url === "undefined"){
                    throw new CalipsoWebClient.Exceptions.UndefinedSound(soundid);
                }
                this.musicChannel.pause();
                this.musicChannel.ended = true;
                this.musicChannel.src = url;
                this.musicChannel.load();
                return {
                    index : MUSIC_CHANNEL,
                    audio : this.musicChannel
                };
            };
            this.assignFreeFxChannel = function(soundid){
                var url = this.sounds[soundid], channelAssigned;
                if (typeof url === "undefined"){
                    throw new CalipsoWebClient.Exceptions.UndefinedSound(soundid);                
                }
                $.each(this.fxchannels, function(i, audio) {
                    if (audio.ended || audio.src === '') {
                        audio.src = url;
                        audio.load();
                        channelAssigned = {
                            index : i,
                            audio : audio
                        };
                        return false;
                    }
                });
                if (typeof channelAssigned === "undefined"){
                    throw new CalipsoWebClient.Exceptions.NoFreeChannels();
                }
                return channelAssigned;
            };
        },
        loadAudio : function(id, url) {
            console.log('loading ', id, '...');
            this.sounds[id] = url;
            this.audioLoader.src = url;
            this.audioLoader.preload = 'auto';
            this.audioLoader.load();
        },
        getChannelsPlaying : function(soundid) {
            var channelsPlaying = [], url = this.sounds[soundid];
            if (typeof url !== "undefined") {
                $.each(this.fxchannels, function(i, audio) {
                    if (audio.url === url) {
                        channelsPlaying.push(audio);
                    }
                });
                if (this.musicChannel.url === url) {
                    channelsPlaying.push(this.musicChannel);
                }
            }
            return channelsPlaying;
        },
        setChannelVolume : function(channel, volume) {
            switch (channel) {
            case ALL_CHANNELS:
                this.musicChannel.volume = volume;
                $.each(this.fxchannels, function(i, audio) {
                    audio.volume = volume;
                });
                break;
            case MUSIC_CHANNEL:
                this.musicChannel.volume = volume;
                break;
            case FX_CHANNELS:
                $.each(this.fxchannels, function(i, audio) {
                    audio.volume = volume;
                });
                break;
            default:
                if (channel < this.fxchannels.length){
                    this.fxchannels[channel].volume = volume;
                }
            }
        },
        replayChannel : function(channel) {
            switch (channel) {
            case ALL_CHANNELS:
                this.musicChannel.play();
                $.each(this.fxchannels, function(i, audio) {
                    audio.play();
                });
                break;
            case MUSIC_CHANNEL:
                this.musicChannel.play();
                break;
            case FX_CHANNELS:
                $.each(this.fxchannels, function(i, audio) {
                    audio.play();
                });
                break;
            default:
                if (channel < this.fxchannels.length){
                    this.fxchannels[channel].play();
                }
            }
        },
        stopChannel : function(channel) {
            switch (channel) {
            case ALL_CHANNELS:
                this.musicChannel.pause();
                this.musicChannel.ended = true;
                $.each(this.fxchannels, function(i, audio) {
                    audio.pause();
                    audio.ended = true;
                });
                break;
            case MUSIC_CHANNEL:
                this.musicChannel.pause();
                this.musicChannel.ended = true;
                break;
            case FX_CHANNELS:
                $.each(this.fxchannels, function(i, audio) {
                    audio.pause();
                    audio.ended = true;
                });
                break;
            default:
                if (channel < this.fxchannels.length) {
                    this.fxchannels[channel].pause();
                    this.fxchannels[channel].ended = true;
                }
            }
        },
        /**
         * Method to play a sound identified by soundid choosing the first free
         * channel of the channel type specified.
         * 
         * @param channeltype
         *            the channels's type:
         *            <ul>
         *            <li>FX_CHANNELS: fx
         *            channels.
         *            <li>MUSIC_CHANNEL: music
         *            channel.
         *            <ul>
         * @param soundid
         *            Sound's id to play.
         * @param {Boolean}
         *            loop if true specifies a sound which should be played
         *            continuously. If undefined defaults to false.
         * @param {Number}
         *            volume the volume to set on the assigned channel.
         * 
         * @returns a number identifying the channel assigned.
         * 
         * @throws CalipsoWebClient.Exceptions.NoFreeChannels
         *             when there is no free channel.
         * @throws CalipsoWebClient.Exceptions.UndefinedSound
         *             when the soundid is incorrect.
         */
        play : function(channeltype, soundid, loop, volume) {
            var channel, audio;
            switch (channeltype) {
            case MUSIC_CHANNEL:
                channel = this.assignMusicChannel(soundid);
                break;
//            case FX_CHANNELS: //default case
            default:
                channel = this.assignFreeFxChannel(soundid);
            }
            audio = channel.audio;
            if (loop) {
                audio.loop = true;
            } else {
                audio.loop = false;
            }
            if (typeof volume !== "undefined") {
                audio.volume = volume;
            }
            audio.play();
            return channel.index;
        },
        replay : function(channel) {
            this.replayChannel(channel);
        },
        /**
         * If soundorchannelid is a string identifying a sound, then this method
         * ends the channels playing that sound, but if soundofchannelid is a number
         * identifying a channel, then the method only stops that channel. The
         * channels stopped are freed for assignment.
         * 
         * @param {string|Number}
         *            soundid the sound's id.
         */
        end : function(soundorchannelid) {
            if (soundorchannelid instanceof Number) {
                this.stopChannel(soundorchannelid);
            } else {
                var channelList = this.getChannelsPlaying(soundorchannelid);
                $.each(channelList, function(i, audio) {
                    audio.pause();
                    audio.ended = true;
                });
            }
        },
        /**
         * Adds a new set of sounds to this Audio Manager.
         * 
         * @param {Object}
         *            sounds an object composed by {id : url}.
         */
        load : function(sounds) {
            var that = this;
            $.each(sounds, function(id, url) {
                that.loadAudio(id, url);
            });
        }
    });
    Audiopkg.AudioManager.MUSIC_CHANNEL = MUSIC_CHANNEL;
    Audiopkg.AudioManager.ALL_CHANNELS = ALL_CHANNELS;
    Audiopkg.AudioManager.FX_CHANNELS = FX_CHANNELS;
    return Audiopkg;
}());