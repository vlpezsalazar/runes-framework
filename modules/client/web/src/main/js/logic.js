/*global CalipsoWebClient, Class */
/**
 * Game Logic Engine. Contains classes to perform game tasks like processing
 * game logic's actions and other kind of tasks.
 * 
 * Rule is a Task subclass. This class performs the game actions's logic,
 * i.e. walk will set the characters's position to a new one depending on their 
 * direction. Game Engine is a simple inference engine. It matches the user
 * character's fired actions in the EventManager with their Rules and fire
 * those which match. 
 * 
 * @requires scheduler.js, communication.js
 *  
 * @author Víctor López Salazar.
 */

/**
 * Package's object. It holds the following package classes:
 * <ul>
 * <li> Logic.Rule
 * <li> Logic.GameEngine
 * <ul>
 */
CalipsoWebClient.Logic = (function(){
    var Logic = {};
    function firefuncs() {
        // console.log('firefuncs(): ', this);
        var done = this.ruleFunction(this.subject, this.params, this.context);
        if (typeof done === "undefined") {
            done = false;
        }
        return done;
    }

    /**
     * This class is a task pool for a specific game action. Every action has a
     * subject (and perhaps an object) and the pool contains tasks to each subject
     * performing the action. The Rule class provides methods to create and set
     * these tasks into the scheduler and finish them when required.
     * 
     * A Rule should be created using the new keyword. It not uses the singleton
     * pattern because several rules are needed (at least one per game's action).
     * 
     * @constructor
     * 
     * @param actionInfo
     *            an object with the action info associated to this rule. This
     *            object must contain the following fields:
     *            <p style="margin-left: 50px">
     *            <dl>
     *            <dt> loop:
     *            <dd> (true|false) indicates if the action's effects have to be
     *            applied continuously (true) or not (false).
     *            <dt> gameRuleFunction:
     *            <dd> a string defining a function with the following signature:
     *            function ({Object} subject, {Object} context). This function
     *            process the effects of the subject character's action and applies
     *            them. This function has get the object of the action if needed and
     *            usually it just has to update some attributes of that object if
     *            the parameters of the action are set inside the character
     *            <dt> serverNeedsToKnow:
     *            <dd>(true|false) if true, this flag allows sending the character
     *            information to the server. It is of use if the action had some
     *            effects on the character.
     *            </dl>
     *            </p>
     * 
     * 
     */
    Logic.Rule = Class.extend({
        init : function(ruleFunction, ruleContext) {
            this.taskPool = {};
            this.ruleFunction = ruleFunction;
            this.ruleContext = ruleContext;
        },
        /**
         * This fires the rule's action with subject as the one who performed the
         * action. It sets a task in the scheduler with the gameRuleFunction
         * associated to subject.
         * 
         * @param {Berserker.Entities.Character}
         *            userchar the user's character
         * @param {Berserker.Entities.Character}
         *            subject the character performing the action.
         * @para {object} param the action's parameters             
         */
        fire : function(userchar, subject, params, ruleTaskSequence) {
            var id = subject.getBId(),
                that,
                funcdata,
                task;
            if (!this.taskPool.hasOwnProperty(id) || 
                 this.taskPool[id].isDone() || 
                 this.taskPool[id] === ruleTaskSequence) {
                that = this;
                funcdata = {
                    ruleFunction : that.ruleFunction,
                    params : params,
                    userchar : userchar,
                    subject : subject,
                    context : that.ruleContext,
                    properties : that.ruleFunction.properties
                };
                task = new CalipsoWebClient.Scheduler.Task(funcdata, firefuncs);
                if (typeof ruleTaskSequence !== "undefined"){
                    ruleTaskSequence.addTask(task);
                    if (this.taskPool[id] !== ruleTaskSequence){
                        this.taskPool[id] = ruleTaskSequence;
                        CalipsoWebClient.getScheduler().scheduleGameTask(ruleTaskSequence);
                    }
                } else {
                    this.taskPool[id] = task;
                    CalipsoWebClient.getScheduler().scheduleGameTask(task);
                }
            }
        },

        /**
         * This method finishes the subject's task previously created by the
         * fire function.
         * 
         * @param {Berserker.Entities.Character}
         *            subject the subject which performed the action.
         */
        finish : function(subject) {
            var id = subject.getBId();
            if (this.taskPool.hasOwnProperty(id)) {
                this.taskPool[id].setDone();
                console.log('borrando tarea : ' + this.taskPool[id].getId(),
                            ', this.taskPool[id].isDone():', this.taskPool[id].isDone());
                delete this.taskPool.id;
            }
        },
        isOngoing : function(subject) {
            var done = true,
                id = subject.getBId();
            if (this.taskPool.hasOwnProperty(id)) {
                done = this.taskPool[id].isDone();
            }
            return done;
        }
    });
    /**
     * The game engine is an object which receives the game actions from the server
     * and each module uses it to fire the appropriate rules when a character or the
     * module itself needs to perform a game's action. A map of (action, rule) lets
     * it to match the actions with their rules and fire them when needed.
     * 
     * The object is a singleton which can be accessed by the other game modules.
     */
    Logic.GameEngine = Class.extend({
        init : function(gameFunctionRules, ruleContext) {
            var name,
                ruleFunction, 
                eventQueue = this.eventQueue = [];
            this.userchar = null;
            this.gameRules = {};
            for (name in gameFunctionRules) {
                ruleFunction = gameFunctionRules[name];
                if (typeof ruleFunction === "function") {
                    this.newRule(name, ruleFunction, ruleContext);
                }
            }

            function eventDaemon() {
                var event;
                while (eventQueue.length > 0) {
                    event = eventQueue.shift();
                    event.doIt();
                }
                return false;
            }
            CalipsoWebClient.getScheduler().scheduleGameTask(new CalipsoWebClient.Scheduler.Task(this, eventDaemon));
        },
        setUserCharacter : function(character) {
            this.userchar = character;
        },
        /**
         * This method is called within a module when it needs to perform
         * any action which has some effect over the game entities. The
         * method gets the rule corresponding to the action and fires it
         * with subject as the action's subject.
         * 
         * @param {CalipsoWebClient.Graphics.Animatable} animatable the entity performing the action.
         * @param {object => {
         *            actionname : {actiondata} }} action the action to be
         *            performed.
         */
        actionDone : function(/* {CalipsoWebClient.Graphics.Animatable} */ animatable, 
                               /* {object} */ action) {
            var name,
                rule;
            for (name in action) {
                rule = this.gameRules[name];
                if (typeof rule !== "undefined") {
                    if (animatable instanceof CalipsoWebClient.Graphics.AnimatableObject){
//                        animatable.doAction(action);
                        console.log('disparando regla: ', action, ' para ',
                                animatable);
                        animatable.startMotion(name, action[name]);
                    }
                    rule.fire(this.userchar, animatable, action[name]);
                }
            }
        },
        doSequence : function(entity, sequence){
            var name,
                rule,
                ruleObject,
                index,
                task = new CalipsoWebClient.Scheduler.TaskSequence();
            
            for (index in sequence) {
                ruleObject = sequence[index];
                for (name in ruleObject){
                    rule = this.gameRules[name];
                    if (typeof rule !== "undefined") {
//                        entity.doAction(name);
                        console.log('disparando regla: ', name, ' para ',
                                entity);
                        if (entity instanceof CalipsoWebClient.Graphics.AnimatableObject){
                            entity.startMotion(name, ruleObject[name]);
                        }
                        rule.fire(this.userchar, entity, ruleObject[name], task);
                    }

                }
            }
        },
        actionHasProperties : function(/* {string} */action, /* {int} */ properties) {
            return this.gameRules[action].hasProperties(properties);
        },
        /**
         * This method is called within a module when it needs to finish an
         * ongoing action previously fired by a actionDone call. The method
         * gets the rule and ends the associated task in the scheduler.
         * 
         * @param {CalipsoWebClient.Graphics.Animatable} animatable
         *            subject the subject performing the action.
         * @param {string}
         *            action the action performed.
         */
        actionFinished : function(/* {CalipsoWebClient.Graphics.Animatable} */animatable, /* {string} */ action) {
            var rule = this.gameRules[action], 
                queueMotion = animatable.getQueueMotionFromAction(action);

            if (typeof rule !== "undefined") {
                rule.finish(animatable);
            }

            if (queueMotion && queueMotion.loop) {
                animatable.endLoopMotion(queueMotion);
            }
        },
        isActionFinished : function(/* {CalipsoWebClient.Graphics.Animatable} */animatable, /* {string} */action) {
            var rule = this.gameRules[action], done = true;
            if (typeof rule !== "undefined") {
                done = rule.isOngoing(animatable);
            }
            return done;
        },
        newRule : function(name, ruleAction, ruleContext) {
            this.gameRules[name] = new Logic.Rule(ruleAction, ruleContext);
        },
        hasRule : function(name){
            return typeof this.gameRules[name] !== "undefined";
        },
        pushEvent : function(event) {
            this.eventQueue.push(event);
        }
    });
    return Logic;
}());


