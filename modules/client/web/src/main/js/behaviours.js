/*global CalipsoWebClient, $, Class, toType*/
CalipsoWebClient.Behaviours = (function(Task, GameEntities, Scheduler){
    
    
    function findActivity (sequence, objid, actionName){
        var i, 
            pos = -1;
        for (i in sequence){
            if (sequence[i][objid] === actionName){
                pos = i;
                break;
            }
        }
        return pos;
    }
    function createArrayFunction(activity, method){
        var f;
        if (activity.type === "parallel"){
            activity.completionList = [];
            f = function(){
                var i,
                    completionList = activity.completionList,
                    end = true;
                for (i in this){
                    if (completionList[i] === "undefined"){
                        completionList.push(method.apply(this[i], arguments[i])); 
                    } else if (!completionList[i]){
                        completionList[i] = method.apply(this[i], arguments[i]);
                    }
                    end = end && completionList[i];
                }
                return end;
            };
        } else if (activity.type === "sequential"){
            activity.index = 0;
            f = function(){
                var end = false;
                if (method.apply(this[activity.index], arguments[activity.index])){
                    activity.index++; 
                }
                if (activity.index >= this.length){
                    activity.index = 0;
                    end = true;
                }
                return end;
            };
        }
        return f;
    }
    var template = /(array)<(\w+)>|(object)<\w+,(\w+)>/,
        motionNames = /_(\w+)_/,
        ActivityFunctions = {
            checkDependsOnList : function(activity){
                var deponlist = activity.dependsOn,
                    i,
                    ii,
                    seq = this.sequence,
                    end = true;
                for (i in deponlist){
                    ii = deponlist[i];
                    if (!seq[ii].end){
                        end = false;
                        break;
                    }
                }
                return end;
            },
            checkTriggers : function(activity, state){
                var triggers = this.triggers,
                    fireWhenList = activity.fireWhen,
                    i,
                    trigger,
                    result = true;
                if (typeof fireWhenList !== "undefined"){
                    for (i in fireWhenList){
                        trigger = triggers[fireWhenList[i]];
                        if (!trigger.checked){
                            trigger.result = trigger.trigger(state);
                            trigger.checked = true;
                        }
                        result = result && trigger.result;
                        if (!result){
                            break;
                        }
                    }
                }
                return result;
            },
            clearTriggers : function(){
                var triggers = this.triggers,
                    trigger,
                    i;
                for (i in triggers){
                    trigger = triggers[i];
                    trigger.checked = false;
                    trigger.result = false;
                }
            }
        },
        Behaviours = {
            defineBehaviours : function(/*object*/behaviourSchemas, /*object*/stateSchema){
                var id,
                    behaviourDefinition = {},
                    behaviourName,
                    behaviour,
                    applyTo,
                    triggers,
                    type,
                    what,
                    dynamic,
                    definition,
                    obj,
                    pair,
                    actionName,
                    actionMethods,
                    parameters,
                    activityIndex;

                for (behaviourName in behaviourSchemas){
                    behaviour = behaviourSchemas[behaviourName];
                    applyTo = behaviour.applyTo;
                    dynamic = [];
                    definition = {
                        name : behaviourName,
                        applyTo : behaviour.applyTo,
                        sequence : behaviour.activities
                    };
                    for (id in applyTo){
                        obj = applyTo[id];
                        type = (toType(stateSchema[id]) === "object") ? stateSchema[id].type : stateSchema[id];
                        if (typeof type === "undefined"){
                            type = obj.type;
                        }
                        if (template.test(type)){
                            type = template.exec(type);
                            if (typeof GameEntities.gameEntityClasses[type[2]] !== "undefined"){
                                actionMethods = GameEntities.gameEntityClasses[type[2]].prototype.actionMethods;                                
                            }
                        } else if (typeof GameEntities.gameEntityClasses[type] !== "undefined"){
                            actionMethods = GameEntities.gameEntityClasses[type].prototype.actionMethods;
                        }  else {
                            actionMethods = undefined;
                        }
                        if (typeof actionMethods !== "undefined"){ //es una GameEntity
                            if (typeof obj.selector !== "undefined"){
                                what = {
                                    obj : obj.selector
                                };
                            } else {
                                what = {};
                            }
                            for (actionName in obj){
                                parameters = obj[actionName];
                                if (actionName !== "type" && actionName !== "selector"){
                                    pair = {};
                                    if (typeof parameters === "function" && 
                                            typeof actionMethods[actionName] !== "undefined"){
                                            pair.method = actionName;
                                            pair.parameters = parameters;
                                    }
                                    if (typeof what.obj !== "undefined" || 
                                            (typeof parameters === "function" && 
                                             typeof actionMethods[actionName] !== "undefined")){
                                        activityIndex = findActivity(behaviour.activities, id, actionName);
                                        if (activityIndex > -1){
                                            dynamic.push($.extend({activity : activityIndex}, what, pair));
                                        }
                                    }
                                }
                            }
                        } else if (typeof obj.selector !== "undefined"){ //es un objeto dinámico que no es una gameEntity
                            what = {
                                    obj : obj.selector
                            };
                            for (actionName in obj){
                                activityIndex = findActivity(behaviour.activities, id, actionName);
                                if (activityIndex > -1){
                                    dynamic.push($.extend({activity : activityIndex}, what));
                                }

                            }

                        }
                    }
                    definition.dynamic = dynamic;
                    triggers = behaviour.triggers;
                    for (id in triggers){
                        triggers[id] = {
                                trigger : triggers[id],
                                checked : false,
                                result : false
                        };
                    }
                    definition.triggers = triggers;
                    behaviourDefinition[behaviourName] = $.extend(definition, ActivityFunctions); 
                }
                return behaviourDefinition;
            }
        };

    function getUpdateActivityObjectOnChangeCallback(activity){
        return function(obj){
            if (activity.end){
                activity.obj.splice(0, activity.obj.length);
            }
            activity.obj.push(obj);
        };
    }
    /**
     * Función para ejecutar las actividades de un comportamiento.
     */
    function run(){
        var sequence = this.sequence,
            i,
            ii,
            obj,
            activity,
            activityend,
            end = true,
            state = this.state,
            stateobj,
            parameters;
        
        for (i in sequence){
            activity = sequence[i];
            if (typeof activity.obj !== "undefined" && !activity.end && (activity.start || (this.checkDependsOnList(activity) && this.checkTriggers(activity, state)))){
                activity.start = true;
                if (activity.obj.length > 1){
                    activity.end = true;
                    for (ii in activity.obj){
                        obj = activity.obj[ii];
                        if (typeof obj !== "undefined"){
                            if (activity.parameters.length > ii){
                                parameters = activity.parameters[ii];
                            } else {
                                parameters = activity.parameters[0];
                            }
                            activityend = activity.method.apply(obj, parameters);
                        } 
                        if (activityend){
                            activity.obj.splice(ii, 1);
                            if (activity.parameters.length > 1){
                                activity.parameters.splice(ii, 1);
                            }
                        }
                        activity.end = activityend && activity.end;
                        end = end && activityend;
                    }
                } else {
                    obj = activity.obj[0];
                    parameters = activity.parameters[0];
                    if (typeof obj !== "undefined"){
                        activity.end = activity.method.apply(obj, parameters);
                        
                    } else {
                        activity.end = true;
                    }
                    end = end && activity.end;

                }
            }
        }
        this.clearTriggers();
        return end;
    }
    function createActionWithStartAndEndMotion(action, motion){
        var propertyStarted = "actionBehaviourStarted_"+action;
        return function(){
            var started = this.getProperty(propertyStarted), 
                end,
                queueMotion;
            if (typeof started === "undefined" || started === false){
                this.putProperty(propertyStarted,  true);
                this.startMotion(motion, (arguments.length >0) ? $.extend({}, arguments[0], this.state) : this.state);
            }
            end = action.apply(this, arguments);
            if (end){
                this.putProperty(propertyStarted, false);
                this.setAnimation();//para el nodo global
                console.log('Finishing movement for ', this);
                queueMotion = this.getQueueMotionFromAction(motion);
                if (queueMotion && queueMotion.loop){
                    this.endLoopMotion(queueMotion);
                }
            }
            return end;
        };

    }
    /*
     *                     
     */
    Behaviours.Behaviour = Class.extend({
        init : function(definition, gameEntity){
            var i, 
                sequence = [],
                action,
                parameters,
                activity,
                sequencedefinition = definition.sequence, 
                activitydefinition,
                typeProperties,
                type,
                dummy = "",
                obj,
                stateSchema = gameEntity.stateSchema,
                state = gameEntity.state,
                arrayofstate = [state],
                stateobj,
                subtype,
                motionName,
                bufferedAction,
                methodName,
                isAction,
                method;
            this.definition = definition;
            this.definition.state = state;
            this.task = new Task(definition, run);
            this.task.setDone();
            for (i in sequencedefinition) {
                activitydefinition = sequencedefinition[i];
                activity = {
                    end : true,
                    parameters :[],
                    type : (typeof activitydefinition.type === "undefined") ? "sequential" : activitydefinition.type,
                    dependsOn : activitydefinition.dependsOn,
                    fireWhen : activitydefinition.fireWhen
                };
                for (obj in activitydefinition){
                    if (obj !== "dependsOn" && obj !== "fireWhen" && obj !== "type"){
                        action = undefined;
                        methodName = activitydefinition[obj];
                        bufferedAction = motionNames.exec(methodName); 
                        motionName = (bufferedAction !== null) ? bufferedAction[1] : methodName; 
                        if (typeof stateSchema[obj] !== "undefined"){ //es un array o un objeto existentes en el estado
                            activity.obj = (typeof state[obj] !== "undefined") ? [state[obj]] : [];
                            typeProperties = stateSchema[obj];
                            if (typeProperties.primaryType === "array"){
                                type = typeProperties.secondaryType;
                            } else if (typeProperties.primaryType === "object"){
                                type = typeProperties.terciaryType;
                            } else {
                                type = typeProperties.primaryType;
                            }
                            gameEntity.addUpdateCallbackOnStateVar("onSet", obj, definition.name+i, getUpdateActivityObjectOnChangeCallback(activity));
                        } else {
                            typeProperties = {};
                            type = definition.applyTo[obj].type;
                            if (typeof type !== "undefined"){
                                activity.obj = [];
                                subtype = type.match(template);
                                if (subtype !== null){//FIXME QUE ESTO PROCESE TAMBIÉN LOS TIPOS COMPLEJOS
                                    typeProperties.primaryType = subtype[1];
                                    type = subtype[2];
                                } 
                            } else {  // es una actividad que simplemente requiere ejecutar una función no asociada a ningún objeto del estado.
                                activity.obj = dummy;
                            }
                        }
                        stateobj = GameEntities.gameEntityClasses[type];
                        if (typeof stateobj !== "undefined" && typeof stateobj.prototype.actionMethods !== "undefined"){
                            action = stateobj.prototype.actionMethods[methodName];
                        }
                        if (typeof action === "undefined"){
                            isAction = false;
                            action = definition.applyTo[obj][methodName];
                            activity.parameters.push(arrayofstate);
                        } else {
                            isAction = true;
                            activity.parameters.push(definition.applyTo[obj][methodName]);
                        }
                        if (typeProperties.primaryType === "array" || typeProperties.primaryType === "object"){
                            activity.method = createArrayFunction(activity, (isAction) ? createActionWithStartAndEndMotion(action, motionName): action);
                        } else  {
                            activity.method = (isAction) ? createActionWithStartAndEndMotion(action, motionName): action;
                        }
                        activity.typeProperties = typeProperties;
                    }
                }
                sequence.push(activity);
            }
            definition.sequence = sequence;
        },
        /**
         * First, process the dynamic part of this behaviour so it doesn't need to be done on 
         * execution and then launches the behaviour.
         */
        run : function(){
            var definition = this.definition,
                state = definition.state,
                dynamic = definition.dynamic,
                sequence = definition.sequence,
                i, 
                id,
                list,
                argumentlist,
                what,
                activity;

            for (i in dynamic){
                activity = sequence[dynamic[i].activity];
                what = dynamic[i].obj;
                if (typeof what !== "undefined"){
                    list = what(state);
                    if ((this.task.isDone() || activity.end) && activity.obj.length > 0){
                        activity.obj.splice(0, activity.obj.length);
                    }
                    activity.obj.push(list);
                } else /*if (activity.typeProperties.primaryType === "array")*/{
                    list = activity.obj[activity.obj.length-1];
                }
                what = dynamic[i].parameters;
                if (typeof what !== "undefined" && typeof list !== "" && typeof list !== "undefined"){
                    if ((this.task.isDone() || activity.end) && activity.parameters.length > 0){
                        activity.parameters.splice(0, activity.parameters.length);
                    }
                    if (toType(list) === "array"){
                        argumentlist = [];
                        for (id in list){
                            argumentlist.push(what(id, list[id], state));
                        }
                        activity.parameters.push(argumentlist);
//                        list = undefined;
                    } else {
                        activity.parameters.push(what.call(list, state));
                    }
                }
            }
            if (this.task.isDone()){
                for (i in sequence){
                    sequence[i].end = false;
                    sequence[i].start = false;
                }
                this.task.setDone(false);
                Scheduler.scheduleGameTask(this.task);
            }
        }
    });

    return Behaviours;
}(CalipsoWebClient.Scheduler.Task, CalipsoWebClient.GameEntities, CalipsoWebClient.Scheduler.Scheduler));
