/*global CalipsoWebClient, Ut*/
/**
 * Archivo de animaciones para pruebas.
 */
CalipsoWebClient.MotionsFuncs = (function(){
    return {
        greatherthan : function(action, constraint) {
            var prop;
            if ((!action || !Ut.hasProperties(action))
                    && (constraint && Ut.hasProperties(constraint))){
                return false;
            }
            for ( prop in constraint) {
                if (prop !== 'eval' && constraint.hasOwnProperty(prop)) {
                    if (!action[prop] || action[prop] <= constraint[prop]) {
                        console.log('evaluating ', prop, ', action[prop] = ',
                                action[prop], '> constraint[prop] = ',
                                constraint[prop], '? ',
                                action[prop] > constraint[prop]);
                        return false;
                    }
                }
            }
            console.log('in greatherthan : true, action: ', action,
                    ', constraint: ', constraint);
            return true;
        },
        lessequalthan : function(action, constraint) {
            if ((!action || !Ut.hasProperties(action))
                    && (constraint && Ut.hasProperties(constraint))) {
                return false;
            }
            var prop;
            for ( prop in constraint) {
                if (prop !== 'eval' && constraint.hasOwnProperty(prop)) {
                    if (!action[prop] || action[prop] > constraint[prop]) {
                        console.log('evaluating ', prop, ', ¿action[prop] = ',
                                action[prop], '<= constraint[prop] = ',
                                constraint[prop], '? ',
                                action[prop] > constraint[prop]);
                        return false;
                    }
                }
            }
            console.log('in lessequealthan : true, action: ', action,
                    ', constraint: ', constraint);
            return true;
        }
    };
}());
