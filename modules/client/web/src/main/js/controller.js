/*global $, CalipsoWebClient, Class, console */
/**
 * Controller Package.
 * 
 * This package has the following classes to control the keystrokes and mouse
 * events:
 * <ul>
 * <li>Controller: this object controls user events like keystrokes and mouse
 * events.
 * </ul>
 * 
 * @requires scheduler.js, logic.js
 * 
 * @autor Víctor López Salazar.
 */

CalipsoWebClient.Controller = (function(Class, $){
    var Controller = {};

    function passEventToGE(gameEntitiesEvents, key, event){
        var i,
            entity,
            entities,
            method;
        
        entities = gameEntitiesEvents[key];
        if (typeof entities !== "undefined"){
            for (i in entities){
                entity = entities[i];
                method = entity.userEvents[key];
                if (!method.call(entity, event)){
                    break;
                }
            }
        }
    }
    function mouseController(event){
        passEventToGE(this.gameEntitiesEvents, event.type, event);
    }

    function keyboardController(event){
        passEventToGE(this.gameEntitiesEvents, event.type+"_"+event.which, event);
    }
    
    Controller.CanvasController = Class.extend({
        init : function(/* {HTMLCanvas} */canvas) {
            this.gameEntitiesEvents = {};
            this.canvas = canvas;
            canvas.controller = this;

            this.canvas.gameEntitiesEvents = this.gameEntitiesEvents;

            $(canvas).keyup(keyboardController);
            $(canvas).keydown(keyboardController);
            $(canvas).mousemove(mouseController);
            $(canvas).mousedown(mouseController);
            $(canvas).mouseup(mouseController);
            canvas.onmousewheel = mouseController;
            canvas.addEventListener('DOMMouseScroll', canvas.onmousewheel, false);
        },
        removeGameEntity : function(entity){
            var event, 
                gameEntitiesEvents = this.gameEntitiesEvents, 
                userEvents = entity.userEvents,
                gameEntitiesQueue;
            for (event in userEvents){
                gameEntitiesQueue = gameEntitiesEvents[event];
                if (typeof gameEntitiesQueue !== "undefined"){
                    gameEntitiesQueue.splice(gameEntitiesQueue.indexOf(entity), 1);
                }
            }
        },
        addGameEntity : function(entity){
            var event, 
                gameEntitiesEvents, 
                userEvents = entity.userEvents;
            for (event in userEvents){
                gameEntitiesEvents  = this.gameEntitiesEvents[event];
                if (typeof gameEntitiesEvents === "undefined"){
                    gameEntitiesEvents = [];
                    this.gameEntitiesEvents[event] = gameEntitiesEvents;
                }
                gameEntitiesEvents.push(entity);
            }
        }
    });

    return Controller;
}(Class, $));

