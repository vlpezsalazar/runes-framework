/*jslint newcap : false*/
/**
 * Utility functions to support class extension and comparison in javascript. 
 */
var console = (function(console){
    if (typeof console === "undefined"){
        console = {
            log : function(){}
        };
    }
    return console;
}(console));

var toType = function(obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
};

var Uint8Array = (function(Uint8Array){
    if (typeof Uint8Array !== "undefined"){
        return Uint8Array;
    }
    return [];
}(Uint8Array));


/**
 * Copyright (c) Mozilla Foundation http://www.mozilla.org/
 * This code is available under the terms of the MIT License
 */
if (!Array.prototype.filter) {
    Array.prototype.filter = function(fun /*, thisp*/) {
        if (typeof fun !== "function") {
            throw new TypeError();
        }
        var len = this.length >>> 0,
            res = [],
            thisp = arguments[1],
            i,
            val;
        for (i = 0; i < len; i++) {
            if (typeof this[i] !== "undefined") {
                val = this[i]; // in case fun mutates this
                if (fun.call(thisp, val, i, this)) {
                    res.push(val);
                }
            }
        }
        return res;
    };
}
/* 
 * DOMParser HTML extension 
 * 2012-02-02 
 * 
 * By Eli Grey, http://eligrey.com 
 * Public domain. 
 * NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK. 
 */  
  
/*! @source https://gist.github.com/1129031 */  
 
var DOMParser = (function(DOMParser) {  
      
  
    var DOMParser_proto = DOMParser.prototype, 
        real_parseFromString = DOMParser_proto.parseFromString;  
  
    // Firefox/Opera/IE throw errors on unsupported types  
    try {  
        // WebKit returns null on unsupported types  
        if ((new DOMParser()).parseFromString("", "text/html")) {  
            // text/html parsing is natively supported  
            return DOMParser;  
        }  
    } catch (ex) {}  
  
    DOMParser_proto.parseFromString = function(markup, type) {  
        if (/^\s*text\/html\s*(?:;|$)/i.test(type)) {  
            var  
              doc = document.implementation.createHTMLDocument("")  
            , doc_elt = doc.documentElement  
            , first_elt  
            ;  
  
            doc_elt.innerHTML = markup;  
            first_elt = doc_elt.firstElementChild;  
  
            if ( // are we dealing with an entire document or a fragment?  
                   doc_elt.childElementCount === 1  
                && first_elt.localName.toLowerCase() === "html"  
            ) {  
                doc.replaceChild(first_elt, doc_elt);  
            }  
  
            return doc;  
        } else {  
            return real_parseFromString.apply(this, arguments);  
        }  
    };
    return DOMParser;
}(DOMParser));

var Class = function(){};
Class.prototype.decorator = function(obj, exceptionList){
    var method,
        methodFactory = function(method){
            return function(){
                var tmp = method.apply(this.decoratorObject, arguments);
                return tmp;
            };
        };
    exceptionList = (toType(exceptionList) === "array") ? exceptionList : [];
    for (method in obj){
        if (typeof this[method] === "function" && method !== "decorator" && method !== "$super" && method !== "init" && exceptionList.indexOf(method) === -1){
            this[method] = methodFactory(obj[method]);
        }
    }
    this.decoratorObject = obj;
};

Class.extend = (function(){
    var fnTest = /xyz/.test(function(){xyz;}) ? /\B\$super\b/ : /.*/;
    return function(definition){
        Class.initReady = false;
        var $super = this.prototype,
            _newClass_ = function(){
                if (typeof this.init !== "undefined" && Class.initReady){
                    this.init.apply(this, arguments);
                }
             },
             prototype = new this(),
             i,
             methodFactory = function(name, method){
                 return function(){
                     this.$super = $super[name];
                     var tmp = method.apply(this, arguments);
                     return tmp;
                 };
             };
        for (i in definition){
            if (typeof prototype[i] === "function" && fnTest.test(definition[i])){
                prototype[i] = methodFactory(i, definition[i]);
            } else {
                prototype[i] = definition[i]; 
            }
        }
        Class.initReady = true;
        prototype.parent = this;
        _newClass_.constructor = _newClass_;
        _newClass_.prototype = prototype;
        _newClass_.extend = arguments.callee;
        _newClass_.augment = Class.augment;
        _newClass_.isChildOf = Class.isChildOf;
    
        return _newClass_;
    };
}());
Class.augment = function () {
    var prototype = this.prototype, 
        i, 
        j,
        prototype1;
    for (i in arguments){
        prototype1 = arguments[i].prototype;
        for (j in prototype1){
            if (typeof prototype[j] === "undefined"){
                prototype[j] = prototype1[j];
            }
        }
    }
};

Class.isChildOf = function(parentClass){
    var presentParent = this;
    
    while (presentParent !== parentClass && presentParent !== Class){
        presentParent = presentParent.prototype.parent;
    }
    return presentParent === parentClass;
};

/************************************************************/
var AbstractNotifiableComponent = Class.extend({
    init : function(){
        this.updateSchema = {};
    },
    get : function(id){
        throw new Exception("ERROR: AbstractNotifiableComponent.get() is not implemented.");
    },
    addListener : function(on, id, callbackName, callback){
        var callbacks = this.updateSchema[id];
        if (typeof callbacks === "undefined"){
            callbacks = {
                    onSet : {},
                    onPush :{},
                    onDelete : {}
            };
            this.updateSchema[id] = callbacks;
        }
        callbacks[on][callbackName] = callback;
    },
    removeListener : function(varName, callbackName){
        var callbacks = this.updateSchema[varName];
        if (typeof callbacks !== "undefined"){
            if (typeof callbacks.onSet[callbackName] !== "undefined"){
                delete callbacks.onSet[callbackName];
            } else if (typeof callbacks.onPush[callbackName] !== "undefined"){
                delete callbacks.onPush[callbackName];
            } else if (typeof callbacks.onDelete[callbackName] !== "undefined"){
                delete callbacks.onDelete[callbackName];
            }
        }
    }
});
/*************************************************************/


var Future = Class.extend({
    init : function(object){
        this.callbacks = [];
        if (typeof object !== "undefined"){
            this.object = object;
        }
    },
    get : function(){
        return this.object;
    },
    set : function(object){
        this.object = object;
        var callbacks = this.callbacks, 
            success;
        while (callbacks.length>0){
            success = callbacks.pop();
            if (typeof success !== "undefined"){
                if (typeof success === "function"){
                    success(object);
                } else if (typeof success.onSuccess === "function"){
                    success.onSuccess(object);
                }                                
            }
        }

    },
    isReady : function(){
        return typeof this.object !== "undefined";
    },
    whenReady : function(callback){
        this.callbacks.push(callback);
    }
});

var ObjectCache = Class.extend({
    init : function (/*{function}*/objectFactoryFunction, /*{function}*/preloadFunction){
        this.cache = {};
        this.objectFactory = objectFactoryFunction;
        if (typeof preloadFunction !== "undefined"){
            preloadFunction(this);
            this.preloaded = true;
        } else {
            this.preloaded = false;
        }
    },
    setObject : function(objectid, object){
        this.cache[objectid] = {
                object : new Future(object)
        };
    },
    /**
     * This methods gets an object from the cache previously stored with the objectid key. If the object
     * doesn't exist, it calls the object factory function passed in the initialization to create one.
     * 
     * @param {string} objectid object's id.
     * @param [{object}] objectData the data needed to create the object
     * @param {boolean} fromCache if true the object is recovered from the cache, in any other case the object is created.
     * @param {function(object)} callback the callback to run when the object is recovered or created.
     * 
     * @return a future object which eventually would get the required value.
     */
    getObject : function(/*{string}*/objectid, 
                          /*[{object}]*/objectData, 
                          /*{boolean}*/fromCache, 
                          /*function(object) | 
                           * {onSuccess: function, onError: function}*/callback){
        var that = this, 
            cacheEntry = this.cache[objectid], 
            object;
        if (fromCache){
            if (typeof cacheEntry !== "undefined" && cacheEntry.object.isReady()){
                if (typeof callback === "function" ){
                    callback(cacheEntry.object.get());                    
                } else if (typeof callback.onSuccess === "function"){
                    callback.onSuccess(cacheEntry.object.get());
                }
            } else if (typeof cacheEntry !== "undefined"){
                cacheEntry.object.whenReady(callback);
            } else {
                cacheEntry = this.cache[objectid] = {
//                        ready : false,
//                      callbacks : [callback],
                        object : new Future()
                };
                cacheEntry.object.whenReady(callback);
                if (!this.preloaded){
                    this.objectFactory(objectid, objectData, {
                        onSuccess : function(object){
                            var callbacks = cacheEntry.callbacks,
                                success;
                            cacheEntry.object.set(object);
//                            cacheEntry.ready = true;
    /*                        while (callbacks.length>0){
                                success = callbacks.pop();
                                if (typeof success !== "undefined"){
                                    if (typeof success === "function"){
                                        success(object);
                                    } else if (typeof success.onSuccess === "function"){
                                        success.onSuccess(object);
                                    }                                
                                }
                            }*/
                        },
                        onError : function() {
    /*                        var callbacks = cacheEntry.callbacks,
                                error;
                            delete that.cache[objectid];*/
                            console.log('ObjectCache.getObject(', objectid, objectData,'): error creando objeto (CUIDADO, NO SE LLAMA A NINGUN CALLBACK)');
    /*                        while (callbacks.length>0){
                                error = callbacks.pop();
                                if (typeof error.onError === "function"){
                                    error.onError(object);
                                }
                            }
    */
                        }
                    });                    
                }
            }
            object = cacheEntry.object;
        } else {
            this.objectFactory(objectid, objectData, callback);
        }
        return object;
    }
});
//diversas funciones útiles
var Ut = {
        hasProperties : function (cl){
            var i;
            for (i in cl){
                if (cl.hasOwnProperty(i)){
                    return true;
                }
            }
            return false;
        }
};
/*
function checkInPrototype(name, prototype){

    return typeof prototype[name] === "function";
}
*/
