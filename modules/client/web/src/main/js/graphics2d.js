/*global jQuery, CalipsoWebClient, Class, toType, console */

CalipsoWebClient.Graphics.Graphics2D = (function($, Graphics, Scheduler) {
    "use strict";
    
    var Graphics2D = {},
        HTMLObject,
        CanvasObject2D;
    
    HTMLObject = Class.extend({
        init : function(DOMParent, callbacks) {
            var callback;

            this.DOMParent = DOMParent;

            for (callback in callbacks) {
                $(DOMParent)[callback](callbacks[callback]);
            }
        },
        css : function(css) {
            $(this.DOMParent).css(css);
        },
        setId : function(id){
            this.id = id;
        }
    });

    CanvasObject2D = Class.extend({
        init : function(callbacks) {
            this.callbacks = {obj : this, callbacks : callbacks};

            if (typeof this.x === "undefined") {
                this.setPosition(0, 0);
                this.setDimensions(0, 0);
            }
            this.inScene = false;
        },
        setInScene : function(){
            this.inScene = true;
        },
        isInScene : function() {
            return this.inScene;
        },
        getCallbacks : function() {
            return this.callbacks;
        },
        isPicked : function(context, x, y) {
            var myx = this.getX(), myy = this.getY();
            return (myx <= x) && (x <= myx + this.getWidth(context)) && (myy <= y)
                    && (y <= myy + this.getHeight(context));
        },
        update : function(source) {
            // ABSTRACT METHOD
        },
        setPosition : function(x, y) {
            this.x = x;
            this.y = y;
        },
        setDimensions : function(width, height) {
            this.width = width;
            this.height = height;
        },
        setX : function(x) {
            this.x = x;
        },
        getX : function() {
            return this.x;
        },
        setY : function(y) {
            this.y = y;
        },
        getY : function() {
            return this.y;
        },
        setWidth : function(width) {
            this.width = width;
        },
        getWidth : function() {
            return this.width;
        },
        setHeight : function(height) {
            this.height = height;
        },
        getHeight : function() {
            return this.height;
        },
        paint : function(context) { // ABSTRACT METHOD
        },
        setId : function(id){
            this.id = id;
        },
        getId : function(){
            return this.id;
        }
    });
    
    Graphics2D.HTMLObject = HTMLObject;
    Graphics2D.CanvasObject2D = CanvasObject2D;
    
    Graphics2D.factory = function(definition, DOMParent) {
        var i,
            g2d,
            element,
            elementDOMParent,
            id = definition.id,
            Type = definition.type,
            init = definition.init,
            css = definition.css,
            source = definition.source,
            elements = definition.elements,
            background = definition.background,
            callbacks = definition.callbacks;

        for (i in elements) {
            element = elements[i];
            // type = Graphics2D[toType(elements[i])];
            if (toType(element) === "object" && (typeof Graphics2D[element.type] !== "undefined")) {
                if (typeof id !== "undefined"){
                    elementDOMParent = $(DOMParent).find("#" + id).get(0);
                }
                elements[i] = Graphics2D.factory(element, (typeof elementDOMParent !== "undefined") ? elementDOMParent : DOMParent);
            }
        }

        Type = Graphics2D[Type];
        if (typeof Type !== "undefined"){
            if (Type.isChildOf(HTMLObject)) { // es un controlador de html
                if (typeof elements === "undefined"){
                    elements = source;
                }
                // todos los tipos 2d deben cumplir este interfaz
                if (typeof init !== "undefined") {
                    g2d = new Type(DOMParent, elements, callbacks, init[0],
                            init[1], init[2], init[3], init[4]);
                } else {
                    g2d = new Type(DOMParent, elements, callbacks);
                }
                // todos los tipos 2d deben cumplir este interfaz

                if (typeof definition.update !== "undefined"){
                    g2d.update = definition.update;
                }
                
                if (typeof g2d.setBackground === "function"
                        && typeof background !== "undefined") {
                    g2d.setBackground(background);
                }
                if (typeof g2d.css !== "undefined" && typeof css !== "undefined") {
                    g2d.css(css);
                }
                if (typeof g2d.paint !== "undefined"){
                    g2d.paint();
                }
            } else { // es un objeto de canvas.
                if (typeof source === "undefined"){
                    source = elements;
                }
                if (typeof init !== "undefined") {
                    g2d = new Type(source, callbacks, init[0], init[1], init[2],
                            init[3], init[4]);
                } else {
                    g2d = new Type(source, callbacks);
                }
            }
            g2d.setId(id);
        }
        return g2d;
    };

    function createPositionCallback(canvas, context, element, handler) {
        return function(e) {
            var xy = canvas.getBoundingClientRect(),
                event = $.extend({
                    posx : ((e.clientX - xy.left)/xy.width)*canvas.width,
                    posy : ((e.clientY - xy.top)/xy.height)*canvas.height
                }, e);
            if (element.isPicked(context, event.posx, event.posy)){
                handler.call(element, event);
            }
        };
    }

    Graphics2D.Group2D = CanvasObject2D.extend({
        init : function(elements, callbacks) {
            var i; 

            if (typeof callbacks !== "undefined"){
                this.callbacks = [{obj : this, callbacks : callbacks}];
            } else {
                this.callbacks = [];
            }
            this.elements = [];

            for (i in elements){
                if (elements[i] instanceof CanvasObject2D){
                    this.addElement(elements[i]);                    
                }
            }
        },
        getElements : function(){
            return this.elements;
        },
        getChilds : function(id){
            var elements = this.elements,
                element,
                childs = [],
                i;

            if (this.id === id){
                childs.push(this);
            }
            for (i in elements){
                element = elements[i];
                if (element.getId() === id){
                    childs.push(element);
                }
                if (element instanceof Graphics2D.Group2D){
                    childs = childs.concat(element.getChilds(id)); 
                }
            }

            return childs;
        },
        addElement : function(component){
            var callbacks = component.getCallbacks(), 
                allCallbacks = this.callbacks;

            if (toType(callbacks) === "array"){
                this.callbacks = allCallbacks.concat(callbacks);
            } else {
                allCallbacks.push(callbacks);
            }

            this.elements.push(component);
            //FIXME HACE FALTA AÑADIR LOS CALLBACKS A LA ESCENA CON LA QUE SE ASOCIA EL GRUPO DE OBJETOS 
        },
        removeElements : function(id){
            var elements = this.elements,
                element,
                i;
            for (i = 0; i < elements.length;){
                element = elements[i];
                if (element.id === id){
                    elements.splice(i, 1);
                } else {
                    i += 1;
                    if (element instanceof Graphics2D.Group2D){
                        element.removeElements(id);
                    }

                }
            }
        },
        paint : function(context){
            var i,
                elements = this.elements;
            for (i in elements){
                elements[i].paint(context);
            }
        }
    });

    Graphics2D.FlowArea = Graphics2D.Group2D.extend({
        init : function(elements, callbacks, dir, paddingprop) {
            this.$super(elements, callbacks);
            this.setPosition(0,0);
            this.dir = dir;
            this.maxWidth = 0;
            this.maxHeight = 0;
            if (typeof paddingprop === "undefined"){
                this.padding = {
                        top : 0,
                        bottom : 0,
                        left : 0,
                        right : 0
                };
            } else {
                this.padding = {
                        top : (typeof paddingprop.top === "undefined") ? 0 : paddingprop.top,
                        bottom : (typeof paddingprop.bottom === "undefined") ? 0 : paddingprop.bottom,
                        left :  (typeof paddingprop.left === "undefined") ? 0 : paddingprop.left,
                        right : (typeof paddingprop.right === "undefined") ? 0 : paddingprop.right
                };                
            }
        },
        setDirection : function (dir){
            this.dir = dir;  
        },
        setMaxWidth : function(maxWidth){
            this.maxWidth = maxWidth;
        },
        getMaxWidth : function(){
            return this.maxWidth;
        },
        setMaxHeight : function (maxHeight){
            this.maxHeight = maxHeight;
        },
        getMaxHeight : function (){
            return this.maxHeight;
        },
        paint : function(context){
            var i,
                padding = this.padding,
                element,
                elements = this.elements,
                x = this.getX() + padding.left,
                maxX = 0,
                y = this.getY() + padding.top,
                maxY = 0,
                maxWidth,
                maxHeight,
                nextX = x,
                nextY = y,
                maxElementX = 0,
                maxElementY = 0;

            if (this.maxWidth === 0){
                this.setMaxWidth(context.canvas.width);
                maxWidth = this.maxWidth;
            }

            if (this.maxHeight === 0){
                this.setMaxHeight(context.canvas.height);
                maxHeight = this.maxHeight; 
            }

            for (i in elements){
                element = elements[i];
                if (typeof element.paddingX === "undefined"){
                    element.paddingX = element.getX();
                }
                if (typeof element.paddingY === "undefined"){
                    element.paddingY = element.getY();
                }
                element.setPosition(nextX + element.paddingX, nextY + element.paddingY);
                element.paint(context);
                if (this.dir === "horizontal"){
                    if (maxElementY < element.getHeight(context)){
                        maxElementY = element.getHeight(context);
                        maxY = nextY + maxElementY;
                    }

                    nextX  += element.getWidth(context) + padding.right;

                    if (maxX < nextX){
                        maxX = nextX;
                    }

                    if (nextX >= maxWidth){
                        nextX = x;
                        nextY = nextY + maxElementY + padding.bottom;
                        maxElementY = 0;
                    }

                } else {

                    if (maxElementX < element.getWidth(context)){
                        maxElementX = element.getWidth(context);
                        maxX = nextX + maxElementX;
                    }

                    nextY  += elements[i].getHeight(context)  + padding.bottom;

                    if (maxY < nextY){
                        maxY = nextY;
                    }

                    if (nextY >= maxHeight){
                        nextY = y;
                        nextX = nextX + maxElementX + padding.right;
                        maxElementX = 0;
                    }

                }
                if (nextX >= maxWidth || nextY >= maxHeight){
                    break;
                }
            }
            this.setDimensions(maxX-x, maxY-y);

        }
    });
    Graphics2D.Scene = HTMLObject.extend({
        /**
         * Crea una escena en 2D sobre un canvas al que se le añaden capacidades
         * de animación y paso de eventos a objetos por posicionamiento.
         * 
         * @param canvasId
         *            nombre u objeto canvas.
         * @param elements
         *            elementos que controla este canvas ordenados de menor a
         *            mayor z-order.
         * @param callbacks
         *            callbacks para la escena
         */
        type : "Scene",
        init : function(canvas, elements, callbacks, width, height) {
            this.$super(canvas, callbacks);

            this.context = canvas.getContext("2d");
            if (typeof width !== "undefined"){
                this.setWidth(width);
            }
            if (typeof height !== "undefined"){
                this.setHeight(height);
            }
            this.update(elements);

            var scenetask = new Scheduler.Task(this, this.paint);
            Scheduler.Scheduler.scheduleGraphicTask(scenetask);

        },
        setWidth : function(width){
            this.DOMParent.width = width;
        },
        setHeight : function(height){
            this.DOMParent.height = height;
        },
        setDimensions : function(width, height) {
            this.DOMParent.width = width;
            this.DOMParent.height = height;
        },
        getWidth : function(){
            return this.DOMParent.width;
        },
        getHeight : function(){
            return this.DOMParent.height;
        },
        setBackground : function(background) {
            this.background = Graphics.getAsset(background);
        },
        update : function(elements) {
            var canvas = this.DOMParent,
                element, 
                i,
                j,
                elementCallback,
                obj,
                innerCallbacks,
                callbacks,
                callback;

            this.elements = [];
            for (i in elements) {
                element = elements[i];
                /*
                 * si es un CanvasObject2D
                 */
                if (element instanceof CanvasObject2D) {
                    element.setInScene();
                    this.elements.push(element);
                    callbacks = element.getCallbacks();
                    if (toType(callbacks) === "array"){
                        for (j in callbacks) {
                            elementCallback = callbacks[j];
                            innerCallbacks = elementCallback.callbacks;
                            obj = elementCallback.obj;
                            for (callback in innerCallbacks){
                                $(canvas)[callback](createPositionCallback(canvas, this.context,
                                        obj, innerCallbacks[callback]));
                            }
                        }
                    } else if (typeof callbacks !== "undefined"){
                        innerCallbacks = callbacks.callbacks;
                        obj = callbacks.obj;
                        for (callback in innerCallbacks){
                            $(canvas)[callback](createPositionCallback(canvas, this.context,
                                    obj, innerCallbacks[callback]));                            
                        }
                    }
                } else {
                    console.log("Warning: el objeto", element,
                            "no es un CanvasObject2D");
                }
            }
            this.paint();
        },
        paint : function() {
            var i, 
                elements = this.elements;

            if (typeof this.background !== "undefined") {
                this.context.drawImage(this.background, 0, 0, this.getWidth(), this.getHeight());
            }
            for (i in elements) {
                elements[i].paint(this.context);
            }
        }
    });

    Graphics2D.GridLayout = Graphics2D.Scene.extend({
        init : function(canvas, elements, callbacks, rows, columns) {
            this.rows = rows;
            this.columns = columns;
            this.$super(canvas, elements, callbacks);
        },
        paint : function() {
            var imagewidth = this.DOMParent.width / this.columns, imageheight = this.DOMParent.height
                            / this.rows, firstindex = 0, i, n, element, numberOfImages = this.rows
                            * this.columns; // ((this.dir ==
                                            // HUD.CanvasElementSlider.SLIDEUPDOWN)
                                            // ? sliderwidth/imagewidth :
                                            // sliderheight/imageheight);

            if (typeof this.background !== "undefined") {
                this.context.drawImage(this.background, 0, 0);
            }

            for (i = 0, n = this.elements.length; i < numberOfImages
                && firstindex < n; i++, firstindex++) {
                element = this.elements[firstindex];
                element.setPosition(imagewidth * (i % this.columns),
                        imageheight * (Math.floor(i / this.rows)));
                element.setDimensions(imagewidth, imageheight);
                element.paint(this.context);
            }
        }
    });


    Graphics2D.Image = CanvasObject2D.extend({
        init : function(source, callbacks, x, y, width, height) {
            var that = this;
            this.drawableObject = Graphics.getAsset(source);
            if (typeof this.drawableObject === "undefined"){
                this.drawableObject = new Image();
                this.drawableObject.onload = function(){
                    var context;
                    that.drawableObject.ready = true;
                    Graphics.putAsset(source, that.drawableObject);
                    while (that.myContexts.length > 0){
                        context = that.myContexts.pop();
                        that.paint(context);
                    }
                };
                this.drawableObject.src = source;
            }
            if (toType(x) === "number" && toType(y) === "number") {
                this.setPosition(x, y);
            } else {
                this.setPosition(0, 0);
            }
            if (toType(width) === "number" && toType(height) === "number") {
                this.setDimensions(width, height);
            } else {
                this.setDimensions(this.drawableObject.width,
                        this.drawableObject.height);
            }
            this.myContexts = [];
            this.$super(callbacks);
        },
        update : function(source, context) {
            var that = this;
            this.drawableObject.onload = Graphics.getAsset(source);
            if (typeof this.drawableObject === "undefined"){
                this.drawableObject = new Image();
                this.drawableObject.onload = function(){
                    Graphics.putAsset(source, that.drawableObject);
                    that.paint(context);
                };
                this.drawableObject.src = source;
            } else {
                this.paint(context);
            }
        },
        paint : function(context) {
            if (this.drawableObject.ready){
                context.drawImage(this.drawableObject, this.getX(), this.getY(),
                        this.getWidth(), this.getHeight());                
            } else {
                this.myContexts.push(context);
            }
        }
    });

    Graphics2D.Text = CanvasObject2D.extend({
        init : function(source, callbacks, x, y, font, color, size) {
            this.drawableObject = source;

            if (toType(x) === "number" && toType(y) === "number") {
                this.setPosition(x, y);
            } else {
                this.setPosition(0, 0);
            }

            if (typeof font === "undefined") {
                this.font = "Arial";
            } else {
                this.font = font;
            }

            if (typeof color === "undefined") {
                this.color = "#000";
            } else {
                this.color = color;
            }

            if (typeof size === "undefined") {
                this.size = "12pt";
                this.setHeight(12);
            } else {
                this.setHeight(size);
                this.size = size + "pt";
            }
            this.$super(callbacks);
        },
        getWidth : function (context){
            context.textBaseline = "top";
            context.font = this.size + " " +this.font;
            return context.measureText(this.drawableObject).width;
        },
        update : function(source, context) {
            this.drawableObject = source;
            this.paint(context);
        },
        paint : function(context) {
            context.textBaseline = "top";
            context.font = this.size + " " +this.font;
//            console.log("empleando la fuente: ", this.size + " " +this.font);
            context.fillStyle = this.color;
            context.fillText(this.drawableObject, this.getX(), this.getY());
        }
    });

    Graphics2D.LoaderDIV = Class.extend({
        init : function(){
            var div = $("<div />",{
                style : "position:absolute;left:30%;top:50%;z-index:10"
            });

            $("body").append(div.get(0));
            this.display = div;

        },
        getUpdateCallback : function(){
            var that = this;
            return function(type, id, statistics){
                that.display.html("Loading " + type + ":" + id + " - " + statistics.loaded + "/" + statistics.total);
                if (statistics.loaded === statistics.total){
                    that.display.slideUp();
                }
            };
        }
    });
    
    return Graphics2D;
}(jQuery, CalipsoWebClient.Graphics, CalipsoWebClient.Scheduler));