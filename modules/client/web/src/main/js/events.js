/*global $, CalipsoWebClient, Class, console */

/**
 * 
 */

if (typeof CalipsoWebClient.Logic.Events === "undefined"){

	CalipsoWebClient.Logic.Events = {};
	
(function(pke){
	pke.Event = Class.extend({
	    init: function(object){
	        this.eventObject = object;
	    },
		eventOriginator : null,
		eventTarget : null,
		/**
		 * This method checks if the event should be
		 * included in the daemon's event queue.
		 */
		check : function(originator, target){
			this.eventOriginator = originator;
			this.eventTarget = target;
			return true; 
		},
		/**
		 * This method executes the actions associated
		 * with the event.
		 */
		doIt : function(){}
	});

	pke.SoundEvent = Class.extend({
	    init : function(object){
	        pke.Event.call(this, object);
	    },
		check : function(originator, target){
//para cada objeto que se asocia a un evento se va a tener
//una propiedad que caracteriza a este chequeo, p.e. 
// object['radioaccion'] o constante.
			return true; 
		},
		/**
		 * This method executes the actions associated
		 * with the event.
		 */
		doIt : function(){
			//llamar al SoundEngine
			
		}
	});

	pke.BurnEvent = Class.extend({
	    init: function(object){
	        pke.Event.call(this, object);
	    },
		check : function(originator, target){
//para cada objeto que se asocia a un evento se va a tener
//una propiedad que caracteriza a este chequeo, p.e. 
// object['radioaccion'] o constante.
			return true; 
		},
		/**
		 * This method executes the actions associated
		 * with the event.
		 */
		doIt : function(){
			//target.getParams().intensity = object.intensity
			//llamar a GameEngine.doAction(target, 'burn')
			//
		}
	});
}(CalipsoWebClient.Logic.Events));
}