
import static org.junit.Assert.*;

import java.util.Properties;

import javax.sql.DataSource;

import org.hsqldb.jdbc.JDBCDataSourceFactory;
import org.junit.Before;
import org.junit.Test;

import es.calipso.framework.core.dao.User;
import es.calipso.framework.storage.DBManager;
import es.calipso.framework.storage.exceptions.CannotRegisterException;
import es.calipso.framework.storage.exceptions.ClassNotRegisteredException;
import es.calipso.framework.storage.exceptions.EntityManagerNotSetException;
import es.calipso.framework.storage.exceptions.EntityNotFoundException;

public class TestDAO {
    private static final String TESTMANAGER = "TESTMANAGER";
    private static final String TESTDAO1DB = "/sql/tables.ddl.sql";
    private static final String TESTDAO2DB = "/sql/data.sql";
    private DataSource ds;

    public TestDAO() throws Exception {
        Properties p = new Properties();
        p.put("user", "test");
        p.put("password", "test");
        p.put("url", "jdbc:hsqldb:mem:test");

        ds = JDBCDataSourceFactory.createDataSource(p);
        DBManager.newManager(TESTMANAGER, ds, TestDAO.class.getResourceAsStream(TESTDAO1DB), TestDAO.class.getResourceAsStream(TESTDAO2DB));
    }
    @Before
    public void cleanup() throws EntityManagerNotSetException {
        DBManager manager = DBManager.getManager(TESTMANAGER);
        manager.dropTypes();
    }
    @Test
    public void testTestUsers() throws EntityManagerNotSetException, CannotRegisterException, EntityNotFoundException, ClassNotRegisteredException{
        DBManager manager = DBManager.getManager(TESTMANAGER);
        manager.registerType(User.class);

        User victor = manager.loadEntity(User.class, "victor");

        assertEquals("victor", victor.getLogin());
        assertEquals("m4o2r5te", victor.getPassword());
//        assertEquals("victor", victor.getNickname());

        User pablo = manager.loadEntity(User.class, "pablo");
        assertEquals("pablo", pablo.getLogin());
        assertEquals("chess_master", pablo.getPassword());
//        assertEquals("pablo", pablo.getNickname());

        User valver = manager.loadEntity(User.class, "valver");
        assertEquals("valver", valver.getLogin());
        assertEquals("role_master", valver.getPassword());

    }

}
