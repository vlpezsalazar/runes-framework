insert into calipso.User (nickname, login, password) values
-- Usuario por defecto, se emplea para almacenar configuraciones por defecto en la bd
('defaultUser', 'defaultUser', '20x-.,una_clave-muy.complicada595)43,3.48{*+ç^}'),
-- Usuarios de prueba
('Pablo Arroyo', 'pablo', 'chess_master'),
('Víctor López', 'victor', 'm4o2r5te'),
('Jose Valverde', 'valver', 'role_master');


insert into calipso.UserRole (userid, role) values 
('defaultUser', 'game'),
('pablo', 'user'), 
('victor', 'user'), 
('valver', 'user');