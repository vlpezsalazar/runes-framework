package es.calipso.framework.core.context;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import es.calipso.framework.core.communications.GameResponseMessage;
import es.calipso.framework.core.dao.UUIDGenerator;
import es.calipso.framework.core.dao.User;
import es.calipso.framework.core.exceptions.UserNotFoundException;
import es.calipso.framework.core.gameengine.rules.ChangeSet;

public class GameResponseSet {

    public GameResponseSet() {}

    /**
     * 
     * @param changeSet paramétro sólo para completar log en debug.
     * @param clientData conjunto de cambios a añadir
     * @param dataTargets
     * @param timeStamp
     */
    void add(Map<ChangeSet, List<GameResponseData<?>>> changeSets, Set<String> ctx) {
        Set<Entry<ChangeSet,List<GameResponseData<?>>>> changeSetsEntries = changeSets.entrySet();
        for (Entry<ChangeSet, List<GameResponseData<?>>> changeSetEntry : changeSetsEntries){

            ChangeSet changeSet = changeSetEntry.getKey();
            List<GameResponseData<?>> clientData = changeSetEntry.getValue();

            debug[0] = changeSet;
            for (GameResponseData<?> grd : clientData){
                Set<String> targets = grd.getTargets();
                if (targets == null){
                    targets = ctx;
                }
                if (targets != null){
                    debug[1] = grd;
                    debug[2] = targets;
    
                    log.debug("Contexto: {} - aplicando cambios {} a los objetivos: {}", debug);
                    
                    for (String target : targets){
                        GameResponseMessage userResponse = responses.get(target);
                        if (userResponse == null){
                            userResponse = ObjectPool.get(GameResponseMessage.factory);
                            userResponse.setTimeStamp(changeSet.getServerTimeStamp());
                            responses.put(target, userResponse);
                        }
                        //en caso que el conjunto de cambios provenga de una petición asociada al cliente actual
                        if (target.equals(changeSet.getSource())){
                            //establecemos el id del mensaje de vuelta al sello de tiempo que proviene del cliente
                            userResponse.setTimeStamp(changeSet.getClientTimeStamp());
                        }
                        userResponse.compose(grd);
                    }                
                } else {
                    log.warn("¡El conjunto de datos actual no tiene ningún objetivo!");
                }
    
            }
        }
    }

    public GameResponseMessage get(User us) throws UserNotFoundException{
        String userid = UUIDGenerator.getKeyCode(us);
        GameResponseMessage grm = responses.get(userid);
        return grm;
    }
    public void tryDispose() {
        responsesSent++;
        if (responsesSent == responses.size()){
            Collection<Entry<String, GameResponseMessage>> c = responses.entrySet();
            Iterator<Entry<String, GameResponseMessage>> iterator = c.iterator();
            Entry<String, GameResponseMessage> item;
            while (!iterator.hasNext()){
                item = iterator.next();
                iterator.remove();
                GameResponseMessage grm = item.getValue();
                synchronized (responses){
                    if (!grm.isClean()){
                        ObjectPool.release(GameResponseMessage.factory, grm);                        
                    }
                }
            }
        }
    }
    public void clean() {
        for (GameResponseMessage grm : responses.values()){
            ObjectPool.release(GameResponseMessage.factory, grm);
        }
    }
    public boolean isEmpty() {
        return responsesSent == responses.size();
    }

    @Override
    public String toString() {
        return responses.toString();
    }
    private int responsesSent = 0;
    private ConcurrentHashMap<String /*userid*/, GameResponseMessage /*userresponse */> responses = new ConcurrentHashMap<>(8, 0.9f, 1);
    private static final Logger log = LoggerFactory.getLogger(GameResponseSet.class);
    //objeto usado solo para debug en método add
    private Object debug[] = new Object[3];
}

