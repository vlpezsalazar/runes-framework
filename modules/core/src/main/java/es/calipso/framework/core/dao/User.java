package es.calipso.framework.core.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializableWithType;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.TypeSerializer;

import es.calipso.framework.storage.annotations.DBEntity;
import es.calipso.framework.storage.annotations.NamedQuery;
import es.calipso.framework.storage.annotations.PK;
import es.calipso.framework.storage.annotations.Populate;

@DBEntity(table={"User", "UserRole"},
        queries= {
        @NamedQuery(name = User.RETRIEVEUSER, query = "select $id, $password FROM $$ u where u.login = $login", type = 1),
        @NamedQuery(name = User.RETRIEVEUSERROLES, query = "select role from $$1 r where r.userid = $login", type = 1)
})
public class User implements JsonSerializableWithType {

    public User(){}

    public User(String login, String password) {
	    this.login = login;
	    this.password = password;
    }

    @Override
    public void serialize(JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {
        jgen.writeStartObject();
        jgen.writeStringField("className", "User");
        jgen.writeObjectField("state", personalization);
        jgen.writeEndObject();
    }

    @Override
    public void serializeWithType(JsonGenerator jgen,
            SerializerProvider provider, TypeSerializer typeSer)
            throws IOException, JsonProcessingException {
        serialize(jgen, provider);
    }

    public void addRole(String role){
        roles.add(role);
    }
    public String getLogin() {
        return login;
    }
    public String getPassword(){
        return password;
    }
    public ArrayList<String> getRoles(){
        return roles;
    }
    public void logInGame(String gameId) {
        gamesLogged.add(gameId);
    }
    public Set<String> getGamesLogged() {
        return gamesLogged;
    }
    public void clearPersonalization(){
        personalization.clear();
    }
    public void setPersonalization(String key, Object obj){
        personalization.put(key, obj);
    }
    public Object getAttribute(String key) {
        return userInfo.get(key);
    }
    public void setAttribute(String key, Object obj) {
        userInfo.put(key, obj);
    }
    public void setInfo(String property, String value){
        userInfo.put(property, value);
    }

    public String getInfo(String property){
        return (String) userInfo.get(property);
    }

    public int getId() {
        return id;
    }

    public static final String RETRIEVEUSER = "RETRIEVEUSER",
                                 RETRIEVEUSERROLES = "RETRIEVEUSERROLES";

    private int id;

    @PK
    private String login;

//    @PK(1)
    private String password;
    
    @Populate(query=RETRIEVEUSERROLES, 
              init="$this.addRole((String)#role);")
    private ArrayList<String> roles  = new ArrayList<String>();    

    //información temporal que puede almacenar un usuario (como por ejemplo un personaje asociado a un juego).
    private HashMap<String, Object> userInfo = new HashMap<String, Object>();
    
    private HashMap<String, Object> personalization = new HashMap<String, Object>();

    private Set<String> gamesLogged = new HashSet<String>();
}