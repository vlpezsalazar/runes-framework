package es.calipso.framework.core.management;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.calipso.framework.core.GameTypeRegister;
import es.calipso.framework.core.IGame;
import es.calipso.framework.core.communications.GameRequestMessage;
import es.calipso.framework.core.communications.Messenger;
import es.calipso.framework.core.context.Context;
import es.calipso.framework.core.context.InitialContext;
import es.calipso.framework.core.gameengine.GameEngineFactory;
import es.calipso.framework.core.gameengine.IGameEngine;
import es.calipso.framework.core.gameengine.GameEngine.Type;
import es.calipso.framework.text.TextBuilder;

/**
 * Esta clase es un gestor de juegos. Se encarga de instanciar un juego concreto
 * y posteriormente de mandar una petición de actualización al juego que le
 * corresponde.
 * 
 * @author Víctor López Salazar.
 *
 */
public class GameManager {
    public static GameManager getInstance(){
        return gm;
    }

    static GameManager newInstance(Messenger msn){
        if (gm == null){
            gm = new GameManager();
            gameEngine = GameEngineFactory.getInstance(msn);
            Context.setGE(gameEngine);
        }
        return gm;
    }

    private GameManager() {
        games = new HashMap<String, IGame>();
    }
    /**
     * Crea un nuevo juego a partir del descriptor, la factoria Un descriptor de juego es 
     * una configuración para un juego concreto. Aparte de la configuración
     * necesariamente debe contener el nombre de la factoría que instancia
     * el juego del descriptor y el identificador del mismo.
     * 
     * @see GameDescriptor
     * 
     * @param gd descriptor del juego.
     * @param gameFactory cargador asociado al juego.
     * @param t Tipo de gameEngine que ejecuta el juego.
     * @return un código que identifica al juego en este GameManager. 
     * @throws Throwable si se produce alguna excepción.
     */
    public String newGame(IGame gd, GameTypeRegister gameFactory, Type t) throws Throwable {
        String gameName = gameFactory.getGameName();
        games.put(gameName, gd);
        initialContexts.put(gameName, new InitialContext());
        gameEngine.loadGameRules(gameName, gameFactory.getGameRules(), t);
        gameEngine.addServerRules(gameFactory.getContexts());
        return gameName;
    }
    public IGame getGame(String gameid) {
        return games.get(gameid);
    }
	public void dispatchAction(GameRequestMessage wur) {
        IGame game = games.get(wur.getGameId());
        Context ctx = null;
        if (game != null){
            ctx = game.getContext(wur); 
        }

        if (ctx == null){
            ctx = initialContexts.get(wur.getGameId());
        }

        if (ctx != null){
	        if (!gameEngine.dispatchAction(wur, ctx)){
	            log.warn("La acción no fue servida. wur = {}, ctx = {}.", wur, ctx);
	        }
	    } else {
	        log.warn(m.get("UNREGISTEREDGAME", wur));
	    }
	}

    private HashMap<String, InitialContext> initialContexts = new HashMap<String, InitialContext>(); 
	//TODO IMPLEMENTAR LA CLASE "ProductOwner" que permita autenticar a un usuario del framework.
    private HashMap<String, IGame> games;
 

    private static IGameEngine gameEngine;
	private Logger log = LoggerFactory.getLogger(GameManager.class);
	private TextBuilder m = new TextBuilder();
	private static GameManager gm;
	private static final String LOGMESSAGESFILE = "/messages/logmessages";
    static {
        TextBuilder.load(GameManager.class.getResourceAsStream(LOGMESSAGESFILE));
    }
}