package es.calipso.framework.core.communications;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import es.calipso.framework.core.context.GameResponseSet;
import es.calipso.framework.core.exceptions.FactoryNotRegisteredException;
import es.calipso.framework.core.exceptions.NotSerializableException;

/**
 * Clase abstracta que ofrece facilidades de serialización/deserialización de
 * mensajes enviados/recibidos. Este clase debe extenderse para ligar el método
 * <i>sendToClients(<{@link GameResponseSet} rs)</i> a algún mecanismo de
 * transporte que permita enviar los mensajes de respuesta a los clientes. Los
 * mensajes de petición de los clientes al servidor deben propocionarse
 * mediante un InputStream.
 * 
 * @author Víctor López Salazar
 *
 */
public abstract class Messenger {

	/**
	 * Metodo que debe extenderse
	 * @param rs conjunto de respuestas a enviar a los clientes.
	 */
    public abstract void sendToClients(GameResponseSet rs);
    

    public static GameRequestMessage getMessage(InputStream inputStream) throws ClassNotFoundException, FactoryNotRegisteredException{
        GameRequestMessage message = null;
        try {
            @SuppressWarnings("unchecked")
            HashMap<String, Object> data = om.readValue(inputStream, HashMap.class);
            String type = (String) data.get(TYPE);
            IMessageFactory<? extends GameRequestMessage> mesfact = mesfactory.get(type);
            if (mesfact == null){
                Class.forName(type, true, cloader);
                mesfact = mesfactory.get(type);
                if (mesfact == null) throw new FactoryNotRegisteredException(type);
            }
            message = mesfact.newMessage(data);
        } catch (JsonParseException e) {
            log.error("", e);
        } catch (JsonMappingException e) {
            log.error("", e);
        } catch (IOException e) {
            log.error("", e);
        }
        return message;
    }
    /**
     * Serializa en formato JSON los mensajes de respuesta que se deben enviar
     * a los clientes.
     * 
     * @param object el mensaje de respuesta a serializar. 
     * @return una cadena que representa al objeto serializado en formato JSON.
     * 
     * @throws NotSerializableException si el objeto pasado como parametro no
     *  es serializable.
     */
    public static final String serialize(Object object) throws NotSerializableException {
        String message = null;
        try {
            message = om.writeValueAsString(object);
//            log.info("Enviando respuesta {}", message);
//            asyncIOWriter.write(message);
        } catch (Throwable e) {
            throw new NotSerializableException(e);
        }
        return message;
    }
    /**
     * 
     * @return devuelve el mapeador de objetos que comparten todas las 
     * especializaciones de esta clase.
     */
    public static final ObjectMapper getObjectMapper() {
        return om;
    }
    /**
     * Añade una implementación de factoría concreta para un tipo de mensaje
     * de petición. Si sólo se utilizan los mensajes de tipo GameRequestMessage
     * no es necesario utilizar este método.
     * 
     * @param f factoría a añadir.
     */
    public static final void addFactory(IMessageFactory<?> f){
        mesfactory.put(f.getType(), f);
    }
    private static ClassLoader cloader = Messenger.class.getClassLoader();
    private static final String TYPE = "type";

    private static final ObjectMapper om = new ObjectMapper();
    private static final Logger log = LoggerFactory.getLogger(GameRequestMessage.class);    
    private static final HashMap<String, IMessageFactory<?>> mesfactory = new HashMap<String, IMessageFactory<?>>();
}
