package es.calipso.framework.core.gameengine.rules;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Rule {

    boolean active();

    String[] requestFeatures() default "";

}
