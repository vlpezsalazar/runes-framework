package es.calipso.framework.core.gameengine;

import es.calipso.framework.core.communications.GameResponseMessage;
import es.calipso.framework.core.context.GameResponseSet;
import es.calipso.framework.core.dao.User;
import es.calipso.framework.core.exceptions.UserNotFoundException;

public class TimeMessage extends GameResponseSet {
    
    public TimeMessage() {
        m.compose(time);
    }
    public void setTime(long quantum){
        time.setMixture("time", quantum);
    }
    @Override
    public GameResponseMessage get(User us) throws UserNotFoundException {
        return m;
    }
    
    public void clean() {
    }
    public boolean isEmpty() {
        return false;
    }
    @Override
    public String toString() {
        return m.toString();
    }
    private TimeMessageData time = new TimeMessageData();
    private GameResponseMessage m = new GameResponseMessage();

}
