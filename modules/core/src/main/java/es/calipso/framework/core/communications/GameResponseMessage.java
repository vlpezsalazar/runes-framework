package es.calipso.framework.core.communications;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializableWithType;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.TypeSerializer;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import es.calipso.framework.core.context.GameResponseData;
import es.calipso.framework.core.context.GameResponseSet;
import es.calipso.framework.core.context.ObjectPool;


/**
 * Mensaje de respuesta del servidor para un conjunto de clientes. Cuando un 
 * GameEngine procesa en un quantum todas las peticiones de los clientes de un
 * juego ejecutando las reglas correspondientes, cada cliente recibe un mensaje
 * de respuesta que contiene los efectos combinados de las acciones de todos
 * los clientes que se ejecutaron en ese quantum y que le afectan. Esta clase
 * formaliza la estructura de esos mensajes mediante un contenedor de
 * GameResponseData y provee los métodos necesarios para serializarlos. La
 * asociación entre un GameResponseMessage y el usuario al que se dirige esa
 * respuesta se establece en la clase GameResponseSet.
 * 
 * @see {@link GameResponseData}, {@link GameResponseSet}
 * 
 * @author Víctor López Salazar
 *
 */
public class GameResponseMessage implements JsonSerializableWithType {

	/**
	 * Establece un sello de tiempo en la respuesta.
	 * 
	 * @param ts tiempo en l
	 */
    public void setTimeStamp(long ts){
        timeStamp = ts;
    }
    /**
     * Añade datos a la respuesta actual.
     * 
     * @param grd datos de la respuesta actual. 
     */
    public void compose(GameResponseData<?> grd) {
        
        grd.setInPool(false);

//        log.debug("añadiendo datos {} al mensaje {}", grd, this);
        if (!orderSet.contains(grd)){
            orderSet.add(grd);
            data.add(grd);
        }

    }
    /**
     * Límpia la respuesta actual de datos y elimina la marca de
     * "respuesta enviada" de la misma.
     */
    public void clean(){
        sent = false;
        for (GameResponseData<?> m : data) {
            if (!m.isInPool()){
                ObjectPool.release(m.getFactory(), m);
                m.setInPool(true);
            }
        }
        data.clear();
        orderSet.clear();
    }
    /**
     * Indica si esta respuesta contiene datos o no.
     * @return true si la respuesta está vacia, false en otro caso.
     */
    public boolean isClean(){
        return data.isEmpty();
    }
    /**
     * Serializa la respuesta en formato JSON para enviarla por la red al
     * cliente. Este método es llamado automáticamente por el servidor.
     */
    @Override
    public void serialize(JsonGenerator arg0, SerializerProvider arg1)
            throws IOException, JsonProcessingException {
        sent = true;
        arg0.writeStartObject();
        arg0.writeNumberField("timeStamp", timeStamp);
        for (GameResponseData<?> m : data) {
            // log.debug(m.toString());
            arg0.writeObject(m);
        }
        arg0.writeEndObject();
    }
    /**
     * Indica si el mensaje se envió al cliente.
     * @return true si el mensaje fue enviado al cliente, false en otro caso.
     */
    public boolean isSent(){
        return sent;
    }
    @Override
    public void serializeWithType(JsonGenerator arg0, SerializerProvider arg1,
            TypeSerializer arg2) throws IOException, JsonProcessingException {
        serialize(arg0, arg1);

    }
    /**
     * Util para depuración. 
     * @return una cadena que representa el mensaje en formato texto.
     */
    @Override
    public String toString() {
        return data.toString();
    }
    
    private long timeStamp = 0;

    private boolean sent = false;

    private Set<GameResponseData<?>> data = new LinkedHashSet<>();
    private Set<GameResponseData<?>> orderSet = new TreeSet<>();

//    private static final Logger log = LoggerFactory.getLogger(GameResponseMessage.class);

    public static BasePoolableObjectFactory factory = new BasePoolableObjectFactory() {

        @Override
        public Object makeObject() throws Exception {
            return new GameResponseMessage();
        }
        public void passivateObject(Object obj) throws Exception {
            GameResponseMessage grm = (GameResponseMessage) obj;
            grm.clean();
        }
    };
    
    static {
        ObjectPool.newFactory(factory);
    }

}