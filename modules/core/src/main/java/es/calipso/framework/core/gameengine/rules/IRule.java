package es.calipso.framework.core.gameengine.rules;

import es.calipso.framework.core.communications.GameRequestMessage;
import es.calipso.framework.core.context.Context;

/**
 * Interfaz para implementar una regla de juego.  
 *
 * @param <T> tipo de mensaje que la regla recibe. El tipo padre suele ser
 * más que suficiente.
 * 
 * 
 * @author Víctor López Salazar
 */
public interface IRule<T extends GameRequestMessage> {
    /**
     *  Recibe un mensaje de tipo GameRequestMessage con los parámetros de su 
     *  ejecución. Crea una serie de efectos que posteriormente serán ordenados
     *  y aplicados al contexto ctx.
     *  
     * @param request mensaje de petición de disparo de la regla que contiene
     * los parámetros de su ejecución actual. 
     * @param ctx contexto sobre el que se aplica la regla.
     * @param effects el conjunto de cambios que la regla producirá. Para
     * producir un efecto se utilizan los métodos {@link ChangeSet#writeMessageForClient(String, ClientRule)},
     * {@link ChangeSet#writeMessageForClient(String, String, Object)},
     * {@link ChangeSet#addEffectOnTarget(Object, Effect, Object, Object...)},
     * 
     * @return true si la regla produjo algún efecto sobre el contexto (hay
     * efectos que procesar) o false en caso contrario.
     * @throws Throwable 
     * 
     * @see ChangeSet
     */
    public boolean prepare (T request, Context ctx, ChangeSet effects) throws Throwable;
}
