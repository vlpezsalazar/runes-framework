package es.calipso.framework.core.exceptions;

import es.calipso.framework.text.TextBuilder;

@SuppressWarnings("serial")
public class FeatureNotFoundException extends Exception {
    public FeatureNotFoundException(String messageclass, String featid){
        super(m.get("FEATURENOTFOUNDEXCEPTION", messageclass, featid));
    }
    private static final TextBuilder m = TextBuilder.getSingletonInstance();
}
