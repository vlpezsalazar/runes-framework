package es.calipso.framework.core.gameengine;

import java.util.ArrayList;
import java.util.List;

import es.calipso.framework.core.communications.GameRequestMessage;
import es.calipso.framework.core.context.Context;
import es.calipso.framework.core.gameengine.GameEngine.Type;
import es.calipso.framework.core.gameengine.rules.IRule;

/**
 * Interfaz que especifica la API de un servidor de lógica de juego local y
 * remoto.
 *  
 * @author Víctor López Salazar
 */
public interface IGameEngine {
    
    public void loadGameRules(String gameid, List<IRule<? extends GameRequestMessage>> ruleList, Type t) throws Throwable;

    /**
     * Ejecutará una acción de manera asíncrona.
     * 
     * @param wur datos que contienen la acción a ejecutar y los parametros de
     * la misma. Un parámetro importante es el sello de tiempo en que se realizó
     * la acción, pues si esta tiene un retardo mayor a 2 quantos la petición
     * queda descartada.
     * @param ctx contexto sobre el que se ejecutará la acción.
     * 
     * @return true si la petición se procesó, false en otro caso.
     */
    public boolean dispatchAction(GameRequestMessage wur, Context ctx);

    /**
     * Añade las reglas activas de los contextos pasados como parámetro al
     * GameEngine .
     * 
     * @param arrayList lista de contextos en los que existirán métodos 
     * marcados con la anotacion Rule(active = true).
     */
    public void addServerRules(ArrayList<Class<? extends Context>> arrayList);

    /**
     * Añade un contexto a la lista de contextos sobre los que se ejecutan las
     * reglas activas del servidor. Solo se ejecutarán aquellas reglas que
     * sean apropiadas para dicho contexto.
     * 
     * @param ctx contexto sobre el que activar las reglas.
     */
    public void activateContextRules(Context ctx);
}
