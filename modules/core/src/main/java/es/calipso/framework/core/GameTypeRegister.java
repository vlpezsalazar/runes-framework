package es.calipso.framework.core;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.calipso.framework.core.context.Context;
import es.calipso.framework.core.gameengine.rules.IRule;
import es.calipso.framework.core.gameengine.rules.Rule;
import es.calipso.framework.storage.DBManager;
import es.calipso.framework.storage.annotations.DBEntity;
import es.calipso.framework.storage.exceptions.CannotRegisterException;
import es.calipso.framework.storage.exceptions.EntityManagerNotSetException;

/**
 * Permite cargar dinámicamente las clases de un juego extendiendo un
 * ClassLoader. Cada juego utiliza su propio GameTypeRegister.
 * 
 * @author Víctor López Salazar
 *
 */
public class GameTypeRegister extends URLClassLoader{

	/**
	 * Este constructor explora un archivo en busca de clases de juego,
	 * reglas y entidades de juego para cargarlas a memoria de forma
	 * dinámica.
	 * @param game archivo jar donde se encuentra empaquetado el juego.
	 * 
	 * @throws EntityManagerNotSetException Si el juego requiere de
	 *  persistencia pero no hay declarado ningún EntityManager.
	 */
    @SuppressWarnings("unchecked")
    public GameTypeRegister(File game) throws EntityManagerNotSetException {
        super(new URL[]{}, Thread.currentThread().getContextClassLoader());
        JarFile jar= null;
        try {
            this.addURL(game.toURI().toURL());
            jar = new JarFile(game);
            // Getting the files into the jar
            Enumeration<? extends JarEntry> enumeration = jar.entries();
            // Iterates into the files in the jar file
            while (enumeration.hasMoreElements()) {
                ZipEntry zipEntry = enumeration.nextElement();
                // Is this a class?
                if (zipEntry.getName().endsWith(".class")) {

                    // Relative path of file into the jar.
                    String className = zipEntry.getName();

                    // Complete class name
                    className = className.replace(".class", "").replace("/", ".");
                    // Load class definition from JVM
                    try {
                        Class<?> clazz = this.loadClass(className);
                        // Verify the type of the "class"
                        if (clazz.isAnnotationPresent(Rule.class)) {
                            log.info("Encontrada clase de regla: {}", clazz.getCanonicalName());
                            rules.add(clazz);
                        } else if (clazz.isAnnotationPresent(Game.class)) {
                            Game g = clazz.getAnnotation(Game.class);
                            gdclass = (Class<? extends IGame>) Class.forName(className, true, this);
                            gameName = g.value();
                        } else if (Context.class.isAssignableFrom(clazz)){
                            log.info("Encontrado contexto con reglas de servidor: {}", clazz.getCanonicalName());
                            contexts.add((Class<? extends Context>) clazz);
                        }
                        if (clazz.isAnnotationPresent(DBEntity.class)){
                            daos.add(clazz);
                        }
                    } catch (ClassCastException e) {
                        log.error("", e);
                    } catch (ClassNotFoundException e) {
                        log.error("", e);
                    }
                }
            }
            if (!daos.isEmpty()){
                try {
                    dbmanager = DBManager.newManager(getGameName(),
                                ds,
                                gdclass.getResourceAsStream(
                                        "/daodata/tables.ddl.sql"),
                                gdclass.getResourceAsStream(
                                        "/daodata/data.sql"));
                    } catch (SQLException e) {
                        if (SCHEMAEXISTS.equals(e.getSQLState())) {
                            try {
                                dbmanager = DBManager.getManager("");
                            } catch (EntityManagerNotSetException e1) {

                            }
                        } else {
                            log.error("", e);
                        }
                    } catch (IOException e) {
                        log.error("", e);
                    } catch (URISyntaxException e) {
                        log.error("", e);
                    } finally {
                        if (dbmanager != null){
                            for (Class<?> cl : daos){
                                dbmanager.registerType(cl);
                            }                            
                        }
                    }
                    
            }
            for (Class<?> cl : rules){
                irules.add((IRule<?>) cl.newInstance());
            }
        } catch (IOException e) {
            log.error("", e);
        } catch (CannotRegisterException e) {
            log.error("", e);
        } catch (InstantiationException e) {
            log.error("", e);
        } catch (IllegalAccessException e) {
            log.error("", e);
        } finally {
        	if (jar != null){
        		try {
					jar.close();
				} catch (IOException e) {
				}
        	}
        }
    }
    /**
     * 
     * @return una lista que contiene las reglas del juego cargado mediante
     * este GameTypeRegister.
     */
    public ArrayList<IRule<?>> getGameRules(){
        return irules;
    }
    /**
     * @return la lista de contextos de este juego.
     */
    public ArrayList<Class<? extends Context>> getContexts(){
        return contexts;
    }
    /**
     * 
     * @return la clase principal que inicializa el juego cargado mediante este
     * GameTypeRegister.
     */
    public Class<? extends IGame> getGameDescriptorClass(){
        return gdclass;
    }
    /**
     *  
     * @return el nombre de la clase que extiende al descriptor de juego.
     */
    public String getGameName(){
        return (gameName == null || gameName.isEmpty()) ? gdclass.getCanonicalName() : gameName;
    }
    /**
     * 
     * @return el paquete que contiene las reglas del juego.
     */
    public String getRulesPackage(){
        String rulesPackageName = null;
        if (!rules.isEmpty()){
            rulesPackageName = rules.get(0).getPackage().getName();
        }
        return rulesPackageName;
    }
    private DBManager dbmanager;
    private Logger log = LoggerFactory.getLogger(GameTypeRegister.class);

    private ArrayList<Class<? extends Context>> contexts = new ArrayList<>();
    private ArrayList<Class<?>> rules = new ArrayList<Class<?>>();
    private ArrayList<IRule<?>> irules = new ArrayList<IRule<?>>();
    private ArrayList<Class<?>> daos = new ArrayList<Class<?>>();

    

    private String gameName;
    private Class<? extends IGame> gdclass;

    /**
     * Establece el DataSource de la base de datos que compartirán los
     * GameTypeRegister.
     * 
     * @param lds el DataSource de la base de datos que compartirán los
     * GameTypeRegister
     */
    public static void setDataSource(DataSource lds){
        ds = lds;
    }
    


    private static final String SCHEMAEXISTS = "";
    private static DataSource ds;
    
}
