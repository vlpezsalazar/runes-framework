package es.calipso.framework.core.gameengine.rules;

import es.calipso.framework.core.context.Context;
import es.calipso.framework.core.context.GameResponseData;

public abstract class Effect<Y, T> {

    public abstract boolean check(Context ctx, Y source, T target, Object[] data);

    public abstract GameResponseData<?> apply(Context ctx, Y source, T target, Object[] data) throws Throwable;
    
    public abstract void rollback(Context ctx, Y source, T target, Object[] data);
}
