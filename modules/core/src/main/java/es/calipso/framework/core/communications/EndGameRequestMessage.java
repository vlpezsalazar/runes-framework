package es.calipso.framework.core.communications;

import es.calipso.framework.core.dao.User;

/**
 * Mensaje de finalización de juego. Mensaje que un cliente envía al servidor
 * cuando el usuario sale del juego o se cierra la aplicación.
 *   
 * @author Víctor López Salazar
 *
 */
public class EndGameRequestMessage extends GameRequestMessage {
	
    public EndGameRequestMessage(User us) {
        super();
        setUser(us);
        data.put(RULE, "es.calipso.framework.core.rules.EndGame");
    }
    public void setGameId(String gameid){
        data.put(GAMEID, gameid);
    }
}
