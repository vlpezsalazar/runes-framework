package es.calipso.framework.core.exceptions;

@SuppressWarnings("serial")
public class NotSerializableException extends Exception {
    public NotSerializableException(Throwable cause){
        super(cause);
    }
}
