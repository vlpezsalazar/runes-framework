package es.calipso.framework.core.gameengine;

import es.calipso.framework.core.communications.Messenger;

/**
 * Factoria de GameEngine que instancia un singleton de GE. Se emplea en las
 * distintas instancias de juego para acceder al GE.
 *  
 * @author Víctor López Salazar
 *
 */
public class GameEngineFactory {

    public static synchronized IGameEngine getInstance(Messenger msg) {
        if (GEnodeInstance == null){
            GEnodeInstance = new GameEngine(msg);
        }
        return GEnodeInstance;
    }
/*    public static synchronized IGameEngine getInstance(ThreadPoolExecutor tpe, Messenger msg){
        
        if (GEnodeInstance == null){
            GEnodeInstance = new GameEngine(tpe, msg);
        }        
        return GEnodeInstance;
    }*/
    private static GameEngine GEnodeInstance = null;
}
