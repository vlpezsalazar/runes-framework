package es.calipso.framework.core.context;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import es.calipso.framework.core.dao.UUIDGenerator;
import es.calipso.framework.core.gameengine.IGameEngine;
import es.calipso.framework.core.gameengine.rules.ChangeSet;
import es.calipso.framework.storage.exceptions.EntityNotFoundException;

/**
 * Clase abstracta que permite el acceso a un conjunto persistente de entidades
 * de juego y de información volatil compartida. La implementación del método
 * abstracto <i>getEntity</> debe proveer la funcionalidad necesaria para
 * realizar un acceso sincronizado a entidades que se estan modificando
 * permitiendo al mismo tiempo el acceso concurrente al resto.
 * 
 * Para dar soporte a un contexto puede emplearse una base de datos o un cto.
 * de clases, pero siempre debe tener en cuenta que el acceso a las mismas debe
 * estar sincronizado para distintas hebras. FIXME cosa que ahora mismo no se tiene en cuenta
 * 
 * @author Víctor López Salazar
 *
 */
public abstract class Context{

	public Context(){
		keyCode = UUIDGenerator.getKeyCode(this);
	}

    /**
     * Devuelve el identificador del contexto.
     * @return el identificador del contexto;
     */
    public String getKeyCode(){
    	return keyCode;
    }
    /**
     * Método que resuelve los conflictos en un conjunto de los cambios.
     * Estos cambios son creados por las reglas de un juego y lo que este
     * método recibe son los efectos de esos cambios pasados en el parámetro
     * 'changes'. Las reglas que crean estos efectos no tienen en cuenta
     * la ejecución secuencial de los mismos por lo que su salida es un 
     * conjunto de "instrucciones" que dan el resultado de ejecutar la regla.
     * Este método recibe todas esas instrucciones, elimina las que entran en 
     * conflicto (siguiendo algún tipo de política, como p.e. definir que unas
     * acciones son más prioritarias que otras) y las pone en orden.
     * 
     * Una vez hecho esto, ejecuta el programa resultante sobre las entidades
     * mantenidas en el contexto y crea una respuesta con el resultado final
     * para los clientes. 
     * 
     * @param changes conjunto de cambios a resolver.
     * @return la respuesta para los clientes que contiene el conjunto de 
     *          cambios que finalmente se han hecho.
     */
    public GameResponseSet update(SortedSet<ChangeSet> changes){
        GameResponseSet wur = new GameResponseSet();
        LinkedHashMap<ChangeSet, List<GameResponseData<?>>> clientData = new LinkedHashMap<>();

        for (ChangeSet change : changes){
            if (change.check(this)){
                clientData.put(change, change.apply(this));
            }
        }
        wur.add(clientData, this.getUsersInContext());
        return wur;
    }

    /**
     * 
     * @return
     */
    public abstract Set<String> getUsersInContext();

    /**
     * Obtiene una entidad asociada al contexto que ha sido anteriormente creada.
     * Este método se debe emplear para recuperar entidades que son referenciadas
     * en una petición del cliente (cuyo keycode no conocería si previamente no
     * hubieran sido cargadas).
     * 
     * @param <T> Tipo de la entidad a devolver.
     * @param entityid identificador de la entidad.
     * @return la entidad con ese identificador.
     * @throws EntityNotFoundException 
     */
    public abstract <T> T getEntity(String keycode) throws EntityNotFoundException;
    
    public void addSharedInfo(String source,
            HashMap<String, Object> sharedInfo){
        this.sharedInfo.put(source, sharedInfo);
        
    }
    @SuppressWarnings("unchecked")
    public <T> T getSharedInfo(String entityid, String property){
        T info = null;
        HashMap<String, Object> propertiesMap = sharedInfo.get(entityid);
        if (propertiesMap != null){
            info = (T) propertiesMap.get(property);
        }
        return info;
    }
    public void activateContext(){
        if (ge != null){
            ge.activateContextRules(this);            
        }
    }
    public static void setGE(IGameEngine gameEngine){
        ge = gameEngine;
    }
	private String keyCode;
    private static IGameEngine ge;
    private HashMap<String, HashMap<String, Object>> sharedInfo = new HashMap<String, HashMap<String,Object>>();
    
//    private static final Logger log = LoggerFactory.getLogger(Context.class);
}
