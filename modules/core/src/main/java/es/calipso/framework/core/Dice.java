package es.calipso.framework.core;

import java.util.Random;

/**
 * This class provides methods which implement a dice's roll of several kinds.
 * Each roll ranges from 1 to the maximum value called in the method.
 * 
 * @author Víctor López Salazar
 *
 */
public class Dice {

    /**
     * This method rolls a dice which ranges from 1 to max.
     * 
     * @param max the maximum value of the dice to roll.
     * @return the value of the roll.
     */
    public static int roll(int max){
        return dice.nextInt(max)+1;
    }

    /**
     * This method rolls a dice which ranges from 1 to 20.
     * 
     * @return the value of the roll.
     */
    public static int rolld20(){
        return dice.nextInt(20)+1;
    }

    /**
     * This method rolls a dice which ranges from 1 to 100.
     * 
     * @return the value of the roll.
     */
    public static int rolld100(){
        return dice.nextInt(100)+1;
    }

    /**
     * This method implements several rolls of a dice which ranges from 1 to
     * max. If the first roll is equal to or greater than eroll then another
     * roll is made until the roll's value falls below eroll. The final value
     * returned is the addition of all those rolls which is called a
     * <i>extended roll</i>.
     *  
     * @param max the maximum value of the dice.
     * @param eroll the minimum value for which the dice should be rolled again.
     * 
     * @return the dice extended roll's value.
     */
    public static int extendedRoll(int max, int eroll){
        int value = 0, roll;
        do {
            roll  = dice.nextInt(max)+1;
            value += roll;
        } while (roll >= eroll);

        return value;
    }
    private static Random dice = new Random(); 
}
