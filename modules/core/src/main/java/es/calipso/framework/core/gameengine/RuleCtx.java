package es.calipso.framework.core.gameengine;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.PoolableObjectFactory;

import es.calipso.framework.core.communications.GameRequestMessage;
import es.calipso.framework.core.context.Context;
import es.calipso.framework.core.context.ObjectPool;

class RuleCtx {
    private RuleCtx(){}
    void set(GameRequestMessage wur, Context ctx) {
        this.wur = wur;
        this.ctx = ctx;
    }
    GameRequestMessage wur;
    Context ctx;
    static final PoolableObjectFactory factory = new BasePoolableObjectFactory() {
        @Override
        public Object makeObject() throws Exception {
            return new RuleCtx();
        }
    };
    static {
        ObjectPool.newFactory(factory);
    }
}
