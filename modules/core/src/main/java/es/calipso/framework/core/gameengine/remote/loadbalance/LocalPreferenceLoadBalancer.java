package es.calipso.framework.core.gameengine.remote.loadbalance;
/**
 * Esta clase proporciona una utilidad que abstrae la distibución de los
 * peticiones entre los GameEngines asignados a un juego de manera que cuando
 * llega una petición sigue la siguiente política de equilibrado de carga:
 * <ul>
 * <li> Si hay un GameEngine en la máquina local y la carga que tiene es menor
 *      de un 70% encolar la petición en el GameEngine local.
 * <li> Sino buscar el GameEngine con la menor carga y si la diferencia entre
 *      este y la carga del local es menor que un 30% y la carga del local es menor
 *      de un 100% asignarlo al local.
 * <li> Sino asignarlo al GameEngine remoto con menor carga.
 * <ul>    
 * 
 * @author Víctor López Salazar 
 *
 */
public class LocalPreferenceLoadBalancer {
//TODO class LocalPreferenceLoadBalancer
}
