/**
 * Paquete que contiene clases para hacer persistentes los juegos. 
 */
@es.calipso.framework.storage.annotations.DBConfig(schema="calipso")
package es.calipso.framework.core.dao;