package es.calipso.framework.core.communications;

import java.util.HashMap;

import es.calipso.framework.core.dao.UUIDGenerator;
import es.calipso.framework.core.dao.User;
import es.calipso.framework.text.TextBuilder;

/**
 * Clase que encapsula un mensaje entre cliente y servidor. Cuando un cliente
 * envía una petición al servidor el mensaje se encapsula dentro de una 
 * instancia de esta clase. Un mensaje es un conjunto de parejas &lt;clave, 
 * valor&gt; transmitidas en formato JSON. El conjunto de claves que se
 * utilizan en los mensajes lo define cada juego, salvo por una serie de
 * claves que siempre aparecen en un mensaje y que son:
 * 
 * <ul>
 * <li>"rule": regla a la que se dirige el mensaje. Normalmente es el nombre
 *  totalmente cualificado de la clase que implementa la regla. Debe
 *  establecerlo el cliente para indicar el tipo de acción que desea realizar
 *  dentro del juego.
 *  <li>"userkeycode": código de usuario. Lo incluye de forma automática el
 *  cliente.
 *  <li>"gameid": identificador del juego del que proviene el mensaje. Lo 
 *  incluye de forma automática el cliente. 
 *  <li>"timeStamp": sello de tiempo en el que se emitió el mensaje. Lo 
 *  incluye de forma automática el cliente.
 * <ul>
 * 
 * @see Messenger
 * 
 * @author Víctor López Salazar.
 *
 */
@SuppressWarnings("unchecked")
public class GameRequestMessage {

    protected static final String RULE = "rule";
    protected static final String USERKEYCODE = "userkeycode";
    protected static final String GAMEID = "gameid";
    private static final String USERID = "userid";
    private static final String TIMESTAMP = "timeStamp";
    private static final String GAMEENTITY = "ge";
    private static final String CONTEXT = "context";

    /**
     * El constructor recibe un mensaje de petición de un cliente encapsulado 
     * en formato json.
     * 
     * @param pdata HashMap que contiene la petición en formato clave, valor. 
     * 
     */
    public GameRequestMessage(HashMap<String, Object> pdata) {
        data = pdata;
    }

    /**
     * Constructor por defecto. Crea un mensaje vacio que deberá rellenarse
     * posteriormente. Es útil para simular mensajes del cliente.
     */
    public GameRequestMessage() {
        data = new HashMap<String, Object>();
    }
    
    /**
     * Permite obtener el id del juego del que proviene este mensaje. 
     * @return el identificador del juego del que proviene el mensaje.
     */
    public String getGameId(){
        return (String) data.get(GAMEID);
    }
    /**
     * Obtiene el contexto al que está asociado el mensaje.
     * @return el keycode del contexto al que está asociado el mensaje.
     */
    public String getContext() {
        return (String)data.get(CONTEXT);
    }
    /**
     * Permite establecer el quantum en el que actua el mensaje. Un quantum
     * es un periodo de tiempo en el que un GameEngine procesa un lote de 
     * mensajes de petición que actuaran de manera concurrente. De esta manera
     * las peticiones que entran en el quantum n actuan antes que las que 
     * entran en el quantum n-1. Esto es establecido por el GameEngine.
     * @param tick quantum a establecer
     */
    public void setQuantum(long tick){
        quantum = tick;
    }
    /**
     * Obtiene el quantum en el que actua el mensaje.
     * @return el quantum en el que actua el mensaje.
     */
    public Long getQuantum() {
        return quantum;
    }
    /**
     * Establece el orden en el que se procesa el mensaje dentro del quantum en
     * el que actua en un GameEngine.
     * 
     * @param t orden de procesamiento del mensaje.
     */
    public void setTimeLapse(long t){
        timelapse = t;
    }
    /**
     * Obtiene el orden establecido para este mensaje dentro del quantum en el
     * que actua.
     * 
     * @return el orden del mensaje dentro del quantum.
     */
    public Long getTimeLapse() {
        return timelapse;
    }

    public long getTimeStamp() {
        Number a = (Number) data.get(TIMESTAMP);
        return (a == null) ? null : a.longValue();
    }
    /**
     * @return la regla del juego en el servidor a la que se dirige el mensaje.
     */
    public String getRule() {
        return (String)data.get(RULE);
    }
    public String getGameEntity() {
        return (String)data.get(GAMEENTITY);
    }
    /**
     * Este método devuelve el keycode del usuario que envía el mensaje. El 
     * KeyCode es creado en el servidor para cada usuario y se asigna de manera
     * automática a los mensajes.
     * 
     * @return el código del usuario que está enviando el mensaje.
     */
    public String getUserKeyCode() {
        return (String)data.get(USERKEYCODE);
    }
    /**
     * 
     * @return el id de la clave primaria del usuario dentro de la base de datos.
     */
    public Integer getUserId(){
        return (Integer)data.get(USERID);
    }
    /**
     * Establece el usuario del mensaje. Esto lo realiza automáticamente el
     * servidor cuando recibe la petición inicial
     * 
     * @param us usuario que envía el mensaje.
     */
    public void setUser(User us) {
        data.put(USERKEYCODE, UUIDGenerator.getKeyCode(us));
        data.put(USERID, us.getId());
    }
    /**
     * Permite obtener un valor del mensaje a partir de la clave asociada y 
     * del mismo tipo que la variable a la que se asigna:<p>
     * <code>
     * //obtiene un entero indicando los puntos de vida<br> 
     * int puntosDeVida = gameRequestMessage.getFeature("puntosDeVida");<br><br>
     * //obtiene una cadena indicando el nombre del personaje<br>
     * String nombreDePersonaje = gameRequestMessage.getFeature("nombreDePersonaje");
     * <code><p>
     * @param featid clave asociada al valor que se quiere obtener
     * @return el valor asociado a featid de tipo T. Este tipo T del valor debe
     * ser del mismo tipo de la variable a la que se asigna o se producira una 
     * excepción ClassCastException.
     *  
     */
    public <T> T getFeature(String featid)/* throws FeatureNotFoundException*/{
//        if (!data.containsKey(featid)) throw new FeatureNotFoundException(getClass().getCanonicalName(), featid);
        return (T) data.get(featid);
    }
    /**
     * Util para depuración.
     * 
     * @return representación en formato texto del mensaje. 
     * 
     */
    @Override
    public String toString() {
        return m.text("({0}): {1}", data.get(RULE), data);
    }
    
    private long quantum;
    private long timelapse;

    protected HashMap<String, Object> data;
    
    private TextBuilder m = new TextBuilder();
    
    static {
        Messenger.addFactory(new IMessageFactory<GameRequestMessage>() {
            @Override
            public GameRequestMessage newMessage(HashMap<String, Object> data) {
                return new GameRequestMessage(data);
            }

            @Override
            public String getType() {
                return "es.calipso.framework.core.communications.GameRequestMessage";
            }
        });
    }
}
