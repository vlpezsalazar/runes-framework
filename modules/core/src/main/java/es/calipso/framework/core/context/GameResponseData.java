package es.calipso.framework.core.context;

import java.util.Set;

import org.codehaus.jackson.map.JsonSerializableWithType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.calipso.framework.text.TextBuilder;

public abstract class GameResponseData<T> implements Comparable<GameResponseData<T>>, JsonSerializableWithType{

    public String getId(){
        return mixtureId;
    }
    public T getData(){
        return mixtureData;
    }
    public abstract PoolableGameResponseDataFactory<? extends GameResponseData<T>> getFactory();
    public void setMixture(String id, T data){
        mixtureId = id;
        mixtureData = data;
    }
    @Override
    public int compareTo(GameResponseData<T> o) {
        int compare = o.getClass().toString().compareTo(this.getClass().toString());
        if (compare == 0){
            compare = mixtureId.compareTo(o.getId());
            if (compare == 0){
                o.mix(this);
            }
        }
//        log.debug("comparando this = {} con o = {} : {}", new Object[]{this, o, compare});
        return compare;
    }

    public void setInPool(boolean in){
        inPool = in;
    }
    public boolean isInPool(){
        return inPool;
    }

    @Override
    public boolean equals(Object obj) {
        boolean eq = false;
        if (obj.getClass().equals(this.getClass())){
             eq = mixtureId.equals(((GameResponseData<?>)obj).getId());
        }
//        log.debug("this:{} == obj{} : {}", new Object[]{this, obj, eq});
        return eq;
    }
    protected abstract void mix(GameResponseData<T> anotherMixture);

    //quien debe enterarse de este mensaje
    public Set<String> getTargets(){
//        log.debug("En GameResponseData.getTargets(): {}", targets);
        return targets;
    }

    public void setTargets(Set<String> targets){
        this.targets = targets;
    }
    public void addTarget(String targetid){
        if (targets != null){
            targets.add(targetid);
        }
    }

    @Override
    public String toString() {
        return tb.text("{0}:{1}", mixtureId, mixtureData);
    }
    protected Set<String> targets;

    private String mixtureId;
    private T mixtureData;
    protected TextBuilder tb = TextBuilder.getSingletonInstance(); 
    private boolean inPool = false;
    
    @SuppressWarnings("unused")
    private static final Logger log = LoggerFactory.getLogger(GameResponseData.class); 
}
