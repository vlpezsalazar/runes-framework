/**
 * Contiene clases utiles para la comunicación entre el servidor
 * y los clientes.
 */
package es.calipso.framework.core.communications;
