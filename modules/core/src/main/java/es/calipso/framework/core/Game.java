package es.calipso.framework.core;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Definición de un juego. Esta anotación se utiliza para marcar una clase como
 * la clase que contiene las reglas de inicialización y finalización de un
 * juego.<p>
 * 
 * <code>
 *  &#64;Game<br>
 *  public class MyGame { ... }<b>
 * </code>
 * </p>
 * 
 *   
 * @author Víctor López Salazar.
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Game {

    String value() default "";

}
