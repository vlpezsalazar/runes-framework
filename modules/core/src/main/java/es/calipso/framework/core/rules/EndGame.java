package es.calipso.framework.core.rules;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.calipso.framework.core.IGame;
import es.calipso.framework.core.communications.GameRequestMessage;
import es.calipso.framework.core.context.Context;
import es.calipso.framework.core.dao.User;
import es.calipso.framework.core.gameengine.rules.ChangeSet;
import es.calipso.framework.core.gameengine.rules.IRule;
import es.calipso.framework.core.gameengine.rules.Rule;
import es.calipso.framework.core.management.GameManager;
import es.calipso.framework.storage.DBManager;
import es.calipso.framework.storage.exceptions.ClassNotRegisteredException;
import es.calipso.framework.storage.exceptions.EntityManagerNotSetException;

@Rule(active = false)
public class EndGame implements IRule<GameRequestMessage> {


    public EndGame() throws EntityManagerNotSetException, ClassNotRegisteredException {
        dbManager = DBManager.getManager(User.class);
    }
    @Override
    public boolean prepare(GameRequestMessage request, Context ctx,
            ChangeSet effects) {
        boolean done = false;
        String gameid = request.getGameId();
        if (gameid != null){
            IGame game = gameManager.getGame(gameid);
            String userKeyCode = request.getUserKeyCode();

            try {
                User user = dbManager.getEntity(userKeyCode);
                done = game.finish(user, ctx, effects);
            } catch (Throwable e) {
                log.error("", e);
            }
        }
        return done;
    }
    private DBManager dbManager;
    private GameManager gameManager = GameManager.getInstance();
    private Logger log = LoggerFactory.getLogger(EndGame.class);
}
