package es.calipso.framework.core.context;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.pool.BaseKeyedPoolableObjectFactory;
import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.GenericKeyedObjectPool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ObjectPool extends BaseKeyedPoolableObjectFactory{
    public final static void newFactory(PoolableObjectFactory factory){
        keyedObjectFactories.addFactory(factory, factory);
        pool.preparePool(factory, false);
    }
    public final static void newFactory(Object id, PoolableObjectFactory factory){
        keyedObjectFactories.addFactory(id, factory);
        pool.preparePool(id, false);
    }
    public final static boolean hasFactory(Object id){
        return keyedObjectFactories.objectFactories.containsKey(id);
    }
    private final void addFactory(Object id, PoolableObjectFactory factory) {
        objectFactories.put(id, factory);
    }

    @SuppressWarnings("unchecked")
    public synchronized final static <T> T get(Object factoryId){
        T o = null;
        try {
            o = (T) pool.borrowObject(factoryId);
//            log.debug("obteniendo del pool el objeto {}", o);
        } catch (Throwable e) {
            log.error("", e);
        }
        return o;
    }
    public synchronized final static void releaseCollection(Object id, Collection<?> objects) {
        try {
            for (Object o : objects){
//                log.debug("devolviendo al pool el objeto {}", o);
                pool.returnObject(id, o);
            }
        } catch (Exception e) {
            log.error("", e);
        }
    }
    public synchronized final static void release(Object key, Object object) {
        try {
//            log.debug("devolviendo al pool el objeto {}", object);
            pool.returnObject(key, object);
        } catch (Exception e) {
            log.error("", e);
        }
    }

    @Override
    public Object makeObject(Object arg0) throws Exception {
        PoolableObjectFactory factory = objectFactories.get(arg0);
        return factory.makeObject();
    }
    @Override
    public void passivateObject(Object key, Object obj) throws Exception {
        PoolableObjectFactory factory = objectFactories.get(key);
        factory.passivateObject(obj);
    }
    private final Map<Object, PoolableObjectFactory> objectFactories = new ConcurrentHashMap<Object, PoolableObjectFactory>(); 
    
    private static final ObjectPool keyedObjectFactories = new ObjectPool(); 
    private static final GenericKeyedObjectPool pool = new GenericKeyedObjectPool(keyedObjectFactories, -1, GenericKeyedObjectPool.WHEN_EXHAUSTED_GROW, 0, 100);

    private static Logger log = LoggerFactory.getLogger(ObjectPool.class);
}
