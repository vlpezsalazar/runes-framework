package es.calipso.framework.core.context;

import org.apache.commons.pool.BasePoolableObjectFactory;

public abstract class PoolableGameResponseDataFactory<Y extends GameResponseData<?>> extends
        BasePoolableObjectFactory {
    public PoolableGameResponseDataFactory() {
        ObjectPool.newFactory(this);
    }

}
