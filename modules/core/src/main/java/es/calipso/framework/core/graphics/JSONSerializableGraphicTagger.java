package es.calipso.framework.core.graphics;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializableWithType;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.TypeSerializer;

public abstract class JSONSerializableGraphicTagger implements JsonSerializableWithType{
    public JSONSerializableGraphicTagger(String tag){
        modelFieldName = tag;
    }

    @Override
    public void serializeWithType(JsonGenerator arg0, SerializerProvider arg1,
            TypeSerializer arg2) throws IOException, JsonProcessingException {
        serialize(arg0, arg1);
    }

    @Override
    public void serialize(JsonGenerator jsongen, SerializerProvider arg1)
            throws IOException, JsonProcessingException {
        jsongen.writeStringField(modelFieldName, doTag());
        
    }

    public abstract String doTag();
    
    private String modelFieldName;
}
