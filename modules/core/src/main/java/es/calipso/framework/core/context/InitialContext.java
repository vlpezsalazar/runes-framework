package es.calipso.framework.core.context;

import java.util.Set;

import es.calipso.framework.storage.exceptions.EntityNotFoundException;

public class InitialContext extends Context {

    @Override
    public Set<String> getUsersInContext() {
        return null;
    }

    @Override
    public <T> T getEntity(String keycode) throws EntityNotFoundException {
        return null;
    }

}
