package es.calipso.framework.core.rules;

import java.util.ArrayList;

import org.codehaus.jackson.map.JsonSerializableWithType;
import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

import es.calipso.framework.core.IGame;
import es.calipso.framework.core.communications.ClientEntityMessage;
import es.calipso.framework.core.communications.GameRequestMessage;
import es.calipso.framework.core.context.Context;
import es.calipso.framework.core.context.ObjectPool;
import es.calipso.framework.core.dao.User;
import es.calipso.framework.core.gameengine.rules.ChangeSet;
import es.calipso.framework.core.gameengine.rules.IRule;
import es.calipso.framework.core.gameengine.rules.Rule;
import es.calipso.framework.core.management.GameManager;
import es.calipso.framework.storage.DBManager;
import es.calipso.framework.storage.exceptions.ClassNotRegisteredException;
import es.calipso.framework.storage.exceptions.EntityManagerNotSetException;

@Rule(active=false)
public class InitGame implements IRule<GameRequestMessage>{

    private DBManager dbManager;
    public InitGame() throws EntityManagerNotSetException, ClassNotRegisteredException {
        dbManager = DBManager.getManager(User.class);
    }
    @Override
    public boolean prepare(GameRequestMessage request, Context ctx,
            ChangeSet effects) {
        boolean done = true;
        String gameid = request.getGameId();
        ArrayList<JsonSerializableWithType> initData = new ArrayList<JsonSerializableWithType>();
        IGame game = gameManager.getGame(gameid);
        String userKeyCode = request.getUserKeyCode();
        try {
            if (game.isPersonalizedPerUser()){
                User user = dbManager.getEntity(userKeyCode);
                game.setInitGameData(user);
                initData.add(user);
            }
            initData.add(game);
            ClientEntityMessage clientInitMessage = ObjectPool.get(ClientEntityMessage.factory);
            clientInitMessage.set("Client", "initializeGameEntities", initData);
            effects.writeMessageForClient(userKeyCode, clientInitMessage);
        } catch (Throwable e) {
            log.error("", e);
            done = false;
        }
        return done;
    }
    private GameManager gameManager = GameManager.getInstance();
    private Logger log = LoggerFactory.getLogger(InitGame.class);
}
