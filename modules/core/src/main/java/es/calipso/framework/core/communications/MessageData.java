package es.calipso.framework.core.communications;

import java.io.IOException;
import java.util.HashSet;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.TypeSerializer;

import es.calipso.framework.core.context.GameResponseData;
import es.calipso.framework.core.context.PoolableGameResponseDataFactory;

/**
 * Extensión de tipo String de un GameResponseData. Los valores del mensaje de
 * respuesta en instancias de esta clase son cadenas de texto.
 * 
 * @see GameResponseData
 * 
 * @author Víctor López Salazar
 *
 */
public class MessageData extends GameResponseData<String> {

    public MessageData() {
        targets = new HashSet<String>(); 
    }
    @Override
    public void serializeWithType(JsonGenerator jgen,
            SerializerProvider provider, TypeSerializer typeSer)
            throws IOException, JsonProcessingException {
        serialize(jgen, provider);
    }

    @Override
    public void serialize(JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {
        jgen.writeFieldName(getId());
        jgen.writeRawValue(getData());
    }
    /**
     * Establece el cliente al que se deben enviar estos datos del mensaje.
     * 
     * @param target keycode del usuario (cliente) al que se debe enviar el
     * mensaje.
     */
    public void setTarget(String target){
        targets.clear();
        targets.add(target);
    }
    
    @Deprecated
    @Override
    protected void mix(GameResponseData<String> anotherMixture) {
        // nothing to do
    }
    @Override
    public PoolableGameResponseDataFactory<? extends GameResponseData<String>> getFactory() {
        return factory;
    }
    public static PoolableGameResponseDataFactory<MessageData> factory = new PoolableGameResponseDataFactory<MessageData>() {
        @Override
        public Object makeObject() throws Exception {
            return new MessageData();
        }
    };
}