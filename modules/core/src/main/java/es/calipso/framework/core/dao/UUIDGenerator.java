package es.calipso.framework.core.dao;

import java.util.UUID;

import es.calipso.framework.storage.daotype.KeyCode;

public class UUIDGenerator {

    public static String getKeyCode(Object object){
        String keycode = null;
        if (object instanceof KeyCode){
            keycode = ((KeyCode)object).get();
        } else {
            keycode = UUID.nameUUIDFromBytes(object.toString().getBytes()).toString();
        }
        return keycode;
    }

    public static Reference getReference(Object object) {
        String keycode = null;
        if (object instanceof KeyCode){
            keycode = ((KeyCode)object).get();
        } else {
            keycode = UUID.nameUUIDFromBytes(object.toString().getBytes()).toString();
        }
        return new Reference(keycode);
    }
}
