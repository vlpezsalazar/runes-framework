package es.calipso.framework.core.graphics;

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class GLGEGraphicTagger extends JSONSerializableGraphicTagger {

    public GLGEGraphicTagger(String modelFieldName) {
        super(modelFieldName);
        try {
            documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            document = documentBuilder.newDocument();
            root = document.createElement("glge");
            document.adoptNode(root);
            document.appendChild(root);
            source = new DOMSource(document);
            transformer = TransformerFactory.newInstance().newTransformer();
        } catch (ParserConfigurationException e){
            log.error("", e);
        } catch (TransformerConfigurationException e) {
            log.error("", e);
        } catch (TransformerFactoryConfigurationError e) {
            log.error("", e);
        }
    }
    public String doTag(){
        StreamResult result = new StreamResult(new StringWriter());
        try {
            transformer.transform(source, result);
        } catch (TransformerException e) {
            log.error("", e);
        }
        String res = result.getWriter().toString();
        return res;
    }
    public Element newChildTag(String tagName){
        Element element = document.createElement(tagName);
        root.appendChild(element);
        return element;
    }
    private Transformer transformer;
    private DocumentBuilder documentBuilder;
    private Document document;
    private Element root;
    private DOMSource source;
    private Logger log = LoggerFactory.getLogger(GLGEGraphicTagger.class);
}
