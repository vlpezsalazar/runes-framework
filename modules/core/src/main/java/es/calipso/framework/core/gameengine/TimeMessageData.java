package es.calipso.framework.core.gameengine;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.TypeSerializer;

import es.calipso.framework.core.context.GameResponseData;
import es.calipso.framework.core.context.PoolableGameResponseDataFactory;

public class TimeMessageData extends GameResponseData<Long>{

    public TimeMessageData() {
        super.setInPool(true);
    }

    @Override
    public void serializeWithType(JsonGenerator jgen,
            SerializerProvider provider, TypeSerializer typeSer)
            throws IOException, JsonProcessingException {
        serialize(jgen, provider);
    }

    @Override
    public void serialize(JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {
        jgen.writeNumberField("time", getData());
    }

    @Override
    public void setInPool(boolean in) {}

    @Override
    public PoolableGameResponseDataFactory<? extends GameResponseData<Long>> getFactory() {
        return null;
    }

    @Deprecated
    @Override
    protected void mix(GameResponseData<Long> anotherMixture) {}

}
