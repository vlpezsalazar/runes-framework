package es.calipso.framework.core.gameengine.rules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.PoolableObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.calipso.framework.core.communications.ClientEntityMessage;
import es.calipso.framework.core.context.Context;
import es.calipso.framework.core.context.GameResponseData;
import es.calipso.framework.core.context.ObjectPool;
import es.calipso.framework.core.dao.UUIDGenerator;
import es.calipso.framework.text.TextBuilder;

/**
 * Un conjunto de cambios (ChangeSet) es el resultado de la ejecución de una
 * regla de juego. Todos los efectos incluidos en un ChangeSet se ejecutan
 * de forma atómica.
 * 
 * Esta clase es bastante importante para permitir la distribución de los
 * contextos (Context) y la comunicación entre las reglas de juego que corren
 * localmente. Desliga la función de escritura de los cambios en el contexto de
 * los mismos, permitiendo el procesamiento distribuido de las peticiones
 * de actualización de un juego (WorldUpdateRequest). Un contexto distribuido
 * se mantiene coherente gracias a que es esta clase la que almacena la
 * información a compartir y que es la que se obtiene como respuesta de la 
 * distribución de actualización de juego.
 * 
 * Los métodos <i>addSharedInfo</i> y <i>writeSharedInfo</i> permiten establecer
 * y escribir la información compartida entre las reglas del juego en un
 * contexto sin que dicha información afecte a las entidades de juego.
 * Posteriormente el método update de context empleará los métodos <i>check</i>
 * y <i>apply</i> para comprobar los cambios que la regla indicó y
 * escribir aquellos que apliquen.
 * 
 * @author Víctor López Salazar
 *
 */
public class ChangeSet implements Comparable<ChangeSet>{
    
    public static final long UNDEFINED = -1;

    ChangeSet() {}

    public void setChangeInfo(String sourceId, String contextId, long gamequantum, long gametime, long msgTimeStamp){
        source = sourceId;
        context = contextId;
        quantum = gamequantum;
        time = gametime;
        timeStamp = msgTimeStamp;
    }

    public void addSharedInfo(String tag, Object info){
        ruleSharedInfo.put(tag, info);
    }
    public void writeSharedInfo(Context ctx){
        ctx.addSharedInfo(source, ruleSharedInfo);
    }
    public void setCondition(ICondition c){
        condition = c;
    }
    public boolean check(Context ctx){
        boolean apply = true;
        if (condition != null){
            apply = condition.check(source, ctx); 
        }

        return apply;
    }
    public List<GameResponseData<?>> apply(Context ctx) {
        result.clear();
        for (ArrayList<ClientEntityMessage> messages : messagesForTargets.values()){
            result.addAll(messages);
        }
        for (Entry<String, ArrayList<EffectWrapper<?, ?>>> targetEffects : effectsOnTargets.entrySet()){
            for (EffectWrapper<?, ?> effect : targetEffects.getValue()){
                if (effect.check(ctx)){
                    GameResponseData<?> grd = effect.apply(ctx);
                    if (grd != null){
                        result.add(grd);
                    }
                }
            }
        }
        return result;
    }
    public void setSource(String sourceid){
        source = sourceid;
    }
    public String getSource(){
        return source;
    }

    public void writeMessageForClient(String targetid, ClientEntityMessage msgData) {
        ArrayList<ClientEntityMessage> messages = messagesForTargets.get(targetid);
        
        if (msgData.getTargets() == null){
            msgData.setTargets(new HashSet<String>());
        }
        msgData.addTarget(targetid);

        if (messages == null){
            messages = new ArrayList<ClientEntityMessage>();
            messagesForTargets.put(targetid, messages);
        }
        messages.add(msgData);
    }
/*    public <T> void writeMessageForClient(String targetid, String clientRule, T msgData){
        MessageData messageData = ObjectPool.get(MessageData.factory);
        try {
            messageData.setMixture(clientRule, jsonMapper.writeValueAsString(msgData));
            messageData.setTarget(targetid);
            ArrayList<MessageData> messages = messagesForTargets.get(targetid);
            if (messages == null){
                messages = new ArrayList<MessageData>();
                messagesForTargets.put(targetid, messages);
            }
            messages.add(messageData);
        } catch (JsonGenerationException e) {
            log.warn("", e);
        } catch (JsonMappingException e) {
            log.warn("", e);
        } catch (IOException e) {
            log.warn("", e);
        }
    }*/
    public <Y, T> void addEffectOnTarget(Y source, Effect<Y, T> effect, T target, Object... effectdata){
        String targetKeyCode = UUIDGenerator.getKeyCode(target);
        ArrayList<EffectWrapper<?, ?>> effectsAndData = effectsOnTargets.get(targetKeyCode);
        if (effectsAndData == null){
            effectsAndData = new ArrayList<EffectWrapper<?, ?>>();
            effectsOnTargets.put(targetKeyCode, effectsAndData);
        }
        EffectWrapper<Y, T> ew = ObjectPool.get(EffectWrapper.getFactory(effect));
        ew.set(source, target, effect, effectdata); 
        effectsAndData.add(ew);
    }
/*    public void addEffectsOnTarget(Integer targetid, List<Effect<?>> effectList){
        ArrayList<EffectWrapper<?>> effectsAndData = effectsOnTargets.get(targetid);
        if (effectsAndData == null){
            effectsAndData = new ArrayList<EffectWrapper<?>>();
            effectsOnTargets.put(targetid, effectsAndData);
        }
        for (Effect<?> effect : effectList){
//            effectsAndData.add(effectWrapperPool.get(effect));
            EffectWrapper<?> ew  = ObjectPool.get(EffectWrapper.getFactory(effect));
            effectsAndData.add(ew);
        }
    }*/

    public void clean() {
        messagesForTargets.clear();
        effectsOnTargets.clear();
        ruleSharedInfo.clear();
        condition = null;
    }
    @Override
    public String toString() {
        String format  = (time == UNDEFINED) ? "ChangeSet'{'source:{0}, context:{1}, tick: ({2}), time: undefined'}'" : "ChangeSet'{'source:{0}, context:{1}, tick:({2}), time:{3}ms'}'"; 
        return m.text(format, source, context, quantum == UNDEFINED ? "undefined" : quantum, time);
    }
    @Override
    public int compareTo(ChangeSet o) {
        long sub = context.compareTo(o.context);
        if (sub == 0 && quantum != UNDEFINED && o.quantum != UNDEFINED){
        	sub = quantum - o.quantum;
        	if (sub == 0 && time != UNDEFINED && o.time != UNDEFINED){
        		sub = time - o.time;
        	}
        }

        int res = 0;
        if (sub > 0){
            res = 1;
        } else if (sub < 0){
            res = -1;
        }
        return res;
    }

    public long getClientTimeStamp(){
        return timeStamp;
    }
    public long getServerTimeStamp() {
        return time;
    }

    private String context;
    private long quantum;
    private long time;
    private long timeStamp;

    private String source;

    private HashMap<String, ArrayList<EffectWrapper<?, ?>>> effectsOnTargets = new HashMap<String, ArrayList<EffectWrapper<?, ?>>>();
    
    /*
     * Las diferencias entre estos y los efectos de arriba son 2:
     * 
     * 1. Estos mensajes no tienen posibilidad de mezcla, por lo tanto son constantes y siempre aplican
     * (no necesitan comprobación sino que se envían directamente).
     * 
     * 2. Estos mensajes se crean mediante el método writeMessageForTarget ejecutado en una regla
     * y por tanto potencialmente en paralelo.
     *
     */
    private HashMap<String, ArrayList<ClientEntityMessage>> messagesForTargets = new HashMap<String, ArrayList<ClientEntityMessage>>();

//    private static final ObjectMapper jsonMapper = new ObjectMapper(); 
//    private static EffectWrapperPool effectWrapperPool = new EffectWrapperPool();

    private HashMap<String, Object> ruleSharedInfo = new HashMap<String, Object>();
    private ICondition condition;

    private ArrayList<GameResponseData<?>> result = new ArrayList<GameResponseData<?>>();
    
    private static class EffectWrapper<Y, T> {
        public void set(Y s, T t, Effect<Y, T> ef, Object[] d){
            source = s;
            target = t;
            effect = ef;
            data = d;
        }
        boolean check(Context ctx){
            boolean apply = false;
            try {
//                log.debug("checking {}", this);
                apply = effect.check(ctx, source, target, data);
            } catch (Throwable t){
                log.warn("", t);
                apply = false;
            } finally {
                if (!apply){
                    ObjectPool.release(getFactory(effect), this);
                }
            }
            return apply; 
        } 
        GameResponseData<?> apply(Context ctx){
            GameResponseData<?> results = null;
            try {
//                log.debug("applying {}", this);
                results = effect.apply(ctx, source, target, data);
            } catch (Throwable t){
                log.warn("", t);
            } finally {
                ObjectPool.release(getFactory(effect), this);
            }
            return results;
        }
        private Object[] data;
        private Y source;
        private T target;
        private Effect<Y, T> effect;
        private static  <Y, T, J extends Effect<Y, T>> Object getFactory(J e){
            Object factoryId = e.getClass();
            if (!ObjectPool.hasFactory(factoryId)){
                PoolableObjectFactory factory = new BasePoolableObjectFactory() {
                    @Override
                    public Object makeObject() throws Exception {
                        return new EffectWrapper<Y, T>();
                    }

                    @SuppressWarnings("unchecked")
                    @Override
                    public void passivateObject(Object obj) throws Exception {
                        ((EffectWrapper<Y, T>)obj).data = null;
                        ((EffectWrapper<Y, T>)obj).source = null;
                        ((EffectWrapper<Y, T>)obj).target = null;
                    }

                };
                ObjectPool.newFactory(factoryId, factory);
            }
            return factoryId;
        }
//        private EffectWrapperPool pool;
    }
    private TextBuilder m = new TextBuilder();
    private static final Logger log = LoggerFactory.getLogger(ChangeSet.class);
    public static final PoolableObjectFactory factory = new BasePoolableObjectFactory() {
        @Override
        public Object makeObject() throws Exception {
            return new ChangeSet();
        }
        @Override
        public void passivateObject(Object obj) throws Exception {
            ((ChangeSet)obj).clean();
        };
    };
    static {
        ObjectPool.newFactory(factory);
    }
}
