package es.calipso.framework.core.gameengine;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.calipso.framework.core.communications.GameRequestMessage;
import es.calipso.framework.core.communications.Messenger;
import es.calipso.framework.core.context.Context;
import es.calipso.framework.core.context.GameResponseSet;
import es.calipso.framework.core.context.ObjectPool;
import es.calipso.framework.core.gameengine.rules.ChangeSet;
import es.calipso.framework.core.gameengine.rules.IRule;
import es.calipso.framework.core.gameengine.rules.Rule;
import es.calipso.framework.core.rules.EndGame;
import es.calipso.framework.core.rules.InitGame;
import es.calipso.framework.text.TextBuilder;

public class GameEngine implements IGameEngine{

    GameEngine(Messenger datamsg) {
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(5);
        pool.scheduleAtFixedRate(updateContexts, 0, QUANTUM, TimeUnit.MILLISECONDS);
        threadPool = pool;
        messenger = datamsg;
        try {
            actions.put(InitGame.class.getCanonicalName(), new RuleWrapper(new InitGame()));
            actions.put(EndGame.class.getCanonicalName(), new RuleWrapper(new EndGame()));
        } catch (Throwable  e) {
            log.error("", e);
        }
    }
    @Override
    public void loadGameRules(String gameKeyCode, List<IRule<? extends GameRequestMessage>> rules, Type t)  throws Throwable{
        for (IRule<? extends GameRequestMessage> action : rules){
            String actionName = action.getClass().getCanonicalName();
            if (!actions.containsKey(actionName)){
                actions.put(actionName, new RuleWrapper(action));
            }
        }
        type.put(gameKeyCode, t);
    }

    @Override
    public void addServerRules(ArrayList<Class<? extends Context>> ctxClassList) {
        ArrayList<Method> rmethods = new ArrayList<>();
        for (Class<? extends Context> ctxClass : ctxClassList){
            String ctxClassName = ctxClass.getCanonicalName();
            if (!serverRules.containsKey(ctxClassName)){
                Method[] methods = ctxClass.getMethods();
                for (Method m : methods){
                    Rule rule = m.getAnnotation(Rule.class);
                    if ( rule != null && rule.active() ){
                        rmethods.add(m);
                    }
                }
                if (!rmethods.isEmpty()){
                    serverRules.put(ctxClassName, rmethods);
                    rmethods = new ArrayList<>();
                }

            }
            
        }
    }
    @Override
    public void activateContextRules(Context ctx){
        activeContexts.add(ctx);
        contexts.add(ctx);
    }
    public int getNumberOfPendingRequests(GameRequestMessage wur){
        RuleWrapper action = actions.get(wur.getRule());
        return action.pendingRequests();
    }
    public Type getGEType(String gameId){
        return type.get(gameId);
    }
    public Long getTick() {
        return tick;
    }
    @Override
    public boolean dispatchAction(GameRequestMessage wur, Context ctx) {
        if (ctx == null) return false;
//        log.debug("Despachando acción para la regla {}, quantum actual: {}", new Object[]{wur, tick});
        wur.setQuantum(tick);
        wur.setTimeLapse(System.currentTimeMillis());
        contexts.add(ctx);
        RuleWrapper aw = actions.get(wur.getRule());
        if (aw == null){
            log.warn("No hay ninguna regla en este GameEngine con el nombre {}", wur.getRule());
            return false;
        }
//        log.info("Peticiones pendientes para {}:{}", wur.getRule(), aw.pendingRequests());
        aw.pushResquest(wur, ctx);
        threadPool.submit(aw);
        return true;
    }

    /**
     * Tipo de GameEngine. Se utiliza para decidir si el GE debe llevar el
     * reloj del juego o no. Esto se puede emplear posteriormente al implementar
     * políticas de equilibrado de carga entre los distintos GEs.
     *   
     * @author Víctor López Salazar
     *
     */
    public enum Type {
        /**
         * Este tipo de GameEngine maestro lleva el tiempo para sincronizar
         * los clientes de un juego.
         */ 
        MASTER,
        /**
         * Un GE esclavo no lleva el tiempo del juego.
         */
        SLAVE
    }
    private Map<String, Type> type = new ConcurrentHashMap<String, GameEngine.Type>();
    private final ChangeSet previousQuantumGroup = ObjectPool.get(ChangeSet.factory);
    private final ChangeSet quantumGroup = ObjectPool.get(ChangeSet.factory);

    // Indica el quantum en el que estamos actualmente.
    private long tick = 0; 
    /*
     * Dependiendo de la longitud del quantum, los GEs procesaran un mayor número
     * de peticiones (si el quantum es grande) o crearan respuestas de actualización
     * de clientes con más frecuencia (si el quantum es pequeño).
     */
    private static final long QUANTUM = 400; 
    /*
     * Esta tarea es la que ejecutará de forma cíclica. Es la encargada de actualizar los contextos
     * con los cambios que se produzcan. 
     */
    private Runnable updateContexts = new Runnable() {
        
        @Override
        public void run() {
            GameResponseSet response;
            SortedSet<ChangeSet> changes;
            String contextId;
            //ejecutamos las reglas de servidor.
            if (!activeContexts.isEmpty()){
                List<Method> rules;
                for (Context ctx : activeContexts){
                    rules = serverRules.get(ctx.getClass().getCanonicalName());
                    if (rules != null){
                        for (Method rule : rules){
                            try {
                                ChangeSet effects = ObjectPool.get(ChangeSet.factory);
                                effects.setChangeInfo(null, ctx.getKeyCode(), tick, System.currentTimeMillis(), ChangeSet.UNDEFINED);
                                if ((boolean) rule.invoke(ctx, effects)){
                                    changeSets.add(effects);
                                }
                            } catch (Throwable e) {
                                log.error("", e);
                            }
                        }
                    }
                }
            }
            if (!changeSets.isEmpty()){
//                log.debug("changeSets: {}", changeSets);
                for (Context ctx : contexts){
                    try{
                        contextId = ctx.getKeyCode();
//                      if (type.get(gameId) == Type.MASTER){ 
                        previousQuantumGroup.setChangeInfo(null, contextId, tick-1, ChangeSet.UNDEFINED, ChangeSet.UNDEFINED);
                        quantumGroup.setChangeInfo(null, contextId, tick+1, ChangeSet.UNDEFINED, ChangeSet.UNDEFINED);
  /*
                      } else {//de momento sólo tenemos GEs maestros.
                          quantumGroup.setChangeInfo(null, contextId+1, ChangeSet.UNDEFINED, ChangeSet.UNDEFINED, ChangeSet.UNDEFINED);
                          previousQuantumGroup.setChangeInfo(null, contextId, ChangeSet.UNDEFINED, ChangeSet.UNDEFINED, ChangeSet.UNDEFINED);
                      }
  */
                        changes = changeSets.subSet(previousQuantumGroup, quantumGroup);
                        if (!changes.isEmpty()){
//                            log.debug("aplicando cambios {} al contexto {} ", changes, ctx);
                            response = ctx.update(changes);
                            //                          log.debug("liberando {}", changes);
                            ObjectPool.releaseCollection(ChangeSet.factory, changes);
                            changeSets.removeAll(changes);
                            messenger.sendToClients(response);
                        }
                    } catch (Throwable t){
                        log.debug("", t);
                    }
                }
            }
            tick++;
        }
    };

    private Messenger messenger;

    private Map<String, RuleWrapper> actions = new ConcurrentHashMap<String, RuleWrapper>();
    
    private Map<String, List<Method>> serverRules = new ConcurrentHashMap<>();

    //contextos activos (contienen reglas activas de servidor)
    private List<Context> activeContexts = new ArrayList<>();
    
    private Set<Context> contexts = new CopyOnWriteArraySet<Context>();

    private SortedSet<ChangeSet> changeSets = new ConcurrentSkipListSet<ChangeSet>();

    //un pool para reutilizar pares de <peticion, contexto> minimizando su creación.
//    private Queue<RuleWrapper.RuleCtx> actionCtxPool = new LinkedBlockingQueue<RuleWrapper.RuleCtx>();

    private ExecutorService threadPool;

    private Logger log = LoggerFactory.getLogger(GameEngine.class);
    private TextBuilder m = new TextBuilder();

    /**
     * Clase interna que permite ejecutar las acciones cargadas por el GE a las
     * hebras del treadPool. Posee una cola de manera que si dos peticiones
     * piden realizar la misma acción y esta, por algún motivo está bloqueada se
     * encola la petición y no se elimina hasta que la hebra ha finalizado.
     * Esto permite disponer de un indicador de carga para las acciones del GE.
     *  
     * @author Víctor López Salazar.
     *
     */
    private class RuleWrapper implements Runnable{

        private RuleWrapper(IRule<?> rule) {
            this.rule = rule;
        }
        private void pushResquest(GameRequestMessage wur, Context ctx){
            RuleCtx rctx = ObjectPool.get(RuleCtx.factory);
            rctx.set(wur, ctx);
            if(!requestQueue.offer(rctx)){
                log.warn(m.get("REQUESTDROPPED", wur));
            }
        }
        private int pendingRequests(){
            return requestQueue.size();
        }
        @SuppressWarnings("unchecked")
        @Override
        public void run() {
            RuleCtx rctx = null;
            ChangeSet result = null;
            try {
                rctx = requestQueue.remove();
                result = ObjectPool.get(ChangeSet.factory);
                long quantum = rctx.wur.getQuantum();
                long timelapse = rctx.wur.getTimeLapse();
                long timeStamp = rctx.wur.getTimeStamp();
                result.setChangeInfo(rctx.wur.getUserKeyCode(), rctx.ctx.getKeyCode(), quantum, timelapse, timeStamp);
                log.debug("Regla {}: obteniendo efectos para {}", rctx.wur.getRule(), result);
                boolean changes = rule.prepare(rctx.wur, rctx.ctx, result);

                if ((changes && (tick - quantum < 2)) || rule instanceof InitGame || rule instanceof EndGame){ //pasamos el control de tiempo para las reglas de inicio y fin
//                    log.debug("Añadiendo conjunto de cambios {} para la regla {}", result, rule);
                    if (!changeSets.add(result)){
                        log.error("Regla {} - {}: no se han añadido los efectos al contexto, es posible que exista otro contexto con los mismos datos.", rctx.wur.getRule(), result);
                    }
//                    log.debug("Hecho");
                } else if (!changes){
                    log.info("Regla {} - {}: la regla se ejecutó pero no produjo ningún cambio en el contexto {}.", rctx.wur.getRule(), result);
                } else {
                    log.warn("Regla {} - {}: tardo demasiado en calcular efectos y ha sido descartada.", rctx.wur.getRule(), result);
                }
                result.writeSharedInfo(rctx.ctx);
            } catch (Throwable t){
                log.error("", t);
                if (result != null){
                    ObjectPool.release(ChangeSet.factory, result);
                }
            } finally {
                if (rctx != null){
                    ObjectPool.release(RuleCtx.factory, rctx);
                }
            }
        }

        @SuppressWarnings("rawtypes")
        private IRule rule;
        private Queue<RuleCtx> requestQueue = new LinkedBlockingQueue<RuleCtx>();
        
    }
}
