package es.calipso.framework.core.communications;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.TypeSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.calipso.framework.core.context.GameResponseData;
import es.calipso.framework.core.context.ObjectPool;
import es.calipso.framework.core.context.PoolableGameResponseDataFactory;

public class ClientEntityMessage extends GameResponseData<String> {

    public static PoolableGameResponseDataFactory<ClientEntityMessage> factory = new PoolableGameResponseDataFactory<ClientEntityMessage>() {
        @Override
        public Object makeObject() throws Exception {
            return new ClientEntityMessage();
        }

        @Override
        public void passivateObject(Object obj) throws Exception {
            ClientEntityMessage cr = (ClientEntityMessage)obj;
            cr.forbidTargets.clear();
            cr.targets = null;
            ArrayList<ClientEntityMessage> otherRules = cr.otherRulesForSubject;
            log.debug("limpiando clientRule: {} ({}), tamaño de la lista de reglas: {}", new Object[]{cr.getData(), cr.ruleData, otherRules.size()});
            cr.ruleData = null;
            if (cr.callback != null){
                ObjectPool.release(ClientEntityMessage.factory, cr.callback);
                cr.callback = null;
            }

            if (1 < otherRules.size()){
                Iterator<ClientEntityMessage> iterator = otherRules.iterator();
                while(iterator.hasNext()){
                    ClientEntityMessage cr1 = iterator.next();
                    if (cr != cr1){
                        iterator.remove();
                        ObjectPool.release(ClientEntityMessage.factory, cr1);
                    }
                }
            }
        }
    };

    @Override
    public PoolableGameResponseDataFactory<? extends GameResponseData<String>> getFactory() {
        return factory;
    }

    public ClientEntityMessage() {
        otherRulesForSubject.add(this);
    }

    @Override
    public void mix(GameResponseData<String> anotherMixture) {
        if (this != anotherMixture){
//            log.debug("mezclando {} con {}", this, anotherMixture);
            ClientEntityMessage cr = (ClientEntityMessage) anotherMixture;
            otherRulesForSubject.addAll(cr.otherRulesForSubject);
        }
    }
    public ClientEntityMessage set(String subject, String method, Object... params) throws JsonGenerationException, JsonMappingException, IOException{
        setMixture(subject, method);
        if (this.callback != null){
            ObjectPool.release(ClientEntityMessage.factory, this.callback);
            this.callback = null;
        }
        if (params != null){
            ruleData = jsonMapper.writeValueAsString(params);            
        }
        return this;
    }
    public ClientEntityMessage callback(String subject, String method, Object... params) throws JsonGenerationException, JsonMappingException, IOException{
        if (this.callback == null){
            this.callback = ObjectPool.get(factory);
        }
        callback.set(subject, method, params);
        return callback;
        
    }
    public ClientEntityMessage callback(){
        if (this.callback == null){
            this.callback = ObjectPool.get(factory);
        }
        return callback;
    }
    @Override
    public void addTarget(String targetid){
        if (targets == null){
            targets = new HashSet<String>();
        }
        targets.add(targetid);
    }
    @Override
    public void serialize(JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {
        jgen.writeArrayFieldStart(getId());
        for (ClientEntityMessage cr : otherRulesForSubject){ //la primera regla es this
            jgen.writeStartObject();
            if (cr.callback != null){
                jgen.writeObjectFieldStart("callback");
                jgen.writeStringField("o", cr.callback.getId());
                jgen.writeStringField("m", cr.callback.getData());
                if (cr.callback.ruleData != null){
                    jgen.writeFieldName("p");
                    jgen.writeRawValue(cr.callback.ruleData);
                }
                jgen.writeEndObject();
            }
            jgen.writeStringField("m", cr.getData());
            if (cr.ruleData != null){
                jgen.writeFieldName("p");
                jgen.writeRawValue(cr.ruleData);
            }
            jgen.writeEndObject();
        }
        jgen.writeEndArray();

/*        if (serializeRulesOnly){
            jgen.writeStartArray();
            for (ClientEntityMessage cr : otherRulesForSubject){
                jgen.writeStartObject();
                if (callback != null){
                    callback.setSerializeRulesOnly();
                    jgen.writeObjectField("callback", cr.callback);
                    callback.unSetSerializeRulesOnly();
                }
                jgen.writeObjectField("m", cr.getData()); //method to invoke in the client.
                jgen.writeObjectField("p", cr.ruleData); //params for the method.
                jgen.writeEndObject();
            }
            jgen.writeEndArray();
        } else {
            jgen.writeArrayFieldStart(getId());
            for (ClientEntityMessage cr : otherRulesForSubject){ //la primera regla es this
                jgen.writeStartObject();
                if (callback != null){
                    jgen.writeObjectField("callback", cr.callback);
                }
                jgen.writeObjectField(cr.getData(), cr.ruleData);
                jgen.writeEndObject();
            }
            jgen.writeEndArray();
        }*/
    }
    @Override
    public void serializeWithType(JsonGenerator jgen,
            SerializerProvider provider, TypeSerializer typeSer)
            throws IOException, JsonProcessingException {
        serialize(jgen, provider);
    }
    @Override 
    public Set<String> getTargets() {
        Set<String> t = targets;
        if (t != null && forbidTargets.size()>0){
            t = new HashSet<String>(targets);
            t.removeAll(forbidTargets);
        }
        log.debug("En ClientEntityMessage.getTargets(): t = {}, forbidTargets = {}", targets, forbidTargets);
        return t;
    }
    public void forbidTarget(String target){
        forbidTargets.add(target);
    }
    public String toString(){
        String rules, 
               otherRules = "";
        int n = otherRulesForSubject.size(); 
        if (n == 1){
            rules = tb.text("{0}.{1}({2})", new Object[]{getId(), getData(), ruleData});  
        } else {
            otherRules = tb.text("[{0}.{1}({2}), ", new Object[]{getId(), getData(), ruleData});
            ClientEntityMessage cr;
            for (int i = 1; i < n-1; i++){
                cr = otherRulesForSubject.get(i);
                otherRules = tb.text("{0}, {1}", otherRules, cr);
            }
            cr = otherRulesForSubject.get(n-1);
            rules = tb.text("{0}, {1}]", otherRules, cr);
        }
        
        if (callback != null){
            rules = tb.text("{0}:callback = {1}.{2}({3})", new Object[]{rules, callback.getId(), callback.getData(), callback.ruleData});
        }
    
        return rules;
    }
    
    private ArrayList<ClientEntityMessage> otherRulesForSubject = new ArrayList<ClientEntityMessage>();
    
    private String ruleData;
    
    private ClientEntityMessage callback;
    
    private ArrayList<String> forbidTargets = new ArrayList<String>();
//    @SuppressWarnings("unused")
    private static final ObjectMapper jsonMapper = new ObjectMapper();
    private static final Logger log = LoggerFactory.getLogger(ClientEntityMessage.class);

}