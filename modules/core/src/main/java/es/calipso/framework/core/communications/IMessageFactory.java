package es.calipso.framework.core.communications;

import java.util.HashMap;

/**
 * Interfaz para implementar una factoria para un tipo concreto de mensaje de
 * petición cliente/servidor. Normalmente un juego no debe implementar ningún
 * tipo de mensaje concreto y usa siempre la clase GameRequestMessage y por 
 * tanto la factoria por defecto para este tipo de mensajes.
 * 
 * @author Víctor López Salazar
 *
 * @param <T> Tipo del mensaje que extiende a GameRequestMessage. 
 */
public interface IMessageFactory<T extends GameRequestMessage> {
    public T newMessage(HashMap<String, Object> data);

    public String getType();
}
