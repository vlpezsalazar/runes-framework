package es.calipso.framework.core;

import org.codehaus.jackson.map.JsonSerializableWithType;

import es.calipso.framework.core.communications.GameRequestMessage;
import es.calipso.framework.core.context.Context;
import es.calipso.framework.core.dao.User;
import es.calipso.framework.core.gameengine.rules.ChangeSet;

/**
 * Este es el interfaz que deben implementar todos los juegos.
 * 
 * @author Víctor López Salazar
 *
 */
public interface IGame extends JsonSerializableWithType {
    /**
     * Devuelve el contexto asociado a una petición.
     * 
     * Todas las peticiones que un juego recibe de los clientes tienen un 
     * contexto asociado. Un contexto es una división lógica de las entidades
     * de juego (GameEntities) que sirve para restringir el procesamiento. 
     * Cuando una GameEntity actua dentro de un contexto, puede ver las
     * acciones del resto de GameEntities que están en el mismo contexto, pero
     * no las acciones de las GameEntities que están en otro.
     * 
     * @param wur mensaje del cliente.
     * @return el contexto asociado a una petición.
     * @see Context
     */
    public Context getContext(GameRequestMessage wur);
    
    /**
     * Establece los datos de personalización del
     * juego para este usuario. Si la personalización
     * del juego es por usuario estos datos de 
     * personalización se almacenan en el usuario (será
     * necesario crear una gameEntity usuario).  
     *  
     * @param user usuario para el que se debe personalizar el juego.
     *  
     */
    public void setInitGameData(User user) throws Throwable;

    /**
     * Indica si la personalización de los usuarios es única o por cada juego.
     * 
     * @return true si la personalización es por cada juego, false en otro caso.
     */
    public boolean isPersonalizedPerUser();
    
    /**
     * Regla de finalización del juego.
     * @return 
     */
    public boolean finish(User user, Context ctx, ChangeSet effects) throws Throwable;
    
}
