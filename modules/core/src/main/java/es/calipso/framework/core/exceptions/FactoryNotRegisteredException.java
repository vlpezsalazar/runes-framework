package es.calipso.framework.core.exceptions;

public class FactoryNotRegisteredException extends Exception {

    private static final long serialVersionUID = 1L;

    public FactoryNotRegisteredException() {

    }

    public FactoryNotRegisteredException(String message) {
        super(message);
    }

    public FactoryNotRegisteredException(Throwable cause) {
        super(cause);
    }

    public FactoryNotRegisteredException(String message, Throwable cause) {
        super(message, cause);
    }

}
