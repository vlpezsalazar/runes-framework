package es.calipso.framework.core.management;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Timer;
import java.util.TimerTask;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.calipso.framework.core.GameTypeRegister;
import es.calipso.framework.core.IGame;
import es.calipso.framework.core.communications.Messenger;
import es.calipso.framework.core.dao.User;
import es.calipso.framework.core.gameengine.GameEngine.Type;
import es.calipso.framework.storage.DBManager;
import es.calipso.framework.storage.exceptions.CannotRegisterException;
import es.calipso.framework.storage.exceptions.EntityManagerNotSetException;
import es.calipso.framework.text.TextBuilder;

public class GamesDaemon {
    public static ClassLoader getClassLoader(String game) {
        return daemon.gamesFiles.get(game);
    }
    public static synchronized GamesDaemon get(DataSource ds, String gamesPath, Type t, Messenger msn) {
        if (daemon == null) {
            try {
                TextBuilder.load(GamesDaemon.class.getResourceAsStream("/libmessages/logmessages"));
                daemon = new GamesDaemon(gamesPath, ds, t);
                gm = GameManager.newInstance(msn);
            } catch (Throwable e) {
                log.error("", e);
            }
        }
        return daemon;
    }

    private GamesDaemon(String gamesPath, DataSource ds, Type t) throws SQLException,
            IOException, URISyntaxException, CannotRegisterException {
        this.gamesPath = gamesPath;
        GEType = t;
        try {
            GameTypeRegister.setDataSource(ds);
            dbmanager = DBManager.newManager("CALIPSOLIB", ds, 
                    GamesDaemon.class.getResourceAsStream("/sql/tables.ddl.sql"),
                    GamesDaemon.class.getResourceAsStream("/sql/data.sql"));
            dbmanager.registerType(User.class);
        } catch (SQLException e) {
            if ("".equals(e.getSQLState())) {// FIXME CODIGO DE ERROR PARA CUANDO UN ESQUEMA EXISTA
                try {
                    dbmanager = DBManager.getManager("");
                } catch (EntityManagerNotSetException e1) {
                }
            } else {
                throw e;
            }
        }
    }

    public void refresh() {
        task.run();
    }

    public void start() {
        t.schedule(task, DELAY, PERIOD);
    }

    public void stop() {
        t.cancel();
        dbmanager.close();
    }
    private FilenameFilter filter = new FilenameFilter() {
        @Override
        public boolean accept(File dir, String name) {
            return name.endsWith(".jar");
        }
    };

    private TimerTask task = new TimerTask() {
        @Override
        public void run() {
            registerNewGames();
            loadNewGames();
        }
        private void registerNewGames() {
            File gameFiles = new File(gamesPath);
            File[] jarGames = gameFiles.listFiles(filter);
            String gameName;
            if (jarGames != null){
                for (File game : jarGames){
                    gameName = game.getName();
                    if (!gamesFiles.containsKey(gameName)){
                        try {
                            gamesFiles.put(gameName, new GameTypeRegister(game));
                        } catch (EntityManagerNotSetException e) {
                            log.error("", e);
                        } catch (Throwable t){
                            log.error("", t);
                        }
                    }
                }
            }
        }
        private void loadNewGames() {
            for (GameTypeRegister gtr : gamesFiles.values()) {
                Class<? extends IGame> gameDescriptorClass = gtr.getGameDescriptorClass();
                try {
                	if (!loadedGames.contains(gtr.getGameName())){
                	    log.info("cargando nuevo juego: {}", gtr.getGameName());
                    	loadedGames.add(gm.newGame(gameDescriptorClass.newInstance(), gtr, GEType));                		
                	}
                } catch (Throwable e) {
                    log.error("", e);
                }
            }
        }
    };

    private Type GEType = Type.MASTER;
    private String gamesPath;
    private HashMap<String, GameTypeRegister> gamesFiles = new HashMap<String, GameTypeRegister>();
    private HashSet<String> loadedGames = new HashSet<>();
    private static GameManager gm;
    private Timer t = new Timer(true);
    private DBManager dbmanager;
    private static final long DELAY = 1000;
    private static final long PERIOD = 60000;
    private static GamesDaemon daemon = null;
    private static final Logger log = LoggerFactory.getLogger(GamesDaemon.class);
}
