package es.calipso.framework.core.gameengine.rules;

import es.calipso.framework.core.context.Context;

public interface ICondition {
    public boolean check(String source, Context ctx);
}
