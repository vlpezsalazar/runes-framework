package es.calipso.framework.core.dao;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializableWithType;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.TypeSerializer;

public class Reference implements JsonSerializableWithType{
    
    Reference(String code){
        keycode = code;
    }
    private String keycode;

    @Override
    public void serialize(JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {
        jgen.writeStartObject();
        jgen.writeStringField("reference", keycode);
        jgen.writeEndObject();
        
    }

    @Override
    public void serializeWithType(JsonGenerator jgen,
            SerializerProvider provider, TypeSerializer typeSer)
            throws IOException, JsonProcessingException {
        // TODO Auto-generated method stub
        
    }
}
