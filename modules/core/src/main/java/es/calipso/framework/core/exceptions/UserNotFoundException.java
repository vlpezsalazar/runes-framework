package es.calipso.framework.core.exceptions;

import es.calipso.framework.text.TextBuilder;

public class UserNotFoundException extends Exception {
    public UserNotFoundException(String login, String password){
       super(message.get("USERNOTFOUNDEXCEPTION", login, password));
    }
    public UserNotFoundException(int id){
        super(message.get("USERIDNOTFOUNDEXCEPTION", id));
     }
    public UserNotFoundException() {
        super(message.get("USERNOTLOGGEDEXCEPTION"));
    }

    public UserNotFoundException(Exception e) {
        super(e);
    }

    private static TextBuilder message = TextBuilder.getSingletonInstance();
    /**
     * 
     */
    private static final long serialVersionUID = 1545706195864812572L;

}
