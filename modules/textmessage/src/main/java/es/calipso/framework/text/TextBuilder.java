package es.calipso.framework.text;

import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.Properties;

/*import org.slf4j.Logger;
import org.slf4j.LoggerFactory;*/

public class TextBuilder {

    public static TextBuilder getSingletonInstance(){
        return m;
    }
    public static void load(InputStream messagesFile){
        m.loadMessages(messagesFile);
    }
    /**
     * Método que permite cargar un conjunto de clave-mensaje, añadiendolos al
     * repositorio central de mensajes.
     * 
     * @param messagesFile archivo donde se encuentran los mensajes a cargar.
     */
    private void loadMessages(InputStream messagesFile){
        Properties p = new Properties(); 
        try {
            p.load(messagesFile);
            messages.putAll(p);
//            log.debug("Mensajes en repositorio: {}", messages);
        } catch (Throwable e) {}

    }

    private synchronized String getMessage(String type, Object... data) {
        String pattern = messages.getProperty(type);
        String message = MessageFormat.format("Message.get(): Tipo de mensaje {0} no encontrado ", type);

        if (pattern != null) {
            f.applyPattern(pattern);
            f.format(data, b.delete(0, b.length()), null);
            message = b.toString();
        }
        return message;
    }
    public String get(String type, Object...data){
        return m.getMessage(type, data);
    }
    public String text(String pattern, Object...data){
        String message = "";
        if (this == m){
            synchronized (this) {
                if (pattern != null) {
                    f.applyPattern(pattern);
                    f.format(data, b.delete(0, b.length()), null);
                    message = b.toString();
                }                
            }
        } else {
            if (pattern != null) {
                f.applyPattern(pattern);
                f.format(data, b.delete(0, b.length()), null);
                message = b.toString();
            }            
        }
        return message;        
    }
    private static final Locale ES = new Locale("es", "ES");
    private static final TextBuilder m = new TextBuilder();
    private MessageFormat f = new MessageFormat("", ES);
    private StringBuffer b = new StringBuffer();
    private Properties messages = new Properties();
//    private static final Logger log = LoggerFactory.getLogger(TextBuilder.class);
}
