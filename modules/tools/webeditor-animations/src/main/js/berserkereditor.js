/**
 * 
 * Editor para el juego Berserker. 
 *
 * @author Pablo Arroyo Coll, Víctor López Salazar 
 */

if (typeof BerserkerGame.Editor == "undefined"){
    BerserkerGame.Editor = {};
    
(function(pkedit){
    /**
     * Clase que representa un keyframe de una animación. Un keyframe es un 
     * instante de una animación que establece la posición de las articulaciones
     * de un esqueleto a la que hay que llegar interpolando.
     * <p>
     * En este keyframe se almacenan los siguientes datos:
     * <p>
     * data = {} Es un conjunto de todas las articulaciones de un esqueleto 
     * (jointid = "cuello", "hombroizq", "rodillaizq"...) que han sufrido un
     * cambio y para las que se especifica un estado en este keyframe.
     * <p>
     * El estado se define mediante la posición y la rotación de la articulación
     * (jointid) indicadas como dos vectores:  
     * <p>
     * <ul>
     * <li>data[jointid].translate = [x, y, z];
     * <li>data[jointid].rotation = [qx, qy, qz, qw];
     * </ul>
     * <p>
     * En donde translate es un vector con la posición en el espacio de la
     * articulación y rotate es otro vector con la orientación de la articulación
     * establecida mediante un quaternion. Sobre esto último basta saber que es
     * la forma más conveniente de dar una rotación y que  hay formas de convertir
     * una matriz de rotación en un quaternion:<br> 
     * <a>http://en.wikipedia.org/wiki/Rotation_matrix#Quaternion</a> 
     * <p>
     * Los últimos datos que contiene el estado de una articulación son los de
     * animación.<p>
     *  
     * data[jointid].channel[channelid] = {<br> 
     *                 type : type,<br>
     *                 points : points<br>
     *             };<br>
     * 
     * Para cada posible "canal de animación" (channelid): 
     * <dl>
     * <dt>LocX <dd>posición x del espacio
     * <dt>LocY <dd>posición y del espacio
     * <dt>LocZ <dd>posición z del espacio
     * <dt>QuatX<dd>rotación en X del espacio.
     * <dt>QuatY<dd>rotación en Y del espacio.
     * <dt>QuatZ<dd>rotación en en Z del espacio.
     * <dt>QuatW<dd>posicion en X del espacio. 
     * </dl>
     * 
     * se establece la función de interpolación (type):
     * <dl>
     * <dt>pkedit.Motion.STEP = 0 <dd> función salto.
     * <dt>pkedit.Motion.LINEAR = 1 <dd> función lineal de interpolación.
     * <dt>pkedit.Motion.BEZIER = 2 <dd> función de interpolación mediante curva bezier.
     * </dl>
     */
    pkedit.Keyframe = Class.extend({
        /**
         * Constructor de un frame
         */
        init : function(data){
            var empty = true;

            this.isEmpty = function(){
                return empty;
            };
            this.setUsed = function(){
                empty = false;
            };
            data = ((data instanceof pkedit.Keyframe) ? data.data : data);
            this.data = $.extend({}, data);
        },
        mirror : function(){
            
        },
        /**
         * Establece el estado de una articulación en este keyframe.
         * @param {string} jointid id de la articulación
         * @param {Array} rotationQuat vector de 4 componentes con la
         *                 orientación de la articulación expresado como un
         *                 quaternion
         * @param {Array } translateVec vector de 3 componentes con el
         *                  desplazamiento de la articulación.
         */
        setJointState : function(/*{string}*/jointid, /*{Array}*/translateVec, /*{Array}*/rotationQuat) {
            this.setUsed();
            var data = this.data;
            var p = pkedit.Motion.channels;
            data[jointid] = {
                    getChannelValue : function(channel){
                        if (typeof channel == "number"){
                            switch (channel){
                            case p.LOCX :
                            case p.DLOCX :
                                return this.translate[0];
                                break;
                            case p.LOCY :
                            case p.DLOCY :
                                return this.translate[1];
                                break;
                            case p.LOCZ :
                            case p.DLOCZ :
                                return this.translate[2];
                                break;
                            case p.QUATX :
                            case p.ROTX :
                                return this.rotation[0];
                                break;
                            case p.QUATY :
                            case p.ROTY :
                                return this.rotation[1];
                                break;
                            case p.QUATZ :
                            case p.ROTZ :
                                 return this.rotation[2];
                                break;
                             default:
                                 break;
                            }
                        }
                        return null;
                    }
            };
            data[jointid].channel = {};
            data[jointid].rotation = rotationQuat;
            data[jointid].translate = translateVec;
        },
        /**
         * Establece la función de animación para un canal determinado en una articulación.
         *  
         * @param {string} jointid id de articulación.
         * @param {string} channel canal de animación de la articulación.
         * @param {Number} type tipo de función de interpolación definido en
         *            las constantes [Motion.STEP|Motion.LINEAR|Motion.BEZIER].
         * @param [optional] {Array} points vector de 4 flotantes conteniendo 
         *            2 puntos de control para una curva Bezier de 3 puntos:
         *            <ul>
         *            <li>points[0] : x - número del frame del primer punto la curva.
         *            <li>points[1] : y - posición de la que parte la curva.
         *            <li>points[2] : y - posición del punto de control de la curva para el x = frame actual.
         *            <li>points[3] : x - frame final para la posición y = valor del canal para jointid en el
         *                            keyframe actual
         *            <ul>
         * @see {BerserkerGame.Graphics.Motions}
         */
        setJointChannel : function(/*{string}*/jointid, /*{string}*/channel, /*{Number}*/type, /*[optional] {Array}*/points){
            var data = this.data;
            if (typeof data[jointid] == "undefined")
                return;
            data[jointid].channel[channel] = {
                    type : type,
                    points : points
            };
        },
        /**
         * Establece el número de frames en que interpolar desde el anterior
         *  
         * @param {Number} número de intervalos
         * @see {BerserkerGame.Graphics.Motions}
         */
        setIntervals : function(/*{number}*/intervals){
			this.intervals=intervals;
        },
        /**
         * Devuelve el número de frames en que interpolar desde el anterior
         *  
         */
        getIntervals : function(){
			return this.intervals;
        },
        /**
         * Devuelve los datos de la animación en el formato especifico del 
         * engine gráfico que se está empleando.
         * 
         * @param frame numero del keyframe actual en la secuencia.
         * @returns {___anonymous3796_3797}
         */
        getMotionData : function(frame){
            var /*{Object}*/ motion = {};
            if (GLGE){
                var point, val;
                $.each(this.data, function(jointid, jointdata){
                    motion[jointid] = {};
                    $.each(pkedit.Motion.channels, function(channelName, channelIndex){
                        var channeldata = jointdata.channel[channelIndex];
                        val = jointdata.getChannelValue(channelIndex);
                        if (val != null){
                            if ((typeof channeldata != "undefined") && (channeldata.type != pkedit.Motion.NONE)) {
                                switch (channeldata.type){
                                case pkedit.Motion.STEP:
                                    point = new GLGE.StepPoint(frame, val);
                                    break;
                                case pkedit.Motion.LINEAR:
                                    point = new GLGE.LinearPoint();
                                    point.setX(frame);
                                    point.setY(val);
                                    break;
                                case pkedit.Motion.BEZIER:
                                    if (typeof channeldata.points != "undefined"){
                                        point = new GLGE.BezTriple();
                                        point.setX1(channeldata.points[0]); point.setY1(channeldata.points[1]);
                                        point.setX2(frame); point.setY2(channeldata.points[2]);
                                        point.setX3(channeldata.points[3]); point.setY3(val);
                                    }
                                    break;
                                }
                                motion[jointid][channelIndex] = point;
                            }/* else {
                                point = new GLGE.LinearPoint();
                                point.setX(frame);
                                point.setY(val);
                            }
                            motion[jointid][channelIndex] = point;*/
                        }
                    });
                });
            } else {
                //TODO Seleccionar otro engine.
            }
            return motion;
        },
        clone : function(){
            return new pkedit.Keyframe(this.data);
        }
    });
    /**
     * Representa a una animación completa, una secuencia de keyframes para
     * los que se crea una animación con un numero de frames concreto.<br>
     * 
     * Un frame no es lo mismo que un keyframe. Una animación puede tener
     * 4 keyframes y estar finalmente compuesta por 50 frames. El número
     * de frames de la animación da una medida de la suavidad en la interpolación
     * entre keyframes.<br>
     * 
     * La interpolación entre keyframes se realiza empleando las funciones de
     * interpolación STEP, LINEAR Y BEZIER. <br>
     * Para saber como se interpreta que una función de interpolación que toma
     * valores en el plano (x,y) se emplea en un canal que esta definido en R
     * tenemos que asimilar la x de esas funciones al número del frame
     * actual, mientras que la y es el valor que se le asigna al canal y que se
     * establece por el estado en el que se encuentra la articulación en el keyframe
     * para el canal de animación actual. Por ejemplo si este es el keyframe 10
     * y el estado de la articulación "hombroder" para el canal QuatX es 0.3 y se
     * emplea una función salto, esta se define como un par (10,0.3)
     * 
     * ¿Cómo se interpola entre dos keyframes? Esto se hace cogiendo aquellos 2
     * keyframes A, B cuyas x para el canal que se está animando actualmente
     * quedan justamente por debajo y por encima del frame actual. Luego se usa
     * el tipo de interpolación que marque el frame B. Por ejemplo en caso que 
     * A sea (xa,ya), B sea (xb, yb) y tenga una interpolación lineal, tendríamos
     * que emplear la formula  ya + (frame - xa)*(yb-ya)/(xb-xa) para obtener
     * el valor del canal en el frame de animación actual.
     * 
     * Si 2 keyframes A,B consecutivos son definidos como Bezier la interpolación que 
     * se realiza es de una Bezier con 4 puntos (ver glge-compiled.js: 4670), en caso
     * de ser Linear-Bezier o viceversa se utiliza una Bezier con sólo 3 puntos.
     *  
     */
    pkedit.Motion = Class.extend({
        init : function(keyframeArray){
            if (typeof keyframeArray != "undefined" ){
                this.frames = keyframeArray;
            } else {
                this.frames = new Array(); //lista de frames.                
            }
        },
        /**
         * Establece el nombre de esta animación.
         * @param {String} name nombre de la animación.
         */
        setName : function(/*{string}*/name){
            this.name = name;
        },
        /**
         * Devuelve el nombre de esta animación.
         */
        getName : function(){
            return this.name;
        },
        /**
         * Añade un conjunto de keyframes desde una posición pos que tienen
         * el estado del keyframe en dicha posición.
         * @param {Number} pos indice del keyframe desde el que insertar. 
         * @param {Number} num número de keyframes a insertar.
         */
        newKeyframes : function(/*{Number}*/pos, /*{Number}*/num){
            if (typeof pos == "undefined"){
                pos = this.frames.length;
            }
            if (!num) num = 1;
            var frames = this.frames;
            var framepos = pos;
            if (pos >=frames.length){
                pos = frames.length-1;
                framepos = frames.length;
            } else if (pos<0){
                pos = 0;                framepos = 0;
            }
            var frame = frames[pos];
            for (var i = 0; i<num; i++){
                frames.splice(framepos, 0, (typeof frame == "undefined") ? new pkedit.Keyframe() : frame.clone());
            }
            return framepos;
        },
        getNumberOfKeyframes : function (){
            return this.frames.length;
        },
        /**
         * Elimina un conjunto de keyframes de la animación actual.
         * 
         * @param {Number} from índice de comienzo de los keyframes a eliminar. 
         * @param {Number} howmany número de keyframes a eliminar. 
         */
        removeKeyframes : function(/*{Number}*/from, /*{Number}*/howmany){
            this.frames.splice(from, howmany);
        },
        getKeyframe : function(i){
            if (i < 0 || this.frames.length <= i) return false;
            return this.frames[i];
        },
        /**
         * Establece el estado de una articulación para un keyframe.
         * @param {Number} i indice del keyframe.
         * @param {string} jointid articulación a establecer.
         * @param {Array} translatevec vector de 3 reales indicando la posición de la articulación. 
         * @param {Array} rotationquat vector de 4 reales indicando la orientación de la articulación. 
         */
        setKeyframe : function(/*{Number}*/i, /*{string}*/jointid, /*{Array}*/translatevec, /*{Array}*/rotationquat){
            if (i < 0 || this.frames.length <= i) return;
            this.frames[i].setJointState(jointid, translatevec, rotationquat);
		},
        setLinearKeyframe : function(/*{Number}*/i, /*{string}*/jointid, /*{Array}*/translatevec, /*{Array}*/rotationquat){
            if (i < 0 || this.frames.length <= i) return;
            this.frames[i].setJointState(jointid, translatevec, rotationquat);
            //this.setKeyframeTypeForJoint(i, jointid, Motion.channels.LOCX, Motion.LINEAR);
            //this.setKeyframeTypeForJoint(i, jointid, Motion.channels.LOCY, Motion.LINEAR);
            //this.setKeyframeTypeForJoint(i, jointid, Motion.channels.LOCZ, Motion.LINEAR);
            this.setKeyframeTypeForJoint(i, jointid, Motion.channels.ROTX, Motion.LINEAR);
            this.setKeyframeTypeForJoint(i, jointid, Motion.channels.ROTY, Motion.LINEAR);
            this.setKeyframeTypeForJoint(i, jointid, Motion.channels.ROTZ, Motion.LINEAR);
		},
        setKeyframeByTypes : function(/*{Number}*/i, /*{string}*/jointid, /*{Array}*/translatevec, /*{Array}*/rotationquat, /*{Array}*/translateTypes, /*{Array}*/rotationTypes, /*integer*/intervals){
            if (i < 0 || this.frames.length <= i) return;
            this.frames[i].setJointState(jointid, translatevec, rotationquat);
			this.frames[i].setIntervals(intervals);
            this.setKeyframeTypeForJoint(i, jointid, Motion.channels.LOCX, translateTypes[0]);
            this.setKeyframeTypeForJoint(i, jointid, Motion.channels.LOCY, translateTypes[1]);
            this.setKeyframeTypeForJoint(i, jointid, Motion.channels.LOCZ, translateTypes[2]);
            this.setKeyframeTypeForJoint(i, jointid, Motion.channels.DLOCX, translateTypes[3]);
            this.setKeyframeTypeForJoint(i, jointid, Motion.channels.DLOCY, translateTypes[4]);
            this.setKeyframeTypeForJoint(i, jointid, Motion.channels.DLOCZ, translateTypes[5]);
            this.setKeyframeTypeForJoint(i, jointid, Motion.channels.ROTX, rotationTypes[0]);
            this.setKeyframeTypeForJoint(i, jointid, Motion.channels.ROTY, rotationTypes[1]);
            this.setKeyframeTypeForJoint(i, jointid, Motion.channels.ROTZ, rotationTypes[2]);
            //this.setKeyframeTypeForJoint(i, jointid, Motion.channels.QUATW, rotationTypes[3]);
		},
        /**
         * Establece una funcion de animacion para un canal de una articulación en un frame determinado.
         *  
         * @param {Number} frame numero de frame a establecer.
         * @param {string} jointid identificador del joint 
         * @param {string} channel canal de animación. Alguno de los definidos en Motion.channels.
         * @param {Number} type tipo de animación.  Alguna de estas constantes [Motion.STEP|Motion.LINEAR|Motion.BEZIER].
         * @param bezpoints vector de 4 flotantes conteniendo 
         *            2 puntos de control para una curva Bezier de 3 puntos:
         *            <ul>
         *            <li>points[0] : x - número del frame del primer punto la curva.
         *            <li>points[1] : y - posición de la que parte la curva.
         *            <li>points[2] : y - posición del punto de control de la curva para el x = frame actual.
         *            <li>points[3] : x - frame final para la posición y = valor del canal para jointid en el
         *                            keyframe actual 
         * FIXME los frames en los puntos de control de una bezier sólo se deben establecer cuanto se conozca el número de frames de la animación.
         *            <ul> 
         */
        setKeyframeTypeForJoint : function (/*{Number}*/frame, /*{string}*/jointid, /*{Motion.channels}*/channel, /*{Number}*/type, /*{Array}*/bezpoints){
            if (frame < 0 || this.frames.length <= frame) return;
            this.frames[frame].setJointChannel(jointid, channel, type, bezpoints);
        },
        /**
         * Función que duplica un conjunto de keyframes pero cambiando las articulaciones
         * del lado izquierdo (aquellas que acaben por izq) a las del lado derecho, sustituyendo
         * estas por der. 
         * @param {Number} start keyframe de inicio. 
         * @param {Number} end keyframe de fin.
         */
        mirrorKeyframes : function(/*{Number}*/start, /*{Number}*/end){
//         TODO
        },
        /**
         * Devuelve un nuevo objeto Motion que incluye a los keyframes desde from hasta to.
         * @param {Number} from keyframe origen
         * @param {Number} to keyframe destino
         */
        slice : function (/*{Number}*/from, /*{Number}*/to){
            return new pkedit.Motion(this.frames.slice(from, to));
        },
        /**
         * Procesa los frames de la animación y los transforma a los objetos
         * que emplee el engine gráfico que se esté usando.
         * 
         * @param {Number} totalFrames numero total de frames que va a tener
         *  la animación
         * @returns la animación en un formato adecuado para emplearlo con el
         *           engine que estemos empleando actualmente. 
         */
        getMotion : function(){
            var motion;

            if (typeof GLGE != "undefined"){
                motion = new GLGE.Action();
                var channel = {};
                var length = this.frames.length;
				var totalFrames = 0;
                $.each(this.frames, function(i, frame){
					totalFrames += frame.getIntervals();
console.log("totalFrames "+totalFrames);
                    var joints = frame.getMotionData(totalFrames);
                    $.each(joints, function(jointid, channels){
                        if (typeof channel[jointid] == "undefined"){
                            var animationVector = new GLGE.AnimationVector(); 
                            channel[jointid] = {
                                    animationVector : animationVector
                            };
                            var actionChannel = new GLGE.ActionChannel();
                            actionChannel.setAnimation(animationVector);
                            actionChannel.setTarget(jointid);
                            motion.addActionChannel(actionChannel);
                        }
                        $.each(channels, function(ch, point){
                            var actionCurve = channel[jointid][ch];
                            if (typeof actionCurve == "undefined"){
                                actionCurve = new GLGE.AnimationCurve();
                                actionCurve.setChannel(pkedit.Motion.channels.GLGE[ch]);
                                channel[jointid][ch] = actionCurve;
                                var animationVector = channel[jointid].animationVector;
                                animationVector.addAnimationCurve(actionCurve);
                            }
                            actionCurve.addPoint(point);
                        });
                    });
                });
                motion.setFrames(totalFrames);
            } else {
                //TODO EMPLEAR OTRO ENGINE PARA 3D QUE NO UTILICE WEBGL
            }
            return motion;
        }
    });
    /**
     * Esta constante define la función de animación salto entre frame y frame.
     */
    pkedit.Motion.NONE = 9;
    /**
     * Esta constante define la función de animación salto entre frame y frame.
     */
    pkedit.Motion.STEP = 0;
    /**
     * Esta constante define la función de interpolación lineal entre frame y frame.
     */
    pkedit.Motion.LINEAR = 1;
    /**
     * Esta constante define la función de interpolación mediante una curva bezier entre frame y frame..
     */
    pkedit.Motion.BEZIER = 2;
    /**
     * Canales de animación.
     */
    pkedit.Motion.channels = {
            LOCX : 0,
            LOCY : 1,
            LOCZ : 2,
            QUATX : 3,
            QUATY : 4,
            QUATZ : 5,
            ROTX : 6,
            ROTY : 7,
            ROTZ : 8,
            DLOCX : 9,
            DLOCY : 10,
            DLOCZ : 11,
            GLGE : ["LocX","LocY","LocZ", "QuatX","QuatY","QuatZ", "RotX", "RotY", "RotZ","DLocX","DLocY","DLocZ"]
    };

})(BerserkerGame.Editor);

if(typeof BerserkerGame.Editor.GUI == "undefined"){
    BerserkerGame.Editor.GUI = {};

(function(pkedgui){
        /**
         * Clase padre de la jerarquia.
         */
        pkedgui.CanvasWidget = Class.extend({
            init : function (canvas, orix, oriy, width, height, controllers){
                this.canvas = canvas;
                if (typeof controllers !== null && typeof controllers != "undefined"){
                    var that = this;
                    for (var callback in controllers){
                        var controller = (function(handler, previousHandler){
                            return function (e){
                                var xy = that.getXYpos(canvas);
                                //trasformamos las coordenadas de cliente a coordenadas de canvas
                                var posx = e.clientX - xy['x'],
                                posy = canvas.height - (e.clientY - xy['y']);
                                if (posx>orix && posx<orix+width && posy > canvas.height - height && posy < oriy + height){
                                    handler($.extend({posx: posx, posy: posy}, e));
                                } else if (typeof previousHandler != "undefined" && previousHandler !== null){ 
                                    previousHandler(e);
                                }                    
                            };
                        })(controllers[callback], canvas["on"+callback]); 
                        if (callback == "mousewheel"){
                            canvas.onmousewheel = controller; 
                        } else {
                            $(canvas)[callback](controller);                            
                        }
                    }
                }
            },
            getXYpos : function (elem) {
                if (!elem) {
                   return {"x":0,"y":0};
                }
                var xy={"x":elem.offsetLeft,"y":elem.offsetTop};
                var par=this.getXYpos(elem.offsetParent);
                for (var key in par) {
                   xy[key]+=par[key];
                }
                return xy;
            },
            paint : function(){
                return;
            }
        });
        /**
         * Una clase abstracta que ofrece métodos útiles para generar un deslizante de elementos
         * sobre un canvas. 
         */
        pkedgui.CanvasElementSlider = pkedgui.CanvasWidget.extend({
            init : function(canvas, slidedirection, rows, columns, orix, oriy, width, height, step, elements){
                this.$super(canvas, orix, oriy, width, height, this.elementSelected ?  
                        {
                            mousedown : function controller(e){
                                var elementheight = height/rows;
                                var elementwidth = width/columns;
                                var index = Math.floor(((oriy + elementheight - e.posy)/elementheight)*columns + (e.posx - orix) / elementwidth);
                                that.presentElement += index;
                                that.elementSelected(that.presentElement);
                            } 
                        } : null);
                this.dir = slidedirection;
                this.rows = rows;
                this.columns = columns;
                this.orix = orix;
                this.oriy = oriy;
                this.width = width;
                this.height = height;
                this.step = step;
                this.elements = new Array();
                var that = this;
                if (typeof elements != "undefined"){
                    $.each(elements, function(i, element){
                        that.elements.push(element);
                    });
                }
                if (this.elements.length>0){
                    this.presentElement = 0;
                    this.paint();                    
                }

            },
            /**
             * Controlador para pasar a la imagen previa.
             */
            previousElements : function(){
                if (this.presentElement < this.step){
                    this.presentElement = 0;
                } else {
                    this.presentElement-= this.step;                    
                }
                if (typeof this.elementSelected == "function"){
                    this.elementSelected(this.presentElement);
                }
                this.paint();
            },
            /**
             * Controlador para pasar a la imagen siguiente.
             */
            nextElements : function (){
                if (this.elements.length - this.presentElement < this.step){
                    if (this.elements.length>this.step){
                        this.presentElement = this.elements.length - this.step;
                    } else {
                        this.presentElement = 0;
                    }
                } else {
                    this.presentElement+= this.step;                    
                }
                if (typeof this.elementSelected == "function"){
                    this.elementSelected(this.presentElement);
                }
                this.paint();
            }

/*
 *   Funcion abstracta que deben implementar los hijos.
 * 
 *           elementSelected : function(){ 
 *              return;
 *           }
 */
        });
        pkedgui.CanvasElementSlider.SLIDEUPDOWN = 0;
        pkedgui.CanvasElementSlider.SLIDELEFTRIGHT = 1;

        /**
         * Un "deslizante" (más bien un repintador) de imagenes.
         * 
         */
        pkedgui.CanvasImageSlider = pkedgui.CanvasElementSlider.extend({
            /**
             * Constructor de un deslizante de imagenes en un canvas.
             */
            init : function(canvas, orix, oriy, width, height){
                this.context = canvas.getContext("2d");
                this.$super(canvas, pkedgui.CanvasElementSlider.SLIDELEFTRIGHT, 1, 5, orix, oriy, width, height, 1);
                //TODO pintar las flechas de desplazamiento y añadir los controladores a las mismas. 
            },
            /**
             * Añade una imagen al deslizante de imágenes
             */
            addImage : function(imagedata){
                var imd = this.context.createImageData(imagedata.width, imagedata.height);
                imd.data = imagedata.data;
                this.elements.push(imd);
                if (this.presentElement == -1) this.presentElement++;
                this.paint();
            },
            /**
             * Cambia el tamaño del deslizante de imagenes. 
             */
            resize : function(neworix, neworiy, width, height){
                this.orix = neworix;
                this.width = width;
                this.oriy = neworiy;
                this.height = height;
                this.paint();

            },
            paint: function(){
                var orix = this.orix, oriy = this.oriy,
                    sliderwidth = this.width, 
                    sliderheight = this.height;
                var imagewidth = ((this.dir == pkedgui.CanvasImageSlider.SLIDELEFTRIGHT) ? sliderwidth/(this.rows*this.columns) : sliderwidth),
                    imageheight = ((this.dir == pkedgui.CanvasImageSlider.SLIDEUPDOWN) ? sliderheight/(this.rows*this.columns) : sliderheight);

                var context = this.context;
                context.fillStyle = "rgba(0,0,0,0.3)";

                context.fillRect(orix, oriy, sliderwidth, sliderheight);

                var numberOfImages = ((this.dir == pkedgui.CanvasImageSlider.SLIDEUPDOWN) ? sliderwidth/imagewidth : sliderheight/imageheight);
                
                if (this.presentElement != -1){
                    var despx = ((this.dir == pkedgui.CanvasImageSlider.SLIDELEFTRIGHT) ? sliderwidth/imagewidth : 0),
                         despy = ((this.dir == pkedgui.CanvasImageSlider.SLIDEUPDOWN) ? sliderheight/imageheight : 0), 
                         firstindex = Math.ceil(this.presentImage - numberOfImages/2);
                    if (this.elements.length<=numberOfImages){
                        firstindex = 0;
                    }
                    for (var i = 0, n = this.elements.length; i<numberOfImages && firstindex<n; i++, firstindex++){
                        console.log("dibujando imagen");
                        context.putImageData(this.elements[firstindex], orix+despx*i, oriy+ despy*i, 0,0,imagewidth, imageheight); 
                    }
                }                
            },
            /**
             * Ocultar el slider.
             */
            hide : function(){
                
            },
            /**
             * Mostrar el slider.
             */
            show : function() {
                
            }
        });

        /**
         * TODO
         * Toolbar para los frames.
         */
        pkedgui.FrameToolBar = pkedgui.CanvasImageSlider.extend({
            init : function(canvas){
               this.$super(canvas, 0, 0, canvas.width, canvas.height);
               this.animation = new BerserkerGame.Editor.Motion();
               
            },
            setModel: function(skeleton){
                this.skeleton = skeleton;                
            },
            play : function(from, to, numberofframes, time){
                var motion;
                if (arguments.length == 2){
                    numberofframes = from;
                    time = to;
                    motion = this.animation;
                } else if (arguments.length == 3){
                    time = numberofframes;
                    numberofframes = to;
                    to = from;
                    from = 0;
                }
                if (typeof motion == "undefined"){
                    motion = this.animation.slice(from, to); 
                }
                this.skeleton.doAnimation({
                    animation : motion.getMotion(numberofframes),
                    time : time,
                    loop : false
                });

            },
            setKeyframeData : function(i, frameImage, frameData){
                var skeleton = this.skeleton;
                var that = this;
                $.each(frameData, function(jointid, channel){
                    var boneid = skeleton.getBoneIdbyJoint(jointid);
                    var boneState = skeleton.getBoneState(boneid);
                    that.animation.setKeyframe(i, jointid, boneState.translate, boneState.rotation);
                    $.each(channel, function(chname, type){
                        that.animation.setKeyframeTypeForJoint(i, jointid, channel, type[0], type[1]);
                    });
                });                    
            },
            newKeyframe : function(frameImage, frameData){
                this.addImage(frameImage);
                var index = this.animation.newKeyframes();
                this.setKeyframeData(index, frameImage, frameData);

            },
            elementSelected : function(frameIndex){
                var frame  = this.animation.getFrame(frameIndex);
                var skeleton = this.skeleton;
                $.each(frame.data, function(jointid, jointState){
                    var boneid = skeleton.getBoneIdbyJoint(jointid);
                    skeleton.setBoneState(boneid, jointState.translate, jointState.rotation);
                });
            }
        });
        /**
         * TODO
         * Tablero para la edición de los modelos.
         */
        pkedgui.EditorBoard = pkedgui.CanvasWidget.extend({
            init : function (graphicEngine, width, height, jointsControl){
			    this.jointsCombo=jointsControl;
                var ge = graphicEngine;
                var boardoriy = ge.getCanvas().height - height;
                var startPos = [], drag = false;
                this.$super(ge.getCanvas(), 0, boardoriy, width, height, {
                    mousedown : function(e){
                        if(e.button==0){
                            drag=true;
                            startPos[0]= e.clientX;
                            startPos[1]= e.clientY;
                        }
                    },
                    mouseup : function(e){
                        that.camera.move(BerserkerGame.Graphics.Camera.STOPMOVE);
                        drag = false;
                    },
                    mousemove : function(e){
                        var mousePos=[e.clientX,e.clientY];
                        if (drag){
                            var dx = (mousePos[0]-startPos[0]>0) ? BerserkerGame.Graphics.Camera.MOVELEFT : BerserkerGame.Graphics.Camera.MOVERIGHT;
                            console.log(dx);
                            that.camera.move(dx);
                        }
                    },
                    mousewheel : function(e){
                        if (typeof that.selectedModel != "undefined"){
                            var wheelData = e.detail ? e.detail/10 : e.wheelDelta/-30;
                            that.camera.setZoomSpeed(wheelData);
                        }
                    }
                });
                this.width = width;
                this.height = height;
                /**
                 * 
                 */
                this.selectedJoint;
                var that = this;

                this.scene = new BerserkerGame.Graphics.Scene("editorboard.xml", function(){
                    var camera = that.camera = new BerserkerGame.Graphics.Camera([-10, 10, -10]);
                    if (typeof that.selectedModel != "undefined"){
                        that.scene.compose(that.selectedModel);
                    }
                    //draw grid
                    var positions=[];
                    for(var x=-50; x<50;x++){
                        if(x!=0){
                            positions.push(x);positions.push(0);positions.push(-50);
                            positions.push(x);positions.push(0);positions.push(50);
                            positions.push(50);positions.push(0);positions.push(x);
                            positions.push(-50);positions.push(0);positions.push(x);
                        }
                    }
                    
                    var line=(new GLGE.Object).setDrawType(GLGE.DRAW_LINES);
                    line.setMesh((new GLGE.Mesh).setPositions(positions));
                    line.setMaterial(that.scene.getDocument().getElement( "lines" ));
                    that.scene.compose(line);
                    ge.createViewport(0, that.canvas.height - height, width, height, that.scene, camera);
                });
            },
            getWidth : function(){
                return this.width;
            },
            getHeight : function(){
                return this.height;
            },
            
            /**
             * Muestra las articulaciones de un modelo.
             */
            showJoints : function(){
                
            },
            /**
             * Oculta las articulaciones de un modelo.
             */
            hideJoints : function (){
                
            },
            /**
             * Mueve una articulación del modelo a una posición (siempre
             * mediante una rotación en un plano).
             * 
             */
            moveJoint : function(x, y){
                
            },
            /**
             * Establece el modelo a editar.
             * 
             * @param {BerserkerGame.Graphics.Skeleton} model modelo a situar
             * en el tablero de edición.
             */
            setModel : function(model){
                if (typeof model != "undefined"){
                    if (typeof this.scene != "undefined"){
                        if (typeof this.selectedModel != "undefined"){
                            this.scene.remove(this.selectedModel);
                        }
                        this.scene.compose(model);
                    }
                    this.selectedModel = model;
                    this.camera.setLookat(model);
                    this.camera.setFollow(model);
					// Rellena el select con los joints
					console.log("Rellena el select con los joints");
					var selectList=this.jointsCombo;
					for (x = selectList.length; x >= 0; x = x - 1) {
						selectList[x] = null;
					}
					$.each(model.getJoints(), function(id, joint){
						console.log("joint="+joint);
					    selectList[selectList.length] = new Option(id,id);
					});
                }
            },
            snapshot : function(){
                return this.camera.snapshot(this.scene, 0, 0, this.width, this.height);
            },
        });
        /**
         * TODO
         * Barra para los modelos.
         */
        pkedgui.ModelsBar = pkedgui.CanvasElementSlider.extend({
            /**
             * Contructor de la barra de modelos.
             * @param {BerserkerGame.Graphics.GraphicEngine} graphicEngine motor gráfico 
             * @param {BerserkerGame.Editor.EditorBoard} editorBoard tablero
             *  del editor donde se situan los modelos.
             * @param {Array<BerserkerGame.Graphics.Skeleton>} models modelos
             * a mostrar en la barra
             */
            init : function(graphicEngine, editorBoard, frameToolBar, models){
                var ge = this.ge = graphicEngine;
                var canvas = graphicEngine.getCanvas();
                var orix = editorBoard.getWidth(), oriy = 0;//editorBoard.getHeight();
                var width = canvas.width - orix,
                    height = editorBoard.getHeight();
                this.viewports = new Array();
                for (var i = 0, viewportHeight = height/5; i < 5; i++){
                    var scene = new BerserkerGame.Graphics.Scene();
                    var camera = new BerserkerGame.Graphics.Camera([10,10,10]);
                    /*
                     * un viewport es un area de renderizado dentro del canvas.
                     * Se compone de una escena y una camara. En caso que estas
                     * no se pasen como parámetros son creadas una nueva escena
                     * y una nueva cámara de forma automática.
                    */ 
					//console.log("orix="+orix+" oriy="+oriy+" width="+width+" viewportHeight="+viewportHeight);
                    this.viewports.push(ge.createViewport(orix, oriy+i*viewportHeight, width, viewportHeight, scene, camera));
                }
                this.$super(canvas, pkedgui.CanvasElementSlider.SLIDEUPDOWN, 5, 1, 
                        orix, oriy, width, height, 1, models);
                this.editorBoard = editorBoard;
                this.frameToolBar = frameToolBar;

                //TODO pintar las flechas para deslizar modelos.

            },
            paint : function(){
                var ge = this.ge;
                for (var i = 0, nv = this.viewports.length; i < nv && this.presentElement + i < this.elements.length; i++){
                    var viewportid = this.viewports[i];
                    var scene = ge.getViewportScene(viewportid);
                    scene.removeAny(this.elements);
                    scene.compose(this.elements[this.presentElement+i]);
                }
            },
            /**
             * Controlador para el evento de seleccionar un modelo.
             */
            elementSelected : function(modelindex){
                var model = this.elements[modelindex];
                this.editorBoard.setModel(model);
                this.frameToolBar.setModel(model);
            }
        });
})(BerserkerGame.Editor.GUI);
}
}
